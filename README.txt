Neuro Imaging Pipeline implemented on Python 3.7.
These function have a lot on dependencies (Nipype, Deepbrain, Nibabel, Matlab Engine ... )
They also demand the use of Neuro-imaging Softwares as ANTs, FreeSurfer, FSL, Converter3D
A .yml file will be provided with instructions to recreate the python environnement to use these functions.
Matlab functions can be found under sepcode/matsep/ but need to be updated.
