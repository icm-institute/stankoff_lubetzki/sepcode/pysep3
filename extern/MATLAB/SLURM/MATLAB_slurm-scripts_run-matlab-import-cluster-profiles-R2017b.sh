#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=8
#SBATCH --mem=16G
#SBATCH --time=01:00:00
#SBATCH --job-name=import-icm-cluster-profile-R2017b
#SBATCH --chdir=.
#SBATCH --output=/network/lustre/iss01/lubetzki-stankoff/clinic/code/sepcode/pysep3/extern/MATLAB/import-icm-cluster-profile-R2017b_%j.txt
#SBATCH --error=/network/lustre/iss01/lubetzki-stankoff/clinic/code/sepcode/pysep3/extern/MATLAB/import-icm-cluster-profile-R2017b_%j.txt

module load MATLAB/R2017b
matlab -nodesktop -softwareopengl -nosplash -nodisplay < matlab-scripts/import-icm-cluster-profile.m
