#!/bin/bash
#SBATCH --partition=normal
#SBATCH --cpus-per-task=8
#SBATCH --mem=16G
#SBATCH --time=01:00:00
#SBATCH --job-name=test-matlab-parfor-R2017b
#SBATCH --chdir=.
#SBATCH --output=/network/lustre/iss01/lubetzki-stankoff/clinic/code/sepcode/pysep3/extern/MATLAB/import-icm-cluster-parfor-R2017b_%j.txt
#SBATCH --error=/network/lustre/iss01/lubetzki-stankoff/clinic/code/sepcode/pysep3/extern/MATLAB/import-icm-cluster-parfor-R2017b_%j.txt

module load MATLAB/R2017b

echo -e "\e[31m--- Without parfor ---\e[0m"

matlab -nodesktop -noopengl -nodisplay < matlab-scripts/test-for.m

echo -e "\n\e[32m--- With parfor ---\e[0m"

matlab -nodesktop -noopengl -nodisplay < matlab-scripts/test-parfor.m

