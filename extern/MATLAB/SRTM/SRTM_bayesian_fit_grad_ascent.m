function [R1_name, B1_name, Beta_name] = SRTM_bayesian_fit_grad_ascent(PET_path,MASK_path,MAT_path,name,N_clusters,N_last)
if nargin < 4 
    error('The SRTM_bayesian_fit requires [PET_path, MAT_path, MASK_path, name]')
elseif nargin < 5 
    N_clusters = 4;
    N_last = 3;
elseif nargin < 6
    N_last = 3;
end
%% LOADING VARS
% Load MASK
info_mask = niftiinfo(MASK_path);
mask      = logical(niftiread(info_mask));
% Load PET and Infos 
info_PET = niftiinfo(PET_path);
dynPET   = double(niftiread(info_PET));
PET2D    = conv4Dto2D(dynPET,mask);
% Load tacts and times
MAT      = load(MAT_path);
time     = MAT.tacs.time/60;
Cref     = MAT.tacs.tac;
%% Mean last 'N_last' frames
if N_last >1
    PET2D_last_mean = [PET2D(1:end-N_last,:) ; mean(PET2D(end-N_last+1:end,:))];
else
    PET2D_last_mean = PET2D;
end
time_last_mean   = [time(1:end-N_last); mean([time(end-N_last+1),time(end)])];
frames_last_mean = mid2frames(time_last_mean);
%% Weight PET with decay correction factor
half_life = 20.4; % in minutes
lambda = log(2)/half_life;
T1 = frames_last_mean(:,1);  % frames start
T2 = frames_last_mean(:,2);  % frames end
delta = T2 - T1;  % frames duration
dcf = exp(T1*lambda).*(delta)*lambda./(1-exp(-lambda*(delta)));  % decay correction factor
weights_last_mean = sqrt(delta.^2./(nanmean(PET2D_last_mean,2).*dcf));
weights_last_mean(end) = weights_last_mean(1); % made to prevent giving too much weight to the last point
% Weighting PET
weighted_PET = zeros(size(PET2D_last_mean));
for timepoint = 1 : length(PET2D)
    weighted_PET(:,timepoint) = weights_last_mean .* PET2D_last_mean(:,timepoint);
end
%% Clustering on the wheighted PET with last frames averaged
idx = kmeans(weighted_PET',N_clusters,'Display','final','MaxIter', 1000); % 'Replicates', 3,
%% Cleaning some variables
clear weighted_PET weights_last_mean dcf delta T2 T1 frames_last_mean time_last_mean PET2D_last_mean
%% Recalculating decay correction factor for all frames
frames  = mid2frames(time);
T1 = frames(:,1);  % frame start
T2 = frames(:,2);  % frame end
delta = T2 - T1;
dcf = exp(T1*lambda).*(delta)*lambda./(1-exp(-lambda*(delta)));  % decay correction factor
%% Nonlinear Fitting of the model onto each regionnal TAC
dt = 0.05;
tv = (0:dt:frames(end,2))';
Cr_tv = interp1(time,Cref,tv,'linear','extrap');
Cr_tv(isnan(Cr_tv)) = 0;
info.Cref = Cr_tv;
info.tCref = tv;
p0 = [0.1;0.1;0.1];
TACs = zeros(length(time), N_clusters);
R11D = zeros(size(PET2D(1,:)));
B11D = zeros(size(PET2D(1,:)));
beta1D = zeros(size(PET2D(1,:)));
% parpool('cluster-slurm-icm',27);
for cluster = 1:N_clusters
    %% Time Activity Curve (TAC) of the cluster
    TACs(:,cluster) = nanmean(PET2D(:,idx == cluster), 2);
    weights_cluster = sqrt(delta.^2./(TACs(:,cluster).*dcf));
    est.FUN = 'SRTM_nonlin';  % model to invert FUN(par,fixed_par,time)
    est.fixed_par = info;  % fixed parameters
    est.data = TACs(:,cluster);  % data to fit
    est.time = time;  % time of data
    est.weights = weights_cluster;  % weight defined as 1./SD
    est.p0 = p0 ;  % initial values, if a matrix it tests all the columns as initial values and return the one with the lower WRSS
    est.pup = [inf; inf; inf];  % upper bound
    est.pdown = [0; 0; 0];  % Lower bound
    cluster_estimation = SingleSubjectModelFit(est);
    %% RECOVERING THE ESTIMATED PARAMETERS
    wCref = Cref.*weights_cluster;  % weighted Cref
    mu_R1 = cluster_estimation.par(1);
    mu_B1 = cluster_estimation.par(2);
    mu_beta = cluster_estimation.par(3);
    cv_beta = cluster_estimation.cvpar(3);
    sigma_beta = cv_beta*mu_beta/100;
    cov_R1_B1 = cluster_estimation.Cov(1:2,1:2);
    %% CREATING PRIOR
    PriorMdl = bayeslm(2,'ModelType','conjugate',  'Mu',[mu_R1; mu_B1], 'V', cov_R1_B1, 'Intercept', false, 'VarNames', ["R1" "B1"]);%,'A', 50, 'B', 10e6);
    % RECOVERING THE CLUSTER's PET
    y = PET2D(:,idx == cluster);
    % finding best beta to maximise likelihood
    beta_step = mu_beta*sigma_beta/20;
    max_iter = 100;
    % ESTIMATING R1 AND B1 IN CLUSTER
    R1_cluster = zeros(size(y(1,:)));
    B1_cluster = zeros(size(y(1,:)));
    for voxel = 1:length(y)
        beta = mu_beta;
        beta_new = mu_beta;
        iter = 1;
        diff = 1;
        while (iter<=max_iter) && (abs(diff)>1e-5) && (beta_new>1e-3)
            beta = beta_new;
            beta1 = beta+(beta_step/2);
            beta2 = beta-(beta_step/2);
            conv_tv = dt*filter(exp(-beta1.*tv),1,Cr_tv);
            wConvMat = interp1(tv,conv_tv,time).*weights_cluster;
            G = [wCref, wConvMat];
            lkh1 = marginY(PriorMdl,G,y(:,voxel).*weights_cluster);
            conv_tv = dt*filter(exp(-beta2.*tv),1,Cr_tv);
            wConvMat = interp1(tv,conv_tv,time).*weights_cluster;
            G = [wCref, wConvMat];
            lkh2 = marginY(PriorMdl,G,y(:,voxel).*weights_cluster);
            diff = (lkh1-lkh2);
            beta_new = beta + diff; % '+' sign because it is a gradient ascent
            iter = iter +1;
        end
        conv_tv = dt*filter(exp(-beta.*tv),1,Cr_tv);
        wConvMat = interp1(tv,conv_tv,time).*weights_cluster;
        G = [wCref, wConvMat];
        PosteriorMdl = estimate(PriorMdl,G,y(:,voxel).*weights_cluster, 'Display', false);
        R1_cluster(voxel) = PosteriorMdl.Mu(1);
        B1_cluster(voxel) = PosteriorMdl.Mu(2);
    end
    R11D(idx==cluster) = R1_cluster;
    B11D(idx==cluster) = B1_cluster;
    beta1D(idx==cluster) = beta_cluster;
end
%% Rearanging images in 3D
R1 = zeros(size(mask));
B1 = zeros(size(mask));
beta_img = zeros(size(mask));
R1(mask) = R11D;
B1(mask) = B11D;
beta_img(mask) = beta1D;
%% Saving Image 
R1_name = strcat(name, '_R1');
B1_name = strcat(name, '_B1');
Beta_name = strcat(name, '_beta');
niftiwrite(single(R1),R1_name,info_mask,'Compressed',true)
niftiwrite(single(B1),B1_name,info_mask,'Compressed',true)
niftiwrite(single(beta_img),Beta_name,info_mask,'Compressed',true)
end