function [tacs_file, estimation_file, cluster_file] = SRTM_estimate_clusters(PET_path,MASK_path,MAT_path,name,N_clusters)
if nargin < 4 
    error('The SRTM_bayesian_fit requires [PET_path, MAT_path, MASK_path, name]')
elseif nargin < 5 
    N_clusters = 4;
end
N_last=3;
%% LOADING VARS
% Load MASK
info_mask = niftiinfo(MASK_path);
mask      = logical(niftiread(info_mask));
% Load PET and Infos 
info_PET = niftiinfo(PET_path);
dynPET   = double(niftiread(info_PET));
PET2D    = conv4Dto2D(dynPET,mask);
% Load tacts and times
MAT      = load(MAT_path);
time     = MAT.tacs.time/60;
Cref     = MAT.tacs.tac;
%% Mean last 'N_last' frames
if N_last >1
    PET2D_last_mean = [PET2D(1:end-N_last,:) ; mean(PET2D(end-N_last+1:end,:))];
else
    PET2D_last_mean = PET2D;
end
time_last_mean   = [time(1:end-N_last); mean([time(end-N_last+1),time(end)])];
frames_last_mean = mid2frames(time_last_mean);
%% Weight PET with decay correction factor
half_life = 20.4; % in minutes
lambda = log(2)/half_life;
T1 = frames_last_mean(:,1);  % frames start
T2 = frames_last_mean(:,2);  % frames end
delta = T2 - T1;  % frames duration
dcf = exp(T1*lambda).*(delta)*lambda./(1-exp(-lambda*(delta)));  % decay correction factor
weights_last_mean = sqrt(delta.^2./(nanmean(PET2D_last_mean,2).*dcf));
weights_last_mean(end) = weights_last_mean(1); % made to prevent giving too much weight to the last point
% Weighting PET
weighted_PET = zeros(size(PET2D_last_mean));
for timepoint = 1 : length(PET2D)
    weighted_PET(:,timepoint) = weights_last_mean .* PET2D_last_mean(:,timepoint);
end
%% Clustering on the wheighted PET with last frames averaged
idx = kmeans(weighted_PET',N_clusters,'Display','final','MaxIter', 1000); % 'Replicates', 3,
%% saving Image
clusters = zeros(size(mask));
clusters(mask) = idx;
cluster_file = strcat(name,'_kmeans');
niftiwrite(single(clusters),cluster_file,info_mask,'Compressed',true)
cluster_file = strcat(name,'_kmeans.mat');
save(cluster_file,'idx')
%% Cleaning some variables
clear weighted_PET weights_last_mean dcf delta T2 T1 frames_last_mean time_last_mean PET2D_last_mean dynPET
%% Recalculating decay correction factor for all frames
frames  = mid2frames(time);
T1 = frames(:,1);  % frame start
T2 = frames(:,2);  % frame end
delta = T2 - T1;
dcf = exp(T1*lambda).*(delta)*lambda./(1-exp(-lambda*(delta)));  % decay correction factor
%% Nonlinear Fitting of the model onto each regionnal TAC
dt = 0.05;
tv = (0:dt:frames(end,2))';
Cr_tv = interp1(time,Cref,tv,'linear','extrap');
Cr_tv(isnan(Cr_tv)) = 0;
info.Cref = Cr_tv;
info.tCref = tv;
p0 = [0.1;0.1;0.1];
TACs = zeros(length(time), N_clusters);
for cluster = 1:N_clusters
    %% Time Activity Curve (TAC) of the cluster
    TACs(:,cluster) = nanmean(PET2D(:,idx == cluster), 2);
    weights_cluster = sqrt(delta.^2./(TACs(:,cluster).*dcf));
    est.FUN = 'SRTM_nonlin';  % model to invert FUN(par,fixed_par,time)
    est.fixed_par = info;  % fixed parameters
    est.data = TACs(:,cluster);  % data to fit
    est.time = time;  % time of data
    est.weights = weights_cluster;  % weight defined as 1./SD
    est.p0 = p0 ;  % initial values, if a matrix it tests all the columns as initial values and return the one with the lower WRSS
    est.pup = [inf; inf; inf];  % upper bound
    est.pdown = [0; 0; 0];  % Lower bound
    cluster_estimation(cluster) = SingleSubjectModelFit(est);
end
tacs_file = strcat(name,'_SRTM_cluster_tacs.mat');
save(tacs_file, 'TACs')
estimation_file = strcat(name,'_SRTM_cluster_estimation.mat');
save(estimation_file, 'cluster_estimation')
end