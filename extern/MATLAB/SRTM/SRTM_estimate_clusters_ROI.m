function estimation_file = SRTM_estimate_clusters_ROI(PET_path,MASK_path,MAT_path,name,WM,GM,DG,CB,CG,CW)
%% LOADING VARS
% Load MASK
info_mask = niftiinfo(MASK_path);
mask      = logical(niftiread(info_mask));
sizem = size(mask);
% Load PET and Infos 
info_PET = niftiinfo(PET_path);
dynPET   = double(niftiread(info_PET));
% Load tacts and times
MAT      = load(MAT_path);
time     = MAT.tacs.time/60;
Cref     = MAT.tacs.tac;
%% LOADING ROIS
masks = zeros(sizem(1),sizem(2),sizem(3),6);
N_voxs = zeros(6,1);
info = niftiinfo(WM);
WM   = logical(niftiread(info));
masks(:,:,:,1) = WM;
N_voxs(1) = sum(WM,'all');
info = niftiinfo(GM);
GM   = logical(niftiread(info));
masks(:,:,:,2) = GM;
N_voxs(2) = sum(GM,'all');
info = niftiinfo(DG);
DG   = logical(niftiread(info));
masks(:,:,:,3) = DG;
N_voxs(3) = sum(DG,'all');
info = niftiinfo(CB);
CB   = logical(niftiread(info));
masks(:,:,:,4) = CB;
N_voxs(4) = sum(CB,'all');
info = niftiinfo(CG);
CG   = logical(niftiread(info));
masks(:,:,:,5) = CG;
N_voxs(5) = sum(CG,'all');
info = niftiinfo(CW);
CW   = logical(niftiread(info));
masks(:,:,:,6) = CW;
N_voxs(6) = sum(CW,'all');
%% Weight PET with decay correction factor
half_life = 20.4; % in minutes
lambda = log(2)/half_life;
%% Recalculating decay correction factor for all frames
frames  = mid2frames(time);
T1 = frames(:,1);  % frame start
T2 = frames(:,2);  % frame end
delta = T2 - T1;
dcf = exp(T1*lambda).*(delta)*lambda./(1-exp(-lambda*(delta)));  % decay correction factor
%% Nonlinear Fitting of the model onto each regionnal TAC
dt = 0.05;
tv = (0:dt:frames(end,2))';
Cr_tv = interp1(time,Cref,tv,'linear','extrap');
Cr_tv(isnan(Cr_tv)) = 0;
info.Cref = Cr_tv;
info.tCref = tv;
p0 = [0.1;0.1;0.1];
TACs = zeros(length(time), 6);
labels = ["WM", "GM", "DG", "CB", "CG", "CW"];
for cluster = 1:6
    %% Time Activity Curve (TAC) of the cluster
    TACs(:,cluster) = nanmean(conv4Dto2D(dynPET,masks(:,:,:,cluster)), 2);
    weights_cluster = sqrt(delta.^2./(TACs(:,cluster).*dcf));
    est.FUN = 'SRTM_nonlin';  % model to invert FUN(par,fixed_par,time)
    est.fixed_par = info;  % fixed parameters
    est.data = TACs(:,cluster);  % data to fit
    est.time = time;  % time of data
    est.weights = weights_cluster;  % weight defined as 1./SD
    est.p0 = p0 ;  % initial values, if a matrix it tests all the columns as initial values and return the one with the lower WRSS
    est.pup = [inf; inf; inf];  % upper bound
    est.pdown = [0; 0; 0];  % Lower bound
    cluster_estimation(cluster) = SingleSubjectModelFit(est);
%     cluster_estimation(cluster).nb_vox_ROI = N_voxs(cluster);
%     cluster_estimation(cluster).ROI_name = labels(cluster);
end
[cluster_estimation(:).nb_vox_ROI] = deal(N_voxs(:));
[cluster_estimation(:).ROI_name] = deal(labels(:));
estimation_file = strcat(name,'_SRTM_cluster_estimation.mat');
save(estimation_file, 'cluster_estimation')
end