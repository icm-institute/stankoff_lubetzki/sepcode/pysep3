function [R1_name, B1_name, Beta_name] = SRTM_generate_image(PET_path,MASK_path,MAT_path,name,estimation_file,idx_file,TACs_file)
%% LOADING VARS
% Load MASK
info_mask = niftiinfo(MASK_path);
mask      = logical(niftiread(info_mask));
% Load PET and Infos 
info_PET = niftiinfo(PET_path);
dynPET   = double(niftiread(info_PET));
PET2D    = conv4Dto2D(dynPET,mask);
% Load tacts and times
MAT      = load(MAT_path);
time     = MAT.tacs.time/60;
Cref     = MAT.tacs.tac;
% Cluster estimation
EST = load(estimation_file);
estimation = EST.cluster_estimation;
% idxs of kmeans
IDX = load(idx_file);
idx = IDX.idx;
% TACs of kmeans
TACs = load(TACs_file);
TACs = TACs.TACs;
%% Cleaning some variables
clear dynPET
%% Recalculating decay correction factor for all frames
half_life = 20.4; % in minutes
lambda = log(2)/half_life;
frames  = mid2frames(time);
T1 = frames(:,1);  % frame start
T2 = frames(:,2);  % frame end
delta = T2 - T1;
dcf = exp(T1*lambda).*(delta)*lambda./(1-exp(-lambda*(delta)));  % decay correction factor
%% Nonlinear Fitting of the model onto each regionnal TAC
dt = 0.05;
tv = (0:dt:frames(end,2))';
Cr_tv = interp1(time,Cref,tv,'linear','extrap');
Cr_tv(isnan(Cr_tv)) = 0;
R11D = zeros(size(PET2D(1,:)));
B11D = zeros(size(PET2D(1,:)));
beta1D = zeros(size(PET2D(1,:)));
for cluster = 1:length(estimation)
    cluster_estimation = estimation(cluster);
    weights_cluster = sqrt(delta.^2./(TACs(:,cluster).*dcf));
    %% RECOVERING THE ESTIMATED PARAMETERS
    wCref = Cref.*weights_cluster;
    mu_R1 = cluster_estimation.par(1);
    mu_B1 = cluster_estimation.par(2);
    mu_beta = cluster_estimation.par(3);
    cv_beta = cluster_estimation.cvpar(3);
    %sigma_beta = cv_beta*mu_beta/100;
    sigma_beta = 0.5*mu_beta*100;
    %cov_R1_B1 = cluster_estimation.Cov(1:2,1:2);
    cov_R1_B1 = diag((0.5*[mu_R1; mu_B1]).^2);
    %% CREATING PRIOR
    PriorMdl = bayeslm(2,'ModelType','conjugate',  'Mu',[mu_R1; mu_B1], 'V', cov_R1_B1, 'Intercept', false, 'VarNames', ["R1" "B1"]);%,'A', 50, 'B', 10e6);
    %% RECOVERING THE CLUSTER's PET
    y = PET2D(:,idx == cluster);
    %% BETA GRID
    N_beta_grid = 1000;
    x = linspace(1/N_beta_grid,1-1/N_beta_grid,N_beta_grid);
    mu_beta_lognorm = log(mu_beta)-(1/2*log((1+(sigma_beta/mu_beta)^2)));
    sigma_beta_lognorm = sqrt(log((1+(sigma_beta/mu_beta)^2)));
    beta_grid = logninv(x,mu_beta_lognorm,sigma_beta_lognorm);
    wConvMat  = zeros(length(time),N_beta_grid);
    for beta_idx = 1:N_beta_grid
        conv_tv = dt*filter(exp(-beta_grid(beta_idx).*tv),1,Cr_tv);
        wConvMat(:,beta_idx) = interp1(tv,conv_tv,time).*weights_cluster;
    end
    R1_cluster = zeros(size(y(1,:)));
    B1_cluster = zeros(size(y(1,:)));
    beta_cluster = zeros(size(y(1,:)));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    logs_fast = zeros(length(y),length(beta_grid));
    y_weighted = y.*repmat(weights_cluster,1,length(y)); 
    parfor beta_idx = 1 : N_beta_grid
        X_weighted = [wCref, wConvMat(:,beta_idx)];        
        logs_fast(:,beta_idx) = marginY_fast(PriorMdl,X_weighted,y_weighted);
    end
    
    parfor voxel = 1 : length(y)
        [~, beta_idx] = max(logs_fast(voxel,:));
        beta = beta_grid(beta_idx);
        G = [wCref, wConvMat(:,beta_idx)];
        PosteriorMdl        = estimate(PriorMdl,G,y_weighted(:,voxel), 'Display', false);
        R1_cluster(voxel)   = PosteriorMdl.Mu(1);
        B1_cluster(voxel)   = PosteriorMdl.Mu(2);
        beta_cluster(voxel) = beta;
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    R11D(idx==cluster) = R1_cluster;
    B11D(idx==cluster) = B1_cluster;
    beta1D(idx==cluster) = beta_cluster;
end
%% Rearanging images in 3D
R1 = zeros(size(mask));
B1 = zeros(size(mask));
beta_img = zeros(size(mask));
R1(mask) = R11D;
B1(mask) = B11D;
beta_img(mask) = beta1D;
%% Saving Image 
R1_name = strcat(name, '_R1');
B1_name = strcat(name, '_B1');
Beta_name = strcat(name, '_beta');
niftiwrite(single(R1),R1_name,info_mask,'Compressed',true)
niftiwrite(single(B1),B1_name,info_mask,'Compressed',true)
niftiwrite(single(beta_img),Beta_name,info_mask,'Compressed',true)
end