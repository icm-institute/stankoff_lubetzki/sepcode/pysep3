function y_final = SRTM_nonlin(par,info,time)

R1 = par(1);
B1 = par(2);
b  = par(3);
tCref = info.tCref;
Cref  = info.Cref;
dt = tCref(2) - tCref(1); 
conv_tv = dt*filter(exp(-b.*tCref),1,Cref);
y_tv = R1 * Cref + B1 * conv_tv;
% y = interp1(tCref,int_y_tv,time);

time_series_y_tv = timeseries(y_tv, tCref);
frames = mid2frames(time);
y_final = zeros(size(time));
for idx = 1:length(time)
    y_final(idx) = trap(time_series_y_tv, frames(idx,1), frames(idx,2)) / (frames(idx,2)-frames(idx,1));
end
end