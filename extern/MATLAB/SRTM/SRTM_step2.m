function SRTM_step2(dynPET,mask,mat, k2prime,name)
%% Load Images and Infos 
info_PET = niftiinfo(dynPET);
dynPET = double(niftiread(info_PET));
info_mask  = niftiinfo(mask);
mask = logical(niftiread(info_mask));
PET2D = conv4Dto2D(dynPET,mask);
MAT = load(mat);
time = MAT.tacs.time/60;
Cref = MAT.tacs.tac;

% ref region interpolation
dt = 0.05;
tv = (0:dt:time(end)+dt)';
Cr_tv = interp1(time,Cref,tv);
Cr_tv(isnan(Cr_tv)) = 0;

% K2 prime calculation
k2prime_mat = load(k2prime);
k2prime = k2prime_mat.k2prime;

% BF conv matrix
n = 500;
k2grid = linspace(0.01,0.1,n);  % same as betagrid in fact
convMat  = zeros(length(time),n);
for i = 1 : n
    conv_tv      = dt*filter(exp(-k2grid(i).*tv),1,Cr_tv);
    conv_tv      = Cr_tv + ((k2prime-k2grid(i)).*conv_tv);
    convMat(:,i) = interp1(tv,conv_tv,time);
end

% Fit
WRSS   = zeros(n,size(PET2D,2));
R1_all = zeros(n,size(PET2D,2));
STDXR1 = zeros(n,size(PET2D,2));
for i = 1 : n
    G = convMat(:,i);
    [par,STDX,mse] = lscov(G,PET2D,weights);
    WRSS(i,:) = mse;
    R1_all(i,:) = par(1,:);
    STDXR1(i,:) = STDX(1,:);
end

% MAPS 2D -> 3D
[~,idx] = min(WRSS);
idx_lin = sub2ind(size(WRSS), idx, 1:length(idx));
R1 = zeros(size(mask));
k2 = zeros(size(mask));
R1(mask) = R1_all(idx_lin);
k2(mask) = k2grid(idx);
BP = R1*k2prime./beta -1;
DVR = R1*k2prime./beta;

% Saving Images
R13D_name = strcat(name, '_', 'R1');
BP3D_name = strcat(name, '_', 'BP');
DVR_name = strcat(name, '_', 'DVR');
k2_name = strcat(name, '_', 'k2');
niftiwrite(single(R1), R13D_name, info_mask,'Compressed',true)
niftiwrite(single(BP), BP3D_name, info_mask,'Compressed',true)
niftiwrite(single(DVR), DVR_name, info_mask,'Compressed',true)
niftiwrite(single(k2), k2_name, info_mask,'Compressed',true)