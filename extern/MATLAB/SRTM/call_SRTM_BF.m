function [R13D_name, B13D_name, beta3D_name] = call_SRTM_BF(PET, mask, mat, name)
% Load Images and Infos 
info_PET = niftiinfo(PET);
dynPET = double(niftiread(info_PET));

info_mask  = niftiinfo(mask);
mask = logical(niftiread(info_mask));

MAT = load(mat);
time = MAT.tacs.time/60;
Cref = MAT.tacs.tac;

% Processing
[R13D, B13D, beta3D] = fit_SRTM_BF(time, Cref, dynPET, mask);
R13D_name = strcat(name, '_', 'R1.nii');
B13D_name = strcat(name, '_', 'B1.nii');
beta3D_name = strcat(name, '_', 'beta.nii');

% Saving Niftis
niftiwrite(single(R13D), R13D_name, info_mask,'Compressed',true)
niftiwrite(single(B13D), B13D_name, info_mask,'Compressed',true)
niftiwrite(single(beta3D), beta3D_name, info_mask,'Compressed',true)
end
