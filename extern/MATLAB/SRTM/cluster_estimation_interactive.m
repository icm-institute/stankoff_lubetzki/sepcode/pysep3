
figure('color','white'); hold on;
%% SRTM Bayes IMPROVEMENT
clear all
clf; hold on;
sujs = {'C_ARIIS', 'C_ARIIS', 'C_BENFL', 'C_BENFL', 'C_BENMA', 'C_BENMA', 'C_CHRKE', 'C_CHRKE', 'C_DUBAN', 'C_DUBAN', 'C_GUECO', 'C_GUECO', 'C_LEBJO', 'C_LEBJO', 'C_LEVVA', 'C_LEVVA', 'C_REVMA', 'C_REVMA', 'C_CARAU', 'C_CARAU'};
viss = {'V01', 'V02', 'V01', 'V02', 'V01', 'V02', 'V01', 'V02', 'V01', 'V02', 'V01', 'V02', 'V01', 'V02', 'V01', 'V02', 'V01', 'V02', 'V01', 'V02', 'V01', 'V02'};
% Choosing a subject
s = 3;
% LOADING VARS
suj = char(sujs(s));
vis = char(viss(s));
disp(strcat(suj,'_',vis))
name=strcat('/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/SRTM_improvement/out/full_map_bayes_greedy/',suj,'_',vis);
% Load MASK
MASK_path = strcat('/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/', suj,'/', vis, '/pibROI/', suj,'_V01_brainmask_p0.90_2pib.nii.gz');
info_mask = niftiinfo(MASK_path);
mask      = logical(niftiread(info_mask));
% Load PET and Infos 
PET_path = strcat('/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/', suj,'/', vis, '/pibpreproc/', suj,'_', vis, '_pib_movcorr.nii.gz');
info_PET = niftiinfo(PET_path);
dynPET   = double(niftiread(info_PET));
PET2D    = conv4Dto2D(dynPET,mask);
% Load tacts and times
MAT_path = strcat('/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/', suj,'/', vis, '/pibsuper/', suj,'_', vis, '_pib_refregion_tac.mat');
MAT      = load(MAT_path);
time     = MAT.tacs.time/60;
Cref     = MAT.tacs.tac;
% output Name
% Mean last 'N' frames
N = 3;  % frames to mean at the end
if N >1
    PET2D_last_mean = [PET2D(1:end-N,:) ; mean(PET2D(end-N+1:end,:))];
else
    PET2D_last_mean = PET2D;
end
time_last_mean   = [time(1:end-N); mean([time(end-N+1),time(end)])];
frames_last_mean = mid2frames(time_last_mean);
%%
workspace_file = strcat(name,'_SRTM_worspace.mat');
save(workspace_file)
%%
clear all
close all
%%
load('/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/SRTM_improvement/out/full_map_bayes_greedy/C_BENFL_V01_SRTM_worspace.mat')
%% Decay correction Factor calculation to weight PET
half_life              = 20.4; % in minutes
lambda                 = log(2)/half_life;
T1                     = frames_last_mean(:,1);  % frames start
T2                     = frames_last_mean(:,2);  % frames end
delta                  = T2 - T1;
dcf                    = exp(T1*lambda).*(delta)*lambda./(1-exp(-lambda*(delta)));  % decay correction factor
weights_last_mean      = sqrt(delta.^2./(nanmean(PET2D_last_mean,2).*dcf));
weights_last_mean(end) = weights_last_mean(1); % made to preventgiving too much weight to the last point
% Weighting PET
weighted_PET = zeros(size(PET2D_last_mean));
for timepoint = 1 : length(PET2D)
    weighted_PET(:,timepoint) = weights_last_mean .* PET2D_last_mean(:,timepoint);
end
%% MANUAL CORRECTION WEIGHT
weighted_PET(:,timepoint) = weighted_PET(:,timepoint)*0.2;
%%
clf; hold on
%% clustering on the 'N_begin' first frames beginning
n_k = 3;
[idx, C] = kmeans(weighted_PET',n_k,'Display','final','MaxIter', 1000,'Replicates', 3);

% Recalculating weights for all frames
frames  = mid2frames(time);
lambda = log(2)/half_life;
T1 = frames(:,1);  % frame start
T2 = frames(:,2);  % frame end
delta = T2 - T1;
dcf = exp(T1*lambda).*(delta)*lambda./(1-exp(-lambda*(delta)));  % decay correction factor
% Nonlinear Fitting of the model onto each regionnal TAC
dt = 0.05;
tv = (0:dt:frames(end,2))';
Cr_tv = interp1(time,Cref,tv,'linear','extrap');
Cr_tv(isnan(Cr_tv)) = 0;
info.Cref = Cr_tv;
info.tCref = tv;
p0 = [0.1;0.1;0.1];
TACs = zeros(length(time), n_k);
%
for cluster = 1:n_k
    TACs(:,cluster) = nanmean(PET2D(:,idx == cluster), 2);
    plot(time, TACs(:,cluster))
end
%%
for cluster = 1:n_k
    weights_cluster = sqrt(delta.^2./(TACs(:,cluster).*dcf));
    est.FUN = 'SRTM_nonlin';        % model to invert FUN(par,fixed_par,time)
    est.fixed_par = info;           % fixed parameters
    est.data = TACs(:,cluster);           % data to fit
    est.time = time;                % time of data
    est.weights = weights_cluster;        % weight defined as 1./SD
    est.p0 = p0 ;                   % initial values, if a matrix it tests all the columns as initial values and return the one with the lower WRSS
    est.pup = [inf; inf; inf];      % upper bound
    est.pdown = [0; 0; 0];          % Lower bound
    cluster_estimation(cluster) = SingleSubjectModelFit(est);
end