function fit_SRTM2_BF(dynPET,mask,mat,name)
%% Load Images and Infos 
info_PET = niftiinfo(dynPET);
dynPET = double(niftiread(info_PET));
info_mask  = niftiinfo(mask);
mask = logical(niftiread(info_mask));
PET2D = conv4Dto2D(dynPET,mask);
MAT = load(mat);
time = MAT.tacs.time/60;
Cref = MAT.tacs.tac;

% ref region interpolation
dt = 0.05;
tv = (0:dt:time(end)+dt)';
Cr_tv = interp1(time,Cref,tv);
Cr_tv(isnan(Cr_tv)) = 0;

% Grid generation
n = 500;
betagrid = linspace(0.01,0.1,n);
convMat  = zeros(length(time),n);
for i = 1 : n
    conv_tv      = dt*filter(exp(-betagrid(i).*tv),1,Cr_tv);
    convMat(:,i) = interp1(tv,conv_tv,time);
end

% Weights
half_life = 20.4; % in minutes
lambda = log(2)/half_life;
frames  = mid2frames(time);
T1 = frames(:,1);  % frame start
T2 = frames(:,2);  % frame end
delta = T2 - T1;   % time between two frames
dcf = exp(T1*lambda).*(delta)*lambda./(1-exp(-lambda*(delta)));  % decay correction factor
weights = sqrt(delta.^2./(nanmean(PET2D,2).*dcf));
weights(isinf(weights)) = 0;
% Fit
WRSS   = zeros(n,size(PET2D,2));
R1_all = zeros(n,size(PET2D,2));
B1_all = zeros(n,size(PET2D,2));
STDXR1 = zeros(n,size(PET2D,2));
STDXB1 = zeros(n,size(PET2D,2));
for i = 1 : n
    G = [Cref, convMat(:,i)];
    [par,STDX,mse] = lscov(G,PET2D,weights);
    WRSS(i,:) = mse;
    R1_all(i,:) = par(1,:);
    B1_all(i,:) = par(2,:);
    STDXR1(i,:) = STDX(1,:);
    STDXB1(i,:) = STDX(2,:);
end

% MAPS 2D -> 3D
[~,idx] = min(WRSS);
idx_lin = sub2ind(size(WRSS), idx, 1:length(idx));
beta = zeros(size(mask));
R1 = zeros(size(mask));
B1 = zeros(size(mask));
beta(mask) = betagrid(idx);
R1(mask) = R1_all(idx_lin);
B1(mask) = B1_all(idx_lin);

% K2 prime calculation
K2primemap = B1 ./ R1 + beta;
BPmap = B1 ./ beta + R1 - 1;
k2prime = median(K2primemap(BPmap>0.1));

% Saving Images
[filepath,name_subj,~] = fileparts(name);
out_SRTM1 = fullfile(filepath, 'SRTM1');
mkdir(out_SRTM1)
name2 = fullfile(out_SRTM1, name_subj);
R13D_name = strcat(name2, '_', 'R1_srtm1');
B13D_name = strcat(name2, '_', 'B1_srtm1');
beta3D_name = strcat(name2, '_', 'beta_srtm1');
k2primemap_name = strcat(name, '_', 'k2prime');
niftiwrite(single(R1), R13D_name, info_mask,'Compressed',true)
niftiwrite(single(B1), B13D_name, info_mask,'Compressed',true)
niftiwrite(single(beta), beta3D_name, info_mask,'Compressed',true)
niftiwrite(single(K2primemap), k2primemap_name, info_mask,'Compressed',true)
save(strcat(k2primemap_name, '.mat'), 'k2prime');

%% %%%%%%%%% SRTM2 PART %%%%%%%%%
% BF conv matrix
n = 500;
k2grid = linspace(0.01,0.1,n);  % same as betagrid in fact
convMat  = zeros(length(time),n);
for i = 1 : n
    conv_tv      = dt*filter(exp(-k2grid(i).*tv),1,Cr_tv);
    conv_tv      = Cr_tv + ((k2prime-k2grid(i)).*conv_tv);
    convMat(:,i) = interp1(tv,conv_tv,time);
end

% Fit
WRSS   = zeros(n,size(PET2D,2));
R1_all = zeros(n,size(PET2D,2));
STDXR1 = zeros(n,size(PET2D,2));
for i = 1 : n
    G = convMat(:,i);
    [par,STDX,mse] = lscov(G,PET2D,weights);
    WRSS(i,:) = mse;
    R1_all(i,:) = par(1,:);
    STDXR1(i,:) = STDX(1,:);
end

% MAPS 2D -> 3D
[~,idx] = min(WRSS);
idx_lin = sub2ind(size(WRSS), idx, 1:length(idx));
R1 = zeros(size(mask));
k2 = zeros(size(mask));
R1(mask) = R1_all(idx_lin);
k2(mask) = k2grid(idx);
BP = R1*k2prime./k2 -1;
DVR = R1*k2prime./k2;

% Saving Images
R13D_name = strcat(name, '_', 'R1');
BP3D_name = strcat(name, '_', 'BP');
DVR_name = strcat(name, '_', 'DVR');
k2_name = strcat(name, '_', 'k2');
niftiwrite(single(R1), R13D_name, info_mask,'Compressed',true)
niftiwrite(single(BP), BP3D_name, info_mask,'Compressed',true)
niftiwrite(single(DVR), DVR_name, info_mask,'Compressed',true)
niftiwrite(single(k2), k2_name, info_mask,'Compressed',true)
end