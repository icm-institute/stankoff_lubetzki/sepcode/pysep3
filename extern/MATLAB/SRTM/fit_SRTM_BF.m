function [R13D,B13D,beta3D] = fit_SRTM_BF(time,Cref,dynPET,mask)
%% ref region interpolation
dt = 0.05;
tv = (0:dt:time(end)+dt)';
Cr_tv = interp1(time,Cref,tv);
Cr_tv(isnan(Cr_tv)) = 0;
%% Grid generation
n = 500;
mu = -3;
sigma = 1.5;
x = icdf('Normal', ((1:(n+1))/(n+1))', mu, sigma);
betagrid = exp(x(1:end-1));
convMat  = zeros(length(time),n);
for i = 1 : n
    conv_tv      = dt*filter(exp(-betagrid(i).*tv),1,Cr_tv);
    convMat(:,i) = interp1(tv,conv_tv,time);
end
%% Fit
PET2D = conv4Dto2D(dynPET,mask);
frames  = mid2frames(time);
delta = frames(:,2) - frames(:,1);
weights = sqrt(delta./nanmean(PET2D,2));
WRSS   = zeros(n,size(PET2D,2));
R1_all = zeros(n,size(PET2D,2));
B1_all = zeros(n,size(PET2D,2));

for i = 1 : n
    G = [Cref, convMat(:,i)];
    [par,~,mse] = lscov(G,PET2D,weights);
    WRSS(i,:) = mse;
    R1_all(i,:) = par(1,:);
    B1_all(i,:) = par(2,:);
end

[~,idx] = min(WRSS);
idx_lin = sub2ind(size(WRSS), idx, 1:length(idx));
beta3D = zeros(size(mask));
R13D   = zeros(size(mask));
B13D   = zeros(size(mask));
beta3D(mask) =  (idx);
R13D(mask) = R1_all(idx_lin);
B13D(mask) = B1_all(idx_lin);
end