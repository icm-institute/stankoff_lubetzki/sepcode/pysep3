function [R13D_name,B13D_name,beta3D_name,percent_outlying_vox,CV_name] = fit_SRTM_BF2(dynPET,mask,mat,name)
% Load Images and Infos 
info_PET = niftiinfo(dynPET);
dynPET = double(niftiread(info_PET));

info_mask  = niftiinfo(mask);
mask = logical(niftiread(info_mask));

MAT = load(mat);
time = MAT.tacs.time/60;
Cref = MAT.tacs.tac;


%% ref region interpolation
dt = 0.05;
tv = (0:dt:time(end)+dt)';
Cr_tv = interp1(time,Cref,tv);
Cr_tv(isnan(Cr_tv)) = 0;
%% Grid generation
n = 500;
betagrid = linspace(0.05,0.6,n);
convMat  = zeros(length(time),n);
for i = 1 : n
    conv_tv      = dt*filter(exp(-betagrid(i).*tv),1,Cr_tv);
    convMat(:,i) = interp1(tv,conv_tv,time);
end
%% Fit
PET2D = conv4Dto2D(dynPET,mask);
frames  = mid2frames(time);
delta = frames(:,2) - frames(:,1);
weights = sqrt(delta./nanmean(PET2D,2));
WRSS   = zeros(n,size(PET2D,2));
R1_all = zeros(n,size(PET2D,2));
B1_all = zeros(n,size(PET2D,2));
STDXR1 = zeros(n,size(PET2D,2));
STDXB1 = zeros(n,size(PET2D,2));
for i = 1 : n
    G = [Cref, convMat(:,i)];
    [par,STDX,mse] = lscov(G,PET2D,weights);
    WRSS(i,:) = mse;
    R1_all(i,:) = par(1,:);
    STDXR1(i,:) = STDX(1,:);
    B1_all(i,:) = par(2,:);
    STDXB1(i,:) = STDX(2,:);
end
[~,idx] = min(WRSS);
idx_lin = sub2ind(size(WRSS), idx, 1:length(idx));
beta3D = zeros(size(mask));
R13D   = zeros(size(mask));
B13D   = zeros(size(mask));
Error  = zeros(size(mask));
STDXr1 = zeros(size(mask));
beta3D(mask) = betagrid(idx);
Error(mask)  = WRSS(idx);
R13D(mask)   = R1_all(idx_lin);
% R13D(R13D<=0)= NaN;
B13D(mask)   = B1_all(idx_lin);
STDXr1(mask)   = STDXR1(idx_lin);
CV = 100*STDXr1./R13D;
CV50=CV;
CV100=CV;
CV50(CV50>50) = NaN;
CV100(CV100>100) = NaN;

%%
test = R13D(mask);
tot = length(test);
Nb=sum(isnan(test(:)));
percent_outlying_vox = Nb/tot*100;

%%
R13D_name = strcat(name, '_', 'R1');
B13D_name = strcat(name, '_', 'B1');
beta3D_name = strcat(name, '_', 'beta');
CV_name = strcat(name, '_', 'CV_R1');
CV_name50 = strcat(name, '_', 'CV50_R1');
CV_name100 = strcat(name, '_', 'CV100_R1');
% Saving Niftis
niftiwrite(single(R13D), R13D_name, info_mask,'Compressed',true)
niftiwrite(single(B13D), B13D_name, info_mask,'Compressed',true)
niftiwrite(single(beta3D), beta3D_name, info_mask,'Compressed',true)
niftiwrite(single(CV), CV_name, info_mask,'Compressed',true)
niftiwrite(single(CV50), CV_name50, info_mask,'Compressed',true)
niftiwrite(single(CV100), CV_name100, info_mask,'Compressed',true)
R13D_name = strcat(R13D_name, '.nii.gz');
B13D_name = strcat(B13D_name, '.nii.gz');
beta3D_name = strcat(beta3D_name, '.nii.gz');

end