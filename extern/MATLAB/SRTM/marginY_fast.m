function logPY = marginY_fast(Mdl,X,y)
% Same as marginY but can fit many TAC at the same time. Each column of y
% is a different TAC.

mu = Mdl.PrivateMu;
V = Mdl.PrivateV;
a = Mdl.PrivateA;
b = Mdl.PrivateB;
Precision = Mdl.NormalPrecision;

[nobs, nvox] = size(y);


% Approximate the deterministic term
Precision(isinf(Precision)) = 1e12;

% The constant term
constant = gammaln(0.5*nobs+a) - gammaln(a) - 0.5*nobs*log(2*a*pi);

% The determinant term
Dinv = X'*X + Precision;
Pchol = chol(Dinv,'lower');
detTerm = 0.5*nobs*log(a*b) - sum(log(diag(Pchol))) - 0.5*log(det(V)) ;


% The quadratic power term
yMean   = repmat(X * mu, 1, nvox); 
yDemean = y - yMean;

logPY = zeros(nvox,1);

for i = 1 : nvox
    yTran     = Pchol \ (X'*yDemean(:,i));
    residSq   = yDemean(:,i)'*yDemean(:,i) - yTran' * yTran;
    powerTerm = -(0.5*nobs+a) * log(1+0.5*b*residSq);
    % Log marginal likelihood
    logPY(i) = constant + detTerm + powerTerm;    
end


