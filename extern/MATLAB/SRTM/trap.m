function [F] = trap(ts, time1, time2)
    newTS = getsampleusingtime(ts, time1, time2);
    F = trapz(newTS.time, newTS.data);
end