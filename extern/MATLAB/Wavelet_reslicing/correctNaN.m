function [vol_c] = correctNaN(vol_uc, value)

vol_c = vol_uc;

mask_NaN = (isnan(vol_uc));
N_NaN = length(find(mask_NaN));
vol_c(mask_NaN) = value*ones(N_NaN, 1);

end