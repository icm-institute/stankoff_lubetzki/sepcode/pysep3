% +-------------------------------------------------------------------------+
% |                           initclickfit.m                                |
% |                         Set up some dummies.                            |
% |    Vin Cunningham, Rainer Hinz                   03/10/16               |
% +-------------------------------------------------------------------------+


function [sif, idwc, inpfn, rreg, p, y, metab]= initclickfit()

sccsid= '@(#)initclickfit.m	2.1 10/16/03 Copyright 2001 - 2003 Hammersmith Imanet Ltd.';
error( nargchk( 0, 0, nargin))          % check the number of input and out-
errmsg= nargchk( 0, 7, nargout);        % put arguments
if ~ isempty( errmsg) error( strrep( errmsg, 'input', 'output')); end

sif.name       = cellstr( 'dummy');
sif.header     = cellstr( 'dummy');
sif.framestart = 1;
sif.frameend   = 1;
sif.prompts    = 1;
sif.randoms    = 1;
sif.totals     = 1;
sif.date       = cellstr( 'dummy');
sif.time       = cellstr( 'dummy');
sif.nrows      = 1;
sif.ncols      = 1;
sif.version    = 1;
sif.scannum    = cellstr( 'dummy');
sif.isotope    = cellstr( 'dummy');
sif.midframes  = 1;
sif.framelength= 1;
sif.truerate   = 1;

inpfn.flag = 0;
inpfn.name = cellstr( 'dummy');
inpfn.nlines = 1;
inpfn.time = 1;
inpfn.parent = 1;
inpfn.blood= 1;

rreg.flag = 0;
rreg.name = cellstr( 'dummy');
rreg.i = 1;
rreg.d = 1;
rreg.t = 1;
rreg.c = 1;

p=0;
     
idwc.flag = 0;
idwc.nlines = 1;
idwc.name = cellstr( 'dummy');
idwc.i = 1;
idwc.d = 1;
idwc.w = 1;
idwc.c = 1;

y= 1;

metab.nlines = 0;
metab.time   = 0;
metab.parent = 0;
metab.name   = {'dummy'};
