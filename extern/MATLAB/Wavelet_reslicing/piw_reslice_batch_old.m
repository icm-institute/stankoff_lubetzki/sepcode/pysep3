function filerea = piw_reslice_batch(filename,tgframe,iframe,fframe)

directory = fileparts(filename);
cd directory % so that temporary files are not registered in the current directory and produce conflicts.
filenamec = filename;
levels = 2;
%=======================================================================
%				-setting wavelet parameters
%=======================================================================
fprintf(1,'    | Reading Volume: ');
[DYN,VOX,~] = readanalyze(filename);
fprintf(1,'done.');
DIM = size(DYN);

%=======================================================================
%				-setting wavelet parameters
%=======================================================================
dimxy = max([DIM(1) DIM(2)]);
for k = 1:8,
    if(2^k>=dimxy)break;end;
end
sizexy	= 2^k;
for k=1:8,
    if(2^k-20>=DIM(3))break;end;
end
sizez = 2^k;
c1 = sizexy/2;
c2 = sizez/2;
if((DIM(3)/2-floor(DIM(3)/2))~=0),
    indexz = c2 - floor(DIM(3)/2)+1;
else
    indexz = c2 - (DIM(3)/2);
end 
[H,K,RH,RK]	= lemarie(sizez/2);
%=======================================================================
%				-transforming cycle
%=======================================================================
fprintf(1,'\n    | Wavelet Filtering');
count = 0;
%	Transforming Cycle
for f=iframe:fframe,
    fprintf(1,'\n    |    Volume no. %d:',f);
    T = DYN(:,:,:,f);
    F = zeros(DIM(1),DIM(2),DIM(3));
    F(:) = T(:);
    clear T
    %	Putting into an adequate frame of dyadic dimensions
    frame = zeros(sizexy,sizexy,sizez);
    frame(:,:,indexz:indexz+DIM(3)-1) = F;
    clear F
    %	3-dimensional wavelet transform
    str	= ' Transforming and denoising';
    fprintf(1,str);
    w = wtnd(frame,H,K,levels);
    %	Zeroing detail coefficients
    w(sizexy/2+1:sizexy, 1:sizexy/2       , 1:sizez/2) = 0;
    w(1:sizexy/2   	   , sizexy/2+1:sizexy, 1:sizez/2) = 0;
    w(sizexy/2+1:sizexy, sizexy/2+1:sizexy, 1:sizez/2) = 0;
    w(1:sizexy/2       , 1:sizexy/2       , sizez/2+1:sizez) = 0;
    w(sizexy/2+1:sizexy, 1:sizexy/2       , sizez/2+1:sizez) = 0;
    w(1:sizexy/2       , sizexy/2+1:sizexy, sizez/2+1:sizez) = 0;
    w(sizexy/2+1:sizexy, sizexy/2+1:sizexy, sizez/2+1:sizez) = 0;
    if (levels==2),
        %	Zeroing 2nd level coefficients
        w(sizexy/4+1:sizexy/2, 1:sizexy/4         , 1:sizez/4) = 0;
        w(1:sizexy/4   	     , sizexy/4+1:sizexy/2, 1:sizez/4) = 0;
        w(sizexy/4+1:sizexy/2, sizexy/4+1:sizexy/2, 1:sizez/4) = 0;
        w(1:sizexy/4         , 1:sizexy/4         , sizez/4+1:sizez/2) = 0;
        w(sizexy/4+1:sizexy/2, 1:sizexy/4         , sizez/4+1:sizez/2) = 0;
        w(1:sizexy/4         , sizexy/4+1:sizexy/2, sizez/4+1:sizez/2) = 0;
        w(sizexy/4+1:sizexy/2, sizexy/4+1:sizexy/2, sizez/4+1:sizez/2) = 0;
    end
    img	= iwtnd(w ,RH,RK,levels);
    I = img(:,:,indexz:indexz+DIM(3)-1);
    count = count+1;
    DYN(:,:,:,count) = I;
    clear frame    		    		
end
%=======================================================================
%				-re-alligning the wavelet filtered image
%=======================================================================
fileout(1,:) = ['temp_00.img'];
T = DYN(:,:,:,tgframe);
writeanalyze(fileout,T,'int16',VOX,0,[0,0,0],[]);
for k=1:count,
    if k+iframe-1<10,
        fileout(1+k,:) = ['temp_0' num2str(k+iframe-1) '.img'];
    else
        fileout(1+k,:) = ['temp_' num2str(k+iframe-1) '.img']; 
    end
    T = DYN(:,:,:,k);
    writeanalyze(fileout(1+k,:),T,'int16',VOX,0,[0,0,0],[]);
end
clear DYN
warning off
fprintf(1,'\n    | Re-allignment of Wavelet Filtered Dynamic...');
if isempty(which('spm')),
    throw(MException('\n    | SPMCheck:NotFound', 'SPM not in matlab path'));
end
[name, version] = spm('ver');
fprintf('\n    | SPM version: %s Release: %s\n',name, version);
fprintf('    | SPM path: %s\n', which('spm'));
spm('Defaults','fMRI');
if strcmp(name, 'SPM8') || strcmp(name(1:5), 'SPM12'),
    spm_jobman('initcfg');
    spm_get_defaults('cmdline', 1);
end
% jobs = {};
% jobs{1}.spm.spatial.realign.estwrite.data = cellstr(char(fileout));
% jobs{1}.spm.spatial.realign.estwrite.eoptions.quality = 1;
% jobs{1}.spm.spatial.realign.estwrite.eoptions.fwhm = 3;
% jobs{1}.spm.spatial.realign.estwrite.eoptions.sep = 2;
% jobs{1}.spm.spatial.realign.estwrite.eoptions.rtm = 1;
% jobs{1}.spm.spatial.realign.estwrite.eoptions.wrap(1) = 0;
% jobs{1}.spm.spatial.realign.estwrite.eoptions.wrap(2) = 0;
% jobs{1}.spm.spatial.realign.estwrite.eoptions.wrap(3) = 0;
% jobs{1}.spm.spatial.realign.estwrite.roptions.which(1) = 2;
% jobs{1}.spm.spatial.realign.estwrite.roptions.which(2) = 1;
% jobs{1}.spm.spatial.realign.estwrite.roptions.interp = 2;
% jobs{1}.spm.spatial.realign.estwrite.roptions.prefix = 'r';
% P = spm_jobman('run', jobs);
flags = struct('quality',1,'fwhm',3,'sep',2,'interp',2,'wrap',[0 0 0],'rtm',1,'PW','','graphics',1,'lkp',1:6);
P  = spm_realign(char(fileout),flags);
delete temp_*.img
delete temp_*.hdr
fprintf(1,'    | Deleted temporary files \n');
%=======================================================================
%				-re-alligning the original dynamic
%=======================================================================
fprintf(1,'    | Re-slicing of the original dynamic file...');
[DYN,VOX,~]  = readanalyze(filenamec);
clear fileout
G(1) = P(1);
fileout(1,:) = ['temp_00.img'];
T  = DYN(:,:,:,tgframe);
scalf = writeanalyze(fileout,T,'int16',VOX,0,[0,0,0],[]);
G(1).pinfo = [scalf 0 0]';
for k=1:DIM(4)
    if k<10
        fileout(k+1,:) = ['temp_0' num2str(k) '.img'];
    else
        fileout(k+1,:) = ['temp_' num2str(k) '.img']; 
    end
    T = DYN(:,:,:,k);
    scalf = writeanalyze(fileout(k+1,:),T,'int16',VOX,0,[0,0,0],[]);
    if(k<iframe)
      G(k+1) = P(2);
    elseif((k>=iframe)&(k<=fframe))
      G(k+1) = P(k-iframe+2);
    elseif(k>fframe)
      G(k+1) = P(count+1);
    end
    G(k+1).fname=fileout(k+1,:);
    G(k+1).pinfo=[scalf 0 0]';
end
clear DYN
spm_reslice(G);
clear fileout
pack
for k=1:DIM(4),
    if k<10,
       fileout(k,:) = ['rtemp_0' num2str(k) '.img'];
    else
       fileout(k,:) = ['rtemp_' num2str(k) '.img']; 
    end
    [T,VOX,~]  = readanalyze(fileout(k,:));
    DYN(:,:,:,k)  =T;
end
filerea = [filename(1:end-7) '_movcorr.nii']; % supposing in .nii.gz
nii=make_nii(DYN, VOX, [], 16);
save_nii(nii,filerea)
delete temp_*.*
delete rtemp_*.*
delete meantemp_*.*
fprintf(1,'    | Deleted temporary files \n');
fprintf(1,'    | Going back in python \n');
close all
return
 