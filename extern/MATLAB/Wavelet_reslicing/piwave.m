function piwave(varargin) 
% Startup routine for PIWave
%
% PIWave: Parametric image wavelet analysis 
%
% PIWave (the collection of files listed by Contents.m) is copyright under
% the GNU general public license.  Please see piw_licence.man for details.
% 
% PIWave written by 
% Federico Turkheimer
%
% The routines for the computation of the WT in 1 and 2 dimensions are from
% the Uvi_Wave.300 package (http://www.tsc.uvigo.es/~wavelets/uvi_wave.html)
% which is also released under the GNU licence. See Content.m files for details.
% Many thanks the Uvi_wave authors:
% (Santiago Gonzalez Sanchez, Nuria Gonzalez Prelcic, Sergio J. Garcia Galan).
%
% Many thanks to Roger N. Gunn (MRC-CU) for the IDWC format and related input routine,
% and the automatic masking function
%
% spm_hwrite routine was borrowed from SPM96 software (http://www.fil.ion.ucl.ac.uk/spm99)
% which is also released under the GNU licence.  Many thanks the SPM authors:
% (John Ashburner, Karl Friston, Andrew Holmes, Jean-Baptiste Poline et al).
%
% readanalyze routine from (Colin Humphries University of California, Irvine 
% colin@alumni.caltech.edu)
%
%-Format arguments
%-----------------------------------------------------------------------
if nargin == 0, Action='strt'; else, Action = varargin{1}; end

%=======================================================================
%				-setting the path
%=======================================================================
if(Action=='strt'),piw_setpath;end;

%=======================================================================
% piwave('make' [,optfile])
% runs Phiwave mex file compilation   
% 
% Inputs
% optfile    - optional options (mexopts) file to use for compile
%   
% You may want to look into the optimizations for mex compilation
% See the SPM99 spm_MAKE.sh file or SPM2 Makefile for examples
% 
%-----------------------------------------------------------------------
if(Action=='make'),
    piw_setpath;
    if nargin < 2
       optfile = '';
    else
    optfile = varargin{2};
    end

    if ~isempty(optfile)
        if ~exist(optfile, 'file')
            error(['optfile ' optfile ' does not appear to exist']);
        end
        optfile = [' -f ' optfile];
    end

    mexfiles = { {'UVI_WAVE'}, ...
	           {'do_wtx.c', 'do_iwtx.c'} };
	     
    pwd_orig = pwd;
    piwave_dir = fileparts(which('piwave.m'));
    if isempty(piwave_dir)
       error('Can''t find piwave on the matlab path');
    end
    try
     for d = 1:size(mexfiles, 1)
         cd(fullfile(piwave_dir, mexfiles{d}{:}));
         files = mexfiles{d, 2};
         for f = 1:length(files)
           fprintf('Compiling %s\n', fullfile(pwd, files{f}));
           if isempty(optfile)
	           mex(files{f});
           else
	           mex(optfile, files{f});
           end
         end
     end
     cd(pwd_orig);
    catch
     cd(pwd_orig);
     rethrow(lasterror);
    end 
    fprintf('Piwave functions compiled -- Type ''piwave'' to start.\n');
    return
end;

%=======================================================================
%				-setting defaults
%=======================================================================
global PIWAVE
if(Action=='strt'),global PIWAVE,load PIWAVE;end;

%=======================================================================
%				-display splash screen
%=======================================================================
 [X,map] = imread('piwave.jpg');

 aspct = size(X,1) / size(X,2);
 ww = 400;
 srect = [200 300 ww+120 ww*aspct];   %-Scaled size splash rectangle
 h = figure('visible','off',...
	    'menubar','none',...
	    'numbertitle','off',...
	    'name','Welcome to PIwave 7.0',...
	    'pos',srect);
 
 im = image(X);
 colormap(map);
 ax = get(im, 'Parent');
 axis off;
 axis image;
 axis tight;
 set(ax,'plotboxaspectratiomode','manual',...
	'unit','pixels',...
	'pos',[0 0 srect(3)+120 srect(4)]);
 set(h,'visible','on');
%=======================================================================
%				-menu' bottons
%======================================================================= 
nbuttons	= 9;
sz 		= srect(4)/nbuttons;
h1		= uicontrol('Style','Pushbutton','Position',[0 srect(4)-  sz+1 120 sz],'Callback','close all;piw_setsetting;','String','Select Settings');
h2		= uicontrol('Style','Pushbutton','Position',[0 srect(4)-2*sz+1 120 sz],'Callback','close all;piw_makemask;','String','Mask Dynamic');
h3		= uicontrol('Style','Pushbutton','Position',[0 srect(4)-3*sz+1 120 sz],'Callback','close all;piw_addimage;','String','Add Image');
h4		= uicontrol('Style','Pushbutton','Position',[0 srect(4)-4*sz+1 120 sz],'Callback','close all;piw_wave_dyn_denoise;','String','Denoise Dynamic');
h5		= uicontrol('Style','Pushbutton','Position',[0 srect(4)-5*sz+1 120 sz],'Callback','close all;piw_wave_dyn_par;','String','Estimation');
h6		= uicontrol('Style','Pushbutton','Position',[0 srect(4)-6*sz+1 120 sz],'Callback','close all;piw_wave_dyn_par_3D_idwc;','String','Wavelet Estimation');
h7		= uicontrol('Style','Pushbutton','Position',[0 srect(4)-7*sz+1 120 sz],'Callback','close all;piw_strc;','String','Structural Denoising');
h8		= uicontrol('Style','Pushbutton','Position',[0 srect(4)-8*sz+1 120 sz],'Callback','close all;piw_reslice;','String','Frame Realignment');
h9		= uicontrol('Style','Pushbutton','Position',[0 srect(4)-9*sz+1 120 sz],'Callback','close all;clear all;piw_rmpath;','String','Exit');

%_______________________________________________________________________
% @(#)PIWAVE    Ver. 7.0 Imperial College London 2008

