% +-------------------------------------------------------------------------+
% |                             vjc_SIFread.m                               |
% |                Read Scan Information Files (*.sif).                     |
% |    Vin Cunningham, Rainer Hinz                   05/05/27               |
% +-------------------------------------------------------------------------+
%
% sifdata = vjc_SIFread( siffile)
%
% Input:	siffile	optional argument, specifying the name of the
%                       Scan Information File file to be read,
%			default: a dialogue will appear
% Return:	sifdata	SIF data structure
% Global:	HAND	handle of CLICKFIT's graphic objects
%
% See also:	/usr/local/mrccu/rpm/rpm/SIFread.m for Roger's routine

function sifdata = vjc_SIFread( siffile)

sccsid= '@(#)vjc_SIFread.m	3.2 05/27/05 Copyright 2001 - 2005 Hammersmith Imanet Ltd.';
error( nargchk( 0, 1, nargin))          % check the number of input and out-
errmsg= nargchk( 0, 1, nargout);        % put arguments
if ~ isempty( errmsg) error( strrep( errmsg, 'input', 'output')); end

nl= sprintf ('\n');                     % new line sequence
beep= sprintf ('\a\a\a');               % alert sequence: ring the bell
[sifdata, idwc, inpfn, rreg, p, y]= initclickfit;  % get default sifdata

if nargin == 0				% no siffile name given
  [fname, pname] = uigetfile('*.sif', 'Choose SIF File');
  if fname == 0 fprintf( beep); return; end
  siffile= fullfile(pname, fname);	% build full filename from parts
else
  if ~ ischar( siffile )
    if iscellstr( siffile )
      siffile= char( siffile);		% convert from cell string
    else
      fprintf( beep)
      errordlg( ['Argument "siffile" must be of type   ' nl ...
        'character or cell string!         '], 'Load SIF Error')
      return
    end
  end
  [pname, fname, fext] = fileparts( siffile);% split into path and file name
  fname= [fname fext];
end

[fid, errmsg]= fopen( siffile, 'r');	% open the file for reading
if fid == -1
  fprintf( beep)
  errordlg( ['"' siffile '":' nl errmsg], 'Load SIF Error')
  return
end

header= fgetl( fid);			% get the header line
fclose(fid);				% close the file
if header == -1
  fprintf( beep)
  errordlg( ['"' siffile '":' nl 'Cannot read header line.'], ...
    'Load SIF Error')
  return
end
headerw= words( header);

sifdata.name  = cellstr( siffile);
sifdata.header= cellstr( header);
sifdata.date  = cellstr( headerw(1,:));
sifdata.time  = cellstr( headerw(2,:));

[sifdata.nrows, count, errmsg]= sscanf( headerw(3,:), '%d' );
if (count ~= 1) | ~ isempty( errmsg) | (sifdata.nrows <= 0)
  fprintf( beep)
  errordlg( ['"' siffile '":' nl 'Cannot parse the number of frames "' ...
     deblank(headerw(3,:)) '": ' errmsg], 'Load SIF Error')
  return
end

[sifdata.ncols, count, errmsg]= sscanf( headerw(4,:), '%d' );
if (count ~= 1) | ~ isempty( errmsg)
  fprintf( beep)
  errordlg( ['"' siffile '":' nl 'Cannot parse the number of columns "' ...
     deblank(headerw(4,:)) '": ' errmsg], 'Load SIF Error')
  return
end
if sifdata.ncols ~= 4
  fprintf( beep)
  errordlg( ['"' siffile '":' nl 'The number of columns ' ...
     num2str(sifdata.ncols) ' in the header line is invalid.' nl ...
     'Only 4 columns are currently implemented.'], 'Load SIF Error')
  return
end

[sifdata.version, count, errmsg]= sscanf( headerw(5,:), '%d' );
if (count ~= 1) | ~ isempty( errmsg) | (sifdata.version <= 0)
  fprintf( beep)
  errordlg( ['"' siffile '":' nl 'Cannot parse the version number "' ...
     deblank(headerw(5,:)) '": ' errmsg], 'Load SIF Error')
  return
end

if size(headerw, 1) < 7			% Old (version 1) SIF files
  sifdata.scannum = cellstr( 'not known');
  sifdata.isotope = cellstr( 'not known');
else
  sifdata.scannum = cellstr( headerw(6,:));
  sifdata.isotope = cellstr( headerw(7,:));
end

[sifdata.framestart sifdata.frameend sifdata.prompts sifdata.randoms]= ...
  textread( siffile, '%f %f %f %f', sifdata.nrows, 'headerlines', 1, ...
  'commentstyle', 'shell');		% ignore characters after #


if (sifdata.nrows ~= length(sifdata.framestart)) | ...
   (sifdata.nrows ~= length(sifdata.frameend)) | ...
   (sifdata.nrows ~= length(sifdata.prompts)) | ...
   (sifdata.nrows ~= length(sifdata.randoms))
  fprintf( beep)			% check for consistency
  errordlg( ['"' siffile '":' nl ...
    'Cannot read the required ' num2str( sifdata.nrows) ' lines ' ...
    'of data.' nl 'Check for inconsistency!'], 'Load SIF Error')
  return
end
	
sifdata.midframes  = (sifdata.framestart+sifdata.frameend)/2;
sifdata.framelength= sifdata.frameend - sifdata.framestart;
sifdata.totals     = sifdata.prompts + sifdata.randoms;
trues              = sifdata.prompts - sifdata.randoms;
sifdata.truerate   = trues./sifdata.framelength;
promptrate         = sifdata.prompts./sifdata.framelength;
randomrate         = sifdata.randoms./sifdata.framelength;
totalrate          = sifdata.totals./sifdata.framelength;

if lower(sifdata.scannum{1}(1)) == 'h'		% 966 tomograph?
  scatter_fraction = 0.42;
  object_diameter  = 18;
  fov_diameter     = 60;
  nec              = trues.^2 * (1-scatter_fraction)^2 ./ ...
   (trues + 2*object_diameter/fov_diameter*sifdata.randoms);
  necrate          = nec./sifdata.framelength;
end

global HAND			% declare HAND as a global variable
if isfield( HAND, 'sifname') & ishandle( HAND.sifname)
  set( HAND.sifname, 'String', fname);
  set( HAND.sifname, 'Visible', 'on');
  set( HAND.sifname, 'Position',[110 412 75 24]); 
end

switch sifdata.isotope{1}
  case {'C-11', 'c11'}
    lambda= 0.0005663;
  case 'F-18'
    lambda= 0.0001053;
  case 'O-15'
    lambda= 0.005656;
  otherwise
    lambda= 0;
end

% -------------------- create figure and plot count rates ------------------------

fhan= figure ('Name', ['Scan information file "' sifdata.name{1} '"']);
					% Get current figure properties:
fpos= get( fhan, 'Position');		%   size of figure in pixels
fpos(2)= fpos(2) - fpos(4);
fpos(4)= 2*fpos(4);
if lambda
  set( fhan, 'Position', fpos)		% resize the figure
end


if lambda
  subplot( 2, 1, 1)
end
hold on
plot( sifdata.midframes, randomrate, 'go')
plot( sifdata.midframes, promptrate, 'b+')
plot( sifdata.midframes, sifdata.truerate, 'rx')
plot( sifdata.midframes, totalrate, 'k.')
title( 'Original data - no decay correction applied')
ylabel( 'Count rate (head curve) in s^{-1}')
xlabel( 'Frame midpoint time in s')
text( sifdata.midframes(sifdata.nrows), promptrate(sifdata.nrows), ...
  '   prompts', 'Color', 'b', 'VerticalAlignment', 'baseline')
text( sifdata.midframes(sifdata.nrows), sifdata.truerate(sifdata.nrows), ...
  '   trues', 'Color', 'r', 'VerticalAlignment', 'cap')
text( sifdata.midframes(sifdata.nrows), randomrate(sifdata.nrows), ...
  '   randoms', 'Color', 'g', 'VerticalAlignment', 'middle')
[totalratemax(2), totalratemax(1)]= max( totalrate);
text( sifdata.midframes(totalratemax(1)), totalratemax(2), ...
  '   totals', 'Color', 'k', 'VerticalAlignment', 'baseline')
if exist( 'nec', 'var')
  plot( sifdata.midframes, necrate, 'm*')
  text( sifdata.midframes(totalratemax(1)), max(necrate),...
    '   NEC (966 only)', 'Color', 'm', 'VerticalAlignment', 'baseline')
end

if lambda
  subplot( 2, 1, 2)
  hold on
  dc= lambda * sifdata.framelength ./ ...         % decay correction
      ( exp(-lambda*sifdata.framestart) - exp(-lambda*sifdata.frameend) );
  plot( sifdata.midframes, dc .* randomrate, 'go')
  plot( sifdata.midframes, dc .* promptrate, 'b+')
  plot( sifdata.midframes, dc .* sifdata.truerate, 'rx')
  plot( sifdata.midframes, dc .* totalrate, 'k.')

  title( [ 'Decay corrected data (' sifdata.isotope{1} '), T_{�}= ' ...
    num2str( log(2.0) / lambda / 60) ' min' ])
  ylabel( 'Count rate (head curve) in s^{-1}')
  xlabel( 'Frame midpoint time in s')
  text( sifdata.midframes(sifdata.nrows), ...
        dc(sifdata.nrows) * promptrate(sifdata.nrows), ...
    '   prompts', 'Color', 'b', 'VerticalAlignment', 'baseline')
  text( sifdata.midframes(sifdata.nrows), ...
        dc(sifdata.nrows) * sifdata.truerate(sifdata.nrows), ...
    '   trues', 'Color', 'r', 'VerticalAlignment', 'cap')
  text( sifdata.midframes(sifdata.nrows), ...
        dc(sifdata.nrows) * randomrate(sifdata.nrows), ...
    '   randoms', 'Color', 'g', 'VerticalAlignment', 'middle')
  [totalratemax(2), totalratemax(1)]= max( dc .* totalrate);
  text( sifdata.midframes(totalratemax(1)), totalratemax(2), ...
    '   totals', 'Color', 'k', 'VerticalAlignment', 'baseline')

  if exist( 'nec', 'var')
    plot( sifdata.midframes, dc .* necrate, 'm*')
    text( sifdata.midframes(sifdata.nrows), ...
          dc(sifdata.nrows) * necrate(sifdata.nrows), ...
      '   NEC (966 only)', 'Color', 'm', 'VerticalAlignment', 'middle')
  end
end

fpos(1)= fpos(1) + 25;		% shift figure slightly to the right
figure( 'Name', ['Scan information file "' sifdata.name{1} '"'], ...
  'Position', fpos)
subplot( 2, 1, 1)
bar( sifdata.framelength, 'r')
set(gca, 'TickDir', 'out', 'Box', 'off')  
title( 'Frame definition')
ylabel( 'Frame length in s')
xlabel( 'Frame number')

subplot( 2, 1, 2)
bar( [sifdata.randoms sifdata.prompts], 'stack')
set(gca, 'TickDir', 'out', 'Box', 'off') 
legend( ['randoms'; 'prompts'])
title( 'Frame statistics')
ylabel( 'Total counts per frame')
xlabel( 'Frame number')
text( 0.01, 0.95, [ num2str(sum(sifdata.randoms)) ' randoms' ], ...
  'Units', 'normalized')
text( 0.01, 0.90, [ num2str(sum(sifdata.prompts)) ' prompts' ], ...
  'Units', 'normalized')
text( 0.01, 0.95, [ num2str(sum(sifdata.randoms)) ' randoms' ], ...
  'Units', 'normalized')
text( 0.01, 0.85, [ num2str(sum(sifdata.totals)) ' totals' ], ...
  'Units', 'normalized')

figure( fhan)			% bring back figure with the count rates

% +-------------------------------------------------------------------------+
% |                      Parse a sentence into words.                       |
% +-------------------------------------------------------------------------+
%
% Reference: Matlab 6 Users Guide "Using MATLAB",
%	     Chapter 18 "Character Arrays (Strings)",
%	     "Searching and Replacing", page 18-13
% Returns an array of strings.

function all_words = words(input_string)
remainder = input_string;
all_words = '';

while (any(remainder))
  [chopped,remainder] = strtok(remainder);
  all_words = strvcat(all_words,chopped);
end
