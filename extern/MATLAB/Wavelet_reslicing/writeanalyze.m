function SCALE=writeanalyze(file,I,type,vox,offset,origin,comment)

str	= ['    |    Saving the file as ' file ': '];
fprintf(1,'\n%s',str);

DIM = size(I);
DIM		= DIM(:)'; if size(DIM,2) < 4; DIM = [DIM 1]; end

T = 0;
type = type(1:5);
if all(type == 'uint8');  T = 2;  
elseif all(type == 'int16');  T = 4;  
else error('Unsupported datatype');
end

volmin=min(I(:));
volmax=max(I(:));

if abs(volmin)>abs(volmax)
    if (T==2)  SCALE=abs(volmin)/255; end
    if (T==4)  SCALE=abs(volmin)/32767; end
else
    if (T==2)  SCALE=abs(volmax)/255; end
    if (T==4)  SCALE=abs(volmax)/32767; end
end

piw_hwrite(file,DIM,vox,SCALE,T,offset,origin,comment);
fid   = fopen(file,'w');
if (DIM(4)==1),
    It  = I(:);
    eval(['fwrite(fid,It/SCALE,piw_type(T));'])
else
   for f=1:DIM(4),
	 temp	= I(:,:,:,f);
	 It	    = temp(:);
	 eval(['fwrite(fid,It/SCALE,piw_type(T));'])
   end
end
fclose(fid);
fprintf(1,'done.');

return

%_______________________________________________________________________
% @(#)PIWAVE    Ver. 6.0 Imperial College London 2006
