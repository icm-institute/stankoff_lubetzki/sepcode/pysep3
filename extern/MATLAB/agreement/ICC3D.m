function ICC3D(test, retest, out)
%% creating some sort of brain mask the lazy way
MASK_path = '/network/lustre/iss01/lubetzki-stankoff/clinic/tools/Atlas/MNI/mni_icbm152_nlin_sym_09c/mni_icbm152_t1_tal_nlin_sym_09c_brain.nii.gz';
info_mask = load_nii(MASK_path);
save_img = info_mask;
mask = logical(info_mask.img);
N = sum(mask(:)==1);
%% just some python to matlab agreement
test = char(test);
retest = char(retest);
disp(out)
%% processing
n_subjects = length(test(:,1));
X = zeros(N,n_subjects,2);
ICC1D = zeros(N,1);
for s = 1:n_subjects
    info_test = load_nii(test(s,:));
    info_retest = load_nii(retest(s,:));
    test3D = info_test.img;
    retest3D = info_retest.img;
    X(:,s,1) = test3D(mask);
    X(:,s,2) = retest3D(mask);
end
for v = 1:N
    x(:,1) = X(v,:,1);
    x(:,2) = X(v,:,2);
    ICC1D(v) = IPN_icc(x,3,'single');
end
ICC3D = NaN(size(mask));
ICC3D(mask) = ICC1D;
save_img.img = single(ICC3D);
save_nii(save_img, out)
end