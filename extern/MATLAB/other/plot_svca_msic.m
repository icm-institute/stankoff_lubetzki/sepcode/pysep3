clf
hold on
A = load('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpasuper_old_classes/BG005_V01_dpa_movcorr_refregion.mat');
plot(A.tacs.time,A.tacs.tac, '-o')
B = load('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpasuper_new_classes_matteo/BG005_V01_dpa_movcorr_refregion.mat');
plot(A.tacs.time,B.tacs.tac, '-o')
C = load('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpasuper_new_classes_arya/BG005_V01_dpa_movcorr_refregion.mat');
plot(A.tacs.time,C.tacs.tac, '-o')
D = load('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpasuper_try/BG005_V01_dpa_movcorr_refregion.mat');
plot(A.tacs.time,D.tacs.tac, '-o')
legend('old HAB classes', 'new HAB classes CTX + WM ero',  'new HAB classes CBGM + WM',  'new HAB classes arya CBGM + WMero')
title('comparison of reference region TAC')


%%

A = importdata('DPA_all_2015_10_01.csv');
A = importdata('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats_final_carotide_mask/merged_HAB_visit1.csv');
plot(A(:,1), A(:,2:5))
legend('CORTEX', 'BLOOD', 'WHITE MATTER', 'HIGH BINDING')
title('new classes for the DPA (HAB only)')