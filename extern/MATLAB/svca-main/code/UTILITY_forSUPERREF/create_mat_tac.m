function create_mat_tac(pet, siffile, reffile, reftacfile)
        fprintf(' -- Computing REF tac\n')
        [time,~,~,delta]=read_SIF(siffile);
        dynPET = readNIFTI(pet,1); %% Correction for compressed nifti
        permPET=permute(dynPET,[4 1 2 3]);
        clear dynPET;
        refmask= readNIFTI(reffile,1);
        %% Computing cluster tac
        tacs(1).time=time;
        [tacs(1).tac,tacs(1).TACs]=tacExtraction_perm(permPET,refmask);
        tacs(1).NSD=sqrt(tacs(1).tac./delta);
        tacs(1).name='REFREGION';
        tacs(1).index=find(refmask>0);
        save(reftacfile,'tacs');
end