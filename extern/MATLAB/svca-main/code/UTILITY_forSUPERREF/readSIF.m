function [time,starttime,endtime,delta]=readSIF(siffile)
% Read .sif files
% 
%
%       [time,starttime,endtime,delta]=readSIF(siffile)
%
aa=importdata(char(siffile));

starttime=aa.data(:,1);
endtime  =aa.data(:,2);
delta    =endtime-starttime;
time     =starttime+0.5*delta;

