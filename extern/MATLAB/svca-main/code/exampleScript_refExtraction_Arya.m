%%
dynPET_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/MIMS/data/process/P02/V01/pkpreproc/P02_V01_pk_movcorr.nii.gz';
file_kineticClasses='/network/lustre/iss01/lubetzki-stankoff/clinic/MIMS/scripts/svca-main/code/UTILITY_forSUPERREF/tissueClassesForSupervisedRef.csv';
BRAIN_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/MIMS/data/process/P02/V01/pkROI/P02_V01_t1_N4_brain_mask_2pk.nii.gz';
GM_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/MIMS/data/process/P02/V01/pkROI/P02_V01_ribbon_ctx_2pk.nii.gz';
siffile = '/network/lustre/iss01/lubetzki-stankoff/clinic/MIMS/utils/pet.sif';
%%
niiPET=load_nii(dynPET_file);
dynPET=double(niiPET.img)/1000; %from Bq to KBq
DIM=size(dynPET);   %PET dimension
brainMask = load_nii(BRAIN_file);
brainMask = double(brainMask.img);
niiGM = load_nii(GM_file);
GM = double(niiGM.img);
GM = GM./max(GM(:)); %Normalize GM between 0 and 1
indMaskExtraction=find(GM>0.9);
refMaskExtraction = zeros(DIM(1:3));
refMaskExtraction(indMaskExtraction) = 1;
[time,starttime,endtime,delta]=readSIF(siffile);
time = time /60;
%% RUN superX_referExtraction
[RefTAC, RefMask] = superX_referExtraction(dynPET,file_kineticClasses,brainMask,refMaskExtraction, time);

%% 

plot(time, RefTAC)