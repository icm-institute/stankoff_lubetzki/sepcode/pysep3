#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 10:09:04 2020

@author: mariem.hamzaoui
"""

import nibabel as nib
import os
import numpy as np
import time
import argparse
from nipype.interfaces.fsl import ImageStats, Threshold
from termcolor import cprint, colored
from skimage.feature import hessian_matrix, hessian_matrix_eigvals
from skimage.filters import threshold_otsu
from skimage.segmentation import clear_border
from skimage.measure import label
from skimage.morphology import closing
from common3 import *
from tools_ants import ApplyRegistration

#------------------------------------------------------------------------------
#--------------------------Function to label lesions---------------------------
#------------------------------------------------------------------------------
""" this function labels lesions while cleaning borders
    therefor we propagate this labels through the mask"""
def Labels (PathFuzzy, PathMask, outdir):    
    img = nib.load(PathFuzzy)
    image_fuzzy = img.get_fdata()
    transform = img.affine
    # Apply a threshold
    thresh = threshold_otsu(image_fuzzy)
    bw = closing (image_fuzzy>thresh)
    # Remove artifacts connected to image border
    cleared = clear_border(bw)
    #label_image regions
    label_image = label(cleared)
    # labeled fuzzy image into a file
    PathLabel = changeName(PathFuzzy, suffix = "_label")
    fuzzy_labeled = nib.Nifti1Image(label_image,transform)    
    fuzzy_labeled.to_filename(PathLabel)
    # Propagate labels using the fuzzy
    PathLabelDilated = changeName(PathLabel, outdir = outdir, suffix = "_dilated")    
    command1 = ["ImageMath", "3",PathLabelDilated, "PropagateLabelsThroughMask", PathMask, PathLabel]
    command(command1)    
    # remove extra files and rename the dilated labeled file
    # extra files are _speed and _dilated_label extra output of the ImageMath command also the _label of the first labeling step we won't need it    
    os.remove(PathLabel)    
    PathLabelDilatedSpeed = changeName(PathLabelDilated, outdir = outdir, suffix = "_speed") # the extra speed file
    os.remove(PathLabelDilatedSpeed)    
    PathLabelDilatedLabel = changeName(PathLabelDilated, outdir=outdir, suffix ="_label") # the extra label file
    os.remove(PathLabelDilatedLabel)    
    os.rename(PathLabelDilated, PathLabel) #just to rename the dilated label file to just label     
    return (PathLabel)
#------------------------------------------------------------------------------
#-----------------------------Big lesions identif------------------------------
#------------------------------------------------------------------------------
"""We identify the big lesions by their volume default is 300
         but user can precise the value they want"""
def BigLesionIdentif (PathFuzzy, PathLabel, thresh_volume):    
    stats = ImageStats(index_mask_file=PathLabel, in_file=PathFuzzy, op_string= '-V').run().outputs # return an object containing a dict
    stats = np.asarray(stats.get()['out_stat'][::2])    
    img = nib.load(PathFuzzy)
    image_fuzzy = img.get_fdata()
    transform = img.affine
    img = nib.load(PathLabel)
    image_label = img.get_fdata()
    size_1 = image_fuzzy.shape[0]
    size_2 = image_fuzzy.shape[1]
    size_3 = image_fuzzy.shape[2]    
    small_lesions_fuzzy = np.zeros((size_1,size_2,size_3))    
    big_lesions_fuzzy = np.zeros((size_1,size_2,size_3))
    rel_small = stats < thresh_volume # Threshold for relatively small lesion
    for i in range(len(rel_small)):
        if rel_small[i]:
            label_voxels = np.asarray(np.where(image_label==(i+1)))
            for j in range(label_voxels.shape[1]):
                small_lesions_fuzzy[label_voxels[0,j],label_voxels[1,j],label_voxels[2,j]] = image_fuzzy[label_voxels[0,j],label_voxels[1,j],label_voxels[2,j]]
        else:
            label_voxels = np.asarray(np.where(image_label==(i+1)))
            for j in range(label_voxels.shape[1]):
                big_lesions_fuzzy[label_voxels[0,j],label_voxels[1,j],label_voxels[2,j]] = image_fuzzy[label_voxels[0,j],label_voxels[1,j],label_voxels[2,j]]                
    return big_lesions_fuzzy, small_lesions_fuzzy, transform
#------------------------------------------------------------------------------
#------------------------------Hessian Algorithm-------------------------------
#------------------------------------------------------------------------------
""" This algorithm is inspired from this paper :
    http://www.ajnr.org/content/early/2018/02/22/ajnr.A5556
    we apply this algorithm on the big lesions only"""
def HessianAlgo (big_lesion, small_lesions):
    hessian_mat = hessian_matrix(big_lesion, sigma = 0.5, order = 'xyz')
    eig_elems = hessian_matrix_eigvals (hessian_mat)
    size_1 = big_lesion.shape[0]
    size_2 = big_lesion.shape[1]
    size_3 = big_lesion.shape[2]
    big_lesion_filtered = np.zeros((size_1,size_2,size_3))
    for i in range(size_1):
        for j in range(size_2):
            for k in range(size_3):
                if (np.max(eig_elems[0:3,i,j,k]) <= 10**(-6)):
                    big_lesion_filtered[i,j,k] = big_lesion[i,j,k]
    fuzzy_filtered = big_lesion_filtered + small_lesions    
    return fuzzy_filtered
#------------------------------------------------------------------------------
#-------------------------------------MAIN-------------------------------------
#------------------------------------------------------------------------------
if __name__=="__main__":
    start = time.time()    
    usage= "usage: %prog [opts]"    
    parser = argparse.ArgumentParser(description='Seperate Confluent lesions based on the hessian (we smoothed by 0.5(gaussian)) of the fuzzy file (algorithm needs the _fuzzy.nii.gz in Vxx/lesions_manual, we thresholded the fuzzy by 0.25). Method inspired from : http://www.ajnr.org/content/early/2018/02/22/ajnr.A5556. Make sure to work with ANTs 2.3.1 and this algorithm works in the halfwaysapce. ')
    group_opt = parser.add_argument_group("Mandatory Options")
    group_opt.add_argument("-p","--projectdir", dest="projectdir",
                      help=" Directory where the project is stored (otherwise PROJECT_DIR defined in os.environment is used), Should not contain a point")
    group_opt.add_argument("-s","--sid"       , dest="sids",
                      help=" File with a list of subjects or subject ids: P01,P02/V01")   
    group_adv = parser.add_argument_group("Advanced Options")
    group_adv.add_argument("-t", "--thresh", dest= "thresh",
                     help= "this is for you to choose your threshold to consider a big lesion")
    group_adv.add_argument("-v","--verbose"   ,action="store_true", dest="verbose",
                           help="Read the commands output in the terminal")
    options = parser.parse_args()
#------------------------------------------------------------------------------
#################Reading mandatory args (SID & PROJECT_DIR)####################
#------------------------------------------------------------------------------  
    projectdir = os.environ["PROJECT_DIR"]
    if options.projectdir :
        projectdir = os.path.abspath(options.projectdir) + os.sep
    if projectdir.find(".")+1:
        cprint("Cannot perform script with this PROJECT_DIR!!! IT CONTAINS A POINT at " +str(projectdir.find(".")+1)+"!!! Please care to change it work with the data in the luster /network/lustre/xx/xx... instead of "+ projectdir,"red",attrs=["bold"])
        sys.exit()
    sids = readSubjectsLists(options.sids, options.verbose)    
    for sid in sids:
        start = time.time()
        if options.verbose:
            print (" ")
            cprint ("="*92,'green',attrs=['bold'])
            cprint ("-"*33 +"Starting patient : "+str(sid["Id"])+"-"*33,'green',attrs=['bold'])
            cprint ("="*92,'green',attrs=['bold'])
            print (" ")
        subjdir = projectdir + "process" + os.sep + sid['Id']
        hwsdir  = get_subdir_regex(subjdir,sid['Id'])
        ch = hwsdir[0][hwsdir[0].find('fs_')+3:]
        print(ch)
        outdir = hwsdir[0] + "/HWSmask"
        PathFuzzyHws = outdir + "/"+sid['Id']+'_V01_t2_t2les_manual_fuzzy_to_hws_'+ch+'.nii.gz'
#---------------------Read the transformation matrix files---------------------           
        matV01_to_hws = get_subdir_regex_files(hwsdir[0], ["V01_t1_N4_to_hws"])           
        matT2_to_T1 = get_subdir_regex_files(subjdir+"/V01/contouringpreproc",["_t2_to_t1_0GenericAffine.mat"])
        #ref volume
        refV01 = get_subdir_regex_files (hwsdir[0],[".nii.gz"])
#------------------------------Read the fuzzy file-----------------------------        
        PathFuzzy = get_subdir_regex_files(subjdir+"/V01/lesions_manual",["_fuzzy.nii.gz"])
        # Take the fuzzy to the halfway space        
        PathFuzzyHws = ApplyRegistration(PathFuzzy[0],refV01[0],[matV01_to_hws[0],matT2_to_T1[0]],PathFuzzyHws,[False,False])            
        # Threshold the fuzzy mask by a value of 0.25
        # Threshold
        PathFuzzyHwsThr = changeName(PathFuzzyHws, outdir = outdir, suffix = "_thresh025")
        PathFuzzyHwsThr = Threshold(in_file = PathFuzzyHws, out_file=PathFuzzyHwsThr, thresh = 0.25).run().outputs.get()["out_file"]
        PathFuzzyHwsFilt = PathFuzzyHwsThr.replace("thresh025","thr025_S05")      
#-------------------------------Label the fuzzy--------------------------------
        PathLabelHws = Labels(PathFuzzyHwsThr, PathFuzzyHwsThr, outdir)
#-----------------------------Identif Big lesions------------------------------
        threshold = 300
        if options.thresh:
            threshold = int(options.thresh)
        Big_lesion_fuzzy, Small_lesion_fuzzy, transform = BigLesionIdentif(PathFuzzyHwsThr, PathLabelHws,threshold)        
#-------------------------Apply Hessian Big lesions----------------------------
        Pathfuzzyfilt= HessianAlgo(Big_lesion_fuzzy,Small_lesion_fuzzy)
        fuzzy_labeled = nib.Nifti1Image(Pathfuzzyfilt,transform)
        fuzzy_labeled.to_filename(PathFuzzyHwsFilt)
#-------------------------------Label the fuzzy--------------------------------
        PathLabelFilt = Labels(PathFuzzyHwsFilt, PathFuzzyHwsThr, outdir)
        if options.verbose:        
            cprint ('-'*23,'red',attrs=['bold'])
            cprint (" -Exec time: "+ str(np.round(time.time()-start,4))+"-",'red',attrs=['bold'])
            cprint ('-'*23,'red',attrs=['bold'])