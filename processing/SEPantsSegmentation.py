#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import shutil
import sys
from termcolor import cprint, colored
from common3 import get_subdir_regex_files, init_script, get_regex
from tools_fsl import fslmaths
from nipype.interfaces.ants.segmentation import CorticalThickness
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

MSG = "ANTS based brain parcellation."

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# ------------------------ Defining path to t1preproc -------------------------
        t1preproc = os.path.join(process, s['Id'], s['Visit'], 't1preproc')
        if not os.path.isdir(t1preproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + t1preproc + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| t1preproc: ', 'green', attrs=['bold']),
              t1preproc)
# ------------------------- Retrieving T1_N4 sequence -------------------------
        N4 = get_subdir_regex_files(t1preproc, '_t1_N4.nii.gz')[0]
        if not os.path.isfile(N4):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1 NOT FOUND', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found T1 seq: ', 'green', attrs=['bold']), N4)
# --------------------------- Retrieving Brain Mask ---------------------------
        b_mask = get_regex(t1preproc, '_brain_mask.nii.gz', False)
        if not b_mask:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: brain mask not found in ' + t1preproc, 'red',
                          attrs=['bold']))
            continue
        b_mask = b_mask[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Brain Mask: ', 'green', attrs=['bold']),
              b_mask)
# --------------------------- Segmenting the Brain ----------------------------
        segdir = os.path.join(process, s['Id'], s['Visit'], 'antsSegmentation')
        TEMPLATE = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Matte"
        TEMPLATE = TEMPLATE + "o/Software/Atlas/ADNI_Normal/"
        CT = CorticalThickness()
        CT.inputs.dimension = 3
        CT.inputs.anatomical_image = N4
        CT.inputs.out_prefix = segdir
        CT.inputs.brain_template = TEMPLATE + 'T_template0.nii.gz'
        CT.inputs.t1_registration_template = TEMPLATE + 'T_template0_BrainCerebellum.nii.gz'
        CT.inputs.brain_probability_mask = TEMPLATE + 'T_template0_BrainCerebellumProbabilityMask.nii.gz'
        CT.inputs.segmentation_priors = [TEMPLATE + '/PRIORS/priors1.nii.gz',
                                         TEMPLATE + '/PRIORS/priors2.nii.gz',
                                         TEMPLATE + '/PRIORS/priors3.nii.gz',
                                         TEMPLATE + '/PRIORS/priors4.nii.gz',
                                         TEMPLATE + '/PRIORS/priors5.nii.gz',
                                         TEMPLATE + '/PRIORS/priors6.nii.gz']
        CT.inputs.num_threads = 8
        CT.inputs.use_floatingpoint_precision = 1
        CT.ignore_exception = True
        # brain extraction (needed for intermediete files)
        CT1 = CT
        CT1.inputs.args = "-y 1"
        CT1.run()
        # replace with manually corrected brain mask
        brain_path = os.path.join(segdir, 'BrainExtractionBrain.nii.gz')
        brain = fslmaths(N4, 'x', b_mask, out=brain_path)
        shutil.copyfile(b_mask, os.path.join(segdir,
                                             'BrainExtractionMask.nii.gz'))
        # template registration
        CT2 = CT
        CT2.inputs.args = "-y 2"
        CT2.run()
        # segmentation
        CT3 = CT
        CT3.inputs.args = "-y 3"
        CT3.run()
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
