#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 17 10:16:17 2020

@author: arya.yazdan-panah
"""


import os
import sys
from tools_c3 import connected_components
from termcolor import cprint, colored
from common3 import get_regex, init_script
from optparse import OptionParser


if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = 'usage: %prog'
    parser = OptionParser(usage=usage)
    parser.add_option('-s', '--subjects', dest='subjects',
                      help='Subjects list', type="string")
    parser.add_option("-r", "--raw", dest="raw",
                      help="Raw directory")
    parser.add_option("-p", "--process", dest="process",
                      help="Process directory")
    parser.add_option("-l", "--lesion_mask", dest="lm", default='t2',
                      help="Lesion_mask to separate (default T2)")
    parser.add_option("-t", "--threshold", dest="thrs", default=50, type=int,
                      help="Threshold for lesions having a volume above [THRS"
                      "], default=50 (in voxels)")
    (options, args) = parser.parse_args()
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
# ----------------------------- Finding Images --------------------------------
    lm_regex = options.lm.lower()
    print(colored("| LESION MASK:", 'blue', attrs=['bold']), lm_regex.upper())
    thrs = options.thrs
    print(colored("| LESION VOLUME THRESHOLD:", 'blue', attrs=['bold']),
          thrs)
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        if s['Id'].startswith('C'):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| ' + s['Id'] + ' is a control, skipping', 'green',
                          attrs=['bold']))
            continue
        s['process'] = os.path.join(process, s['Id'], s['Visit'])
# ---------------------------- Finding lesion fold ----------------------------
        test = get_regex(s['process'], 'lesion', deep_search=False,
                         full=False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: LESION FOLDER NOT FOUND:', 'red',
                          attrs=['bold']))
            continue
        s["man_les"] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found lesion folder:', 'green', attrs=['bold']),
              s["man_les"])
# ---------------------------- Finding lesion mask ----------------------------
        test = get_regex(s['man_les'], lm_regex + '.*nii.gz', False, False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: LESION MASK NOT FOUND:', 'red',
                          attrs=['bold']), lm_regex.upper())
            continue
        s["les_mask"] = test[0]
        print('|', '| Found lesion mask:', s["les_mask"])
# --------------------------- Separating lesion mask --------------------------
        connected_components(s["les_mask"], min_size=thrs)
# ----------------------------- End of 1 subject ------------------------------
        print('|', 'L')
    print('|')
    print('| Script Finished ')
    print('')
