#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 17:09:27 2020

@author: arya.yazdan-panah
"""

import os
import sys
import shutil
import numpy as np
import nibabel as nib
from common3 import init_script, get_regex, createDir, changeName, command
from tools_ants import n4_corr, antsBrainExtraction
from tools_cluster import qsub3
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from importlib import import_module

MSG = "Corrects 3D T1 images with N4BiasFieldCorrection and extracts a brain m\
ask. Three options for brain extractor are available: deepbrain, for fast extr\
action, bet for a fast and rough extraction, Ants for a long but robust extrac\
tion, which also uses a template."


def send_cluster(subject, options):
    print(" ** Sending qsub : " + subject["Id"] + "_" + subject["Visit"])
    comm = sys.argv[0] + " -s " + subject["Id"] + "/" + subject["Visit"]
    comm = comm + " -i " + options.in_image
    comm = comm + " -b " + options.bext
    comm = comm + " -t " + options.template
    logdir = os.path.join(os.environ["PROJECT_DIR"], "BEXT_clulogs")
    createDir(logdir)
    if options.skipN4:
        comm = comm + " -n"
    if options.over:
        comm = comm + " -o"
    name = subject["Id"] + "_" + subject["Visit"] + "_BEXT"
    qsub3(comm, name=name, queue="normal", etime="02:00:00", memory="4G",
          logdir=logdir, cpus='6')


if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    p0 = parser.add_argument_group("Mandatory Options")
    p0.add_argument('-s', '--subjects', dest='subjects',
                    help='Subjects list, if none choices will be provided')
    p0.add_argument("-r", "--raw", dest="raw",
                    help="Raw directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-p", "--process", dest="process",
                    help="Process directory, if none deduced from PROJECT_DIR")
    p0.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                    default=False, help='Make the program talkative')
    p1 = parser.add_argument_group("Advanced Options")
    p1.add_argument('-i', '--input-image', dest='in_image', default='t1',
                    help='Image to exctract brain from')
    p1.add_argument('-b', '--brain-extractor', dest='bext', default='ants',
                    help="Which brain extractor to use? ('deep'/'bet'/'ants')")
    p1.add_argument("-t", "--template", dest="template", default="adni",
                    help=" Atlas to be used for brain extraction, either OASIS"
                    " or ADNI")
    p1.add_argument("-n", "--skip-n4", dest="skipN4", action='store_true',
                    help="Skip N4 (for images that don't need it only!!)")
    p1.add_argument("-o", "--overwrite", dest="over", action="store_true",
                    help="Overwrite existing Contouring preparation")
    p1.add_argument("-q", "--qsub", action="store_true", dest="qsub")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
    bext = options.bext.lower()
    if bext == 'deep':
        from tools_deepbrain import brain_mask
    if bext == 'ants':
        # Retrieving Atlas
        template = options.template.lower()
        if template == "oasis":
            template = getattr(import_module("atlases"), 'oasis_atropos')
        elif template == "adni":
            template = getattr(import_module("atlases"), 'ADNINormal')
        else:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('ERROR: Atlas NOT FOUND', 'red', attrs=['bold']),
                  template)
            sys.exit(1)
        template_anat = template.template0
        template_mask = template.probabilityMask
        print(colored('| Template used:', 'blue', attrs=['bold']),
              template_anat,
              template_mask)
    skipN4 = options.skipN4
    inimage = options.in_image.lower()
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        if options.qsub:
            send_cluster(s, options)
            continue
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# -------------------------- Retrieving T1 sequence ---------------------------
        if inimage == 't1':
            s['raw'] = os.path.join(raw, s['Id'], s['Visit'])
            s['t1'] = get_regex(s['raw'], 'T1.nii.gz', False, True, False)
            if not s['t1']:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: T1 NOT FOUND', 'red', attrs=['bold']))
                print(colored('|', 'blue', attrs=['bold']),
                      colored('L', 'green', attrs=['bold']))
                continue
            s['t1'] = s['t1'][0]
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Found T1 seq: ', 'green', attrs=['bold']), s['t1'])
        elif inimage == 'mp2rage':
            search_dir = os.path.join(process, s['Id'], s['Visit'])
            s['t1'] = get_regex(search_dir, 'mp2rage_uni_denoised.nii.gz',
                                False, True, False)
            if not s['t1']:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: MP2RAGE NOT FOUND', 'red',
                              attrs=['bold']))
                print(colored('|', 'blue', attrs=['bold']),
                      colored('L', 'green', attrs=['bold']))
                continue
            s['t1'] = s['t1'][0]
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Found MP2RAGE: ', 'green', attrs=['bold']), s['t1'])
        else:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: IMAGE NOT FOUND', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
# ------------------------ Defining path to t1preproc -------------------------
        t1preproc = os.path.join(process, s['Id'], s['Visit'],
                                 inimage + 'preproc')
        createDir(t1preproc)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory preproc created/existing: ', 'green',
                      attrs=['bold']), t1preproc)
# ------------------------------- N4 Correction -------------------------------
        if skipN4:
            N4 = s['t1']
        else:
            N4 = n4_corr(s['t1'], changeName(s['t1'], '_N4', t1preproc),
                         verbose=options.verbose)
# --------------------------- Normalizing T1 range ----------------------------
        T1 = nib.load(N4)
        T1_affine = T1.affine
        T1 = T1.get_fdata()
        T1 = 1000 * (T1 - np.min(T1))/np.ptp(T1)
        T1 = nib.Nifti1Image(T1, T1_affine)
        nib.save(T1, N4)
# --------------------------- Deep Brain Extraction ---------------------------
        Mask_path = changeName(N4, suffix='_brain_mask_ants', outdir=t1preproc)
        if os.path.isfile(Mask_path) and options.over:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Mask already made, overwriting', 'green',
                          attrs=['bold']))
            os.remove(Mask_path)
        elif os.path.isfile(Mask_path):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Mask already made, in order to avoid overwriting '
                          'manually corrected masks, rename or delete mannuall'
                          'y', 'green', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        if bext == 'deep':
            brain_mask(N4, Mask_path)
        elif bext == 'bet':
            Mask_path = changeName(Mask_path, remove='_mask')
            command(['bet2', N4, Mask_path, '-m', '-n'])
        elif bext == 'ants':
            tmpdir = os.path.join(t1preproc, 'tmp')
            createDir(tmpdir)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Directory tmp created/existing: ', 'green',
                          attrs=['bold']), tmpdir)
            antsBrainExtraction(N4, template_anat, template_mask, tmpdir,
                                verbose=options.verbose)
            if not os.path.exists(Mask_path):
                mask = get_regex(tmpdir, "BrainExtractionMask$")
                if not mask:
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('|', 'green', attrs=['bold']),
                          colored('ERROR: BMASK NOT FOUND', 'red',
                                  attrs=['bold']))
                mask = mask[0]
                shutil.copyfile(mask, Mask_path)
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
