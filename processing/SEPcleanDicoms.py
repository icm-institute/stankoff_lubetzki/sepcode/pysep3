#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 10:21:40 2020

@author: arya.yazdan-panah
"""

import os
import re
import shutil
import sys
from termcolor import cprint, colored
from common3 import compress, init_script
from pprint import pprint
from optparse import OptionParser


if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = 'usage: %prog'
    parser = OptionParser(usage=usage)
    parser.add_option('-x', '--params', dest='params',
                      help='Params file')
    parser.add_option('-s', '--subjects', dest='subjects',
                      help='Subjects list')
    parser.add_option("-d", "--dicom", dest="dicom",
                      help=" Dicom dir ID/SEQ/*.dic")
    (options, args) = parser.parse_args()
    init = init_script(options)
    data = init['data']
    params = init['params']
    dicom = init['dicom']
    subjects = init['subjects']
# --------------------------------  Cleaning  ---------------------------------
    to_delete = []
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        s['extra'] = os.path.join(dicom, s['extra'])
        if not os.path.isdir(s['extra']):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Dicom folder not found:', 'red',
                          attrs=['bold']), s['extra'])
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        else:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Looking for dicoms in:', 'green', attrs=['bold']),
                  s['extra'])
        s['Dicom_seqs'] = [[], []]
        s['Dicom_seqs'][0] = os.listdir(s['extra'])
        s['Dicom_seqs'][1] = [os.path.splitext(os.path.splitext(x)[0])[0]
                              for x in s['Dicom_seqs'][0]]
        s['Dicom_seqs'][0] = [os.path.join(s['extra'], ds) for ds in
                              s['Dicom_seqs'][0]]
        for p in params:
            r = re.compile((p['seqregex']))
            B = [s['Dicom_seqs'][0][i] for i, x in
                 enumerate(s['Dicom_seqs'][1]) if re.search(r, x)]
            if not(B):
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('Sequence dicom Folder (gzipped or not) not foun'
                              'd: ', 'red', attrs=['bold']), p['seqname'])
                continue
            elif len(B) > 1:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('Multiple files/dirs for', 'red',
                              attrs=['bold']), p['seqname'])
                continue
            b = B[0]
            if os.path.splitext(b)[1] == '.gz':
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Sequence already Gzipped', 'green',
                              attrs=['bold']), p['seqname'])
            elif os.path.isdir(b):
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Gzipping sequence:', 'green',
                              attrs=['bold']), p['seqname'])
                compress(b)
            else:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| WEIRD, PLEASE CHECK PATH:', 'green',
                              attrs=['bold']), b)
            s['Dicom_seqs'][1].pop(s['Dicom_seqs'][0].index(b))
            s['Dicom_seqs'][0].remove(b)
        to_delete.append(s['Dicom_seqs'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
    cprint(' FOLLOWING DIRS FOUND AS EXCESSIVE', 'red', attrs=['bold'])
    pprint(to_delete)
    cprint(' SHOULD I DELETE THEM ??', 'red', attrs=['bold'])
    ans = input(colored(' input yes or no: ', attrs=['bold']))
    if ans == 'yes':
        cprint(' ARE YOU SURE ??', 'red', attrs=['bold'])
        ans2 = input(colored(' input yes or no: ', attrs=['bold']))
        if ans2 == 'yes':
            cprint(' DELETING FOLLOWING FILES', 'red', attrs=['bold'])
            pprint(to_delete)
            for to in to_delete:
                for d in to[0]:
                    shutil.rmtree(d)
