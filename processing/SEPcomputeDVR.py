#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 13:42:55 2020

@author: arya.yazdan-panah
"""

import os
import sys
import shutil
from common3 import get_regex, createDir, init_script
from termcolor import cprint, colored
from optparse import OptionParser


if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = 'usage: %prog'
    parser = OptionParser(usage=usage)
    parser.add_option('-s', '--subjects', dest='subjects',
                      help='Subjects list')
    parser.add_option("-p", "--process", dest="process",
                      help=" Process directory")
    parser.add_option("-t", "--tracer", dest="tracer", default="pib",
                      help="Tracer used (dpa/pib)")
    parser.add_option("-c", "--threshold for k2(BP>thr)", default=0.1,
                      dest="thr")
    (options, args) = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
    tracer = options.tracer.lower()
    print(colored("| Tracer used:", "blue", attrs=["bold"]),
          tracer.upper())
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# --------------------- Defining path to petpreproc ------------------------
        petpreproc = os.path.join(process, s['Id'], s['Visit'], tracer +
                                  'preproc')
        if not os.path.isdir(petpreproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + petpreproc + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + 'preproc: ', 'green', attrs=['bold']),
              petpreproc)
        test = get_regex(petpreproc, '_movcorr.nii.gz', False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, Motion corrected not found', 'red',
                          attrs=['bold']))
            continue
        s['mov_corr'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Motion Corrected ' + tracer + ' :', 'green',
                      attrs=['bold']), s['mov_corr'])
# ------------------------- Defining path to petROI ---------------------------
        petROI = os.path.join(process, s['Id'], s['Visit'], tracer + 'ROI')
        if not os.path.isdir(petpreproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + tracer + 'ROI not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + 'ROI:', 'green', attrs=['bold']),
              petROI)
        test = get_regex(petROI, '_brainmask_p0.90_2' + tracer + '.nii.gz',
                         ignore_extension=False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, Brain Mask not found', 'red',
                          attrs=['bold']))
            continue
        s['brain_msk'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Brain Mask:', 'green', attrs=['bold']),
              s['brain_msk'])
# ------------------------ Defining path to petsuper --------------------------
        petsuper = os.path.join(process, s['Id'], s['Visit'], tracer + 'super')
        if not os.path.isdir(petsuper):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + tracer + 'Super not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + 'Super:', 'green', attrs=['bold']),
              petsuper)
        test = get_regex(petsuper, tracer + '_refregion_tac.mat',
                         ignore_extension=False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, Reference Region not found', 'red',
                          attrs=['bold']))
            continue
        s['ref_rgion'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Reference Region:', 'green', attrs=['bold']),
              s['ref_rgion'])
# ------------------------ Defining path to petSRTM ---------------------------
        out_dir = os.path.join(process, s['Id'], s['Visit'], tracer + 'SRTM2steptest')
        if os.path.isdir(out_dir):
            shutil.rmtree(out_dir)
        createDir(out_dir)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory ' + tracer + 'SRTM created/existing:',
                      'green', attrs=['bold']), out_dir)
# -------------------------------- processing ---------------------------------
        filenames = os.path.join(out_dir, s['Id'] + '_' + s['Visit'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| fit_SRTM2_BF.m ', 'magenta', attrs=['reverse',
                                                             'bold']))
        matlab.fit_SRTM2_BF(s['mov_corr'], s['brain_msk'], s['ref_rgion'],
                            filenames, nargout=0)
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script finished ', 'blue', attrs=['reverse', 'bold'])
    print("")
