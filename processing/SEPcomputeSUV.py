#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 11:06:46 2021

@author: arya.yazdan-panah
"""


import os
import sys
import numpy as np
import nibabel as nib
from tools_cluster import qsub3
from common3 import get_regex, init_script, createDir, changeName
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from tools_svca import readSIF, superX_referExtraction


MSG = "Extract reference region and its TAC based on the SVCA"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    p0 = parser.add_argument_group("Mandatory Options")
    p0.add_argument('-s', '--subjects', dest='subjects',
                    help='Subjects list, if none choices will be provided')
    p0.add_argument("-r", "--raw", dest="raw",
                    help="Raw directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-p", "--process", dest="process",
                    help="Process directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-t", "--tracer", dest="tracer", default="dpa",
                    help="Tracer used (dpa/pib/...)")
    p0.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                    help='Make the program talkative')
    p1 = parser.add_argument_group("Advanced Options")
    p1.add_argument("-R", "--Reference", dest="ref", default='t1',
                    help="Reference Image")
    options = parser.parse_args()
    init = init_script(options)
    raw = init['raw']
    process = init['process']
    subjects = init['subjects']
    tracer = options.tracer.lower()
    print(colored("| Tracer used:", "blue", attrs=["bold"]), tracer.upper())
# -------------------------------- Anat IMG  ----------------------------------
    if options.ref == 't1':
        ref_dir = 't1preproc'
    elif options.ref == 'mp2rage':
        ref_dir = 'mp2ragepreproc'
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('ERROR: Unknown input for Anatomical space', 'red',
                      attrs=['bold']))
        sys.exit()
# ------------------------------- processing  ---------------------------------
    for i, s in enumerate(subjects):
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Weight:', 'green', attrs=['bold']), s['extra2'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Injected Dose:', 'green', attrs=['bold']), s['extra'])
# --------------------- Defining path to petpreproc ---------------------------
        petpreproc = os.path.join(process, s['Id'], s['Visit'], tracer +
                                  '2preproc')
        out_file = os.path.join(petpreproc, s['Id'] + '_' + s['Visit'] +
                                '_suv2.nii.gz')
        if os.path.isfile(out_file):
            continue
        if not os.path.isdir(petpreproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + petpreproc + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + 'preproc: ', 'green', attrs=['bold']),
              petpreproc)
        test = get_regex(petpreproc, '_movcorr.nii.gz', False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, Motion corrected not found', 'red',
                          attrs=['bold']))
            continue
        s['mov_corr'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Motion Corrected ' + tracer + ' :', 'green',
                      attrs=['bold']), s['mov_corr'])
        mov_corr = nib.load(s['mov_corr'])
        mov_corr_affine = mov_corr.affine
        mov_corr = mov_corr.get_fdata()
        timepoints = mov_corr.shape[-1]
# --------------------------- Retrieving Brain Mask ---------------------------
        test = get_regex(petpreproc, '_brain_mask_2', False)

        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: brain mask not found in ' + petpreproc,
                          'red', attrs=['bold']))
            have_mask = False
        else:
            s['b_mask'] = test[0]
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Found Brain Mask: ', 'green', attrs=['bold']),
                  s['b_mask'])
            have_mask = True
        if have_mask:
            b_mask = nib.load(s['b_mask'])
            b_mask_affine = b_mask.affine
            b_mask = b_mask.get_fdata()
        for f in range(timepoints):
            if have_mask:
                mov_corr[..., f] = np.multiply(mov_corr[..., f], b_mask)
            mov_corr[..., f] = mov_corr[..., f] * float(s['extra2']) / (float(s['extra']) * 1000)
        SUV_img = nib.Nifti1Image(mov_corr, mov_corr_affine)
        nib.save(SUV_img, out_file)