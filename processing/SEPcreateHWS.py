#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 27 16:24:19 2020

@author: mariem.hamzaoui
"""

from nipype.interfaces.freesurfer import RobustTemplate, MRIConvert
from nipype.interfaces.freesurfer.utils import LTAConvert
from nipype.interfaces.ants import ApplyTransforms
from nipype.interfaces import fsl
from termcolor import cprint, colored
from optparse import OptionParser,OptionGroup
from common3 import *
import time
import logging
import os
import argparse
#------------------------------------------------------------------------------
#########################To launch the code on cluster#########################
#------------------------------------------------------------------------------
def sendCluster(options, sid, projectdir):
    print (" ** Sending qsub : " + sid["Id"])
    comm=sys.argv[0] + " -s " + sid["Id"] + " -p " + projectdir
    if options.visits:
        comm += " -V " + options.visits 
    qsub(comm,name="HWSCreation_"+sid["Id"],queue="normal,bigmem",etime="2:00:00",memory="8G",cpus="2")
#------------------------------------------------------------------------------
###################Histogram matching instead of MRI_NORMALIZE#################
#------------------------------------------------------------------------------
def HistoMatch (im, out, ref):
    im_HM = changeName(im,outdir=out, suffix="_HM")
    if not(ref):
        command0 = ["ImageMath", "3", im_HM, "HistogramMatch", im, im ]
        command(command0)
    else:
        command0 = ["ImageMath", "3", im_HM, "HistogramMatch", im, ref ]
        command(command0)
    return (im_HM)
#------------------------------------------------------------------------------
#########################Creation of the halfway space#########################    
#------------------------------------------------------------------------------
def CreateHWS (T1_norm, hwsdir, hws_name):
    out_lta = [""]* len(T1_norm)
    out_itk = [""]* len(T1_norm)
    out_hws = hwsdir + hws_name
    for T1 in T1_norm:
        out_lta[T1_norm.index(T1)] =  changeName(T1,outdir=hwsdir, newext="")[0:-13] + "_to_" + changeName((hws_name[hws_name.find('hws'):]),outdir='', newext=".lta")
        out_itk[T1_norm.index(T1)] =  changeName(T1,outdir=hwsdir, newext="")[0:-13] + "_to_" + changeName((hws_name[hws_name.find('hws'):]),outdir='', newext=".txt")
    template = RobustTemplate()
    template.inputs.in_files = T1_norm
    template.inputs.auto_detect_sensitivity = True
    template.inputs.out_file = out_hws
    template.inputs.average_metric = 'median'
    template.inputs.initial_timepoint = 2
    template.inputs.intensity_scaling = True
    template.inputs.transform_outputs = out_lta
    template.terminal_output = "stream"
    template.run()
    for T1 in T1_norm:        
        ltaConvert = LTAConvert(); ltaConvert.inputs.in_lta = out_lta[T1_norm.index(T1)]; ltaConvert.inputs.out_itk = out_itk[T1_norm.index(T1)]
        ltaConvert.run()
        os.remove(out_lta[T1_norm.index(T1)])
    return out_hws, out_itk
#------------------------------------------------------------------------------

if __name__=="__main__":        
#------------------------------------------------------------------------------
############################Arguments & Description############################
#------------------------------------------------------------------------------    
    usage= "usage: %prog [opts]"
    
    parser = argparse.ArgumentParser(description='Creates a halfway space using multiple timepoints with freesurfers MRI_ROBUST_TEMPLATE and a histogram matching of the brains (instead of MRI_NORMALIZE).')

    group_opt = parser.add_argument_group("Mandatory Options")
    group_opt.add_argument("-p","--projectdir", dest="projectdir",
                      help=" Directory where the project is stored (otherwise PROJECT_DIR defined in os.environment is used)")
    group_opt.add_argument("-s","--sid"       , dest="sids",
                      help=" File with a list of subjects or subject ids: P01,P02/V01")   
    group_adv = parser.add_argument_group("Advanced Options")
    group_adv.add_argument("-q","--qsub"      ,action="store_true", dest="qsub",
                      help=" Send each patient to the sge list for parellelized computing (no need for this script it takes ~4mins for 3 timepoints)")
    group_adv.add_argument("-V", "--Visits", dest= "visits",
                     help= "this is for you to choose the visits on which to calculate the halfway space")
    group_adv.add_argument("-v","--verbose"   ,action="store_true", dest="verbose",
                           help="Read the commands output in the terminal")

    options = parser.parse_args()
#------------------------------------------------------------------------------
#################Reading mandatory args (SID & PROJECT_DIR)####################
#------------------------------------------------------------------------------   
    projectdir = os.environ["PROJECT_DIR"]
    if options.projectdir :
        projectdir = os.path.abspath(options.projectdir) + os.sep
    sids = readSubjectsLists(options.sids, options.verbose)    
    for sid in sids:
        start = time.time()
        logging.basicConfig(filename=projectdir + '/process/errors_jacobian_list.log', level=logging.DEBUG, 
                            format='%(asctime)s %(levelname)s %(name)s %(message)s')
        logger=logging.getLogger(__name__)
        if options.verbose:
            print (" ")
            cprint ("="*92,'green',attrs=['bold'])
            cprint ("-"*33 +"Starting patient : "+str(sid["Id"])+"-"*33,'green',attrs=['bold'])
            cprint ("="*92,'green',attrs=['bold'])
            print (" ")
        if options.qsub:
            sendCluster(options,sid,projectdir)
        else: 
#------------------------------------------------------------------------------
####################Get directories and T1 for hws creation####################
#------------------------------------------------------------------------------
            T1 = []
            visits = []
            if options.visits:
#------------------------------------------------------------------------------
#####################if the user provids visits he wants#######################
#------------------------------------------------------------------------------
                prodir =[]
                projdir =[]                
                visits = options.visits.strip('[]').split(',')  
                for visit in visits:
                    try:
                        prodir.append(get_subdir_regex(projectdir,["process",sid["Id"],'^'+visit+'$']))
                        if len(get_subdir_regex(projectdir,["process",sid["Id"],'^'+visit+'$']))==0:
                            logger.error("No "+visit+" found")
                            continue
                        else:
                            T1.append(get_subdir_regex_files(prodir[-1],["t1preproc", "_N4.nii.gz"]))
                            projdir.append(get_subdir_regex(projectdir,["process",sid["Id"],'^'+visit+'$'])[0])
                    except OSError as err:         
                        logger.error (err)        
            else:
                prodir = get_subdir_regex(projectdir,["process", sid["Id"], "V"])
                projdir =[]
                for visit_path in prodir:
                    visit = visit_path.replace(projectdir + "process/" + sid["Id"] + '/' , '')
                    if (len(visit)<4):
                        visits.append(visit_path.replace(projectdir + "process/" + sid["Id"] + '/' , ''))
                        T1.append(get_subdir_regex_files(visit_path, ["t1preproc", "_N4.nii.gz"]))
                        projdir.append(visit_path)
            number_of_visits = len(visits)
            T1 = [t1 for t1 in T1 if t1]
            if (number_of_visits < 2 ) :
                logger.error("can't perform script for" +sid["Id"]+" with one visit there must be at least 2")
                continue
            if (len(T1)<number_of_visits) :
                logger.error ("missing image file for visit for " + sid["Id"]+ "   " + str(T1))
                continue
                #-----------Creat the Halfway space directory--------------------
            if options.verbose:
                cprint ('|Create the halfway space & the transformation files','cyan',attrs = ['bold'])        

            ch = "".join(visits)
            hwsdir = projectdir + "/process/" + sid["Id"] + "/" + sid["Id"] + "_hwf_fs_" + ch + "/"
            T1dir = hwsdir +"T1/"
            HWSmask = hwsdir +"HWSmask/"
            try:           
                os.mkdir(hwsdir)
                os.mkdir(T1dir)
                os.mkdir(HWSmask)
            except OSError as err:         
                logger.error (err)        
            #----------Halwaysapce generation (and relate transform)-----------        
            hws_name = sid["Id"] + "_hws_" + ch + ".nii.gz"
            T1_hws = get_subdir_regex_files (hwsdir,"^" +hws_name)
            mask_dil = [""] * number_of_visits 
            T1_norm = [""] * number_of_visits
            ##------Check if there's already a halfway space created
            if T1_hws :
                try :
                    mat2hws = get_subdir_regex_files(hwsdir, (changeName(hws_name[hws_name.find('hws'):],newext=".txt")))
                except :
                    logger.error("missing transformation files for "+sid["Id"])
                    continue
            if not T1_hws :
                
                try:
            #First we run the histogram matching on the brain and then we create the halfway space
                    if get_subdir_regex_files(projdir[0],["t1preproc","_mask_dil.nii.gz"]):    
                        mask_dil[0] = get_subdir_regex_files(projdir[0],["t1preproc","_mask_dil.nii.gz"])[0]
                    else:
                        mask = get_subdir_regex_files(projdir[0],["t1preproc","_mask.nii.gz"])[0]
                        mask_dil[0] = changeName(mask,suffix="_dil")
                        dilate = fsl.ImageMaths(in_file=mask, op_string ="-dilM", out_file=mask_dil[0])
                        dilate.run()                
                    
                    brains =   [changeName(T1[i][0],suffix="_brain_dil") for i in range(number_of_visits)]
                    masking = fsl.ImageMaths(in_file=T1[0][0], op_string ="-mas", in_file2=mask_dil[0], out_file = brains[0])
                    masking.run()                
                    T1_norm[0] = HistoMatch (brains[0], T1dir , ref = "")
                    for i in range(1,number_of_visits):
                        if get_subdir_regex_files(projdir[i],["t1preproc","_mask_dil.nii.gz"]):    
                            mask_dil[i] = get_subdir_regex_files(projdir[i],["t1preproc","_mask_dil.nii.gz"])[0]
                        else:
                            mask = get_subdir_regex_files(projdir[i],["t1preproc","_mask.nii.gz"])[0]
                            mask_dil[i] = changeName(mask,suffix="_dil")
                            dilate = fsl.ImageMaths(in_file=mask, op_string ="-dilM", out_file=mask_dil[i])
                            dilate.run() 
                        masking = fsl.ImageMaths(in_file=T1[i][0], op_string ="-mas", in_file2=mask_dil[i], out_file = brains[i])
                        masking.run()
                        T1_norm[i] = HistoMatch (brains[i], T1dir, ref = brains[0])
                    T1_hws, mat2hws= CreateHWS(T1_norm, hwsdir, hws_name)                
                except :
                    logger.error(sys.exc_info()[0])
                    continue
            #-----------perform image in T1 registration to halfway space--------------------
            if options.verbose:
                cprint ('|Perform image in T1 registration to halfway space','cyan',attrs = ['bold'])        
            t1_hws =[""] * number_of_visits
            masks_hws = [""]*number_of_visits 
            mask_tot_dil = HWSmask + "/" + sid["Id"]+"_hws_"+ch+"_mask_tot.nii.gz"               
            for i in range(number_of_visits):
                t1_hws[i] = T1dir +sid["Id"]+"_"+ visits[i]+"_t1_to_hws_"+ch+".nii.gz"
                masks_hws[i] = HWSmask +sid["Id"]+"_"+ visits[i]+"_to_hws_"+ch+"_MASK.nii.gz"
                try:
                    at = ApplyTransforms()
                    at.inputs.dimension = 3
                    at.inputs.input_image = T1_norm[i]; at.inputs.reference_image = T1_hws; at.inputs.transforms = mat2hws[i]
                    at.inputs.output_image = t1_hws[i]; at.inputs.float = True
                    at.run()
                    at.inputs.input_image = mask_dil[i];at.inputs.reference_image = T1_hws; at.inputs.transforms = mat2hws[i]
                    at.inputs.output_image = masks_hws[i]; at.inputs.float = True; at.inputs.interpolation = "NearestNeighbor"
                    at.run()                    
                    os.remove(T1_norm[i])
                    os.remove(brains[i])
                    if i>1:
                        add = fsl.ImageMaths(in_file= masks_hws[i],op_string = "-add", in_file2=mask_tot_dil, out_file =mask_tot_dil)
                        add.run()
                        binarize = fsl.ImageMaths(in_file=mask_tot_dil,op_string = "-bin", out_file =mask_tot_dil)
                        binarize.run()                        
                    else:
                        if i:
                            add = fsl.ImageMaths(in_file= masks_hws[i],op_string = "-add", in_file2=masks_hws[i-1], out_file =mask_tot_dil)
                            add.run()
                except:
                    logger.error(sys.exc_info()[0])
        if options.verbose:                
            cprint ('-'*23,'red',attrs=['bold'])
            cprint (" -Exec time: "+ str(np.round(time.time()-start,4))+"-",'red',attrs=['bold'])
            cprint ('-'*23,'red',attrs=['bold'])            