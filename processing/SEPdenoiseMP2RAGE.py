#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 15:27:16 2020

@author: arya.yazdan-panah
"""

import os
import sys
import numpy as np
import nibabel as nib
from copy import deepcopy
from termcolor import cprint, colored
from common3 import init_script, createDir, get_regex, seq_info, changeName
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
np.seterr(all='ignore')


def mp2rage_robust_combination(filename_uni, filename_inv1, filename_inv2,
                               filename_output=None, multiplying_factor=1):
    """ This is a python version of the 'RobustCombination.mat' function
    described by O'Brien et al. and originally implemented by Jose Marques.

    Parameters
    ----------
    filename_uni : string
        path to the uniform T1-image (UNI)
    filename_inv1 : string
        path to the first inversion image (INV1)
    filename_inv2 : string
        path to the second inversion image (INV2)
    filename_output : string  (optional)
        path to output image
    multiplying_factor : int  (optional)
        if it's too noisy, give it a bigger value
    """
    # define relevant functions
    def mp2rage_robustfunc(inv1, inv2, beta):
        return (inv1.conj() * inv2 - beta) / (np.square(inv1) +
                                              np.square(inv2) + 2 * beta)

    def rootsquares_pos(a, b, c):
        return (-b + np.sqrt(np.square(b) - 4 * a * c)) / (2 * a)

    def rootsquares_neg(a, b, c):
        return (-b - np.sqrt(np.square(b) - 4 * a * c)) / (2 * a)

    # load data
    image_uni = nib.load(filename_uni)
    image_inv1 = nib.load(filename_inv1)
    image_inv2 = nib.load(filename_inv2)

    image_uni_fdata = image_uni.get_fdata()
    image_inv1_fdata = image_inv1.get_fdata()
    image_inv2_fdata = image_inv2.get_fdata()

    # scale UNI image values
    if (np.amin(image_uni_fdata) >= 0) and (np.amax(image_uni_fdata >= 0.51)):
        def scale(x):
            ret = (x - np.amax(image_uni_fdata) / 2) / np.amax(image_uni_fdata)
            return ret
        image_uni_fdata = scale(image_uni_fdata)

    # correct polarity for INV1
    image_inv1_fdata = np.sign(image_uni_fdata) * image_inv1_fdata

    # MP2RAGEimg is a phase sensitive coil combination.. some more maths has to
    # be performed to get a better INV1 estimate which here is done by assuming
    # both INV2 is closer to a real phase sensitive combination
    inv1_pos = rootsquares_pos(-image_uni_fdata, image_inv2_fdata,
                               -np.square(image_inv2_fdata) * image_uni_fdata)
    inv1_neg = rootsquares_neg(-image_uni_fdata, image_inv2_fdata,
                               -np.square(image_inv2_fdata) * image_uni_fdata)

    image_inv1_final_fdata = deepcopy(image_inv1_fdata)
    image_inv1_final_fdata[
        np.abs(image_inv1_fdata - inv1_pos) >
        np.abs(image_inv1_fdata - inv1_neg)] = inv1_neg[np.abs(image_inv1_fdata
                                                               - inv1_pos) >
                                                        np.abs(image_inv1_fdata
                                                               - inv1_neg)]
    image_inv1_final_fdata[
        np.abs(image_inv1_fdata - inv1_pos) <=
        np.abs(image_inv1_fdata - inv1_neg)] = inv1_pos[np.abs(image_inv1_fdata
                                                               - inv1_pos) <=
                                                        np.abs(image_inv1_fdata
                                                               - inv1_neg)]
    noiselevel = multiplying_factor * np.mean(np.mean(np.mean(
        image_inv2_fdata[1:, -10:, -10:])))

    image_output = nib.Nifti1Image(mp2rage_robustfunc(image_inv1_final_fdata,
                                                      image_inv2_fdata,
                                                      np.square(noiselevel)),
                                   image_inv1.affine, nib.Nifti1Header())

    if filename_output:
        nib.save(image_output, filename_output)
    else:
        return image_output


MSG = "Denoise MP2RAGE images by combination of UNI, INV1 and INV2 images"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    p0 = parser.add_argument_group("Mandatory Options")
    p0.add_argument('-s', '--subjects', dest='subjects',
                    help='Subjects list, if none choices will be provided')
    p0.add_argument("-r", "--raw", dest="raw",
                    help="Raw directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-p", "--process", dest="process",
                    help="Process directory, if none deduced from PROJECT_DIR")
    p1 = parser.add_argument_group("Advanced Options")
    p1.add_argument("-m", "--multiplying_factor", dest="multiplying_factor",
                    help="If too noisy, higher this value", default=100,
                    type=int)
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
    createDir(process)
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        visit = os.path.join(raw, s['Id'], s['Visit'])
# ----------------------- Retrieving MP2RAGE sequences ------------------------
        s["inv1"] = get_regex(visit, "mp2rage_inv1.nii.gz", 0, 1, 1)
        if not s['inv1']:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1_inv1 NOT FOUND', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        s["inv1"] = s["inv1"][0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('Found T1_inv1 seq:', 'green', attrs=['bold']),
              s['inv1'])

        s["inv2"] = get_regex(visit, "mp2rage_inv2.nii.gz", 0, 1, 1)
        if not s['inv2']:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1_inv2 NOT FOUND', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        s["inv2"] = s["inv2"][0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('Found T1_inv2 seq:', 'green', attrs=['bold']),
              s['inv2'])

        s["uni"] = get_regex(visit, "mp2rage_uni.nii.gz", 0, 1, 1)
        if not s['uni']:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1_uni NOT FOUND', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        s["uni"] = s["uni"][0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('Found T1_uni seq:', 'green', attrs=['bold']),
              s['uni'])
# --------------------- Defining path to mp2ragepreproc -----------------------
        mp2ragepreproc = os.path.join(process, s['Id'], s['Visit'],
                                      'mp2ragepreproc')
        createDir(mp2ragepreproc)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory mp2ragepreproc created/existing: ', 'green',
                      attrs=['bold']), mp2ragepreproc)
# --------------------------- Denoising MP2RAGE uni ---------------------------
        name, seqname, direct = seq_info(s['uni'])
        denoise = changeName(s['uni'], suffix='_denoised',
                             outdir=mp2ragepreproc)
        if os.path.isfile(denoise):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Denoising already done, skipping...', 'green',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        mult = options.multiplying_factor
        mp2rage_robust_combination(s["uni"], s["inv1"], s["inv2"], denoise,
                                   multiplying_factor=mult)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| T1 UNI succesfully denoised:', 'green',
                      attrs=['bold']), denoise)
# ------------------------- Normalizing denoised  -----------------------------
        denoised = nib.load(denoise)
        denoised_affine = denoised.affine
        denoised = denoised.get_fdata()
        denoised = (denoised + np.full(denoised.shape,
                                       abs(np.amin(denoised))))*1000
        denoised = nib.Nifti1Image(denoised, denoised_affine)
        nib.save(denoised, denoise)
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
