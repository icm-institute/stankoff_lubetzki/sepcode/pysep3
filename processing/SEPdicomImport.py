#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 24 14:32:49 2020

@author: arya.yazdan-panah
"""

import os
import sys
from termcolor import colored, cprint
from common3 import createDir, init_script, get_regex, compress, decompress
from tools_dicoms import dicom2nifti
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

MSG = "Correctly import dicoms in the data/raw architecture"


def importSequence(Subject, Sequence, overwrite=False, comp=False, bids=True,
                   verbose=False, option='dcm2niix'):
    """
    Imports 1 sequence for 1 subject

    Parameters
    ----------
    Subject : dict
        subject format obtained by using pysep3 readSubjectsList function.
    Sequence : dict
        sequence format obtained by using pysep3 readprojectparam function.
    overwrite : bool, optional
        overwrite existing sequence? The default is False.
    comp : bool, optional
        compress dicoms? The default is False.

    Returns
    -------
    path or List[path]
        to nifti images
    """
# --------------------- Looking for existing nifti Files ----------------------
    ni_dir = os.path.join(Subject['raw'], Sequence['seqname'])
    niname = '_'.join([Subject['Id'], Subject['Visit'], Sequence['seqname']])
    try:
        niname = niname + "_" + Sequence["suffix"]
    except KeyError:
        pass
    # try:
    #     is_diff = Sequence["diff"]
    # except KeyError:
    #     is_diff = False
    nifti_search = get_regex(ni_dir, niname + "$")
    if nifti_search:
        if overwrite:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Sequence Already imported, deleting previous nift'
                          'i files to replace them', 'cyan',
                          attrs=['bold']))
            for nifti in nifti_search:
                os.remove(nifti)
            i = len(nifti_search)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| ' + str(i) + ' files deleted', 'cyan',
                          attrs=['bold']))
        else:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Sequence Already imported, skipping... ', 'cyan',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']))
            return 1
# ------------------------- Retrieving Dicom Folder ---------------------------
    One_seq = get_regex(Subject['extra'], Sequence["seqregex"], False,
                        deep_search=False)
    if not(One_seq):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored('ERROR: Dicom folder/compressed folder not found ',
                      'red', attrs=['bold']), "'" + Sequence['seqregex'] + "'"
              + ' in ' + Subject['extra'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return False
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Found dicom folder(s):', 'cyan', attrs=['bold']),
              One_seq)
# -------------------------- Retrieving Dicom Files ---------------------------
    outs = []
    N = len(One_seq)
    for i, seq in enumerate(One_seq):
        seq_dir = ni_dir
        # Removing Special Characters
        if ('(' or ')' or ' ') in seq:
            tmp = seq.replace('(', '')
            tmp = tmp.replace(' ', '')
            newpath = tmp.replace(')', '')
            os.rename(seq, newpath)
            seq = newpath
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Special characters found in the folder name, remo'
                          'ved them', 'cyan', attrs=['bold']))
        if os.path.isfile(seq):
            seq = decompress(seq)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Decompressed dicom folder:', 'cyan',
                          attrs=['bold']), seq)
        dcms = get_regex(seq, '.')
        if not(dcms):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('|', 'cyan', attrs=['bold']),
                  colored('ERROR: NO .dic/.dcm in folder:', 'red',
                          attrs=['bold']), seq)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']))
            return False
        else:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Found .dcm/.dic files:', 'cyan', attrs=['bold']),
                  len(dcms))
# --------------------- Importing Dicom files in Nifti  -----------------------

        if N > 1:
            seq_dir = ni_dir + '_' + str(i)
        out = dicom2nifti(seq, seq_dir, niname, option, bids, verbose)
        outs.append(out)
        if comp:
            seq = compress(seq)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Compressed dicom folder:', 'cyan',
                          attrs=['bold']), seq)
    if not(outs):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored('ERROR: dcm2niix ', 'red', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return False
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return outs


if __name__ == '__main__':
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    p0 = parser.add_argument_group("Mandatory Options")
    p0.add_argument('-s', '--subjects', dest='subjects',
                    help='Subjects list, if none choices will be provided')
    p0.add_argument('-x', '--params', dest='params',
                    help='Params file, if none choices will be provided')
    p0.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                    default=False, help='Make the program talkative')
    p1 = parser.add_argument_group("Advanced Options")
    p1.add_argument("-r", "--raw", dest="raw",
                    help="Raw directory, if none deduced from PROJECT_DIR")
    p1.add_argument("-d", "--dicom", dest="dicom",
                    help="Dicom dir ID/SEQ/*.dic, if none deduced from "
                    "parameters file, option -x")
    p1.add_argument("-c", "--compress", dest="comp", action='store_true',
                    help="Compress nifti folders or not? Set to False when "
                    "importing from the cenir!!", default=False)
    p1.add_argument("-b", "--no-bids", dest="bids", action='store_false',
                    help="DON'T Import dicoms in BIDS sidecar")
    p1.add_argument('-o', "--overwrite", action='store_true', dest='overwrite',
                    default=False, help='Overwrites existing directories and '
                    'files')
    p1.add_argument('-C', "--converter", dest='converter', default='dcm2niix',
                    help='Converter to use')
    options = parser.parse_args()
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
    init = init_script(options)
    params = init['params']
    dicom = init['dicom']
    raw = init['raw']
    subjects = init['subjects']
# -------------------------------- Importing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        s['extra'] = os.path.join(dicom, s['extra'])
        if not os.path.isdir(s['extra']):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Dicom folder not found:', 'red',
                          attrs=['bold']), s['extra'])
            continue
        else:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Looking for dicoms in:', 'green', attrs=['bold']),
                  s['extra'])
        s['raw'] = os.path.join(raw, s['Id'], s['Visit'])
        createDir(s['raw'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory created/existing', 'green', attrs=['bold']))
        for p in params:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Importing sequence ' + p['seqname'], 'cyan',
                          attrs=['bold', 'reverse']))
            seq_dir = os.path.join(s['raw'], p['seqname'])
            importSequence(s, p, options.overwrite, options.comp, options.bids,
                           options.verbose, options.converter.lower())
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
