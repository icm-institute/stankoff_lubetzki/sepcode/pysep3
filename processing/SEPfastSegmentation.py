#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 11:13:32 2020

@author: arya.yazdan-panah
"""

import os
import sys
from common3 import init_script, get_regex, changeName
from tools_fsl import fslmaths, fast
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

MSG = "Perform a rapid 3 region fast segmentation from FSL"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# ------------------------ Defining path to t1preproc -------------------------
        t1preproc = os.path.join(process, s['Id'], s['Visit'], 't1preproc')
        if not os.path.isdir(t1preproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + t1preproc + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| t1preproc: ', 'green', attrs=['bold']),
              t1preproc)
# ------------------------- Retrieving T1_N4 sequence -------------------------
        N4 = get_regex(t1preproc, '_t1_N4$')
        if not N4:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1 NOT FOUND', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        N4 = N4[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found T1 seq: ', 'green', attrs=['bold']), N4)
# --------------------------- Retrieving Brain Mask ---------------------------
        b_mask = get_regex(t1preproc, '(_brain_mask|_mask).nii.gz', False)
        if not b_mask:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: brain mask not found in ' + t1preproc, 'red',
                          attrs=['bold']))
            continue
        b_mask = b_mask[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Brain Mask: ', 'green', attrs=['bold']),
              b_mask)
# ---------------------------- Applying Brain Mask ----------------------------
        brain_path = changeName(N4, suffix='_brain')
        if not os.path.isfile(brain_path):
            brain_path = fslmaths(N4, 'x', b_mask, out=brain_path)
# --------------------------- Segmenting the Brain ----------------------------
        fast(brain_path, verbose=False)
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
