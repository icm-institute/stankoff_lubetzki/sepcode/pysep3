#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 12:09:44 2020

@author: arya.yazdan-panah
"""

import os
import sys
from tools_freesurfer import freesurfer7, freesurfer
from tools_cluster import qsub3
from common3 import createDir, init_script, get_regex
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter


def sendClusterFS(subject, options):
    print(" ** Sending qsub : " + subject["Id"] + "_" + subject["Visit"])
    comm = sys.argv[0] + " -s " + subject["Id"] + "/" + subject["Visit"]
    comm = comm + " -d " + options.freesurfer
    comm = comm + " -y " + options.study
    comm = comm + " -R " + options.ref
    comm = comm + " -F " + str(options.fv)
    if options.study == 'base':
        logdir = os.path.join(options.freesurfer, "base.freelogs")
        createDir(logdir)
        name = "baseFS_" + subject["Id"]
        etime = "24:00:00"
    elif options.study == 'long':
        logdir = os.path.join(options.freesurfer, "long.freelogs")
        createDir(logdir)
        name = "longFS_" + subject["Id"] + "_" + subject["Visit"]
        etime = "24:00:00"
    else:
        logdir = os.path.join(options.freesurfer, "freelogs")
        createDir(logdir)
        name = "FS_" + subject["Id"] + "_" + subject["Visit"]
        etime = "24:00:00"
    comm = comm + " -t " + options.steps
    qsub3(comm, name=name, queue="normal", etime=etime, memory="8G",
          logdir=logdir, cpus='6')


MSG = "recon-all"

if __name__ == "__main__":
    print(os.path.basename(sys.argv[0]), ':')
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-d", "--freesurfer", dest="freesurfer",
                        help="Directory where freesurfer stores images")
    parser.add_argument("-y", "--study", dest="study", default='standard',
                        help="'standard', 'long' or 'base' study")
    parser.add_argument("-t", "--steps", dest="steps", default="all",
                        help="Option to choose recon-all steps to perform:"
                             "all (1-23) do all the step (default)"
                             "autorecon2-wm (15-23) wm edits "
                             "pial (21-23) brain edits for pial "
                             " surface", type=str)
    parser.add_argument("-R", "--Reference", dest="ref", default='t1',
                        help="Reference Image")
    parser.add_argument("-F", "--freesurfer-version", dest="fv", default=7,
                        help="Version of freesurfer used", type=int)
    parser.add_argument("-H", "--high-res", dest="high", action="store_true")
    parser.add_argument("-q", "--qsub", action="store_true", dest="qsub")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
    freesurfer_dir = init['freesurfer']
    if not options.freesurfer:
        options.freesurfer = freesurfer_dir
    createDir(freesurfer_dir)
    stud = options.study
    if stud not in ['long', 'base', 'standard']:
        print('| Can only perform standard, long or base study..., exiting')
        sys.exit(1)
    steps = options.steps
    if steps not in ['all', '-autorecon2-wm', 'pial', 'add_brain']:
        print("| Can only perform following steps ['all', 'autorecon2-wm', "
              "'autorecon-pial']")
        sys.exit(2)
    # sys.exit(1)
    if options.ref == 't1':
        ref_dir = 't1preproc'
        regex_ref = '_t1_N4$'
    elif options.ref == 'mp2rage':
        ref_dir = 'mp2ragepreproc'
        regex_ref = '_mp2rage_uni_denoised$'
    if options.qsub:
        options.process = process
        options.freesurfer = freesurfer_dir
        for s in subjects:
            sendClusterFS(s, options)
        sys.exit()
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        print('|')
# ----------------------------- Start of 1 subject ----------------------------
        print('| ' + s['Id'] + ' ' + s['Visit'] + ': ')
# -------------------------- Defining path to leinp ---------------------------
        leinp = os.path.join(process, s['Id'], s['Visit'], 'leinp')
        look_for_N4 = False
        if not os.path.isdir(leinp):
            print('| | ERROR: ' + leinp + ' not found')
            look_for_N4 = True
        else:
            print('| | leinp: ', leinp)
            inpainted = get_regex(leinp, "inpainted.nii.gz",
                                  ignore_extension=False)
            if not inpainted:
                look_for_N4 = True
            else:
                s["anat"] = inpainted[0]
                print('| | inpainted: ', s["anat"])
# ------------------------ Defining path to t1preproc -------------------------
        if look_for_N4:
            t1preproc = os.path.join(process, s['Id'], s['Visit'], ref_dir)
            if not os.path.isdir(t1preproc):
                print('| |', 'ERROR: ' + t1preproc + ' not found')
                print('| L')
                continue
            print('|', '| t1preproc: ', t1preproc)
            t1_N4 = get_regex(t1preproc, s['Visit'] + regex_ref)
            if not t1_N4:
                print('| |', 'ERROR: No anatomical sequence found')
                print('| L')
                continue
            else:
                s["anat"] = t1_N4[0]
# -------------------------------- Recon-all ----------------------------------
        subject_dir = os.path.join(freesurfer_dir, s['Id'] + '_' + s['Visit'])
        if options.fv == 7:
            logfile = os.path.join(subject_dir,
                                   s['Id'] + '_' + s['Visit'] + '.log')
            freesurfer7(s["anat"], options.freesurfer,
                        s['Id'] + '_' + s['Visit'], steps=steps, args=stud,
                        openmp=6, logfile=logfile, highres=options.high)
        elif options.fv == 6:
            freesurfer(s["anat"], options.freesurfer,
                       s['Id'] + '_' + s['Visit'], steps=steps, args=stud,
                       openmp=6)
        else:
            print('Unknown FreeSurfer Version')
            sys.exit(1)
        print('| Patient Finished')
        print('| L')
    print('|')
    print('| Script Finished ')
