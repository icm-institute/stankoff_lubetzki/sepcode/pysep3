#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 14:15:17 2020

@author: arya.yazdan-panah
"""


import os
import sys
from importlib import import_module
from termcolor import cprint, colored
from common3 import get_regex, createDir, init_script, changeName
from tools_ants import AntsRegistrationSyn, ApplyRegistration
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from tools_cluster import qsub3


def sendCluster(subject, options):
    print(" ** Sending qsub : " + subject["Id"] + "_" + subject["Visit"])
    comm = sys.argv[0] + " -s " + subject["Id"] + "/" + subject["Visit"]
    comm = comm + " -i " + options.image + " -t " + options.template
    comm = comm + " -w " + options.hws
    name = "HWS2MNI_registration_" + subject["Id"] + "_" + subject["Visit"]
    etime = "02:30:00"
    qsub3(comm, name=name, queue="normal", etime=etime, memory="8G", cpus='6')


MSG = "Register IMAGE in the HWS to MNI"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-r", "--raw", dest="raw",
                        help="Raw directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-w", "--hws", dest="hws", default="ALL",
                        help="HWS which will be registered on the MNI")
    parser.add_argument("-i", "--image", dest="image", default='hws',
                        help="Image to Register to MNI (default t1)")
    parser.add_argument("-t", "--template", dest="template",
                        default="mni152_sym09c",
                        help="Template onto which register the previous image")
    parser.add_argument("-q", "--qsub", action="store_true", dest="qsub")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
# ----------------------------- Retrieving Atlas ------------------------------
    template = options.template
    if template not in ("mni152", "mni152_asym09a", "mni152_asym09b",
                        "mni152_sym09a", "mni152_sym09c"):
        cprint("ERROR: Template not implemented or non-existant " + template,
               'red', attrs=['bold'])
        sys.exit(1)
    template = getattr(import_module("atlases"), template)
    template = template.brain
    print(colored('| Template used:', 'blue', attrs=['bold']), template)
# ----------------------------- Finding Images --------------------------------
    img = options.image.lower()
    stop = False
    in_hws = False
    if img == 'hws':
        stop = True
    elif img == 'flair':
        img_fold = 'flair2'
        img_regex = '_flair_2hws'
        meth = 'Linear'
        folder = 'flair'
    # elif img == 'flair_label':
    #     img_fold = 'manual_lesion'
    #     img_regex = '_flair_label_2hws'
    #     meth = 'NearestNeighbor'
    # elif img == 'flair_gado_label':
    #     img_fold = 'manual_lesion'
    #     img_regex = '_flair_gado_label_2hws'
    #     meth = 'NearestNeighbor'
    # elif img == 'flair_ngado_label':
    #     img_fold = 'manual_lesion'
    #     img_regex = '_flair_ngado_label_2hws'
    #     meth = 'NearestNeighbor'
# =============================================================================
    elif img == 'flair_label':
        img_fold = 'manual_lesion2'
        img_regex = '_flair_label_2hws_V01V04.nii.gz'
        meth = 'NearestNeighbor'
        folder = 'masks'
    elif img == 'flair_gado_label':
        img_fold = 'manual_lesion2'
        img_regex = '_flair_gado_label_2hws_V01V04.nii.gz'
        meth = 'NearestNeighbor'
        folder = 'masks'
    elif img == 'flair_ngado_label':
        img_fold = 'manual_lesion2'
        img_regex = '_flair_ngado_label_2hws_V01V04.nii.gz'
        meth = 'NearestNeighbor'
        folder = 'masks'
# =============================================================================
    elif img == 'nawm':
        img_fold = 'T1/ROI'
        img_regex = '_NAWM_2hws'
        meth = 'NearestNeighbor'
        folder = 'masks'
    elif img == 'mtr':
        img_fold = 'mtr'
        img_regex = '_mtr_2hws'
        meth = 'Linear'
        folder = 'mtr'
    elif img == 'isovf':
        img_fold = 'noddiv1'
        img_regex = 'V1/rFIT_ISOVF'
        meth = 'Linear'
        folder = 'NODDI'
    else:
        cprint("ERROR: Sequence to MNI not implemented yet: " + img, 'red',
               attrs=['bold'])
        sys.exit(2)
    print(colored("| Registering Sequence:", 'blue', attrs=['bold']), img)
    if options.qsub:
        for s in subjects:
            sendCluster(s, options)
        sys.exit()
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        s['process'] = os.path.join(process, s['Id'], s['Visit'])
# ----------------------------------- HWS  ------------------------------------
        s['Id_dir'] = os.path.join(process, s['Id'])
        if options.hws == 'ALL':
            test = get_regex(s['Id_dir'], '^V\d*$', deep_search=False,
                             full=False)
            test.sort()
            reg_hws = ''.join([os.path.basename(x) for x in test])
        else:
            reg_hws = options.hws
# -------------------------------- HWS dir ------------------------------------
        test = get_regex(s['Id_dir'], 'fs_' + reg_hws)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR:' + reg_hws + ' hws not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        s['hws_dir'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| HWS_dir:', 'green', attrs=['bold']), s['hws_dir'])
# -------------------------------- HWS img ------------------------------------
        test = get_regex(s['hws_dir'], reg_hws + '.nii.gz',  False, False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR:' + reg_hws + ' hws image not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        hws = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| HWS image:', 'green', attrs=['bold']), hws)
# --------------------- Defining path to MNIregistration ----------------------
        MNIreg = os.path.join(s['hws_dir'], "HWSAnts2SS")
        createDir(MNIreg)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| MNIregistration:', 'green', attrs=['bold']), MNIreg)
        trans_mat_dir = os.path.join(MNIreg, 'trans_mat')
        createDir(trans_mat_dir)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Transformation matrices stored in:', 'green',
                      attrs=['bold']), trans_mat_dir)
# ------------------------- Registering T1_N4 to MNI --------------------------
        warp = changeName(hws, '_2mni', MNIreg)
        invwarp = changeName(template, '_DELETE_ME', MNIreg)
        hws_reg = AntsRegistrationSyn(hws, template, warp, invwarp, 's',
                                      big_image=True)
        if os.path.isfile(hws_reg['inverse_warped_image']):
            os.remove(hws_reg['inverse_warped_image'])
        if stop:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
# ---------------- Retrieving HWS to MNI transformation matrix ----------------
        hws_2MNI_mat = hws_reg['forward_transforms']
# ------------------- Retrieving img to register on HWS2MNI -------------------
        subject_dir_img = os.path.join(s['hws_dir'], img_fold)
        s['img'] = get_regex(subject_dir_img, s['Visit'] + img_regex, False)
        if not s['img']:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: IMAGE NOT FOUND:', 'red', attrs=['bold']),
                  img)
            continue
        s["img"] = s["img"][0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found ' + img + ':', 'green', attrs=['bold']),
              s["img"])
# --------------------------- warping  image to MNI ---------------------------
        transformations = [hws_2MNI_mat[1], hws_2MNI_mat[0]]
        out_dir = os.path.join(MNIreg, folder)
        createDir(out_dir)
        warp = changeName(s['img'], '_2mni', out_dir)
        ApplyRegistration(s['img'], template, transformations, warp,
                          interp=meth)
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
cprint('|', 'blue', attrs=['bold'])
cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
print('')
