#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 10:13:40 2020

@author: arya.yazdan-panah
"""

import os
import sys
import time
import numpy as np
import nibabel as nib
from scipy.stats import gaussian_kde
from skimage.morphology import binary_erosion
from termcolor import cprint, colored
from common3 import get_regex, createDir, init_script, changeName
# from common3 import sphere
from tools_ants import ApplyRegistration
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from tools_cluster import qsub3
from tools_fsl import fslmaths

MSG = "Inpaint MS lesions with normal appearing white matter-like voxels"


def send_cluster(subject, options):
    print(" ** Sending Lesion Inpainting:",
          subject["Id"] + "_" + subject["Visit"])
    comm = "%s -s %s/%s -p %s -m %s -t %s -i %s -R %s" % (sys.argv[0],
                                                          subject["Id"],
                                                          subject["Visit"],
                                                          options.process,
                                                          options.method,
                                                          options.threshold,
                                                          options.img,
                                                          options.ref)
    logdir = os.path.join(os.environ["PROJECT_DIR"], "clulogs")
    createDir(logdir)
    name = "LesInp_" + subject["Id"] + "_" + subject["Visit"]
    etime = "02:00:00"
    ID = qsub3(comm, name, "normal", [], etime, "4G", True, logdir, '2')
    print(" **    Job ID:", ID)


if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    p0 = parser.add_argument_group("Mandatory Options")
    p0.add_argument('-s', '--subjects', dest='subjects',
                    help='Subjects list, if none choices will be provided')
    p0.add_argument("-p", "--process", dest="process",
                    help="Process directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-q", "--qsub", action="store_true", dest="qsub")
    p1 = parser.add_argument_group("Advanced Options")
    p1.add_argument("-m", "--method", dest="method", default='K',
                    help="Filling: K = Kernel; NN = Nearest_Neighbor")
    p1.add_argument("-t", "--threshold", dest="threshold", default=0.5,
                    help="Threshold of the lesion mask", type=float)
    p1.add_argument("-i", "--les-image", dest="img", default='t2',
                    help="Image on which the lesion mask was drawn")
    p1.add_argument("-R", "--Reference", dest="ref", default='t1',
                    help="Image to inpaint (t1 or mp2rage)")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
# ---------------------------------- Method -----------------------------------
    if options.method in {None, 'K'}:
        method = 'Kernel'
        min_voxs = 15
        box_size = 2
    elif options.method == 'NN':
        method = 'Nearest_Neighbor'
        min_voxs = 5
        box_size = 1
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('ERROR: Method chosen not implemented, refer to help',
                      'red', attrs=['bold']))
        sys.exit(1)
    cprint('|FILLING METHOD = ' + method, 'blue', attrs=['bold'])
# ---------------------------------- Image ------------------------------------
    if options.ref == 't1':
        ref_dir = 't1preproc'
        regex_ref = '_t1_N4'
    elif options.ref == 'mp2rage':
        ref_dir = 'mp2ragepreproc'
        regex_ref = '_mp2rage_uni_denoised'
# ---------------------------------- Image ------------------------------------
    img = str(options.img).lower()
    if img == 't2':
        lesion_mask_regex = '_t2_les_manual$'
        mat_2t1_regex = '(_t2_2(mp2rage|t1_N4|t1)_ITK|_t2_to_t1_0GenericAffine)'
        mat_2t1_fold = 'contouringpreproc'
    elif img == 't2_energysep':
        lesion_mask_regex = '_t2_les_manual$'
        mat_2t1_regex = '_t2_2mp2rage_ITK'
        mat_2t1_fold = 'contouringpreproc'
    elif img == 'flair':
        lesion_mask_regex = '_flair_les_manual$'
        mat_2t1_regex = '_flair_2mp2rage0GenericAffine'
        mat_2t1_fold = 'contouringpreproc'
    elif img == 'flair2':
        lesion_mask_regex = '_flair_les_manual$'
        mat_2t1_regex = '_flair_2t10GenericAffine'
        mat_2t1_fold = 'flairpreproc'
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('ERROR: Image chosen not implemented', 'red',
                      attrs=['bold']))
        sys.exit(1)
    options.img = img
# -------------------------------- Cluster ------------------------------------
    if options.qsub:
        options.process = process
        for s in subjects:
            send_cluster(s, options)
        sys.exit()
    if options.qsub:
        options.process = process
        for s in subjects:
            send_cluster(s, options)
        sys.exit()
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# ------------------------ Defining path to t1preproc -------------------------
        t1preproc = os.path.join(process, s['Id'], s['Visit'], ref_dir)
        if not os.path.isdir(t1preproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Directory not found', 'red', attrs=['bold']),
                  t1preproc)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + options.ref + 'preproc:', 'green',
                      attrs=['bold']), t1preproc)
# ------------------------- Retrieving T1_N4 sequence -------------------------
        test = get_regex(t1preproc, regex_ref + '$')
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + options.ref + ' not found', 'red',
                          attrs=['bold']))
            continue
        s['N4'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + options.ref + ':', 'green', attrs=['bold']),
              s['N4'])
# ------------------------ Defining path to fast dir --------------------------
        fast = os.path.join(t1preproc, 'fast')
        if not os.path.isdir(fast):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Directory not found', 'yellow',
                          attrs=['bold']), fast)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Maybe old protocol, looking fo tmp in', 'yellow',
                          attrs=['bold']), t1preproc)
            fast = os.path.join(t1preproc, 'tmp')
            if not os.path.isdir(fast):
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: tmp not found', 'red', attrs=['bold']),
                      fast)
                print(colored('|', 'blue', attrs=['bold']),
                      colored('L', 'green', attrs=['bold']))
                continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ROI directory: ', 'green', attrs=['bold']),
              fast)
# ---------------------------- Retrieving WM_mask -----------------------------
        test = get_regex(fast,
                         '(' + regex_ref + '_brain_seg_2|BrainExtractionWM)')
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: White Matter mask not found', 'red',
                          attrs=['bold']))
            continue
        s['WM_mask'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| White Matter mask:', 'green',
                      attrs=['bold']), s['WM_mask'])
# -------------------- Defining path to contouringpreproc ---------------------
        matrix_fold = os.path.join(process, s['Id'], s['Visit'], mat_2t1_fold)
        if not os.path.isdir(matrix_fold):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Directory not found', 'red', attrs=['bold']),
                  matrix_fold)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + mat_2t1_fold + ': ', 'green', attrs=['bold']),
              matrix_fold)
# ---------------------------- Retrieving mat2T1 ------------------------------
        test = get_regex(matrix_fold, mat_2t1_regex)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T2 to T1_N4 transformation matrix not found',
                          'red', attrs=['bold']))
            continue
        s['t2_2N4'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| T2 to T1_N4 transformation matrix:', 'green',
                      attrs=['bold']), s['t2_2N4'])
# ---------------------- Defining path to manual_lesion -----------------------
        pro = os.path.join(process, s['Id'], s['Visit'])
        manual_lesion = get_regex(pro, '(lesions_manual|manual_lesion)',
                                  deep_search=False)
        if not manual_lesion:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Lesions_manual not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        manual_lesion = manual_lesion[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| manual_lesion:', 'green', attrs=['bold']),
              manual_lesion)
# ---------------------------- Retrieving mat2T1 ------------------------------
        test = get_regex(manual_lesion, lesion_mask_regex)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: No lesion mask found', 'red',
                          attrs=['bold']))
            continue
        s['lesion_mask'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Lesion mask:', 'green', attrs=['bold']),
              s['lesion_mask'])
        fslmaths(s['lesion_mask'], '-bin', s['lesion_mask'])
# ----------------- Defining path to lesion inpainting dir --------------------
        lesion_inpainting = os.path.join(process, s['Id'], s['Visit'], 'leinp')
        createDir(lesion_inpainting)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Lesion inpatinting dir created/existing:', 'green',
                      attrs=['bold']), lesion_inpainting)
        inpainted_name = changeName(s['N4'], suffix='_inpainted',
                                    outdir=lesion_inpainting)
        if os.path.isfile(inpainted_name):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Inpainting already done, skipping...', 'green',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('For a new processing, delete/rename:', 'yellow',
                          attrs=['bold']), inpainted_name)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
# --------------------- Registering Lesion Mask on T1 -------------------------
        s['les_N4'] = changeName(s['lesion_mask'], '_2' + options.ref)
        ApplyRegistration(s['lesion_mask'], s['N4'], s['t2_2N4'], s['les_N4'],
                          interp='NearestNeighbor')
# ----------------------- White Matter Mask Erosion ---------------------------
        ES1 = [[[0, 0, 0],
                [0, 0, 0],
                [0, 0, 0]],
               [[0, 1, 0],
                [1, 1, 1],
                [0, 1, 0]],
               [[0, 0, 0],
                [0, 0, 0],
                [0, 0, 0]]]
        ES2 = [[[0, 0, 0],
                [0, 1, 0],
                [0, 0, 0]],
               [[0, 0, 0],
                [1, 1, 1],
                [0, 0, 0]],
               [[0, 0, 0],
                [0, 1, 0],
                [0, 0, 0]]]
        ES3 = [[[0, 0, 0],
                [0, 1, 0],
                [0, 0, 0]],
               [[0, 1, 0],
                [0, 1, 0],
                [0, 1, 0]],
               [[0, 0, 0],
                [0, 1, 0],
                [0, 0, 0]]]
        ES4 = [[[0, 0, 0],
                [0, 1, 0],
                [0, 0, 0]],
               [[0, 1, 0],
                [1, 1, 1],
                [0, 1, 0]],
               [[0, 0, 0],
                [0, 1, 0],
                [0, 0, 0]]]
        erosion_structure = np.array(ES3)
        WM = nib.load(s['WM_mask'])
        affineWM = WM.affine
        WM = WM.get_fdata().astype('bool')
        WM = binary_erosion(WM, erosion_structure)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| White Matter Loaded', 'green', attrs=['bold']))
# ------------------- Filling WM mask with lesion mask ------------------------
        INP = nib.load(s['N4'])
        affineINP = INP.affine
        INP = INP.get_fdata()
        print(colored('|', 'blue', attrs=['bold']),
              colored('| T1_N4 Loaded', 'green', attrs=['bold']))
        LM = nib.load(s['les_N4'])
        affineLM = LM.affine
        LM = LM.get_fdata()
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Lesion Mask Loaded', 'green', attrs=['bold']))
        threshold = np.max(LM) * options.threshold
        LM = (LM > threshold)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Lesion Mask thresholded at:', 'green',
                      attrs=['bold']), threshold)
        WM[LM] = False
        remaining = np.count_nonzero(LM)
        total = remaining
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored(' Total amount of voxels to inpaint:', 'cyan',
                      attrs=['bold', 'reverse']), remaining, 'Voxels')
        all_time = time.time()
        avg_time = 0
        while remaining:
            LM_inside = binary_erosion(LM, erosion_structure)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Lesion Mask Eroded', 'cyan',
                          attrs=['bold']))
            LM_border = np.logical_xor(LM, LM_inside)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Eroded mask subtracted from original one to obta'
                          'in lesion borders', 'cyan',
                          attrs=['bold']))
            search = np.where(LM_border)
            idxs = list(zip(search[0], search[1], search[2]))
            to_process = len(idxs)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Remaining lesion border voxels:', 'cyan',
                          attrs=['bold']), to_process, 'Voxels')
            for idx in idxs:
                vox_time = time.time()
                nb_vox = 0
                while nb_vox < min_voxs:
                    Box = np.full(INP.shape, False)
                    Box[idx[0] - box_size: idx[0] + box_size,
                        idx[1] - box_size: idx[1] + box_size,
                        idx[2] - box_size: idx[2] + box_size] = True
                    # Box = sphere(INP.shape, box_size, idx)
                    if method == 'Nearest_Neighbor':
                        vox_box = INP[np.logical_and(~LM, Box)]
                    elif method == 'Kernel':
                        vox_box = INP[np.logical_and(WM, Box)]
                    voxs = np.ndarray.flatten(vox_box)
                    nb_vox = len(voxs)
                    box_size += 1
                if method == 'Nearest_Neighbor':
                    vox = np.mean(voxs)
                    box_size = 1
                elif method == 'Kernel':
                    KDE = gaussian_kde(voxs)
                    vox = KDE.resample(size=1)[0, 0]
                    box_size = 2
                INP[idx] = vox
                LM[idx] = False
                vox_time = time.time() - vox_time
                avg_time += vox_time
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Border done', 'cyan',
                          attrs=['bold']))
            LM = LM_inside
            remaining = np.count_nonzero(LM)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Total processing time:', 'green', attrs=['bold']),
              time.time() - all_time)
        try:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Average time per voxels:', 'green',
                          attrs=['bold']), avg_time / total)
        except ZeroDivisionError:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Average time per voxels:', 'green',
                          attrs=['bold']), 0)
        INP = nib.Nifti1Image(INP, affineINP)
        nib.save(INP, inpainted_name)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Inpaiting saved as:', 'green', attrs=['bold']),
              inpainted_name)
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script finished ', 'blue', attrs=['reverse', 'bold'])
    print("")
