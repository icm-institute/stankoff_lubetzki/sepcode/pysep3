#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 17:21:33 2020

@author: arya.yazdan-panah
"""


import os
import sys
from common3 import get_regex, init_script, changeName
from tools_deepbrain import brain_mask
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

MSG = "Perform brain extraction on MP2RAGE images with deepbrain"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
# -------------------------------- processing ---------------------------------
    b_masks = []
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# --------------------- Defining path to mp2ragepreproc -----------------------
        mp2ragepreproc = os.path.join(process, s['Id'], s['Visit'],
                                      'mp2ragepreproc')
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory t1preproc existing: ', 'green',
                      attrs=['bold']), mp2ragepreproc)
        s['denoised'] = get_regex(mp2ragepreproc, 'denoised$')
        if not s['denoised']:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: denoised MP2RAGE NOT FOUND', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        s['denoised'] = s['denoised'][0]
# --------------------------- Deep Brain Extraction ---------------------------
        out_file = changeName(s['denoised'], suffix='_brain_mask')
        b_mask = brain_mask(s['denoised'], out_file)
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))

    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
