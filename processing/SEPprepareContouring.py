#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 15:46:34 2020

@author: arya.yazdan-panah
"""

import os
import shutil
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
from termcolor import cprint, colored
from common3 import createDir, init_script, changeName, get_regex
from tools_ants import AntsRegistrationSyn, ApplyRegistration
from tools_c3 import fsl2itk
from tools_fsl import (fast, conv_image, flirt_bbr, convert_xfm, apply_xfm,
                       fslmaths)
from tools_cluster import qsub3

msg = "Preparation for the lesion contouring, if the option is left empty the \
program will attempt to register T2 and flair on T1 and on themselves with a B\
BR registration. It is however possible to ask for a specific sequence wich wi\
ll be registered on T1"


def send_cluster(subject, options):
    print(" ** Sending Contouring preproc:",
          subject["Id"] + "_" + subject["Visit"])
    comm = "%s -s %s/%s -r %s -p %s -R %s" % (sys.argv[0], subject["Id"],
                                              subject["Visit"], options.raw,
                                              options.process, options.ref)
    if options.over:
        comm = comm + " -o"
    logdir = os.path.join(os.environ["PROJECT_DIR"], "clulogs")
    createDir(logdir)
    name = "PC_" + subject["Id"] + "_" + subject["Visit"]
    etime = "02:00:00"
    ID = qsub3(comm, name, "normal", [], etime, "8G", True, logdir, '4')
    print(" **    Job ID:", ID)


if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=msg,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    p0 = parser.add_argument_group("Mandatory Options")
    p0.add_argument('-s', '--subjects', dest='subjects',
                    help='Subjects list, if none choices will be provided')
    p0.add_argument("-r", "--raw", dest="raw",
                    help="Raw directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-p", "--process", dest="process",
                    help="Process directory, if none deduced from PROJECT_DIR")
    p0.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                    default=False, help='Make the program talkative')
    p0.add_argument("-q", "--qsub", action="store_true", dest="qsub")
    p1 = parser.add_argument_group("Advanced Options")
    p1.add_argument("-R", "--Reference", dest="ref", default='t1',
                    help="Reference Image")
    p1.add_argument("-I", "--image", dest="image", default='t2',
                    help=" Input Image (default t2)")
    p1.add_argument("-o", "--overwrite", dest="over", action="store_true",
                    help="Overwrite existing Contouring preparation")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
    if options.ref == 't1':
        ref_dir = 't1preproc'
        regex_ref = '_t1_N4$'
    elif options.ref == 'mp2rage':
        ref_dir = 'mp2ragepreproc'
        regex_ref = '_mp2rage_uni_denoised$'
    if options.qsub:
        options.raw = raw
        options.process = process
        for s in subjects:
            send_cluster(s, options)
        sys.exit()
# -------------------------- Steps of Registration  ---------------------------
    do = {'seq': True,
          'flair': False}
    if options.image not in ['dir', 't1se', 't1gd', 't1pregd', 't2', 'flair',
                             't2_only']:
        cprint(" ** ERROR: Input image asked should be one of the following: "
               "'t2' (default), 'dir', 't1se', 't1gd', 'flair', 't2_only'",
               'red', attrs=['bold'])
        do['seq'] = False
    seqname = options.image
    if seqname == 't2':
        do['flair'] = True
        cprint('|    SEQUENCES = T2', 'blue', attrs=['bold'])
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# ------------------------ Defining path to t1preproc -------------------------
        t1preproc = os.path.join(process, s['Id'], s['Visit'], ref_dir)
        if not os.path.isdir(t1preproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + t1preproc + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| t1preproc: ', 'green', attrs=['bold']),
              t1preproc)
# ---------------------- Defining path to manual_lesion -----------------------
        if not s['Id'].startswith('C'):
            lesion_dir = os.path.join(process, s['Id'], s['Visit'],
                                      'lesions_manual')
            createDir(lesion_dir)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| manual_lesion directory created/existing, store '
                          'lesion mask in the following directory:', 'green',
                          attrs=['bold']), lesion_dir)
# ------------------------- Retrieving T1_N4 sequence -------------------------
        N4 = get_regex(t1preproc, regex_ref)
        if not N4:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1 NOT FOUND', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        N4 = N4[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found T1 seq: ', 'green', attrs=['bold']), N4)
# ------------------ Retrieving input sequence (usually T2) -------------------
        save_seqname = seqname
        if save_seqname == 't2_only':
            seqname = 't2'
        seqdir = os.path.join(raw, s['Id'], s['Visit'], seqname)
        seq = get_regex(seqdir, '_' + seqname + '.nii.gz', False,)
        if not seq:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + seqname.upper() + ' not found, unable to'
                          ' register it on T1', 'red', attrs=['bold']))
            continue
        seq = seq[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found ' + seqname.upper() + ' seq: ', 'green',
                      attrs=['bold']), seq)
        do['seq'] = True
# --------------------------- Retrieving Brain Mask ---------------------------
        b_mask = get_regex(t1preproc, '_brain_mask.nii.gz', False)
        if not b_mask:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: brain mask not found in ' + t1preproc, 'red',
                          attrs=['bold']))
            continue
        b_mask = b_mask[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Brain Mask: ', 'green', attrs=['bold']),
              b_mask)
# ---------------------------- Applying Brain Mask ----------------------------
        brain_path = changeName(N4, suffix='_brain')
        fslmaths(N4, '-mul', b_mask, brain_path)
# ------------------------- Retrieving FLAIR sequence -------------------------
        if seqname == 't2':
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| segmenting brain for BBR registration', 'green',
                          attrs=['bold']))
# --------------------------- Segmenting the Brain ----------------------------
            fast_dir, segpaths = fast(brain_path, verbose=False)
# ----------------------- Retrieving White Matter Mask ------------------------
            WM_mask = get_regex(fast_dir, '_brain_pve_2')
            if not WM_mask:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: WM mask not found in:', 'red',
                              attrs=['bold']), fast_dir)
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: Fast Registration have failed', 'red',
                              attrs=['bold']))
                continue
            WM_mask = WM_mask[0]
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Found WM Mask: ', 'green', attrs=['bold']),
                  WM_mask)
            if save_seqname == 't2':
                flairdir = os.path.join(raw, s['Id'], s['Visit'], 'flair')
                flair = get_regex(flairdir, '_flair.nii.gz', False)
                if not flair:
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('|', 'green', attrs=['bold']),
                          colored('WARNING: FLAIR sequence not found, will not'
                                  ' register FLAIR on T2', 'yellow',
                                  attrs=['bold']))
                    do['flair'] = False
                else:
                    flair = flair[0]
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('| Found FLAIR sequence: ', 'green',
                                  attrs=['bold']), flair)
                    do['flair'] = True
            elif save_seqname == 't2_only':
                do['flair'] = False
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Only registering T2 on T1', 'green',
                              attrs=['bold']))
# -------------------- Defining path to contouringpreproc ---------------------
            contouringpreproc = os.path.join(process, s['Id'], s['Visit'],
                                             'contouringpreproc')
            if options.over:
                shutil.rmtree(contouringpreproc)
            createDir(contouringpreproc)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| contouringpreproc: ', 'green', attrs=['bold']),
                  contouringpreproc)
            trans_mat = os.path.join(contouringpreproc, 'trans_mat')
            createDir(trans_mat)
# ----------------------------- processing T2 ---------------------------------
        if do['seq'] and seqname == 't2':
            t2_float = changeName(seq, '_float', contouringpreproc)
# --------------------------- conv T2 to float --------------------------------
            conv_image(seq, 'float', t2_float)
# --------------------------- register T2 to T1 -------------------------------
            warp = changeName(seq, '_2' + options.ref, contouringpreproc)
            tmp, t2_reg_mat, tmp = flirt_bbr(seq, N4, warp, WM_mask,
                                             verbose=options.verbose)
# --------------------------- register T1 to T2 -------------------------------
            N4_2T2_mat = changeName(N4, '_2t2', trans_mat, newext='.mat')
            convert_xfm(t2_reg_mat, N4_2T2_mat)
            mov2ref = changeName(N4, '_2t2', contouringpreproc)
            apply_xfm(N4, seq, N4_2T2_mat, mov2ref)
# ------------------------- register flair to T1 ------------------------------
            if do['flair']:
                flr2N4 = changeName(flair, '_2' + options.ref,
                                    contouringpreproc)
                N4_2flair = changeName(N4, '_2flair', contouringpreproc)
                flair_reg = AntsRegistrationSyn(flair, N4, flr2N4, N4_2flair,
                                                'r', b_mask, big_image=True,
                                                verbose=options.verbose)
                flair2N4_mat = flair_reg['forward_transforms'][0]
                t2_reg_mat = fsl2itk(t2_reg_mat, N4, seq)
                trans = [t2_reg_mat, flair2N4_mat]
                inv_trans = [flair2N4_mat, t2_reg_mat]
                flair_2T2 = changeName(flair, '_2t2', contouringpreproc)
                T2_2flair = changeName(seq, '_2flair', contouringpreproc)
                ApplyRegistration(flair, seq, trans, flair_2T2, [True, False])
                ApplyRegistration(seq, flair, inv_trans, T2_2flair, [True,
                                                                     False])
# ----------------------- processing other Sequences  -------------------------
        elif do['seq'] and seqname in ['dir', 't1se', 't1gd', 'flair']:
            dirname = seqname + 'preproc'
            seqpreproc = os.path.join(process, s['Id'], s['Visit'], dirname)
            if options.over:
                shutil.rmtree(seqpreproc)
            createDir(seqpreproc)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Created/Existing ' + dirname + ':', 'green',
                          attrs=['bold']), seqpreproc)
            seq_2N4 = changeName(seq, '_2' + options.ref, seqpreproc)
            N4_2seq = changeName(N4, '_2' + seqname, seqpreproc)
            AntsRegistrationSyn(seq, N4, seq_2N4, N4_2seq, 'r', b_mask, 0,
                                True, 6, verbose=options.verbose)
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
