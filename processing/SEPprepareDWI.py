#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 12 14:37:28 2021

@author: arya.yazdan-panah
"""

import os
import shutil
import sys
import numpy as np
import nibabel as nib
from termcolor import cprint, colored
from common3 import get_regex, createDir, init_script, changeName
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from tools_diffusion import (read_json, read_bval, read_bvec, write_bval,
                             write_bvec, dwidenoise)
from tools_fsl import (fslroi, fslmerge, Topup, fslmean, fslEddy, dtifit,
                       fslmaths)
from tools_ants import AntsRegistrationSyn, ApplyRegistration
from tools_cluster import qsub3


MSG = "Implementation of 'The standard FSL diffusion processing pipeline' \
    https://fsl.fmrib.ox.ac.uk/fslcourse/lectures/practicals/fdt1/index.html"


phase_encode = {'i': ['PA', '1 0 0'],
                'i-': ['AP', '-1, 0 0'],
                'j': ['PA', '0 1 0'],
                'j-': ['AP', '0 -1 0']}


def send_cluster(subject, options):
    print(" ** Sending DTI preproc:", subject["Id"] + "_" + subject["Visit"])
    comm = "%s -s %s/%s -r %s -p %s -R %s" % (sys.argv[0], subject["Id"],
                                              subject["Visit"], options.raw,
                                              options.process, options.ref)
    if options.overwrite:
        comm = comm + " -o"
    if not options.dtmp:
        comm = comm + " -t"

    logdir = os.path.join(os.environ["PROJECT_DIR"], "DWI_clulogs")
    createDir(logdir)

    name = "DTI_" + subject["Id"] + "_" + subject["Visit"]
    etime = "96:00:00"
    ID = qsub3(comm, name, "normal", [], etime, "8G", True, logdir, '4')
    print(" **    Job ID:", ID)


if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    p0 = parser.add_argument_group("Mandatory Options")
    p0.add_argument('-s', '--subjects', dest='subjects',
                    help='Subjects list, if none choices will be provided')
    p0.add_argument("-r", "--raw", dest="raw",
                    help="Raw directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-p", "--process", dest="process",
                    help="Process directory, if none deduced from PROJECT_DIR")
    p0.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                    help='Make the program talkative')
    p0.add_argument("-q", "--qsub", action="store_true", dest="qsub")
    p1 = parser.add_argument_group("Advanced Options")
    p1.add_argument("-R", "--Reference", dest="ref", default='t1',
                    help="Reference Image")
    p1.add_argument("-N", "--no-denoise", action='store_true',
                    dest='skip_denoise', help="Add -N to skip denoising")
    p1.add_argument("-B", "--no-bias-field", action='store_false',
                    dest='skip_bias',
                    help="Add -B to skip N4BiasFieldCorrection")
    p1.add_argument('-o', "--overwrite", action='store_true', dest='overwrite',
                    help='Overwrites existing directories and files')
    p1.add_argument('-t', '--no-delete-temp', action='store_false',
                    dest='dtmp', help="Don't remove tmp files if specified")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
    if options.ref == 't1':
        ref_dir = 't1preproc'
        regex_ref = '(_t1_N4|_t1_pre)$'
    elif options.ref == 'mp2rage':
        ref_dir = 'mp2ragepreproc'
        regex_ref = '_mp2rage_uni_denoised$'
    if options.qsub:
        options.raw = raw
        options.process = process
        for s in subjects:
            send_cluster(s, options)
        sys.exit()
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        s['raw'] = os.path.join(raw, s['Id'], s['Visit'])
        s['process'] = os.path.join(process, s['Id'], s['Visit'])
        s['dwi'] = os.path.join(s['process'], 'dwipreproc')
        if options.overwrite and os.path.isdir(s['dwi']):
            shutil.rmtree(s['dwi'])
        createDir(s['dwi'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| dwipreproc directory:', 'green', attrs=['bold']),
              s['dwi'])
# ----------------------------- Recover raw images ----------------------------
        dwi_raw_dir = os.path.join(s['raw'], 'dwi')
        if not os.path.isdir(dwi_raw_dir):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('DWI not found in raw directory', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        diffs = get_regex(dwi_raw_dir, 'dwi.*.nii.gz', 0, 1, 0, 0)
        if not diffs:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('No dwi found in the folder', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found following number of diffusions (.nii.gz):',
                      'green', attrs=['bold']), len(diffs))
        diff_sequences = []
        for img in diffs:
            searchname = changeName(img, outdir='', newext='')
            A = get_regex(dwi_raw_dir, searchname + '.(bvec|bval|json)', False)
            if len(A) == 1 and os.path.splitext(A[0])[1] == '.json':
                diff_sequences.append({'image': img,
                                       'json': A[0],
                                       'bvec': False,
                                       'bval': False})
            elif len(A) == 3:
                diff_sequences.append({'image': img,
                                       'json': A[2],
                                       'bvec': A[1],
                                       'bval': A[0]})
            print(diff_sequences)
# ---------------------------- Denoising raw images ---------------------------
        if options.skip_denoise:
            pass
        else:
            s['denoise'] = os.path.join(s['dwi'], 'denoising')
            createDir(s['denoise'])
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Denoising directory:', 'green', attrs=['bold']),
                  s['denoise'])
            for shell in diff_sequences:
                if not shell['bvec']:
                    continue
                denoised = changeName(shell['image'], '_denoised',
                                      s['denoise'])
                residuals = changeName(shell['image'], '_residuals',
                                       s['denoise'])
                if not os.path.isfile(denoised):
                    dwidenoise(shell['image'], denoised)
                if not os.path.isfile(residuals):
                    fslmaths(shell['image'], '-sub', denoised, residuals)
                shell['image'] = denoised
# ------------------------ Checking Z dimension for TOPUP ---------------------
        Z_dim = nib.load(diff_sequences[0]['image']).shape[2]
        if not (Z_dim % 2 == 0):
            s['crop'] = os.path.join(s['dwi'], 'cropped')
            createDir(s['crop'])
            for shell in diff_sequences:
                cropped = changeName(shell['image'], '_cropped', s['crop'])
                if not os.path.isfile(cropped):
                    fslroi(shell['image'], cropped, 0, -1, 0, -1, 1, Z_dim-1,
                           0, -1)
                shell['image'] = cropped
# --------------------------- Joining B0s for TOPUP ---------------------------
        s['merge'] = os.path.join(s['dwi'], 'merged')
        createDir(s['merge'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory where merged shells are stored:', 'green',
                      attrs=['bold']), s['merge'])
        s['topup'] = os.path.join(s['dwi'], 'topup')
        createDir(s['topup'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Topup directory:', 'green', attrs=['bold']),
              s['topup'])
        B0s_4D = get_regex(s['merge'], '_B0s.nii.gz', False, False)
        acqparams_txt = get_regex(s['topup'], '_B0s_acqparams.txt', False,
                                  False)
        s['eddy'] = os.path.join(s['dwi'], 'eddy')
        createDir(s['eddy'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Eddy directory:', 'green', attrs=['bold']), s['eddy'])
        indices_txt = get_regex(s['eddy'], '_index.txt', False, False)
        diff_2eddy = []
        bvec_2eddy = []
        bval_2eddy = []
        if ((not B0s_4D or not acqparams_txt or not indices_txt)
                or options.overwrite):
            B0s_4D = os.path.join(s['merge'], '%s_%s_B0s.nii.gz' %
                                  (s['Id'], s['Visit']))
            acqparams_txt = os.path.join(s['topup'],
                                         '%s_%s_4D_B0s_acqparams.txt' %
                                         (s['Id'], s['Visit']))
            tmp_dir = os.path.join(s['dwi'], 'tmp')
            createDir(tmp_dir)
            acqparams_txt_opened = open(acqparams_txt, 'w')
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Opened parameter file with phase encoding direct'
                          'ions and total readout times for writing:', 'green',
                          attrs=['bold']), acqparams_txt)
            index_acqparam = 1
            indices_txt = []
            for i, diff_seq in enumerate(diff_sequences):
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']))
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Extracting B0s in:', 'green', attrs=['bold']),
                      diff_seq['image'])
                if diff_seq['bval']:
                    bvals = np.array(read_bval(diff_seq['bval']))
                else:
                    bvals = np.array([0])
                B0_indices = np.where(bvals <= 10)[0].tolist()
                jdata = read_json(diff_seq['json'])
                RT = jdata['TotalReadoutTime']
                PE = phase_encode[jdata['PhaseEncodingDirection']]
                if len(bvals) > 3:
                    index_txt = changeName(diff_seq['image'], '_index',
                                           s['eddy'], '.txt')
                    index_txt_opened = open(index_txt, "w")
                    index_txt_opened.write("%d " % index_acqparam * len(bvals))
                    index_txt_opened.close()
                    indices_txt.append(index_txt)
                    diff_2eddy.append(diff_seq['image'])
                    bvec_2eddy.append(diff_seq['bvec'])
                    bval_2eddy.append(diff_seq['bval'])
                for j, idx in enumerate(B0_indices):
                    B0_name = os.path.join(tmp_dir, '%s_%s_B0_%d_%d.nii.gz' %
                                           (s['Id'], s['Visit'], i, j))
                    fslroi(diff_seq['image'], B0_name, idx, 1)
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('| | B0s at index:', 'green',
                                  attrs=['bold']), idx,
                          colored('- PhaseEncodingDirection:', 'green',
                                  attrs=['bold']), PE[0],
                          colored('- TotalReadoutTime:', 'green',
                                  attrs=['bold']), RT)
                    acqparams_txt_opened.write(PE[1] + ' ' + str(RT) + '\n')
                    index_acqparam += 1
            B0s = get_regex(tmp_dir, '_B0_', 0)
            fslmerge(B0s, B0s_4D)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Merged all B0s:', 'green',
                          attrs=['bold']), B0s_4D)
            acqparams_txt_opened.close()
            if options.dtmp:
                shutil.rmtree(tmp_dir)
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Removed tmp files', 'green', attrs=['bold']))
        else:
            B0s_4D = B0s_4D[0]
            acqparams_txt = acqparams_txt[0]
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Paramter file already exists:', 'green',
                          attrs=['bold']), acqparams_txt)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Merged B0s already exists:', 'green',
                          attrs=['bold']), B0s_4D)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Skipping extraction', 'green', attrs=['bold']))
            next_subject = False
            for txt_file in indices_txt:
                regex_diff = changeName(txt_file, '', '', '', '',
                                        '_index')
                search_no_blip = get_regex(s['denoise'],
                                           regex_diff + '.nii.gz', 0)
                if not search_no_blip:
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('|', 'green', attrs=['bold']),
                          colored('No diffusion sequence found matching the in'
                                  'dex file:', 'red', attrs=['bold']),
                          txt_file)
                    next_subject = True
                    break
                diff_2eddy.extend(search_no_blip)
                regex_diff = changeName(txt_file, '', '', '', '',
                                        '_denoised_index')
                bvec_no_blip = get_regex(s['raw'], regex_diff + '.bvec', 0)
                if not bvec_no_blip:
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('|', 'green', attrs=['bold']),
                          colored('No bvec found matching the index file:',
                                  'red', attrs=['bold']), txt_file)
                    next_subject = True
                    break
                bvec_2eddy.extend(bvec_no_blip)
                bval_no_blip = get_regex(s['raw'], regex_diff + '.bval', 0)
                if not bval_no_blip:
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('|', 'green', attrs=['bold']),
                          colored('No bval found matching the index file:',
                                  'red', attrs=['bold']), txt_file)
                    next_subject = True
                    break
                bval_2eddy.extend(bval_no_blip)
            if next_subject:
                continue
# ------------------------------ Runnning TOPUP -------------------------------
        s['topup'] = Topup(B0s_4D, acqparams_txt, s['topup'],
                           options.verbose, options.overwrite)
        fieldcoef = get_regex(s['topup'], '_fieldcoef.nii.gz', False)
        if not fieldcoef:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('No fieldcoef found in topup dir:', 'red',
                          attrs=['bold']), s['topup'])
            continue
        fieldcoef = fieldcoef[0]
        movpar = get_regex(s['topup'], '_movpar.txt', False)
        if not movpar:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('No movpar found in topup dir:', 'red',
                          attrs=['bold']), s['topup'])
            continue
        movpar = movpar[0]
# ------------- Registering mannually corrected Brain mask on DWI -------------
# ------------------------ Defining path to t1preproc -------------------------
        anatpreproc = os.path.join(process, s['Id'], s['Visit'], ref_dir)
        if not os.path.isdir(anatpreproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + ref_dir + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + ref_dir + ': ', 'green', attrs=['bold']),
              anatpreproc)
# ------------------------- Retrieving T1_N4 sequence -------------------------
        reference = get_regex(anatpreproc, regex_ref)
        if not reference:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + options.ref + ' NOT FOUND', 'red',
                          attrs=['bold']), regex_ref)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        reference = reference[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found ' + options.ref + ' sequence: ', 'green',
                      attrs=['bold']), reference)
# --------------------------- Retrieving Brain Mask ---------------------------
        b_mask = get_regex(anatpreproc, '(_brain_mask|t1_mask).nii.gz', False)
        if not b_mask:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: brain mask not found in ' + ref_dir, 'red',
                          attrs=['bold']))
            continue
        b_mask = b_mask[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Brain Mask: ', 'green', attrs=['bold']),
              b_mask)
# ------------------------ Creating target DWI image --------------------------
        corr_4D = get_regex(s['topup'], '_corrected.nii.gz', 0, 0, 0, 0)
        if not corr_4D:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Corrected B0s not found in topup directory',
                          'red', attrs=['bold']))
            continue
        corr_4D = corr_4D[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Corrected B0s: ', 'green', attrs=['bold']),
              corr_4D)
        DWIref = changeName(corr_4D, '_mean', s['topup'])
        fslmean(corr_4D, DWIref)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Target in DWI space: ', 'green', attrs=['bold']),
              DWIref)
# ---------------------------- register DWI to T1 -----------------------------
        warp = changeName(DWIref, '_2' + options.ref, s['dwi'])
        invwarp = changeName(reference, '_2dwi', s['dwi'])
        reg = AntsRegistrationSyn(DWIref, reference, warp, invwarp, 'r',
                                  verbose=options.verbose,
                                  initial_moving_transform=0,
                                  big_image=True)
        dwi_2t1 = reg['forward_transforms']
        if len(dwi_2t1) > 1:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: There should be only 1 transformation matrix'
                          ', please make sure to use a rigid transformation fo'
                          'r the registration', 'red', attrs=['bold']))
        dwi_2t1 = dwi_2t1[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Registration matrix DWI2T1:', 'green',
                      attrs=['bold']), dwi_2t1)
# --------------------------- register bmask to DWI ---------------------------
        dwi_b_mask = changeName(b_mask, '_2dwi', s['dwi'])
        ApplyRegistration(b_mask, DWIref, dwi_2t1, dwi_b_mask, [True],
                          'NearestNeighbor')
# ------------------------------- Runnning EDDY -------------------------------
        diff_sequences = list(zip(diff_2eddy, indices_txt, bvec_2eddy,
                                  bval_2eddy))
        dtifit_dir = os.path.join(s['dwi'], 'dtifit')
        createDir(dtifit_dir)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| DTIfit directory:', 'green', attrs=['bold']),
              dtifit_dir)
        to_dti_bvecs = []
        to_dti_diffs = []
        for diff_seq in diff_sequences:
            reg_name = changeName(diff_seq[0], '_eddy', '', '', '', '')
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Doing: ' + reg_name, 'green', attrs=['bold',
                                                                  'reverse']))
            out_base = changeName(diff_seq[0], '_eddy', s['eddy'], '')
            fslEddy(diff_seq[0], dwi_b_mask, diff_seq[1], acqparams_txt,
                    diff_seq[2], diff_seq[3], fieldcoef, movpar, out_base,
                    options.verbose, options.overwrite, 7)
# ------------------------------ Quality Control ------------------------------
            """
            A Quality control should be implemented. I was a bit lazy i'll do
            it later.
            https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddyqc
            Eddy QC.
            Maybe another one ?
            """
# ----------------------------------- DTIFIT ----------------------------------
            bvec_corr = get_regex(s['eddy'], reg_name + '.eddy_rotated_bvecs',
                                  0)
            if not bvec_corr:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: Corrected Bvecs not found', 'red',
                              attrs=['bold']))
                continue
            to_dti_bvecs.append(bvec_corr[0])
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Corrected Bvecs:', 'green', attrs=['bold']),
                  bvec_corr[0])

            eddy_corr_diff = get_regex(s['eddy'], reg_name + '.nii.gz', 0)
            if not bvec_corr:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: Corrected DWI not found', 'red',
                              attrs=['bold']))
                continue
            to_dti_diffs.append(eddy_corr_diff[0])
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Eddy Corrected DWI:', 'green', attrs=['bold']),
                  eddy_corr_diff[0])
        merged_diff = os.path.join(s['merge'], '%s_%s_merged_shells.nii.gz' %
                                   (s['Id'], s['Visit']))
        fslmerge(to_dti_diffs, merged_diff)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Merged all Corrected Shells:', 'green',
                      attrs=['bold']), merged_diff)
        merged_bvec = os.path.join(s['merge'], '%s_%s_merged_shells.bvecs' %
                                   (s['Id'], s['Visit']))
        full_bvecs = [[], [], []]
        for bvec in to_dti_bvecs:
            A = read_bvec(bvec)
            for i in range(3):
                full_bvecs[i].extend(A[i])
        write_bvec(full_bvecs, merged_bvec)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Merged all Bvecs:', 'green', attrs=['bold']),
              merged_bvec)
        merged_bval = os.path.join(s['merge'], '%s_%s_merged_shells.bvals' %
                                   (s['Id'], s['Visit']))
        full_bvals = []
        for bval in bval_2eddy:
            A = read_bval(bval)
            full_bvals.extend(A)
        write_bval(full_bvals, merged_bval)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Merged all Bvals:', 'green', attrs=['bold']),
              merged_bval)
        out_base = os.path.join(dtifit_dir, '%s_%s_dti' % (s['Id'],
                                                           s['Visit']))
        print(out_base)
        dtifit(merged_diff, dwi_b_mask, merged_bvec, merged_bval,
               out_base, options.verbose, options.overwrite)
# ------------------------------ T1 registration ------------------------------
        dt12t1_dir = os.path.join(process, s['Id'], s['Visit'], 'dti2t1')
        createDir(dt12t1_dir)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| dti2t1 directory:', 'green', attrs=['bold']),
              dt12t1_dir)
        DTIs = get_regex(dtifit_dir, '.')
        for dti in DTIs:
            out_dti = changeName(dti, '_2t1', dt12t1_dir)
            ApplyRegistration(dti, reference, dwi_2t1, out_dti)
