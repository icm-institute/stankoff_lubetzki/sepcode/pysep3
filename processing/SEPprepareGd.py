#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 15:46:34 2020

@author: arya.yazdan-panah
"""

import os
import shutil
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import sys
from termcolor import cprint, colored
from common3 import createDir, init_script, changeName, get_regex
from tools_ants import AntsRegistrationSyn, ApplyRegistration
from tools_c3 import fsl2itk
from tools_fsl import (fast, conv_image, flirt_bbr, convert_xfm, apply_xfm,
                       fslmaths)
from tools_cluster import qsub3

msg = "Preparation for the GD+ contouring"


def send_cluster(subject, options):
    print(" ** Sending GD preproc:",
          subject["Id"] + "_" + subject["Visit"])
    comm = "%s -s %s/%s -r %s -p %s -R %s" % (sys.argv[0], subject["Id"],
                                              subject["Visit"], options.raw,
                                              options.process, options.ref)
    if options.over:
        comm = comm + " -o"
    logdir = os.path.join(os.environ["PROJECT_DIR"], "clulogs")
    createDir(logdir)
    name = "GD_" + subject["Id"] + "_" + subject["Visit"]
    etime = "02:00:00"
    ID = qsub3(comm, name, "normal", [], etime, "8G", True, logdir, '4')
    print(" **    Job ID:", ID)


if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=msg,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    p0 = parser.add_argument_group("Mandatory Options")
    p0.add_argument('-s', '--subjects', dest='subjects',
                    help='Subjects list, if none choices will be provided')
    p0.add_argument("-r", "--raw", dest="raw",
                    help="Raw directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-p", "--process", dest="process",
                    help="Process directory, if none deduced from PROJECT_DIR")
    p0.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                    default=False, help='Make the program talkative')
    p0.add_argument("-q", "--qsub", action="store_true", dest="qsub")
    p1 = parser.add_argument_group("Advanced Options")
    p1.add_argument("-R", "--Reference", dest="ref", default='t1',
                    help="Reference Image")
    p1.add_argument("-o", "--overwrite", dest="over", action="store_true",
                    help="Overwrite existing Contouring preparation")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
    if options.ref == 't1':
        ref_dir = 't1preproc'
        regex_ref = '_t1_N4$'
    elif options.ref == 'mp2rage':
        ref_dir = 'mp2ragepreproc'
        regex_ref = '_mp2rage_uni_denoised$'
    if options.qsub:
        options.raw = raw
        options.process = process
        for s in subjects:
            send_cluster(s, options)
        sys.exit()
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# ------------------------ Defining path to t1preproc -------------------------
        t1preproc = os.path.join(process, s['Id'], s['Visit'], ref_dir)
        if not os.path.isdir(t1preproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + t1preproc + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| t1preproc: ', 'green', attrs=['bold']),
              t1preproc)
# ------------------------- Retrieving T1_N4 sequence -------------------------
        N4 = get_regex(t1preproc, regex_ref)
        if not N4:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1 NOT FOUND', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        N4 = N4[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found T1 seq: ', 'green', attrs=['bold']), N4)
# ---------------------- Retrieving input sequence T1preGD --------------------
        seqdir = os.path.join(raw, s['Id'], s['Visit'], 't1pregd')
        pregd = get_regex(seqdir, '_t1pregd.nii.gz', False,)
        if not pregd:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: t1pregd not found, unable to'
                          ' register it on T1', 'red', attrs=['bold']))
            continue
        pregd = pregd[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found t1pregd seq: ', 'green',
                      attrs=['bold']), pregd)
# --------------------------- Retrieving Brain Mask ---------------------------
        b_mask = get_regex(t1preproc, '_brain_mask.nii.gz', False)
        if not b_mask:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: brain mask not found in ' + t1preproc, 'red',
                          attrs=['bold']))
            continue
        b_mask = b_mask[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Brain Mask: ', 'green', attrs=['bold']),
              b_mask)
# -------------------- Defining path to contouringpreproc ---------------------
        gdpreproc = os.path.join(process, s['Id'], s['Visit'], 'gdpreproc')
        if options.over:
            shutil.rmtree(gdpreproc)
        createDir(gdpreproc)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| gdpreproc: ', 'green', attrs=['bold']), gdpreproc)
        trans_mat = os.path.join(gdpreproc, 'trans_mat')
        createDir(trans_mat)
# ----------------------------- processing preGD ------------------------------
        seq_2N4 = changeName(pregd, '_2' + options.ref, gdpreproc)
        N4_2seq = changeName(N4, '_2t1pregd', gdpreproc)
        AntsRegistrationSyn(pregd, N4, seq_2N4, N4_2seq, 'r', b_mask, 0, True,
                            6, verbose=options.verbose)
# ---------------------- Retrieving input sequence T1GD -----------------------
        seqdir = os.path.join(raw, s['Id'], s['Visit'], 't1gd')
        gd = get_regex(seqdir, '_t1gd.nii.gz', False,)
        if not gd:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: t1gd not found, unable to'
                          ' register it on T1', 'red', attrs=['bold']))
            continue
        gd = gd[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found t1gd seq: ', 'green',
                      attrs=['bold']), gd)
# ----------------------------- processing GD ---------------------------------
        seq_2pregd = changeName(gd, '_2t1pregd', gdpreproc)
        pregd_2seq = changeName(pregd, '_2t1gd', gdpreproc)
        AntsRegistrationSyn(gd, pregd, seq_2pregd, pregd_2seq, 'r', False, 0,
                            True, 6, verbose=options.verbose)
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
