#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 11:52:05 2020

@author: arya.yazdan-panah
"""

import os
import shutil
import sys
from tools_c3 import computeMTR
from tools_ants import AntsRegistrationSyn
from termcolor import cprint, colored
from common3 import get_regex, createDir, init_script, changeName
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

MSG = "Register MTON, MTOFF on T1 and compute the MTR"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-r", "--raw", dest="raw",
                        help="Raw directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-o", "--overwrite", dest='overwrite',
                        help='overwrite', action="store_true")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        s['raw'] = os.path.join(raw, s['Id'], s['Visit'])
        s['process'] = os.path.join(process, s['Id'], s['Visit'])
        s['mtr_dir'] = os.path.join(s['process'], 'prepareMTR')
        if options.overwrite and os.path.isdir(s['mtr_dir']):
            shutil.rmtree(s['mtr_dir'])
        createDir(s['mtr_dir'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| MTR directory:', 'green', attrs=['bold']),
              s['mtr_dir'])
# ----------------------------- Recover raw images ----------------------------
        test = get_regex(s['raw'], 'mton.nii.gz', False, True, False, False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: MTON SEQUENCE NOT FOUND IN RAW', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        mton = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('Found MTON seq:', 'green', attrs=['bold']), mton)
        test = get_regex(s['raw'], 'mtoff.nii.gz', False, True, False, False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: MTOFF SEQUENCE NOT FOUND IN RAW', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        mtoff = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('Found MTOFF seq:', 'green', attrs=['bold']), mtoff)
# -------------------------- Recover T1 and bmask -----------------------------
        t1preproc = os.path.join(s['process'], 't1preproc')
        if not os.path.isdir(t1preproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: t1preproc not found', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('t1preproc:', 'green', attrs=['bold']), t1preproc)
        test = get_regex(t1preproc, 't1_N4.nii.gz', False, True, False, False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1 SEQUENCE NOT FOUND IN t1preproc', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        t1 = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('Found T1 seq:', 'green', attrs=['bold']), t1)
        test = get_regex(t1preproc, 'brain_mask.nii.gz', False, True, False,
                         False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1 BRAIN MASK NOT FOUND IN t1preproc', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        mask = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('Found brain mask:', 'green', attrs=['bold']), mask)
# ---------------------------- Register mtr on T1 -----------------------------
        warp = changeName(mton, '_2t1', s['mtr_dir'])
        invwarp = changeName(t1, '_2mton', s['mtr_dir'])
        regon = AntsRegistrationSyn(mton, t1, warp, invwarp, 'r', mask)
        warp = changeName(mtoff, '_2t1', s['mtr_dir'])
        invwarp = changeName(t1, '_2mtoff', s['mtr_dir'])
        regoff = AntsRegistrationSyn(mtoff, t1, warp, invwarp, 'r', mask)
        mton = regon['warped_image']
        mtoff = regoff['warped_image']
# -------------------------------- compute MTR --------------------------------
        mtr = os.path.join(s['mtr_dir'], s['Id'] + '_' + s['Visit'] +
                           '_mtr_2t1.nii.gz')
        computeMTR(mton, mtoff, mtr, s['mtr_dir'])
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
cprint('|', 'blue', attrs=['bold'])
cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
print('')
