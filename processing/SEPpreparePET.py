#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 11:10:26 2020

@author: arya.yazdan-panah
"""

import os
import nibabel as nib
import shutil
import sys
from common3 import init_script, get_regex, createDir, changeName
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from termcolor import colored, cprint
# from tools_spm import spmRealign
from tools_piwave import piw_reslice
# from tools_nibabel import flip_nii
from tools_fsl import flirt, convert_xfm, fslmean, fslmaths
from tools_ants import AntsRegistrationSyn
from tools_c3 import fsl2itk

MSG = "Perform motion correction and registration of PET on T1"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-r", "--raw", dest="raw",
                        help="Raw directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-t", "--tracer", dest="tracer", default="pib",
                        help="Tracer used (dpa/pib/...)")
    parser.add_argument("-I", "--initial_frame", dest="initframe", default=2,
                        help="Initial frame considered for realignement, "
                        "counting starts at 1", type=int)
    parser.add_argument("-T", "--target_frame", dest="targframe", default=6,
                        help="Frame considered for realignement of other "
                        "frames on. Consider a frame where the signal is high "
                        "and brain well distinguishable. Counting starts at 1",
                        type=int)
    parser.add_argument("-F", "--Final_frame", dest="finalframe",
                        default=False,
                        help="Final frame considered for realignement on other"
                        " frames. Last one by default. Counting starts at 1",
                        type=int)
    parser.add_argument("-m", "--method", dest="method", default="FSL",
                        help="Method used for PET->T1 registraction "
                        "(FSL/ANTs)")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
    tracer = options.tracer.lower()
    method = options.method
    print(colored("| Tracer used:", "blue", attrs=["bold"]),
          tracer.upper())
    target_frame = options.targframe
    init_frame = options.initframe
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# ------------------------- Retrieving DPA sequence ---------------------------
        s['raw'] = os.path.join(raw, s['Id'], s['Visit'])
        s['pet'] = get_regex(s['raw'], tracer + '.nii.gz', False)
        if not s['pet']:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR:' + tracer.upper() + ' NOT FOUND', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        s['pet'] = s['pet'][0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('Found ' + tracer.upper() + ' seq:', 'green',
                      attrs=['bold']), s['pet'])
# ----------------------- Defining path to petpreproc -------------------------
        petpreproc = os.path.join(process, s['Id'], s['Visit'], tracer +
                                  'preproc')
        createDir(petpreproc)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory ' + tracer + 'preproc created/existing:',
                      'green', attrs=['bold']), petpreproc)
# ----------------------- Defining path to t1preproc --------------------------
        t1preproc = os.path.join(process, s['Id'], s['Visit'], 't1preproc')
        if not os.path.isdir(t1preproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1PREPROC NOT FOUND', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
# ------------------------- Retrieving T1 sequence ----------------------------
        test = get_regex(t1preproc, s['Visit'] + "_t1_(N4|pre).nii.gz", False, False, False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1 SEQUENCE NOT FOUND IN:', 'red',
                          attrs=['bold']), t1preproc)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        s["t1"] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('Found T1 seq:', 'green', attrs=['bold']), s["t1"])
# ------------------------------ Realigning PET -------------------------------
        tmp = nib.load(s['pet'])
        if len(tmp.shape) < 4:
            mean_pet = changeName(s['pet'], outdir=petpreproc)
            shutil.copyfile(s['pet'], mean_pet)
        else:
            if options.finalframe:
                final_frame = options.finalframe
            else:
                final_frame = tmp.shape[3]
            realigned = piw_reslice(s['pet'], target_frame, init_frame,
                                    final_frame, petpreproc)
# =============================================================================
# ----------------------- Correcting Left-Right flip --------------------------
#           bad_realign = changeName(realigned, '_2flip')
#             if os.path.isfile(bad_realign):
#                 print(colored('|', 'blue', attrs=['bold']),
#                       colored('| Skipped flipping, already done', 'green',
#                               attrs=['bold']))
#             else:
#                 os.rename(realigned, bad_realign)
#                 flipped = flip_nii(bad_realign, 0, suffix='tmp')
#                 os.rename(flipped, realigned)
# =============================================================================
# ----------------------- Removing nanss --------------------------------------
            fslmaths(realigned, '-nan', realigned)
# --------------------------------- Mean PET ----------------------------------
            mean_pet = changeName(realigned, '_mean')
            if not os.path.isfile(mean_pet):
                fslmean(realigned, mean_pet)
            else:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Skipped Averaging, already done', 'green',
                              attrs=['bold']))
# ------------------------------ Register on T1 -------------------------------
        pet_2t1 = changeName(mean_pet, suffix='_2t1', outdir=petpreproc)
        t1_2pet = changeName(s["t1"], suffix='_2' + tracer, outdir=petpreproc)
        trans_mat = os.path.join(petpreproc, 'trans_mat')
        createDir(trans_mat)
        pet_2t1_mat = changeName(pet_2t1, newext='.mat', outdir=trans_mat)
        t1_2pet_mat = changeName(t1_2pet, newext='.mat', outdir=trans_mat)
        pet_2t1_itk = changeName(pet_2t1, suffix='_ITK', newext='.mat',
                                 outdir=trans_mat)
        if (os.path.isfile(pet_2t1_mat) and os.path.isfile(t1_2pet_mat) and
                os.path.isfile(pet_2t1) and os.path.isfile(t1_2pet)):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Skipped Registration, already done', 'green',
                          attrs=['bold']))
        else:
            if method.lower() == 'fsl':
                flirt(mean_pet, s['t1'],
                      searchr_x=[-90, 90],
                      searchr_y=[-90, 90],
                      searchr_z=[-90, 90],
                      dof=6, out_matrix_file=pet_2t1_mat,
                      out_file=pet_2t1, cost='mutualinfo')
                convert_xfm(pet_2t1_mat, t1_2pet_mat)
                flirt(s["t1"], mean_pet, apply_xfm=True,
                      in_matrix_file=t1_2pet_mat, out_file=t1_2pet,
                      out_matrix_file=t1_2pet_mat)
                itk_mat = fsl2itk(pet_2t1_mat, s['t1'], mean_pet)
            elif method.lower() == 'ants':
                # option ants
                regpet = AntsRegistrationSyn(mean_pet, s['t1'], pet_2t1,
                                             t1_2pet, 'r')
                tmp_pet_2t1_mat = regpet['forward_transforms'][0]
                shutil.move(tmp_pet_2t1_mat, pet_2t1_itk)
                shutil.rmtree(os.path.dirname(tmp_pet_2t1_mat))
                # convert_matrix_to_fsl
            else:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: method ' + method + ' NOT IMPLEMENTED',
                              'red', attrs=['bold']))
                print(colored('|', 'blue', attrs=['bold']),
                      colored('L', 'green', attrs=['bold']))
                continue
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
