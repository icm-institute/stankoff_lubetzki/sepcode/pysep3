#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 13:41:02 2020

@author: arya.yazdan-panah
"""


import os
import sys
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from common3 import init_script, createDir, get_regex, changeName
from tools_ants import AntsRegistrationSyn, ApplyRegistration

MSG = "Register QSM iMag on T1 and all other QSM images afterwards"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-r", "--raw", dest="raw",
                        help="Raw directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    options = parser.parse_args()
# ---------------------------------- options ----------------------------------
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# ------------------------ Defining path to t1preproc -------------------------
        t1preproc = os.path.join(process, s['Id'], s['Visit'], 't1preproc')
        if not os.path.isdir(t1preproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + t1preproc + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| t1preproc: ', 'green', attrs=['bold']),
              t1preproc)
# ------------------------- Retrieving T1_N4 sequence -------------------------
        N4 = get_regex(t1preproc, '_t1_N4.nii.gz', False)[0]
        if not N4:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1 NOT FOUND', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found T1 seq: ', 'green', attrs=['bold']), N4)
# ----------------------------- fetching all QSMs -----------------------------
        s["process"] = os.path.join(process, s['Id'], s['Visit'])
        s["raw"] = os.path.join(raw, s['Id'], s['Visit'])
        qsm_dir = get_regex(s["process"], "qsm$", False)
        if not qsm_dir:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', "green", attrs=['bold']),
                  colored('QSM images not stored in process, looking in raw',
                          'yellow', attrs=['bold']))
            qsm_dir = get_regex(s["raw"], "qsm$", False)
            if not qsm_dir:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', "green", attrs=['bold']),
                      colored('QSM images not stored in raw', 'red',
                              attrs=['bold']))
                continue
        qsm_dir = qsm_dir[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| found QSM dircetory:', 'green', attrs=['bold']),
              qsm_dir)
# ------------------------------ retrieving iMag ------------------------------
        iMag = get_regex(qsm_dir, "iMag.nii.gz", ignore_extension=False,
                         casensitive=False)
        if not iMag:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', "green", attrs=['bold']),
                  colored('iMag image not found, registration impossible',
                          'red', attrs=['bold']))
            continue
        iMag = iMag[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found iMag seq: ', 'green', attrs=['bold']), iMag)
# ------------------------- Defining path to preproc --------------------------
        qsmpreproc = os.path.join(process, s['Id'], s['Visit'], "qsmpreproc")
        createDir(qsmpreproc)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| qsmpreproc:', 'green', attrs=['bold']), qsmpreproc)
# ----------------------------- registering iMag ------------------------------
        iMag_reg = AntsRegistrationSyn(iMag, N4, qsmpreproc, 'r', imt=1, nt=16)
        if os.path.isfile(iMag_reg["inverse_warped_image"]):
            os.remove(iMag_reg["inverse_warped_image"])
# ----------------------------- retrieving others -----------------------------
        imgs = get_regex(qsm_dir, "_QSM.(?!iMag)", casensitive=False)
        imgs.extend(get_regex(qsm_dir, "QSM$", casensitive=False))
        if not imgs:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', "green", attrs=['bold']),
                  colored('No other QSM than iMag', 'red', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Images to register found:', 'green', attrs=['bold']),
              len(imgs))
# ---------------------------- registering others -----------------------------
        for qsm in imgs:
            warp = changeName(qsm, '_2t1', qsmpreproc)
            ApplyRegistration(qsm, N4, iMag_reg['forward_transforms'], warp,)
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
