#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 24 14:35:05 2021

@author: arya.yazdan-panah
"""

import os
import sys
import nibabel as nib
from subprocess import Popen, PIPE
from termcolor import cprint, colored
from common3 import get_regex, createDir, init_script, changeName
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from tools_diffusion import (nii2mif, dwi2response, dwi2fod, gen5tt,
                             gmwm_interface, tractography, down_sample_tck)
from tools_cluster import qsub3
from tools_ants import ApplyRegistration


def send_cluster(subject, options):
    print(" ** Sending tractography:", subject["Id"] + "_" + subject["Visit"])
    comm = sys.argv[0]
    comm = comm + " -s %s/%s" % (subject["Id"], subject["Visit"])
    comm = comm + " -r " + options.raw
    comm = comm + " -p " + options.process
    comm = comm + " -L " + options.lesion_image
    comm = comm + " -A " + options.algo
    comm = comm + " -R " + options.ref
    if options.not_shelled:
        comm = comm + ' -S'
    logdir = os.path.join(os.environ["PROJECT_DIR"], "TCK_clulogs")
    createDir(logdir)
    name = "TCK_" + subject["Id"] + "_" + subject["Visit"]
    etime = "96:00:00"
    ID = qsub3(comm, name, "normal", [], etime, "8G", True, logdir, '4')
    print(" **    Job ID:", ID)


MSG = "Implementation of global tractography for processing eddy corrected DWI"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    p0 = parser.add_argument_group("Mandatory Options")
    p0.add_argument('-s', '--subjects', dest='subjects',
                    help='Subjects list, if none choices will be provided')
    p0.add_argument("-r", "--raw", dest="raw",
                    help="Raw directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-p", "--process", dest="process",
                    help="Process directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-q", "--qsub", action="store_true", dest="qsub")
    p1 = parser.add_argument_group("Advanced Options")
    p1.add_argument("-L", "--lesion-mask", dest="lesion_image",
                    default='flair',
                    help="Process directory, if none deduced from PROJECT_DIR")
    p1.add_argument("-S", "--not-shelled", dest="not_shelled",
                    action="store_true", help="Option if data is not shelled")
    p1.add_argument("-A", "--algo-tt", dest="algo", default='hsvs')
    parser.add_argument("-R", "--Reference", dest="ref", default='t1',
                        help="Reference Image")
    options = parser.parse_args()
    init = init_script(options)
    data = init['data']
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
    if options.qsub:
        options.raw = raw
        options.process = process
        for s in subjects:
            send_cluster(s, options)
        sys.exit()
    if options.ref == 't1':
        ref_dir = 't1preproc'
        regex_ref = '_t1_N4$'
    elif options.ref == 'mp2rage':
        ref_dir = 'mp2ragepreproc'
        regex_ref = '_mp2rage_uni_denoised$'
# --------------------------------- Lesions -----------------------------------
    lesion_image = str(options.lesion_image).lower()
    if lesion_image == 't2':
        lesion_mask_regex = '_t2_les_manual$'
        mat_regex = '(_t2_2(mp2rage|t1_N4|t1)_ITK|_t2_to_t1_0GenericAffine)'
        mat_2t1_fold = 'contouringpreproc'
    elif lesion_image == 'flair':
        lesion_mask_regex = '_flair_les_manual$'
        mat_regex = '_flair_2mp2rage0GenericAffine'
        mat_2t1_fold = 'contouringpreproc'
    elif lesion_image == 'flair2':
        lesion_mask_regex = '_flair_les_manual$'
        mat_regex = '_flair_2t10GenericAffine'
        mat_2t1_fold = 'flairpreproc'
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('ERROR: Image chosen not implemented', 'red',
                      attrs=['bold']))
        sys.exit(1)
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        s['process'] = os.path.join(process, s['Id'], s['Visit'])
        s['dwi'] = os.path.join(s['process'], 'dwipreproc_denoise')
        if not os.path.isdir(s['dwi']):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: There should be a dwipreproc in process'
                          'please make sure the DWI preprocessing did run',
                          'red', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| dwipreproc directory:', 'green', attrs=['bold']),
              s['dwi'])
# ---------------------------- Recover Brain Mask -----------------------------
        brain_mask = get_regex(s['dwi'], 'brain_mask_2dwi')
        if not brain_mask:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Brain mask not found', 'red',
                          attrs=['bold']))
            continue
        brain_mask = brain_mask[0]
# ---------------------------- Recover Eddy images ----------------------------
        s['merge'] = os.path.join(s['dwi'], 'merged')
        if not os.path.isdir(s['merge']):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Merged directory not found', 'red',
                          attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory where merged shells are stored:', 'green',
                      attrs=['bold']), s['merge'])
        dwi = get_regex(s['merge'], 'merged_shells.nii.gz', 0)
        if not dwi:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Merged eddy corrected diffusion not found',
                          'red', attrs=['bold']))
            continue
        dwi = dwi[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Merged eddy corrected diffusion:', 'green',
                      attrs=['bold']), dwi)
        bvals = changeName(dwi, newext='.bvals')
        if not os.path.isfile(bvals):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Merged bvals not found', 'red',
                          attrs=['bold']))
            continue
        bvecs = changeName(dwi, newext='.bvecs')
        if not os.path.isfile(bvecs):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: Merged bvecs not found', 'red',
                          attrs=['bold']))
            continue
# ------------------------ Convert everything to .MIF -------------------------
        mifdwi = nii2mif(dwi, bvecs, bvals)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| DWI converted to .mif for mrtrix:', 'green',
                      attrs=['bold']), mifdwi)
# ----------------------- Estimating response function ------------------------
        s['respfun'] = os.path.join(s['dwi'], 'response_function')
        createDir(s['respfun'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Response Function Directory:', 'green',
                      attrs=['bold']), s['respfun'])
        wm_rf = changeName(mifdwi, '_wm_RF', s['respfun'], '.txt')
        gm_rf = changeName(mifdwi, '_gm_RF', s['respfun'], '.txt')
        csf_rf = changeName(mifdwi, '_csf_RF', s['respfun'], '.txt')
        voxels_rf = changeName(mifdwi, '_RF_voxels', s['respfun'])
        dwi2response(mifdwi, wm_rf, gm_rf, csf_rf, voxels_rf, brain_mask)
        # Maybe the msmt_5TT algo could be better here, this has to be deepened
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Command for the quality check of the response functio'
                      'ns:', 'green', attrs=['bold']),
              'shview PATH/TO/resp.txt')
# ------------------ Apply Basis function to diffusion data -------------------
        s['fod'] = os.path.join(s['dwi'], 'FOD')
        createDir(s['fod'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Fiber Orientation Densities Directory:', 'green',
                      attrs=['bold']), s['fod'])
        wm_fod = changeName(mifdwi, '_wm_FOD', s['fod'], '.mif')
        gm_fod = changeName(mifdwi, '_gm_FOD', s['fod'], '.mif')
        csf_fod = changeName(mifdwi, '_csf_FOD', s['fod'], '.mif')
        if options.not_shelled:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('WARNING: This options has never been tested, do not'
                          ' run before consulting with an engineer', 'yellow',
                          attrs=['bold']))
            dwi2fod(mifdwi, wm_rf, gm_rf, csf_rf, wm_fod, gm_fod, csf_fod,
                    brain_mask, 'csd')
        else:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Data is shelled, using msmt_csd', 'green',
                          attrs=['bold']))
            dwi2fod(mifdwi, wm_rf, gm_rf, csf_rf, wm_fod, gm_fod, csf_fod,
                    brain_mask, 'msmt_csd')
# ------------------------ Merging FODs in single file ------------------------
        vf = changeName(mifdwi, '_vf', s['fod'], '.mif')
        if not os.path.isfile(vf):
            c = 'mrconvert -quiet -coord 3 0 ' + wm_fod
            c = c + ' -| mrcat -quiet %s %s - %s' % (csf_fod, gm_fod, vf)
            Popen(c, stdout=PIPE, shell=True)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| FODs combined into single file:', 'green',
                      attrs=['bold']), vf)
# ----------------------- Creating 5 Tissue Type Image ------------------------
        s['5tt'] = os.path.join(s['dwi'], '5TTtest_fsl')
        createDir(s['5tt'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('| 5tt directory:', 'green', attrs=['bold']), s['5tt'])
        if options.algo.lower() == 'hsvs':
            s['5ttin'] = os.path.join(data, 'freesurfer',
                                      s['Id'] + '_' + s['Visit'])
            if not os.path.isdir(s['5ttin']):
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: FreeSurfer directory not found', 'red',
                              attrs=['bold']))
                continue
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| FreeSurfer directory:', 'green', attrs=['bold']),
                  s['5ttin'])
        elif options.algo.lower() == 'fsl':
            t1preproc = os.path.join(process, s['Id'], s['Visit'], ref_dir)
            if not os.path.isdir(t1preproc):
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: ' + ref_dir + ' not found', 'red',
                              attrs=['bold']))
                print(colored('|', 'blue', attrs=['bold']),
                      colored('L', 'green', attrs=['bold']))
                continue
            leinp = os.path.join(process, s['Id'], s['Visit'], 'leinp')
            look_for_N4 = False
            if not os.path.isdir(leinp):
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('Warning: leinp  not found', 'yellow',
                              attrs=['bold']))
                look_for_N4 = True
            else:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Found leinp: ', 'green', attrs=['bold']),
                      leinp)
                inpainted = get_regex(leinp, "inpainted.nii.gz",
                                      ignore_extension=False)
                if not inpainted:
                    look_for_N4 = True
                else:
                    s["5ttin"] = inpainted[0]
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('| Inpainted anat: ', 'green',
                                  attrs=['bold']), s["5ttin"])
            if look_for_N4:
                t1_N4 = get_regex(t1preproc, regex_ref)
                if not t1_N4:
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('|', 'green', attrs=['bold']),
                          colored('ERROR: ' + regex_ref + ' not found', 'red',
                                  attrs=['bold']))
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('L', 'green', attrs=['bold']))
                    continue
                else:
                    s["5ttin"] = t1_N4[0]
            if not os.path.isfile(s['5ttin']):
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: Anatomical image not found', 'red',
                              attrs=['bold']))
                continue
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Anatomical image:', 'green', attrs=['bold']),
                  s['5ttin'])
        five_tt = changeName(mifdwi, '_5ttgen', s['5tt'], '.nii.gz')
        gen5tt(s['5ttin'], five_tt, algorithm=options.algo.lower())
# -------------------------- Registering 5TT to dwi ---------------------------
        trans_mat = os.path.join(s['dwi'], 'trans_mat')
        if not os.path.isdir(trans_mat):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: trans_mat directory not found', 'red',
                          attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| trans_mat directory:', 'green', attrs=['bold']),
              trans_mat)
        test = get_regex(trans_mat, '.mat', 0)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: DWI registration matrix not found', 'red',
                          attrs=['bold']))
            continue
        dwi2t1mat = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| DWI registration matrix:', 'green', attrs=['bold']),
              dwi2t1mat)
        test = get_regex(s['dwi'], '_B0s_corrected_mean.nii.gz', 0)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: DWI reference image not found', 'red',
                          attrs=['bold']))
            continue
        ref = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| DWI reference image:', 'green', attrs=['bold']), ref)
        ft2dwi = changeName(five_tt, '_2dwi')
        ApplyRegistration(five_tt, ref, dwi2t1mat, ft2dwi, [True], is_4D=True)
# ------------ Adding Lesions to pathological tissues for patients ------------
        if s['Id'][0].upper() == 'P':
            lesdir = os.path.join(s['process'], 'lesions_manual')
            if not os.path.isdir(lesdir):
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: Lesion Dir not found, processing without'
                              ' lesions', 'red', attrs=['bold']))
                continue
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Manual Lesion Directory:', 'green',
                          attrs=['bold']), lesdir)
            test = get_regex(lesdir, 'les_manual.nii.gz', 0)
            if not test:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: lesion_mask not found',
                              'red', attrs=['bold']))
                continue
            lesion_mask = test[0]
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Lesions:', 'green', attrs=['bold']), lesion_mask)
            matrix_fold = os.path.join(process, s['Id'], s['Visit'],
                                       mat_2t1_fold)
            if not os.path.isdir(matrix_fold):
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: Matrix folder not found', 'red',
                              attrs=['bold']))
                continue
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Lesions matrix folder:', 'green', attrs=['bold']),
                  matrix_fold)
            test = get_regex(matrix_fold, mat_regex)
            if not test:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: Matrix 2T1 not found', 'red',
                              attrs=['bold']))
                continue
            matlesion2t1 = test[0]
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Lesions matrix:', 'green', attrs=['bold']),
                  matlesion2t1)
            lm2dwi = changeName(lesion_mask, '_2dwi', s['dwi'])
            ApplyRegistration(lesion_mask, ref, [dwi2t1mat, matlesion2t1],
                              lm2dwi, [True, False], 'NearestNeighbor')
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Adding Pathological Tissues...', 'green',
                          attrs=['bold']))
            LM = nib.load(lm2dwi).get_fdata().astype(bool)
            img = nib.load(ft2dwi)
            affine = img.affine
            data_mat = img.get_fdata()
            patho_tissue = data_mat[..., 4]
            patho_tissue[LM] = 1
            data_mat[..., 4] = patho_tissue
            img = nib.Nifti1Image(data_mat, affine)
            nib.save(img, ft2dwi)
# ------------------------ Creating Tissue Boundaries -------------------------
        tract_seed = changeName(mifdwi, '_tracto_seed', s['5tt'], '.mif')
        gmwm_interface(ft2dwi, tract_seed)
# -------------------------- Doing the Tractography ---------------------------
        tckdir = os.path.join(s['dwi'], 'tractography_fsl_5tt')
        createDir(tckdir)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| tractography directory:', 'green', attrs=['bold']),
              tckdir)
        tracks = changeName(mifdwi, '_tractography', tckdir, '.tck')
        seeds = changeName(mifdwi, '_seeds', tckdir, '.txt')
        tractography(wm_fod, ft2dwi, tract_seed, tracks, seeds, brain_mask,
                     250, 0.06, 10000000, 4)
        downtck = changeName(tracks, '_downsampled')
        down_sample_tck(tracks, downtck)

# =============================================================================
# tckgen -act 5tt_coreg.mif -backtrack -seed_gmwmi gmwmSeed_coreg.mif -nthreads
# 8 -maxlength 250 -cutoff 0.06 -select 10000000 wmfod_norm.mif tracks_10M.tck
# =============================================================================
