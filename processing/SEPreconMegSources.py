#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  6 15:26:20 2020

@author: arya.yazdan-panah
"""

import os
import numpy as np
import nibabel as nib
import mne
import sys
from termcolor import cprint, colored
from common3 import init_script, createDir, get_regex, seq_info, changeName
from optparse import OptionParser
np.seterr(all='ignore')


if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = 'usage: %prog'
    parser = OptionParser(usage=usage)
    parser.add_option('-s', '--subjects', dest='subjects',
                      help='Subjects list')
    parser.add_option("-p", "--process", dest="process",
                      help="Process directory")
    parser.add_option("-d", "--freesurfer", dest="freesurfer",
                      help=" Directory where freesurfer stores images")
    parser.add_option("-k", "--skip", dest="skip", default=False,
                      help=" Directory where freesurfer stores images")
    (options, args) = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
    freesurfer_dir = init['freesurfer']
    skip = options.skip
    overwrite = not skip
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        fsid = '_'.join([s['Id'], s['Visit']])
        # mne.bem.make_watershed_bem(fsid, freesurfer_dir, overwrite=True,
        #                            verbose=True)
        mne.viz.plot_bem(fsid, freesurfer_dir, brain_surfaces='white',
                         orientation='coronal')
        break































