#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  6 10:54:43 2020

@author: arya.yazdan-panah
"""


import os
import sys
from termcolor import cprint, colored
from common3 import get_regex, createDir, init_script, changeName
from tools_ants import ApplyRegistration
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

MSG = "Register a given image to the HWS specified and already created"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-r", "--raw", dest="raw",
                        help="Raw directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-i", "--image", dest="image", default=None,
                        help="Image to Register to HWS")
    parser.add_argument("-w", "--hws", dest="hws", default="ALL",
                        help="HWS onto which register the previous image")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
# ----------------------------- Finding Images --------------------------------
    img = options.image.lower()
    print(img)
    if img == 'flair':
        regex_mat = "_flair_to_t1_0GenericAffine.mat"
        dir_img = raw
        regex_img = "flair.nii.gz"
        meth = 'Linear'
        hws_out_dir = 'FLAIR'
    elif img == 'flair2':
        regex_mat = "flairpreproc2/trans_mat/.*flair_2t10GenericAffine.mat"
        dir_img = raw
        regex_img = "flair.nii.gz"
        meth = 'Linear'
        hws_out_dir = 'flair2'
    elif img == 'mtr':
        regex_mat = False
        dir_img = process
        regex_img = "prepareMTR/.*mtr_2t1.nii.gz"
        meth = 'Linear'
        hws_out_dir = 'mtr'
    elif img == 'mask_flair':
        regex_mat = "flair_2t10GenericAffine.mat"
        dir_img = process
        regex_img = "manual_lesion/.*flair_label.nii.gz"
        meth = 'NearestNeighbor'
        hws_out_dir = 'manual_lesion'
    elif img == 'mask_flair_gado':
        regex_mat = "flair_2t10GenericAffine.mat"
        dir_img = process
        regex_img = "manual_lesion/.*flair_gado_label.nii.gz"
        meth = 'NearestNeighbor'
        hws_out_dir = 'manual_lesion'
    elif img == 'mask_flair_ngado':
        regex_mat = "flair_2t10GenericAffine.mat"
        dir_img = process
        regex_img = "manual_lesion/.*flair_ngado_label.nii.gz"
        meth = 'NearestNeighbor'
        hws_out_dir = 'manual_lesion'
# =============================================================================
    elif img == 'mask_flair2':
        regex_mat = "flairpreproc2/trans_mat/.*flair_2t10GenericAffine.mat"
        dir_img = process
        regex_img = "manual_lesion/.*flair_label.nii.gz"
        meth = 'NearestNeighbor'
        hws_out_dir = 'manual_lesion3'
    elif img == 'mask_flair_gado2':
        regex_mat = "flairpreproc2/trans_mat/.*flair_2t10GenericAffine.mat"
        dir_img = process
        regex_img = "manual_lesion/.*flair_gado_label.nii.gz"
        meth = 'NearestNeighbor'
        hws_out_dir = 'manual_lesion3'
    elif img == 'mask_flair_ngado2':
        regex_mat = "flairpreproc2/trans_mat/.*flair_2t10GenericAffine.mat"
        dir_img = process
        regex_img = "manual_lesion/.*flair_ngado_label.nii.gz"
        meth = 'NearestNeighbor'
        hws_out_dir = 'manual_lesion3'
# =============================================================================
    elif img == 'wm_mask':
        regex_mat = False
        dir_img = process
        regex_img = "t1preproc/fast/.*_t1_N4_brain_seg_2.nii.gz"
        meth = 'NearestNeighbor'
        hws_out_dir = 'T1/ROI'
    elif img == "fa":
        regex_mat = 'dwipreproc/trans_mat/.*_B0s_corrected_mean_2t10GenericAffine.mat'
        dir_img = process
        regex_img = "dwipreproc/dtifit/.*_dti_FA.nii.gz"
        meth = 'Linear'
        hws_out_dir = 'DTI'
    elif img == "md":
        regex_mat = 'dwipreproc/trans_mat/.*_B0s_corrected_mean_2t10GenericAffine.mat'
        dir_img = process
        regex_img = "dwipreproc/dtifit/.*_dti_MD.nii.gz"
        meth = 'Linear'
        hws_out_dir = 'DTI'
    elif img == "l1":
        regex_mat = 'dwipreproc/trans_mat/.*_B0s_corrected_mean_2t10GenericAffine.mat'
        dir_img = process
        regex_img = "dwipreproc/dtifit/.*_dti_L1.nii.gz"
        meth = 'Linear'
        hws_out_dir = 'DTI'
    elif img == "l2":
        regex_mat = 'dwipreproc/trans_mat/.*_B0s_corrected_mean_2t10GenericAffine.mat'
        dir_img = process
        regex_img = "dwipreproc/dtifit/.*_dti_L2.nii.gz"
        meth = 'Linear'
        hws_out_dir = 'DTI'
    elif img == "l3":
        regex_mat = 'dwipreproc/trans_mat/.*_B0s_corrected_mean_2t10GenericAffine.mat'
        dir_img = process
        regex_img = "dwipreproc/dtifit/.*_dti_L3.nii.gz"
        meth = 'Linear'
        hws_out_dir = 'DTI'
    else:
        cprint("ERROR: Sequence to HWS not implemented yet: " + img, 'red',
               attrs=['bold'])
        sys.exit(2)
    print(colored("| Registering Sequence:", 'blue', attrs=['bold']), img)
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        s['process'] = os.path.join(process, s['Id'], s['Visit'])
# ----------------------------------- HWS  ------------------------------------
        s['Id_dir'] = os.path.join(process, s['Id'])
        if options.hws == 'ALL':
            test = get_regex(s['Id_dir'], '^V\d*$', deep_search=False,
                             full=False)
            test.sort()
            reg_hws = ''.join([os.path.basename(x) for x in test])
        else:
            reg_hws = options.hws
# -------------------------------- HWS dir ------------------------------------
        test = get_regex(s['Id_dir'], 'fs_' + reg_hws)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR:' + reg_hws + ' hws not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        s['hws_dir'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| HWS_dir:', 'green', attrs=['bold']), s['hws_dir'])
# -------------------------------- HWS img ------------------------------------
        test = get_regex(s['hws_dir'], reg_hws + '.nii.gz',  False, False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR:' + reg_hws + ' hws image not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        hws = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| HWS image:', 'green', attrs=['bold']), hws)
# ----------------------------- HWS transfos ----------------------------------
        test = get_regex(s['hws_dir'], s['Visit'] + '.*to.*' + reg_hws,
                         deep_search=False, full=False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR:' + reg_hws + ' transformation to hws not '
                          'found', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        t1_2hws = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| T1 to HWS mat:', 'green', attrs=['bold']), t1_2hws)
# --------------------- Retrieving img to register on HWS ---------------------
        subject_dir_img = os.path.join(dir_img, s['Id'], s['Visit'])
        test = get_regex(subject_dir_img, regex_img, False, casensitive=False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: IMAGE NOT FOUND:', 'red', attrs=['bold']),
                  img)
            continue
        s["img"] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found ' + img + ':', 'green', attrs=['bold']),
              s["img"])
# --------------------- Retrieving matrix image to T1_N4 ----------------------
        if regex_mat:
            mat2N4 = get_regex(s['process'], regex_mat, False)
            if not mat2N4:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: MATRIX NOT FOUND:', 'red',
                              attrs=['bold']), regex_mat)
                continue
            mat2N4 = mat2N4[0]
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Found transformation matrix:', 'green',
                          attrs=['bold']), mat2N4)
            transformations = [t1_2hws, mat2N4]
            flags = [False, False]
        else:
            transformations = [t1_2hws]
            flags = [False]
# --------------------- Defining path to HWSregistration ----------------------
        HWSreg = os.path.join(s['hws_dir'], hws_out_dir, s['Visit'])
        createDir(HWSreg)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| HWSregistration:', 'green', attrs=['bold']), HWSreg)
# --------------------------- warping  image to HWS ---------------------------
        warp = changeName(s["img"], '_2hws_' + reg_hws, HWSreg)
        ApplyRegistration(s["img"], hws, transformations, warp, flags, meth)
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
cprint('|', 'blue', attrs=['bold'])
cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
print('')
