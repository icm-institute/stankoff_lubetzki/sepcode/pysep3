#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 10:56:01 2020

@author: arya.yazdan-panah
"""


import os
import sys
from importlib import import_module
from termcolor import cprint, colored
from common3 import get_regex, createDir, init_script, changeName
from tools_ants import AntsRegistrationSyn, ApplyRegistration
from tools_fsl import fslmaths
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from tools_cluster import qsub3


def sendCluster(subject, options):
    print(" ** Sending qsub : " + subject["Id"] + "_" + subject["Visit"])
    comm = sys.argv[0] + " -s " + subject["Id"] + "/" + subject["Visit"]
    comm = comm + " -i " + options.image + " -t " + options.template
    comm = comm + " -R " + options.ref
    name = "MNI_reg_" + subject["Id"] + "_" + subject["Visit"]
    etime = "02:30:00"
    qsub3(comm, name=name, queue="normal", etime=etime, memory="8G", cpus='6')


MSG = "Register T1 to MNI and then another image if specified"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-r", "--raw", dest="raw",
                        help="Raw directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-i", "--image", dest="image", default='t1', type=str,
                        help="Image to Register to MNI")
    parser.add_argument("-t", "--template", dest="template",
                        default="mni152_sym09c",
                        help="Template onto which register the previous image")
    parser.add_argument("-R", "--Reference", dest="ref", default='t1',
                        help="Reference Image")
    parser.add_argument("-q", "--qsub", action="store_true", dest="qsub")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
# ----------------------------- Retrieving Atlas ------------------------------
    template = options.template
    if template not in ("mni152", "mni152_asym09a", "mni152_asym09b",
                        "mni152_sym09a", "mni152_sym09c"):
        cprint("ERROR: Template not implemented or non-existant " + template,
               'red', attrs=['bold'])
        sys.exit(1)
    template = getattr(import_module("atlases"), template)
    template = template.brain
    print(colored('| Template used:', 'blue', attrs=['bold']), template)
    if options.ref == 't1':
        ref_dir = 't1preproc'
        regex_ref = '_t1_N4$'
    elif options.ref == 'mp2rage':
        ref_dir = 'mp2ragepreproc'
        regex_ref = '_mp2rage_uni_denoised$'
# ----------------------------- Finding Images --------------------------------
    img = options.image.lower()
    stop = False
    is_4D = False
    if img == 't1':
        stop = True
    elif img == 't2':
        regex_mat = "t2_2t1_N4_ITK.mat"
        dir_img = process
        regex_img = "t2_float.nii.gz"
        meth = 'Linear'
    elif img == 'flair':
        regex_mat = "flair_2t1_N4_0GenericAffine.mat"
        dir_img = raw
        regex_img = 'flair.nii.gz'
        meth = 'Linear'
    elif img == "lesion_mask":
        regex_mat = "t2_2(t1_N4|mp2rage)_ITK.mat"
        dir_img = process
        regex_img = "t2_les_manual.nii.gz"
        meth = 'NearestNeighbor'
    elif img == "r1":
        regex_mat = "_pib_movcorr_mean_2t1_ITK.mat"
        dir_img = process
        regex_img = "_R1.nii.gz"
        meth = 'Linear'
    elif img == "wm_mask":
        regex_mat = False
        dir_img = process
        regex_img = "t1preproc/fast/.*_t1_N4_brain_seg_2.nii.gz"
        meth = 'NearestNeighbor'
    elif img == "mtr":
        regex_mat = False
        dir_img = process
        regex_img = "_mtr_2t1.nii.gz"
        meth = 'Linear'
    elif img == "brainstem":
        regex_mat = False
        dir_img = process
        regex_img = "_brainstemSsLabels1.v12.nii.gz"
        meth = 'NearestNeighbor'
    elif img == "fa":
        regex_mat = "_B0s_corrected_mean_2.*0GenericAffine.mat"
        dir_img = process
        regex_img = "dtifit/.*dti_FA.nii.gz"
        meth = 'Linear'
    elif img == "md":
        regex_mat = "_B0s_corrected_mean_2.*0GenericAffine.mat"
        dir_img = process
        regex_img = "dtifit/.*dti_MD.nii.gz"
        meth = 'Linear'
    elif img == "rd":
        regex_mat = "_B0s_corrected_mean_2.*0GenericAffine.mat"
        dir_img = process
        regex_img = "dtifit/.*dti_RD.nii.gz"
        meth = 'Linear'
    elif img == "ad":
        regex_mat = "_B0s_corrected_mean_2.*0GenericAffine.mat"
        dir_img = process
        regex_img = "dtifit/.*dti_L1.nii.gz"
        meth = 'Linear'
    elif img == "t1_map":
        regex_mat = False
        dir_img = raw
        regex_img = "mp2rage_t1_map.nii.gz"
        meth = 'Linear'
    elif img == "carot":
        regex_mat = '_dpa_movcorr_mean_2t1_ITK.mat'
        dir_img = process
        regex_img = "dpaROI/.*dpa_carotid.nii.gz"
        meth = 'NearestNeighbor'
    elif img == "logyoung":
        regex_mat = "_dpa_movcorr_mean_2t1_ITK.mat"
        dir_img = process
        regex_img = "dpalogan_young_classes/.*dpa_logan_vt.nii"
        meth = 'Linear'
# =============================================================================
    else:
        cprint("ERROR: Sequence to MNI not implemented yet: " + img, 'red',
               attrs=['bold'])
        sys.exit(2)
    print(colored("| Registering Sequence:", 'blue', attrs=['bold']), img)
    if options.qsub:
        for s in subjects:
            sendCluster(s, options)
        sys.exit()
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        s['process'] = os.path.join(process, s['Id'], s['Visit'])
# -------------------------- Defining path to leinp ---------------------------
        t1preproc = os.path.join(process, s['Id'], s['Visit'], ref_dir)
        if not os.path.isdir(t1preproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + ref_dir + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        leinp = os.path.join(process, s['Id'], s['Visit'], 'leinp')
        look_for_N4 = False
        if not os.path.isdir(leinp):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Warning: leinp  not found', 'yellow',
                          attrs=['bold']))
            look_for_N4 = True
        else:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Found leinp: ', 'green', attrs=['bold']), leinp)
            inpainted = get_regex(leinp, "inpainted.nii.gz",
                                  ignore_extension=False)
            if not inpainted:
                look_for_N4 = True
            else:
                s["anat"] = inpainted[0]
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Inpainted anat: ', 'green', attrs=['bold']),
                      s["anat"])
# ------------------------ Defining path to t1preproc -------------------------
        if look_for_N4:
            t1_N4 = get_regex(t1preproc, regex_ref)
            if not t1_N4:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: ' + regex_ref + ' not found', 'red',
                              attrs=['bold']))
                print(colored('|', 'blue', attrs=['bold']),
                      colored('L', 'green', attrs=['bold']))
                continue
            else:
                s["anat"] = t1_N4[0]
# -------------------------- Retrieving brain_mask ----------------------------
        mask = get_regex(t1preproc, "(brain_mask|_mask)")
        if not mask:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Mask not found, processing the ' + options.ref +
                          ' non skull-tripped', 'green', attrs=['bold']))
            pass
        else:
            mask = mask[0]
            temp = changeName(s["anat"], '_brain')
            if not os.path.isfile(temp):
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Mask found, extracting brain from the ' + options.ref,
                              'green', attrs=['bold']))
                fslmaths(s["anat"], '-mul', mask, temp)
            else:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Found extracted brain from the t1',
                              'green', attrs=['bold']), temp)
            s["anat"] = temp
# --------------------- Defining path to MNIregistration ----------------------
        MNIreg = os.path.join(s['process'], "t1Ants2SS")
        createDir(MNIreg)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| MNIregistration:', 'green', attrs=['bold']), MNIreg)
        trans_mat_dir = os.path.join(MNIreg, 'trans_mat')
        createDir(trans_mat_dir)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Transformation matrices stored in:', 'green',
                      attrs=['bold']), trans_mat_dir)
# ------------------------- Registering T1_N4 to MNI --------------------------
        warp = changeName(s["anat"], '_2mni', MNIreg)
        invwarp = changeName(template, '_2t1', MNIreg)
        print(s['anat'])
        t1_reg = AntsRegistrationSyn(s["anat"], template, warp, invwarp, 's',
                                     big_image=True)
        if os.path.isfile(t1_reg['inverse_warped_image']):
            os.remove(t1_reg['inverse_warped_image'])
        if stop:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
# -------------- Retrieving T1_N4 to MNI transformation matrix ----------------
        t1_2MNI_mat = t1_reg['forward_transforms']
# --------------------- Retrieving img to register on MNI ---------------------
        subject_dir_img = os.path.join(dir_img, s['Id'], s['Visit'])
        s['img'] = get_regex(subject_dir_img, regex_img, False)
        if not s['img']:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: IMAGE NOT FOUND:', 'red', attrs=['bold']),
                  img)
            continue
        s["img"] = s["img"][0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found ' + img + ':', 'green', attrs=['bold']),
              s["img"])
# --------------------- Retrieving matrix image to T1_N4 ----------------------
        if regex_mat:
            mat2N4 = get_regex(s['process'], regex_mat, False)
            if not mat2N4:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('ERROR: MATRIX NOT FOUND:', 'red',
                              attrs=['bold']), regex_mat)
                continue
            mat2N4 = mat2N4[0]
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Found transformation matrix:', 'green',
                          attrs=['bold']), mat2N4)
            transformations = [t1_2MNI_mat[1], t1_2MNI_mat[0], mat2N4]
# --------------------------- warping  image to MNI ---------------------------
        else:
            transformations = [t1_2MNI_mat[1], t1_2MNI_mat[0]]
        warp = changeName(s['img'], '_2mni', MNIreg)
        ApplyRegistration(s['img'], template, transformations, warp,
                          interp=meth, is_4D=is_4D)
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
cprint('|', 'blue', attrs=['bold'])
cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
print('')
