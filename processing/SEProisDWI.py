#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  5 14:18:28 2021

@author: arya.yazdan-panah
"""

import os
import sys
from common3 import get_regex, createDir, init_script, changeName
from tools_ants import ApplyRegistration
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

MSG = "Register T1ROIs obtained from Freesurfer recon-all on the DWI space"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# --------------------- Defining path to petpreproc ------------------------
        dwipreproc = os.path.join(process, s['Id'], s['Visit'], 'dwipreproc_denoise')
        if not os.path.isdir(dwipreproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: dwipreproc not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| dwipreproc: ', 'green', attrs=['bold']),
              dwipreproc)
# ------------------------- Retrieving PET sequence ---------------------------
        test = get_regex(dwipreproc, 'topup/.*_B0s_corrected_mean.nii.gz',
                         False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, mean motion corrected not found', 'red',
                          attrs=['bold']))
            continue
        s['B0_mean'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found mean motion corrected B0s:',
                      'green', attrs=['bold']), s['B0_mean'])
# ------------------------- Retrieving T1 2 PET mat ---------------------------
        test = get_regex(dwipreproc, 'trans_mat/.*.mat', False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, Matrix from DWI 2 T1 not found',
                          'red', attrs=['bold']))
            continue
        s['dwi2t1'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Matrix from DWI 2 T1:', 'green',
                      attrs=['bold']), s['dwi2t1'])
# ------------------------- Defining path to t1ROI ----------------------------
        t1ROI = os.path.join(process, s['Id'], s['Visit'], 't1ROI')
        if not os.path.isdir(t1ROI):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: t1ROI not found', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| t1ROI: ', 'green', attrs=['bold']),
              t1ROI)
# ------------------------- Retrieving All T1 ROIS ----------------------------
        ROIs = get_regex(t1ROI, '.nii.gz', False)
        if not ROIs:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: NO ROI IN T1ROI', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found ROIs:', 'green', attrs=['bold']), len(ROIs))
# ------------------------- Retrieving brain_mask -----------------------------
        sdir = os.path.join(process, s['Id'], s['Visit'])
        test = get_regex(sdir, 'brain_mask.nii.gz', False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: No Brain Mask', 'red', attrs=['bold']))
        else:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Found Brain Mask:', 'green', attrs=['bold']),
                  test[0])
            ROIs.append(test[0])
# ------------------------ Defining path to petROI ----------------------------
        dwiROI = os.path.join(dwipreproc, 'dwiROI')
        createDir(dwiROI)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory dwiROI created/existing:',
                      'green', attrs=['bold']), dwiROI)
# ------------------------- Registering All T1 ROIS ---------------------------
        for mask in ROIs:
            output = changeName(mask, '_2dwi', dwiROI)
            ApplyRegistration(mask, s['B0_mean'], s['dwi2t1'], output, [True],
                              'NearestNeighbor')
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
