#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Wed Feb  3 15:29:08 2021

@author: arya.yazdan-panah
'''

import os
import sys
import shutil
import numpy as np
import nibabel as nib
from common3 import changeName, command, init_script, createDir, get_regex
from tools_cluster import qsub3
from tools_freesurfer import fsImage2niftispace
from tools_c3 import c3d
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from termcolor import cprint, colored


def open_nii(img_path):
    A = nib.load(img_path)
    return A.get_fdata(), A.affine


def extractCSF(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[((4 <= A) & (A <= 5)) | (A == 14) | (A == 24) | (A == 31) |
             ((43 <= A) & (A <= 44)) | (A == 63)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)
    return out_file


def extractWM(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 2) | (A == 28) | (A == 41) | (A == 60) | (A == 77) |
             ((251 <= A) & (A <= 255))] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)
    return out_file


def extractSCG(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 10) | (A == 11) | (A == 12) | (A == 13) | (A == 17) |
             (A == 18) | (A == 26) | (A == 27) | (A == 28) | (A == 49) |
             (A == 50) | (A == 51) | (A == 52) | (A == 53) | (A == 54) |
             (A == 58) | (A == 59) | (A == 60) | ((251 <= A) & (A <= 255))] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)
    return out_file


def extractTH(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 10) | (A == 49)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)
    return out_file


def extractBRAIN(A, affine, out_file):
    out_data = np.clip(A, 0, 1)
    out_data[((4 <= A) & (A <= 8)) |
             ((14 <= A) & (A <= 16)) |
             (A == 24) |
             ((28 <= A) & (A <= 31)) |
             ((43 <= A) & (A <= 47)) |
             ((60 <= A) & (A <= 63))] = 0
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)
    return out_file


def extractCB(A, affine, out_filegm, out_filewm):
    out_data = np.zeros(A.shape)
    out_data[(A == 47) | (A == 8)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_filegm)

    out_data = np.zeros(A.shape)
    out_data[(A == 46) | (A == 7)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_filewm)
    return out_filegm, out_filewm


def extractCTX(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 42) | (A == 3)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


# =============================================================================
# EXTRACTION OF CORTICAL LOBES
# =============================================================================


def extractLBS(fsdir, output_dir, sujid, refima):
    out3 = os.path.join(output_dir, sujid + "_ctx_lobes.nii.gz")
    if os.path.isfile(out3):
        return out3
    comm1 = ["mri_annotation2label", "--subject", sujid, "--hemi", "lh",
             "--lobesStrict", os.path.join(fsdir, "label",
                                           "lh.freeSurfer_lobes.annot")]
    command(comm1, verbose=False)
    comm2 = ["mri_annotation2label", "--subject", sujid, "--hemi", "rh",
             "--lobesStrict", os.path.join(fsdir, 'label',
                                           'rh.freeSurfer_lobes.annot')]
    command(comm2, verbose=False)
    comm3 = ["mri_aparc2aseg", "--annot", "freeSurfer_lobes", "--s", sujid,
             "--o", out3]
    command(comm3, verbose=False)
    c3d(refima, out3, "-interpolation", "NearestNeighbor", '-reslice-identity',
        '-o', out3)
    return out3


def extractLBSfrontal(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 1001) | (A == 2001)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractLBSparietal(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 2006) | (A == 1006)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractLBSoccipital(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 2004) | (A == 1004)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractLBSinsula(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 2007) | (A == 1007)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractLBStemporal(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 2005) | (A == 1005)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractLBSlimbic(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 2003) | (A == 1003)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


# =============================================================================
# EXTRACTION OF WM LOBES
# =============================================================================


def extractWMLBS(fsdir, output_dir, sujid, refima):
    out3 = os.path.join(output_dir, sujid + "_wm_lobes.nii.gz")
    if os.path.isfile(out3):
        return out3
    comm3 = ["mri_aparc2aseg", "--annot", "freeSurfer_lobes", "--s", sujid,
             "--labelwm", "--wmparc-dmax", "1000", "--rip-unknown",
             "--hypo-as-wm", "--o", out3, "--base-offset", "200"]
    command(comm3, verbose=False)
    c3d(refima, out3, "-interpolation", "NearestNeighbor", '-reslice-identity',
        '-o', out3)
    return out3


def extractWMLBSfrontal(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 3201) | (A == 4201)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractWMLBScingulate(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 3203) | (A == 4203)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractWMLBSoccipital(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 3204) | (A == 4204)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractWMLBStemporal(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 3205) | (A == 4205)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractWMLBSparietal(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 3206) | (A == 4206)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractWMLBSinsula(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[(A == 3207) | (A == 4207)] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


# =============================================================================
# EXTRACTION OF BRAIN STEM
# =============================================================================


def extractBSmidbrain(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[A == 173] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractBSpons(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[A == 174] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractBSmedula(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[A == 175] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


def extractBSscp(A, affine, out_file):
    out_data = np.zeros(A.shape)
    out_data[A == 178] = 1
    out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
    nib.save(out_img, out_file)


# def extractWMfrontal(A, affine, out_file):
#     out_data = np.zeros(A.shape)
#     out_data[(A == 3002) | (A == 4002) | (A == 3003) | (A == 4003) |
#              (A == 3012) | (A == 4012) | (A == 3014) | (A == 4014) |
#              (A == 3017) | (A == 4017) | (A == 3018) | (A == 4018) |
#              (A == 3019) | (A == 4019) | (A == 3020) | (A == 4020) |
#              (A == 3023) | (A == 4023) | (A == 3024) | (A == 4024) |
#              (A == 3026) | (A == 4026) | (A == 3027) | (A == 4027) |
#              (A == 3028) | (A == 4028) | (A == 3032) | (A == 4032) |
#              (A == 5001) | (A == 5002)] = 1
#     out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
#     nib.save(out_img, out_file)


# def extractWMparietal(A, affine, out_file):
#     out_data = np.zeros(A.shape)
#     out_data[(A == 3008) | (A == 4008) | (A == 3022) | (A == 4022) |
#              (A == 3025) | (A == 4025) | (A == 3029) | (A == 4029) |
#              (A == 3031) | (A == 4031)] = 1
#     out_img = nib.Nifti1Image(out_data.astype('int32'), affine)
#     nib.save(out_img, out_file)


def sendClusterFS(subject, options):
    print(" ** Sending qsub : " + subject["Id"] + "_" + subject["Visit"])
    comm = sys.argv[0] + " -s " + subject["Id"] + "/" + subject["Visit"]
    comm = comm + " -d " + options.freesurfer
    comm = comm + " -p " + options.process
    comm = comm + " -R " + options.ref
    name = "ROI_FS_" + subject["Id"] + "_" + subject["Visit"]
    etime = "00:10:00"
    logdir = os.path.join(options.freesurfer, "rois.freelogs")
    createDir(logdir)
    qsub3(comm, name=name, queue="normal", etime=etime, memory="4G",
          logdir=logdir, cpus='4')


MSG = "Extract each region of the brain from the freesurfer parcellation"

if __name__ == "__main__":
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-d", "--freesurfer", dest="freesurfer",
                        help="Directory where freesurfer stores images")
    parser.add_argument("-R", "--Reference", dest="ref", default='t1',
                        help="Reference Image")
    parser.add_argument("-q", "--qsub", action="store_true", dest="qsub")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
    freesurfer_dir = init['freesurfer']
    if options.qsub:
        options.process = process
        options.freesurfer = freesurfer_dir
        for s in subjects:
            sendClusterFS(s, options)
        sys.exit()
    os.environ["SUBJECTS_DIR"] = freesurfer_dir
    if options.ref == 't1':
        ref_dir = 't1preproc'
        regex_ref = '(_t1_N4|_t1_pre$)'
    elif options.ref == 'mp2rage':
        ref_dir = 'mp2ragepreproc'
        regex_ref = '_mp2rage_uni_denoised$'
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        visit = os.path.join(process, s['Id'], s['Visit'])
# ------------------------- Defining path to t1ROI ----------------------------
        t1ROI = os.path.join(visit, 't1ROI')
        createDir(t1ROI)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory t1ROI created/existing: ', 'green',
                      attrs=['bold']), t1ROI)
# ------------------------ Defining path to t1preproc -------------------------
        t1preproc = os.path.join(process, s['Id'], s['Visit'], ref_dir)
        if os.path.isdir(t1preproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Found directory ' + ref_dir + ': ', 'green',
                          attrs=['bold']), t1preproc)
        else:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored(ref_dir + ' not found:', 'red', attrs=['bold']),
                  t1preproc)
# --------------------- Defining path to freesurfer_dir -----------------------
        fsdir = os.path.join(freesurfer_dir, s['Id'] + '_' + s['Visit'])
        # fsdir = os.path.join(freesurfer_dir, s['Id'] + '_' + 'V01')
        if os.path.isdir(fsdir):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Found FS subject dir: ', 'green', attrs=['bold']),
                  fsdir)
        else:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('FS Subject dir not found:', 'red', attrs=['bold']),
                  fsdir)
            continue
# ------------------------- Retrieving T1_N4 sequence -------------------------
        N4 = get_regex(t1preproc, s['Visit'] + regex_ref)
        if not N4:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: T1 NOT FOUND', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        N4 = N4[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found T1 seq: ', 'green', attrs=['bold']), N4)
# ------------------------ Copying Brain mask to T1ROI ------------------------
        bmask = get_regex(t1preproc, s['Visit'] + '_.*brain_mask')
        if not bmask:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: brain mask NOT FOUND', 'red',
                          attrs=['bold']))
        else:
            shutil.copy(bmask[0], t1ROI)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Copied brain_mask to t1ROI', 'green',
                          attrs=['bold']))
# ---------------------- Retrieving MGZs from FreeSurfer ----------------------
        mri_dir = os.path.join(fsdir, 'mri')
        mgz_images = get_regex(mri_dir, "(^aseg|^aparc.aseg|^aparc.a2009s.aseg"
                               "|^wmparc|^ribbon|brainstemSsLabels.v12.FSvoxel"
                               "Space).mgz$", False, False, full=False)
# ---------------------- Convert and reslice MGZ to nii -----------------------
        suj_id = s['Id'] + '_' + s['Visit']
        prefix = suj_id + '_'
        fsImage2niftispace(mgz_images, t1ROI, N4, prefix)
        a2009 = get_regex(t1ROI, 'aparc.nii.gz', False)
        if a2009:
            rename2009 = changeName(a2009[0], '_a2009s+aseg')
            os.rename(a2009[0], rename2009)
# -------------- Exctracting individual regions of aparc+aseg -----------------
        aa_nii = get_regex(t1ROI, '_aparc.aseg.nii.gz', False)
        if not aa_nii:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: aparc+aseg.nii.gz NOT FOUND', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        aa_nii = aa_nii[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found aparc+aseg in nifti:', 'green', attrs=['bold']),
              aa_nii)
        data, affine = open_nii(aa_nii)
        # Defining each name outputs
        out_file_csf = changeName(aa_nii, '_csf')
        out_file_wm = changeName(aa_nii, '_wm')
        out_file_bnv = changeName(aa_nii, '_brain_noventr')
        out_file_cbgm = changeName(aa_nii, '_cbgm')
        out_file_cbwm = changeName(aa_nii, '_cbwm')
        out_file_scg = changeName(aa_nii, '_SubCortGray')
        out_file_th = changeName(aa_nii, '_TH')
        # Extraction
        if not os.path.isfile(out_file_csf):
            extractCSF(data, affine, out_file_csf)
        if not os.path.isfile(out_file_wm):
            extractWM(data, affine, out_file_wm)
        if not os.path.isfile(out_file_bnv):
            extractBRAIN(data, affine, out_file_bnv)
        if not os.path.isfile(out_file_cbgm):
            extractCB(data, affine, out_file_cbgm, out_file_cbwm)
        if not os.path.isfile(out_file_scg):
            extractSCG(data, affine, out_file_scg)
        if not os.path.isfile(out_file_th):
            extractTH(data, affine, out_file_th)
# ---------------- Exctracting individual regions of ribbon -------------------
        ribbon_nii = get_regex(t1ROI, '_ribbon.nii.gz', False)
        if not aa_nii:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ribbon.nii.gz NOT FOUND', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        ribbon_nii = ribbon_nii[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found ribbon in nifti:', 'green', attrs=['bold']),
              ribbon_nii)
        data, affine = open_nii(ribbon_nii)
        # Defining each name outputs
        out_file_ctx = changeName(ribbon_nii, '_ctx')
        extractCTX(data, affine, out_file_ctx)
# ---------------------- Exctracting individual lobes -------------------------
        lobes = extractLBS(fsdir, t1ROI, suj_id, N4)
        data, affine = open_nii(lobes)
        # Defining each name outputs
        out_file_frontal = changeName(lobes, '_frontal')
        out_file_parietal = changeName(lobes, '_parietal')
        out_file_occipital = changeName(lobes, '_occipital')
        out_file_insula = changeName(lobes, '_insula')
        out_file_temporal = changeName(lobes, '_temporal')
        out_file_limbic = changeName(lobes, '_limbic')
        # Extraction
        if not os.path.isfile(out_file_frontal):
            extractLBSfrontal(data, affine, out_file_frontal)
        if not os.path.isfile(out_file_parietal):
            extractLBSparietal(data, affine, out_file_parietal)
        if not os.path.isfile(out_file_occipital):
            extractLBSoccipital(data, affine, out_file_occipital)
        if not os.path.isfile(out_file_insula):
            extractLBSinsula(data, affine, out_file_insula)
        if not os.path.isfile(out_file_temporal):
            extractLBStemporal(data, affine, out_file_temporal)
        if not os.path.isfile(out_file_limbic):
            extractLBSlimbic(data, affine, out_file_limbic)
# --------------------- Exctracting individual WMlobes ------------------------
        wmlobes = extractWMLBS(fsdir, t1ROI, suj_id, N4)
        data, affine = open_nii(wmlobes)
        # Defining each name outputs
        out_file_frontal = changeName(wmlobes, '_frontal')
        out_file_cingulate = changeName(wmlobes, '_cingulate')
        out_file_occipital = changeName(wmlobes, '_occipital')
        out_file_temporal = changeName(wmlobes, '_temporal')
        out_file_parietal = changeName(wmlobes, '_parietal')
        out_file_insula = changeName(wmlobes, '_insula')
        # Extraction
        if not os.path.isfile(out_file_frontal):
            extractWMLBSfrontal(data, affine, out_file_frontal)
        if not os.path.isfile(out_file_cingulate):
            extractWMLBScingulate(data, affine, out_file_cingulate)
        if not os.path.isfile(out_file_occipital):
            extractWMLBSoccipital(data, affine, out_file_occipital)
        if not os.path.isfile(out_file_temporal):
            extractWMLBStemporal(data, affine, out_file_temporal)
        if not os.path.isfile(out_file_parietal):
            extractWMLBSparietal(data, affine, out_file_parietal)
        if not os.path.isfile(out_file_insula):
            extractWMLBSinsula(data, affine, out_file_insula)
# --------------------- Exctracting individual WMparc -------------------------
        # wmparc = get_regex(t1ROI, 'wmparc.nii.gz', False)
        # if not wmparc:
        #     print(colored('|', 'blue', attrs=['bold']),
        #           colored('|', 'green', attrs=['bold']),
        #           colored('ERROR: wmparc.nii.gz NOT FOUND', 'red',
        #                   attrs=['bold']))
        #     print(colored('|', 'blue', attrs=['bold']),
        #           colored('L', 'green', attrs=['bold']))
        #     continue
        # wmparc = wmparc[0]
        # print(colored('|', 'blue', attrs=['bold']),
        #       colored('| Found wmparc in nifti:', 'green', attrs=['bold']),
        #       wmparc)
        # data, affine = open_nii(wmparc)
        # # Defining each name outputs
        # out_file_parietal = changeName(wmparc, '_parietal')
        # out_file_frontal = changeName(wmparc, '_frontal')
        # # Extraction
        # if not os.path.isfile(out_file_parietal):
        #     extractWMparietal(data, affine, out_file_parietal)
        # if not os.path.isfile(out_file_frontal):
        #     extractWMfrontal(data, affine, out_file_frontal)
# ---------------------- Exctracting individual BStem -------------------------
        brainstem = get_regex(t1ROI, 'brainstemSsLabels', False)
        if not brainstem:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: brainstem.nii.gz NOT FOUND', 'red',
                          attrs=['bold']))
        else:
            brainstem = brainstem[0]
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Found brainstem in nifti:', 'green',
                          attrs=['bold']), brainstem)
            data, affine = open_nii(brainstem)
            # Defining each name outputs
            out_file_midbrain = changeName(brainstem, '_midbrain')
            out_file_pons = changeName(brainstem, '_pons')
            out_file_medula = changeName(brainstem, '_medula')
            out_file_scp = changeName(brainstem, '_scp')
            # Extraction
            if not os.path.isfile(out_file_midbrain):
                extractBSmidbrain(data, affine, out_file_midbrain)
            if not os.path.isfile(out_file_pons):
                extractBSpons(data, affine, out_file_pons)
            if not os.path.isfile(out_file_medula):
                extractBSmedula(data, affine, out_file_medula)
            if not os.path.isfile(out_file_scp):
                extractBSscp(data, affine, out_file_scp)
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
