#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 16:45:39 2020

@author: arya.yazdan-panah
"""

import os
import sys
from common3 import get_regex, createDir, init_script, changeName
from tools_cluster import qsub3
# from tools_fsl import flirt
from tools_ants import ApplyRegistration
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

MSG = "Register T1ROIs obtained from Freesurfer recon-all on the PET space"


def sendClusterFS(subject, options):
    print(" ** Sending qsub : " + subject["Id"] + "_" + subject["Visit"])
    comm = sys.argv[0] + " -s " + subject["Id"] + "/" + subject["Visit"]
    comm = comm + " -p " + options.process
    comm = comm + " -t " + options.tracer
    name = "ROI_PET_" + subject["Id"] + "_" + subject["Visit"]
    etime = "00:30:00"
    logdir = os.path.join(options.process, "rois.pet")
    createDir(logdir)
    qsub3(comm, name=name, queue="normal", etime=etime, memory="6G",
          logdir=logdir, cpus='6')


if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-t", "--tracer", dest="tracer",
                        help="Tracer used (dpa/pib/fdg/pk/fmz/...)")
    parser.add_argument("-q", "--qsub", action="store_true", dest="qsub")

    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
    tracer = options.tracer.lower()
    print(colored("| Tracer used:", "blue", attrs=["bold"]),
          tracer.upper())
    if options.qsub:
        options.process = process
        for s in subjects:
            sendClusterFS(s, options)
        sys.exit()
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# --------------------- Defining path to petpreproc ------------------------
        petpreproc = os.path.join(process, s['Id'], s['Visit'], tracer +
                                  'preproc')
        if not os.path.isdir(petpreproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + petpreproc + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + 'preproc: ', 'green', attrs=['bold']),
              petpreproc)
# ------------------------- Retrieving PET sequence ---------------------------
        test = get_regex(petpreproc, '_movcorr_mean.nii.gz', False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, Motion corrected not found', 'red',
                          attrs=['bold']))
            continue
        s['mov_corr_mean'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Motion Corrected ' + tracer + ' mean :',
                      'green', attrs=['bold']), s['mov_corr_mean'])
# ------------------------- Retrieving T1 2 PET mat ---------------------------
        test = get_regex(petpreproc, '_movcorr_mean_2t1_ITK.mat', False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, Matrix from ' + tracer + ' 2 T1 not found',
                          'red', attrs=['bold']))
            continue
        s['pet2t1'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Matrix from ' + tracer + ' 2 T1:', 'green',
                      attrs=['bold']), s['pet2t1'])
# ------------------------- Defining path to t1ROI ----------------------------
        t1ROI = os.path.join(process, s['Id'], s['Visit'], 't1ROI')
        if not os.path.isdir(t1ROI):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: t1ROI not found', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| t1ROI: ', 'green', attrs=['bold']),
              t1ROI)
# ------------------------- Retrieving All T1 ROIS ----------------------------
        ROIs = get_regex(t1ROI, '.nii.gz', False, False)
        if not ROIs:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: NO ROI IN T1ROI', 'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found ROIs:', 'green', attrs=['bold']), len(ROIs))
# ------------------------ Defining path to petROI ----------------------------
        petROI = os.path.join(process, s['Id'], s['Visit'], tracer + 'ROI')
        createDir(petROI)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory ' + tracer + 'ROI created/existing:',
                      'green', attrs=['bold']), petROI)
# ------------------------- Registering All T1 ROIS ---------------------------
        # Old registration of the masks with flirt
        # for mask in ROIs:
        #     output = changeName(mask, '_2' + tracer, petROI)
        #     flirt(mask,  s['mov_corr_mean'], apply_xfm=True,
        #           in_matrix_file=s['pet2t1'], out_file=output,
        #           out_matrix_file=s['pet2t1'])
        for mask in ROIs:
            output = changeName(mask, '_2' + tracer, petROI)
            ApplyRegistration(mask, s['mov_corr_mean'], s['pet2t1'], output,
                              [True], 'NearestNeighbor')
# ------------------------ Retrieving other T1 ROIS ---------------------------
        WMdir = get_regex(t1ROI, '_individual$', deep_search=False)
        if WMdir:
            WMdir = WMdir[0]
            ROIs = get_regex(WMdir, '.nii.gz', False, False)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Found wmROIs:', 'green', attrs=['bold']),
                  len(ROIs))
            outWMdir = changeName(WMdir, '_2dpa', petROI)
            createDir(outWMdir)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| WMpar dir to ' + tracer + ' created:',
                          'green', attrs=['bold']), outWMdir)
            for mask in ROIs:
                output = changeName(mask, '_2' + tracer, outWMdir)
                ApplyRegistration(mask, s['mov_corr_mean'], s['pet2t1'],
                                  output, [True], 'NearestNeighbor')
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
