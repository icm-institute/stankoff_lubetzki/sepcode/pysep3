#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 11:56:14 2021

@author: arya.yazdan-panah
"""

import os
import sys
from common3 import get_regex, createDir, init_script, changeName
from tools_fsl import fslmaths
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from pprint import pprint


MSG = "Create ROIs for the supervised clustering"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-t", "--tracer", dest="tracer", default="pib",
                        help="Tracer used (dpa/pib/...)")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
    tracer = options.tracer.lower()
    print(colored("| Tracer used:", "blue", attrs=["bold"]),
          tracer.upper())
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# ------------------------ Defining path to petROI ----------------------------
        petROI = os.path.join(process, s['Id'], s['Visit'], tracer + 'ROI')
        if not os.path.isdir(petROI):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + tracer + 'ROI not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + 'ROI: ', 'green', attrs=['bold']),
              petROI)
# ------------------------- Retrieving All T1 ROIS ----------------------------
        ROIs = get_regex(petROI, '_(aparc|aparc.aseg_TH|aparc.aseg_cbgm|aparc.'
                         'aseg_wm|ribbon_ctx)_2' + tracer + '.nii.gz', False)
        if len(ROIs) < 5:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: missing some ROI IN ' + tracer + 'ROI',
                          'red', attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        thalamus = ROIs[0]
        cbgm = ROIs[1]
        wm = ROIs[2]
        aparc = ROIs[3]
        ctx = ROIs[4]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Thalamus:', 'green', attrs=['bold']), thalamus)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found CBGM:', 'green', attrs=['bold']), cbgm)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found WM:', 'green', attrs=['bold']), wm)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found aparc:', 'green', attrs=['bold']), aparc)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found cortex:', 'green', attrs=['bold']), ctx)
# ----------------------- Defining path to ROI_SVC ----------------------------
        roiSVC = os.path.join(petROI, 'roiSC')
        createDir(roiSVC)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory roiSC created/existing:', 'green',
                      attrs=['bold']), roiSVC)
# ------------------------------- Creating ROIs -------------------------------
        b_mask = changeName(aparc, '_brainmask', roiSVC, remove='_aparc_2' +
                            tracer)
        fslmaths(aparc, '-bin', b_mask)
        th_ero = changeName(changeName(thalamus, '_ero', roiSVC, remove='_2' +
                                       tracer), remove='_aparc+aseg')
        fslmaths(thalamus, '-ero', th_ero)
        gm_ero = changeName(th_ero, 'gm_ero', remove='TH_ero')
        fslmaths(cbgm, '-ero', '-add', ctx, '-bin', '-kernel', 'sphere', 1.25,
                 '-ero', gm_ero)
        wm_ero = changeName(th_ero, 'wm_ero', remove='TH_ero')
        fslmaths(wm, '-kernel', 'sphere', 3, '-ero', wm_ero)
# ----------------------------- End of 1 subject ------------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
