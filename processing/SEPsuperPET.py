#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 13:52:20 2021

@author: arya.yazdan-panah
"""

import os
import sys
import numpy as np
import csv
from common3 import get_regex, init_script, createDir
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from tools_svca import superX_extractNormTACs, readSIF