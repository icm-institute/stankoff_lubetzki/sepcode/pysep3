#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  6 18:39:08 2020

@author: arya.yazdan-panah
"""

import os
import sys
from termcolor import cprint, colored
from common3 import createDir, init_script, changeName, get_regex
from optparse import OptionParser
from tools_ecat2nii import ecat2nii_tpc
from tools_nibabel import flip_nii

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = 'usage: %prog'
    parser = OptionParser(usage=usage)
    parser.add_option('-s', '--subjects', dest='subjects',
                      help='Subjects list')
    parser.add_option("-b", "--backup", dest="backup", help="vfile dir")
    parser.add_option("-r", "--raw", dest="raw", help=" Raw directory")
    parser.add_option("-t", "--tracer", dest="tracer", default="pib")
    parser.add_option('-v', action='store_true', dest='verbose',
                      default=False)
    (options, args) = parser.parse_args()
    tracer = options.tracer
    init = init_script(options)
    data = init['data']
    dicom = os.path.join(data, 'backup')
    raw = init['raw']
    subjects = init['subjects']
# -------------------------------- Importing  ---------------------------------
    for s in subjects:
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        s['raw'] = os.path.join(raw, s['Id'], s['Visit'])
        tracer_dir = os.path.join(s['raw'], tracer)
        subject_visit_tracer = s['Id'] + "_" + s["Visit"] + "_" + tracer
        nii_file = os.path.join(tracer_dir, subject_visit_tracer + ".nii.gz")
        if os.path.isfile(nii_file):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Import already done:', 'green', attrs=['bold']),
                  nii_file)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        test = get_regex(dicom, s['extra'])
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: subject_dir not found:', 'red',
                          attrs=['bold']))
            continue
        else:
            s["vdir"] = test[0]
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| Found subject_dir', 'green', attrs=['bold']),
                  s['vdir'])
        s["vdir"] = os.path.join(s["vdir"], tracer)
        vfiles = get_regex(s["vdir"], '.v', False)
        createDir(s['raw'])
        createDir(tracer_dir)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Directory created/existing', 'green', attrs=['bold']),
              tracer_dir)
        for i,v in enumerate(vfiles):
            nii, sif = ecat2nii_tpc(v, tracer_dir, subject_visit_tracer + '_' + str(i))
# -------------------------------- Reorienting --------------------------------
        flip_nii(nii, (0, 1, 2))
        print(colored('|', 'blue', attrs=['bold']),
              colored('L', 'green', attrs=['bold']))
    cprint('|', 'blue', attrs=['bold'])
    cprint('| Script Finished ', 'blue', attrs=['reverse', 'bold'])
    print('')
