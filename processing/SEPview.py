#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 11:31:36 2020

@author: arya.yazdan-panah
"""

import os
import sys
import shutil
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from common3 import init_script, get_regex, cmd_and_wait
from importlib import import_module

MSG = "automatic Image viewer to compare preset set of images iterating on \
subjects"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-r", "--raw", dest="raw",
                        help="Raw directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-v", "--view", dest="view", help=" Which view to show"
                        " Viewing options are: ['dicom_import', "
                        "'brain_extraction', 'prepare_contouring',"
                        "'prepare_flair', 'lesion_mask', 'lesion_inpainting',"
                        "'t1_2_mni', 'vfile_import', 'pet_movcorr', "
                        "'prepare_pet', 'denoise_mp2rage',"
                        "'mp2rage_brain_extraction', "
                        "'t1meg_brain_extraction', 'prepareMTR']",
                        default=False)
    options = parser.parse_args()
    init = init_script(options)
    data = init["data"]
    process = init['process']
    raw = init['raw']
    subjects = init['subjects']
    views = ['dicom_import', 'brain_extraction', 'prepare_contouring',
             'prepare_flair', 'lesion_mask', 'lesion_inpainting',
             't1_2_mni', 'vfile_import', 'pet_movcorr', 'prepare_pet',
             'denoise_mp2rage', 'mp2rage_brain_extraction',
             't1meg_brain_extraction', 'prepareMTR', 'R1SRTM2',
             'lesion_mask_emeline']
    if not options.view:
        cprint('|', 'blue', attrs=['bold'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('WARNING: No viewing option was given', 'yellow',
                      attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Following viewing options implemented, please choo'
                      'se one or exit the script with CTRL + C', 'yellow',
                      attrs=['bold']))
        for a, b in enumerate(views):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| ' + str(a + 1) + ':', 'yellow', attrs=['bold']),
                  b)
        idx = int(input(colored('| ', 'blue', attrs=['bold']) +
                        colored('| View number? :', 'yellow',
                                attrs=['bold']))) - 1
        if idx > len(views) - 1:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('ERROR: index given out of range', 'red',
                          attrs=['bold']))
            sys.exit(8)
        cprint('|', 'blue', attrs=['bold'])
        view = views[idx]
    else:
        view = options.view
# --------------------------- Looking for software ----------------------------
    if view in ["vfile_import", "pet_movcorr", "prepare_pet",
                "srtm_perfusion"]:
        cprint('|', 'blue', attrs=['bold'])
        print(colored('|', 'blue', attrs=['bold']),
              colored('TRACER', 'yellow', attrs=['bold']))
        tracer = str(input(colored('| ', 'blue', attrs=['bold']) +
                           colored('| pib / dpa / fdg / fmz :', 'yellow',
                                   attrs=['bold'])))
    if view == "dicom_import":
        search_template = {"directory": raw,
                           "folder": "",
                           "regex": ".*.nii.gz",
                           "ignore": False,
                           "deep": True,
                           "case": False}
        R = [search_template]
    elif view == "brain_extraction":
        search_template1 = {"directory": raw,
                            "folder": "t1",
                            "regex": ".*t1.nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False}
        search_template2 = {"directory": process,
                            "folder": "t1preproc",
                            "regex": ".*t1_N4$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        search_template3 = {"directory": process,
                            "folder": "t1preproc",
                            "regex": ".*brain_mask$",
                            "ignore": True,
                            "deep": False,
                            "case": False,
                            "options": ["-cm green", "--alpha 50"]}
        R = [search_template1, search_template2, search_template3]
    elif view == "prepare_contouring":
        search_template1 = {"directory": process,
                            "folder": "t1preproc",
                            "regex": ".*t1_N4$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        search_template2 = {"directory": process,
                            "folder": "contouringpreproc",
                            "regex": "t2_(2|to_)t1.nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False}
        search_template3 = {"directory": process,
                            "folder": "contouringpreproc",
                            "regex": "flair_(2|to_)t1.nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False}
        R = [search_template1, search_template2, search_template3]
    elif view == "prepare_flair":
        search_template1 = {"directory": process,
                            "folder": "t1preproc",
                            "regex": ".*t1_N4$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        search_template2 = {"directory": process,
                            "folder": "flairpreproc",
                            "regex": ".*flair_2t1.nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False}
        R = [search_template1, search_template2]
    elif view == "lesion_mask":
        search_template1 = {"directory": process,
                            "folder": "contouringpreproc",
                            "regex": "t2_float$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        search_template2 = {"directory": process,
                            "folder": "manual_lesion",
                            "regex": "fuzzy$",
                            "ignore": True,
                            "deep": False,
                            "case": False,
                            "options": ["-cm copper", "--alpha 50"]}
        R = [search_template1, search_template2]
    elif view == "lesion_inpainting":
        search_template1 = {"directory": process,
                            "folder": "t1preproc",
                            "regex": ".*t1_N4$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        search_template2 = {"directory": process,
                            "folder": "leinp",
                            "regex": "(fuzzy_2t1_N4$|mask_bin_2N4$)",
                            "ignore": True,
                            "deep": False,
                            "case": False,
                            "options": ["-cm copper", "--alpha 50"]}
        search_template3 = {"directory": process,
                            "folder": "leinp",
                            "regex": "inpainted$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        R = [search_template1, search_template2, search_template3]
    elif view == "t1_2_mni":
        search_template1 = {"directory": process,
                            "folder": "t1Ants2SS",
                            "regex": "2mni$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        mni = getattr(import_module("atlases"), "mni152_sym09c").brain
        R = [mni, search_template1]
    elif view == "vfile_import":
        search_template = {"directory": raw,
                           "folder": "",
                           "regex": tracer + ".nii.gz",
                           "ignore": False,
                           "deep": True,
                           "case": False,
                           "options": ["-cm hot", "-or 0 40000"]}
        R = [search_template]
    elif view == "pet_movcorr":
        search_template1 = {"directory": raw,
                            "folder": "",
                            "regex": tracer + ".nii.gz",
                            "ignore": False,
                            "deep": True,
                            "case": False,
                            "options": ["-cm hot", "-or 0 40000"]}
        search_template2 = {"directory": process,
                            "folder": "",
                            "regex": tracer + "_movcorr$",
                            "ignore": True,
                            "deep": True,
                            "case": False,
                            "options": ["-cm hot", "-or 0 40000"]}
        R = [search_template1, search_template2]
    elif view == "prepare_pet":
        search_template1 = {"directory": process,
                            "folder": "t1preproc",
                            "regex": "t1_(pre|N4).nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False}
        search_template2 = {"directory": process,
                            "folder": "dpapreprocV02",
                            "regex": tracer + ".*_2t1.nii.gz",
                            "ignore": False,
                            "deep": True,
                            "case": False,
                            "options": ["-cm hot", "-or 0 40000",
                                        "--alpha 50"]}
        R = [search_template1, search_template2]
    elif view == "srtm2_perfusion":
        search_template1 = {"directory": process,
                            "folder": "",
                            "regex": tracer + "_movcorr_mean.nii.gz",
                            "ignore": False,
                            "deep": True,
                            "case": False}
        search_template2 = {"directory": process,
                            "folder": "pibSRTM2",
                            "regex": "R1$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        R = [search_template1, search_template2]
    elif view == "denoise_mp2rage":
        search_template1 = {"directory": raw,
                            "folder": "mp2rage",
                            "regex": ".*uni.nii.gz",
                            "ignore": False,
                            "deep": True,
                            "case": True}
        search_template2 = {"directory": process,
                            "folder": "mp2ragepreproc",
                            "regex": ".*denoised$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        R = [search_template1, search_template2]
    elif view == "mp2rage_brain_extraction":
        search_template2 = {"directory": process,
                            "folder": "mp2ragepreproc",
                            "regex": ".*denoised$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        search_template3 = {"directory": process,
                            "folder": "mp2ragepreproc",
                            "regex": ".*brain_mask_ants$",
                            "ignore": True,
                            "deep": False,
                            "case": False,
                            "options": ["-cm green", "--alpha 50"]}
        R = [search_template2, search_template3]
    elif view == "prepareMTR":
        search_template1 = {"directory": process,
                            "folder": "prepareMTR",
                            "regex": "mton_2t1.nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False,
                            "options": ["-d"]}
        search_template2 = {"directory": process,
                            "folder": "prepareMTR",
                            "regex": "mtoff_2t1.nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False,
                            "options": ["-d"]}
        search_template3 = {"directory": process,
                            "folder": "t1preproc",
                            "regex": "t1_(pre|N4).nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False}
        search_template4 = {"directory": process,
                            "folder": "prepareMTR",
                            "regex": "mtr_2t1.nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False}
        R = [search_template1, search_template2, search_template3,
             search_template4]
    elif view == "R1SRTM2":
        search_template1 = {"directory": process,
                            "folder": "pibSRTM2final",
                            "regex": "DVR.nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False,
                            "options": ["-cm brain_colours_1hot_iso"]}
        search_template2 = {"directory": process,
                            "folder": "pibpreproc",
                            "regex": "_(t1|t1_pre)_2pib_movcorr_mean.nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False}
        R = [search_template1, search_template2]
        R = [search_template1]
    elif view == "lesion_mask_emeline":
        search_template1 = {"directory": process,
                            "folder": "contouringpreproc",
                            "regex": "t2_float$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        search_template3 = {"directory": process,
                            "folder": "contouringpreproc",
                            "regex": "flair_(2|to_)t2.nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False}
        search_template2 = {"directory": process,
                            "folder": "lesions_manual",
                            "regex": "t2_les_manual$",
                            "ignore": True,
                            "deep": False,
                            "case": False,
                            "options": ["-cm green", "--alpha 50"]}
        R = [search_template1, search_template3, search_template2]
    elif view == "lesion_mask_natalia":
        search_template1 = {"directory": process,
                            "folder": "contouringpreproc",
                            "regex": "t2_2flair$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        search_template3 = {"directory": raw,
                            "folder": "flair",
                            "regex": "flair.nii.gz",
                            "ignore": False,
                            "deep": False,
                            "case": False}
        search_template2 = {"directory": process,
                            "folder": "lesions_manual",
                            "regex": "flair_les_manual$",
                            "ignore": True,
                            "deep": False,
                            "case": False,
                            "options": ["-cm green", "--alpha 50"]}
        R = [search_template1, search_template3, search_template2]
    elif view == "lesion_mask_milena":
        search_template1 = {"directory": process,
                            "folder": "contouringpreproc",
                            "regex": "t2_float$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        search_template3 = {"directory": process,
                            "folder": "contouringpreproc",
                            "regex": "flair_2t2$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        search_template2 = {"directory": process,
                            "folder": "lesions_manual",
                            "regex": "_t1_les_spm_lst_auto_2t2_manual_correc$",
                            "ignore": True,
                            "deep": False,
                            "case": False,
                            "options": ["-cm green", "--alpha 50"]}
        R = [search_template1, search_template3, search_template2]
    elif view == "check_dwi":
        search_template1 = {"directory": process,
                            "folder": "mp2ragepreproc",
                            "regex": "denoised$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        search_template2 = {"directory": process,
                            "folder": "dwipreproc",
                            "regex": "_B0s_corrected_mean_2mp2rage$",
                            "ignore": True,
                            "deep": False,
                            "case": False}
        R = [search_template1, search_template2]
    else:
        print(colored("|", 'blue', attrs=['bold']),
              colored("ERROR: no visualization option was given, or option "
                      "given not available", 'red', attrs=['bold']))
        sys.exit(1)
# --------------------------- Looking for software ----------------------------
    test = shutil.which('fsleyes')
    if test is None:
        print(colored("|", 'blue', attrs=['bold']),
              colored("ERROR: no viewing software found", 'red',
                      attrs=['bold']))
        sys.exit(1)
    else:
        cprint('| Software used to visualize: ' + test, 'blue', attrs=['bold'])
    viewer = [test]
# -------------------------------- Importing  ---------------------------------
    for s in subjects:
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
        imgs = []
        for i, r in enumerate(R):
            if isinstance(r, str) and os.path.isfile(r):
                imgs.append(r)
                continue
            folder = os.path.join(r['directory'], s['Id'], s['Visit'],
                                  r["folder"])
            test = get_regex(folder, r['regex'], r['ignore'], r['deep'],
                             r['case'])
            if not test:
                print(folder)
                print(r['regex'])
                continue
            imgs.extend(test)
            try:
                imgs.extend(r["options"])
            except KeyError:
                pass
        if len(imgs) < len(R):
            continue
        cmd = " ".join(viewer + imgs)
        cmd_and_wait(cmd)
