#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  1 16:17:45 2021

@author: arya.yazdan-panah
"""


import os
import numpy as np
import nibabel as nib
from common3 import get_regex, readSubjectsLists
from termcolor import cprint, colored
import pandas as pd
from tools_svca import dynPET_normalization

raw = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw'
process = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/All_extra_frame_reconstruction.list')
tracer = 'dpa'
print(colored("| Tracer used:", "blue", attrs=["bold"]), tracer.upper())
im = ''
# ker_size = 3


def TAC(PET, M=False):
    if M:
        if M.shape != PET.shape[0:-1]:
            return False
        return np.mean(PET[M], 0)
    return np.mean(PET, (0, 1, 2))


df = pd.DataFrame()
for i, s in enumerate(subjects):
    cprint('|', 'blue', attrs=['bold'])
    print(s['Id'], s['Visit'])
    raw_pet_dir = os.path.join(raw, s['Id'], s['Visit'], tracer + im)
    if not os.path.isdir(raw_pet_dir):
        continue
    print(' raw: ', raw_pet_dir)
    test = get_regex(raw_pet_dir, tracer + im + '.sif', False)
    if not test:
        continue
    s['sif'] = test[0]
    print('SIF file :', s['sif'])
    petpreproc = os.path.join(process, s['Id'], s['Visit'], tracer + im +
                              'preproc')
    if not os.path.isdir(petpreproc):
        continue
    test = get_regex(petpreproc, '_movcorr.nii.gz', False)
    if not test:
        continue
    s['mov_corr'] = test[0]
    print('Found Motion Corrected', s['mov_corr'])
    roisvc = os.path.join(process, s['Id'], s['Visit'],
                          tracer + im + 'ROI', 'roi_svca')
    if not os.path.isdir(roisvc):
        continue
    blood = get_regex(petpreproc, 'blood_mask_closed_2dpa')[0]
    # out_mask = changeName(blood, '_ero_' + str(ker_size) + 'mm')
    # fslmaths(blood, '-kernel', 'sphere', ker_size, '-ero', out_mask)
    brain_mask = get_regex(roisvc, 'brainmask$')[0]
    normPET = dynPET_normalization(nib.load(s['mov_corr']).get_fdata(), nib.load(brain_mask).get_fdata().astype(bool))
    df[s['Id']] = np.mean(normPET[nib.load(blood).get_fdata().astype(bool)], 0)
df.to_csv('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/norm_blood_carotmask.csv', index=False)
