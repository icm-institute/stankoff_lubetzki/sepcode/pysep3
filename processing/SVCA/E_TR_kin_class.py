#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 23 12:06:19 2021

@author: arya.yazdan-panah
"""

import csv
import os
import numpy as np
import nibabel as nib
from tools_svca import TAC
from common3 import get_regex, readSubjectsLists

# HAB / MAB
p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
subjects = readSubjectsLists("/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/TR_inflasep.list")
stats_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats'
stats_file = os.path.join(stats_dir, 'TR_rois_HM.csv')

f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'ROI Name',
              'mloganV01',
              'mloganV02',
              'TRlogan',
              'CohenD']


stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
Rnames = ["SubCortGray", "cbgm", "cbwm", "wm", "ctx"]
for s in subjects:
    print(s['Id'])
    V01 = os.path.join(p, s['Id'], 'V01')
    V02 = os.path.join(p, s['Id'], 'V02')
    roi1 = os.path.join(V01, 'dpaROI')
    roi2 = os.path.join(V02, 'dpaROI')
    ROIs1 = get_regex(roi1, '_(aparc.aseg_cbgm|aparc.aseg_cbwm|aparc.aseg_SubCortGray|aparc.aseg_wm|ribbon_ctx)_2dpa')
    ROIs2 = get_regex(roi2, '_(aparc.aseg_cbgm|aparc.aseg_cbwm|aparc.aseg_SubCortGray|aparc.aseg_wm|ribbon_ctx)_2dpa')
    log1 = nib.load(get_regex(os.path.join(V01, 'dpalogan_young_HM'), 'vt')[0]).get_fdata()
    log2 = nib.load(get_regex(os.path.join(V02, 'dpalogan_young_HM'), 'vt')[0]).get_fdata()
    for i, (r1, r2) in enumerate(zip(ROIs1, ROIs2)):
        R1 = nib.load(r1).get_fdata().astype(bool)
        R2 = nib.load(r2).get_fdata().astype(bool)
        mlog1 = np.mean(log1[R1])
        mlog2 = np.mean(log2[R2])
        std = np.std(np.concatenate((log1[R1], log1[R2])))
        tr = 2 * abs(mlog1 - mlog2) / (mlog1 + mlog2)
        ces = (mlog1 - mlog2) / std
        stats.writerow({'Subject': s['Id'],
                        'ROI Name': Rnames[i],
                        'mloganV01': mlog1,
                        'mloganV02': mlog2,
                        'TRlogan': tr,
                        'CohenD': ces})
f.close()

# HAB
p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
subjects = readSubjectsLists("/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/TR_inflasep.list")
stats_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats'
stats_file = os.path.join(stats_dir, 'TR_rois_H.csv')

f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'ROI Name',
              'mloganV01',
              'mloganV02',
              'TRlogan',
              'CohenD']


stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
Rnames = ["SubCortGray", "cbgm", "cbwm", "wm", "ctx"]
for s in subjects:
    print(s['Id'])
    V01 = os.path.join(p, s['Id'], 'V01')
    V02 = os.path.join(p, s['Id'], 'V02')
    roi1 = os.path.join(V01, 'dpaROI')
    roi2 = os.path.join(V02, 'dpaROI')
    ROIs1 = get_regex(roi1, '_(aparc.aseg_cbgm|aparc.aseg_cbwm|aparc.aseg_SubCortGray|aparc.aseg_wm|ribbon_ctx)_2dpa')
    ROIs2 = get_regex(roi2, '_(aparc.aseg_cbgm|aparc.aseg_cbwm|aparc.aseg_SubCortGray|aparc.aseg_wm|ribbon_ctx)_2dpa')
    try:
        log1 = nib.load(get_regex(os.path.join(V01, 'dpalogan_young_H'), 'vt')[0]).get_fdata()
        log2 = nib.load(get_regex(os.path.join(V02, 'dpalogan_young_H'), 'vt')[0]).get_fdata()
    except:
        continue
    for i, (r1, r2) in enumerate(zip(ROIs1, ROIs2)):
        R1 = nib.load(r1).get_fdata().astype(bool)
        R2 = nib.load(r2).get_fdata().astype(bool)
        mlog1 = np.mean(log1[R1])
        mlog2 = np.mean(log2[R2])
        std = np.std(np.concatenate((log1[R1], log1[R2])))
        tr = 2 * abs(mlog1 - mlog2) / (mlog1 + mlog2)
        ces = (mlog1 - mlog2) / std
        stats.writerow({'Subject': s['Id'],
                        'ROI Name': Rnames[i],
                        'mloganV01': mlog1,
                        'mloganV02': mlog2,
                        'TRlogan': tr,
                        'CohenD': ces})
f.close()

# MAB
p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
subjects = readSubjectsLists("/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/TR_inflasep.list")
stats_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats'
stats_file = os.path.join(stats_dir, 'TR_rois_M.csv')

f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'ROI Name',
              'mloganV01',
              'mloganV02',
              'TRlogan',
              'CohenD']


stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
Rnames = ["SubCortGray", "cbgm", "cbwm", "wm", "ctx"]
for s in subjects:
    print(s['Id'])
    V01 = os.path.join(p, s['Id'], 'V01')
    V02 = os.path.join(p, s['Id'], 'V02')
    roi1 = os.path.join(V01, 'dpaROI')
    roi2 = os.path.join(V02, 'dpaROI')
    ROIs1 = get_regex(roi1, '_(aparc.aseg_cbgm|aparc.aseg_cbwm|aparc.aseg_SubCortGray|aparc.aseg_wm|ribbon_ctx)_2dpa')
    ROIs2 = get_regex(roi2, '_(aparc.aseg_cbgm|aparc.aseg_cbwm|aparc.aseg_SubCortGray|aparc.aseg_wm|ribbon_ctx)_2dpa')
    try:
        log1 = nib.load(get_regex(os.path.join(V01, 'dpalogan_young_M'), 'vt')[0]).get_fdata()
        log2 = nib.load(get_regex(os.path.join(V02, 'dpalogan_young_M'), 'vt')[0]).get_fdata()
    except:
        continue
    for i, (r1, r2) in enumerate(zip(ROIs1, ROIs2)):
        R1 = nib.load(r1).get_fdata().astype(bool)
        R2 = nib.load(r2).get_fdata().astype(bool)
        mlog1 = np.mean(log1[R1])
        mlog2 = np.mean(log2[R2])
        std = np.std(np.concatenate((log1[R1], log1[R2])))
        tr = 2 * abs(mlog1 - mlog2) / (mlog1 + mlog2)
        ces = (mlog1 - mlog2) / std
        stats.writerow({'Subject': s['Id'],
                        'ROI Name': Rnames[i],
                        'mloganV01': mlog1,
                        'mloganV02': mlog2,
                        'TRlogan': tr,
                        'CohenD': ces})
f.close()