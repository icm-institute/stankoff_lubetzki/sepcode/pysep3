#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  7 10:51:55 2022

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


f1 = "/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/TR_rois_HM.csv"
f2 = "/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/TR_rois_H.csv"
f3 = "/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/TR_rois_M.csv"

df1 = pd.read_csv(f1)
df1['quantif classes'] = "mixed"
df2 = pd.read_csv(f2).append(pd.read_csv(f3))
df2['quantif classes'] = "own"

df = pd.concat([df1, df2])

plt.figure(1)
sns.boxplot(x=df["ROI Name"], y=df["TRlogan"], hue=df['quantif classes'])
plt.xlabel("")
plt.ylabel('% TR')


# %%
rois = ["ctx", "SubCortGray", "cbgm", "cbwm", "wm"]
cs = ['b', 'darkblue', "cyan", "gold", "orange"]
plt.figure(2)
for r, c in zip(rois, cs):
    dfr = df1.loc[df1["ROI Name"] == r].sort_values('age')
    plt.plot(dfr['age'], dfr["mloganV02"], c=c)
    # plt.scatter(df1['age'], df1["mloganV02"])
plt.legend(rois)


# %%

import statsmodels.api as sm
from patsy import dmatrices

dfstats = df2.loc[df1["ROI Name"] == r].sort_values('age')
y, X = dmatrices('mloganV01 ~  age ', data=dfstats, return_type='dataframe')
mod = sm.OLS(y,X)
res = mod.fit()
print(res.summary())

