#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 12:05:37 2021

@author: arya.yazdan-panah
"""

import os
import numpy as np
import csv
from common3 import get_regex, readSubjectsLists
from termcolor import cprint, colored
from tools_svca import superX_extractNormTACs, readSIF
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm


raw = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw'
process = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/reconstructed_with_more_frames.list')
tracer = 'dpa'
print(colored("| Tracer used:", "blue", attrs=["bold"]), tracer.upper())
N = len(subjects)
use_carotid = True
FWHMs = [0, 1, 2, 3, 4, 5, 6, 7, 8]
outs = []
im = ''
# for fwhm in range(9):
for i, s in enumerate(subjects):
    cprint('|', 'blue', attrs=['bold'])
    print(s['Id'], s['Visit'])
    raw_pet_dir = os.path.join(raw, s['Id'], s['Visit'], tracer + im)
    if not os.path.isdir(raw_pet_dir):
        continue
    print(' raw: ', raw_pet_dir)
    test = get_regex(raw_pet_dir, tracer + im + '.sif', False)
    if not test:
        continue
    s['sif'] = test[0]
    print('SIF file :', s['sif'])
    petpreproc = os.path.join(process, s['Id'], s['Visit'], tracer + im +
                              'preproc')
    if not os.path.isdir(petpreproc):
        continue
    test = get_regex(petpreproc, '_movcorr.nii.gz', False)
    if not test:
        continue
    s['mov_corr'] = test[0]
    print('Found Motion Corrected', s['mov_corr'])
    roisvc = os.path.join(process, s['Id'], s['Visit'],
                          tracer + im + 'ROI', 'roi_svca')
    if not os.path.isdir(roisvc):
        continue
    rois = get_regex(roisvc, '(brainmask|gm_ero|TH_ero|wm_ero)')
    if len(rois) != 4:
        continue
    TH = rois[0]
    WB = rois[1]
    GM = rois[2]
    WM = rois[3]
    print('Thalamus:', TH)
    print('Brain:', WB)
    print('Grey Matter:', GM)
    print('White Matter:', WM)
    if use_carotid:
        CM = get_regex(os.path.join(process, s['Id'], s['Visit'],
                                    tracer + '2ROI', 'roi_svca'),
                       'blood_mask')
        if not CM:
            CM = False
        else:
            CM = CM[0]
            print('Carotid:', CM)
    else:
        CM = False
# ---------------------------- Output Directory -------------------------------
    time, no, no, delta = readSIF(s['sif'])
    if i == 0:
        normGrayTAC = np.zeros((len(time), N))
        normWhiteTAC = np.zeros((len(time), N))
        normIDIF = np.zeros((len(time), N))
        normHighBindTAC = np.zeros((len(time), N))
    A, B, C, D = superX_extractNormTACs(s['mov_corr'], WB, WM, GM, TH,
                                        delta, CM, 100, 0)
    normGrayTAC[:, i] = A
    normWhiteTAC[:, i] = B
    normIDIF[:, i] = C
    normHighBindTAC[:, i] = D
normIDIFm = np.nanmean(normIDIF, 1)
    # outs.append(normIDIFm)
im = '2'
# for fwhm in range(9):
for i, s in enumerate(subjects):
    cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
    print(s['Id'], s['Visit'])
# --------------------- Defining path to petpreproc ------------------------
    raw_pet_dir = os.path.join(raw, s['Id'], s['Visit'], tracer + im)
    if not os.path.isdir(raw_pet_dir):
        continue
    print(' raw: ', raw_pet_dir)
    test = get_regex(raw_pet_dir, tracer + im + '.sif', False)
    if not test:
        continue
    s['sif'] = test[0]
    print('SIF file :', s['sif'])
# --------------------- Defining path to petpreproc ------------------------
    petpreproc = os.path.join(process, s['Id'], s['Visit'], tracer + im +
                              'preproc')
    if not os.path.isdir(petpreproc):
        continue
    print('petpreproc', petpreproc)
    test = get_regex(petpreproc, '_movcorr.nii.gz', False)
    if not test:
        continue
    s['mov_corr'] = test[0]
    print('Found Motion Corrected', s['mov_corr'])
# ----------------------- Defining path to roi_svca ---------------------------
    roisvc = os.path.join(process, s['Id'], s['Visit'],
                          tracer + im + 'ROI', 'roi_svca')
    if not os.path.isdir(roisvc):
        continue
    print('ROI/roi_svca:', roisvc)
# -------------------------- Retrieving SVCA ROIS -----------------------------
    rois = get_regex(roisvc, '(brainmask|gm_ero|TH_ero|wm_ero)')
    if len(rois) != 4:
        continue
    TH = rois[0]
    WB = rois[1]
    GM = rois[2]
    WM = rois[3]
    print('Thalamus:', TH)
    print('Brain:', WB)
    print('Grey Matter:', GM)
    print('White Matter:', WM)
    if use_carotid:
        CM = get_regex(os.path.join(process, s['Id'], s['Visit'],
                                    tracer + '2ROI', 'roi_svca'),
                        'blood_mask')
        if not CM:
            CM = False
        else:
            CM = CM[0]
            print('Carotid:', CM)
    else:
        CM = False
# ---------------------------- Output Directory -------------------------------
    time2, no, no, delta = readSIF(s['sif'])
    if i == 0:
        normIDIF2 = np.zeros((len(time2), 2*N))
    A, B, blood, D = superX_extractNormTACs(s['mov_corr'], WB, WM, GM, TH,
                                            delta, CM, 200)
    print(blood)
    normIDIF2[:, i] = blood
normIDIF2m = np.nanmean(normIDIF2, 1)
#     # print(normIDIFfwhm)
#     # outs.append(normIDIFfwhm)
# %% Plot
plt.close()
fig = plt.figure(1)
plt.style.use('dark_background')
ax = fig.add_subplot(1, 1, 1)
ax.spines['bottom'].set_position('zero')
ax.spines['left'].set_position('zero')
ax.spines['top'].set_color('none')
ax.spines['right'].set_color('none')
colors = cm.rainbow(np.linspace(0, 1, 9))
for fwhm in range(9):
    ax.plot(time, outs[fwhm], color=colors[fwhm], marker='o',
            label='FWHM = ' + str(fwhm) + 'mm')
# ax.set_ylim(-0.35, 5)
# ax.set_xlim(-2, 15)
ax.legend()
# %% Plot
plt.close()
fig = plt.figure(1)
plt.style.use('dark_background')
ax = fig.add_subplot(1, 1, 1)
ax.spines['bottom'].set_position('zero')
ax.spines['left'].set_position('zero')
ax.spines['top'].set_color('none')
ax.spines['right'].set_color('none')
colors = cm.rainbow(np.linspace(0, 1, 9))
# for fwhm in range(10):
#     ax.plot(time, normIDIF[:, fwhm], color=colors[fwhm],
#             label='subject = ' + str(fwhm) + 'mm')
ax.plot(time, normIDIFm, color='cyan', marker='o',
        label='27 frame reconstruction')
ax.plot(time2, normIDIF2m, color='yellow', marker='o',
        label='33 frame reconstruction')
# ax.set_ylim(-0.35, 5)
ax.set_xlim(-2, 10)

ax.legend()
# out_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/clusters/blood_cluster_fr33.csv'
# f = open(out_file, 'w', newline='')
# fieldnames = ['Time', '0', '1', '2', '3', '4', '5', '6', '7', '8']
# clusters = csv.DictWriter(f, fieldnames)
# clusters.writeheader()
# for i in range(len(time)):
#     clusters.writerow({'Time': time[i],
#                        'Gray Matter': normGrayTACfine[i],
#                        'White Matter': normWhiteTACfine[i],
#                        'Blood': normIDIFfine[i],
#                        'High Binding': normHighBindTACfine[i]})
# f.close()
