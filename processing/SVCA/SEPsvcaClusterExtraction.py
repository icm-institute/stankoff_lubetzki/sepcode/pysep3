#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 11:55:10 2021

@author: arya.yazdan-panah
"""

import os
import sys
import pandas as pd
import numpy as np
import scipy.io as sio
from common3 import get_regex, init_script, createDir
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from tools_svca import superX_extractNormTACs_new, readSIF


MSG = "Extract reference region and its TAC based on the SVCA"


if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    p0 = parser.add_argument_group("Mandatory Options")
    p0.add_argument('-s', '--subjects', dest='subjects',
                    help='Subjects list, if none choices will be provided')
    p0.add_argument("-r", "--raw", dest="raw",
                    help="Raw directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-p", "--process", dest="process",
                    help="Process directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-t", "--tracer", dest="tracer", default="dpa",
                    help="Tracer used (dpa/pib/...)")
    p0.add_argument('-v', '--verbose', dest='verbose', action='store_true',
                    help='Make the program talkative')
    p1 = parser.add_argument_group("Advanced Options")
    p1.add_argument("-S", "--sif-time-unit", default='s', dest='sif_time_unit',
                    help="time unit of the sif")
    p1.add_argument("-C", "--carotid-mask", dest='use_carotid',
                    action='store_true',
                    help="Look for a mask of the carotid in the roi dir (mutua"
                    "lly exclusive '--number-of-voxels')")
    p1.add_argument("-n", "--number-of-voxels", dest='nidif', default=100,
                    help="Number of voxels for the IDIF (mutually exclusive "
                    "'--carotid-mask')", type=int)
    options = parser.parse_args()
    init = init_script(options)
    raw = init['raw']
    process = init['process']
    subjects = init['subjects']
    tracer = options.tracer.lower()
    print(colored("| Tracer used:", "blue", attrs=["bold"]), tracer.upper())
    s_count = 0
    out_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats_final_carotide_mask_median'
    createDir(out_dir)
    out_file = os.path.join(out_dir, 'All.csv')
    print(colored("| Out File:", "blue", attrs=["bold"]), out_file)
    start = True
# ------------------------------- processing  ---------------------------------
    for i, s in enumerate(subjects):
        # if s['extra'] == 'LAB':
        #     continue
        s_count += 1
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# --------------------- Defining path to petpreproc ------------------------
        raw_pet_dir = os.path.join(raw, s['Id'], s['Visit'], tracer)  # + '2')
        if not os.path.isdir(raw_pet_dir):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + tracer + ' raw not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + ' raw: ', 'green', attrs=['bold']),
              raw_pet_dir)
        test = get_regex(raw_pet_dir, tracer + '.sif', False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, SIF file not found', 'red', attrs=['bold']))
            continue
        s['sif'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| SIF file :', 'green', attrs=['bold']), s['sif'])
# --------------------- Defining path to petpreproc ------------------------
        petpreproc = os.path.join(process, s['Id'], s['Visit'], tracer +
                                  'preproc')
        if not os.path.isdir(petpreproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + petpreproc + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + 'preproc: ', 'green', attrs=['bold']),
              petpreproc)
        test = get_regex(petpreproc, '_movcorr.nii.gz', False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, Motion corrected not found', 'red',
                          attrs=['bold']))
            continue
        s['mov_corr'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Motion Corrected ' + tracer + ' :', 'green',
                      attrs=['bold']), s['mov_corr'])
        # sm_pet = changeName(s['mov_corr'], '_sm2')
        # fslmaths(s['mov_corr'], '-s', 2, sm_pet)
        # s['mov_corr'] = sm_pet
# ----------------------- Defining path to roi_svca ---------------------------
        roisvc = os.path.join(process, s['Id'], s['Visit'], tracer + 'ROI',
                              'roi_svca')
        if not os.path.isdir(roisvc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + tracer + 'ROI/roi_svca not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + 'ROI/roi_svca:', 'green',
                      attrs=['bold']), roisvc)
# -------------------------- Retrieving SVCA ROIS -----------------------------
        rois = get_regex(roisvc, '(brainmask|gm_ero|TH_ero|wm_ero)')
        if len(rois) != 4:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: expected only 4 masks in roi_svca', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        TH = rois[0]
        WB = rois[1]
        GM = rois[2]
        GM = get_regex(os.path.join(process, s['Id'], s['Visit'], tracer + 'ROI'), 'aseg_cbgm_2dpa')[0]
        WM = rois[3]
        # WM = get_regex(os.path.join(process, s['Id'], s['Visit'], tracer + 'ROI'), 'aseg_wm_2dpa')[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Thalamus:', 'green', attrs=['bold']), TH)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Brain:', 'green', attrs=['bold']), WB)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Grey Matter:', 'green', attrs=['bold']), GM)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| White Matter:', 'green', attrs=['bold']), WM)
        if True:
            CM = get_regex(roisvc, '_dpa_carotid$')
            if not CM:
                CM = False
            else:
                CM = CM[0]
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Carotid:', 'green', attrs=['bold']), CM)
        else:
            CM = False
# ---------------------------- Output Directory -------------------------------
        time, no, no, delta = readSIF(s['sif'], timeunit=options.sif_time_unit)
        A, B, C, D = superX_extractNormTACs_new(s['mov_corr'], WB, WM, GM, TH, CM)
        if start:
            gt = np.full((29, len(time)), np.nan)
            wt = np.full((29, len(time)), np.nan)
            it = np.full((29, len(time)), np.nan)
            ht = np.full((29, len(time)), np.nan)
        if start:
            start = False
            normGrayTAC = A
            normWhiteTAC = B
            normIDIF = C
            normHighBindTAC = D
        else:
            normGrayTAC = normGrayTAC + A
            normWhiteTAC = normWhiteTAC + B
            normIDIF = normIDIF + C
            normHighBindTAC = normHighBindTAC + D
        gt[i, :] = A
        wt[i, :] = B
        it[i, :] = C
        ht[i, :] = D
    df = pd.DataFrame({'Time': time,
                       'GrayMatter': normGrayTAC / s_count,
                       'Blood': normIDIF / s_count,
                       'WhiteMatter': normWhiteTAC / s_count,
                       'HighBinding': normHighBindTAC / s_count})
    # df.to_csv(out_file, index=False, header=False)
