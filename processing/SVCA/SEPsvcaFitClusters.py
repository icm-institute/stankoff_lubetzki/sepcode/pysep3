#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 16:17:23 2021

@author: arya.yazdan-panah
"""


import os
import sys
# import numpy as np
import nibabel as nib
from tools_cluster import qsub3
from common3 import get_regex, init_script, createDir, changeName
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from tools_svca import readSIF, superX_referExtraction
import matlab.engine as me


def send_cluster(subject, options):
    print(" ** Sending SVCA:",
          subject["Id"] + "_" + subject["Visit"])
    comm = "%s -s %s/%s -r %s -p %s" % (sys.argv[0], subject["Id"],
                                        subject["Visit"], options.raw,
                                        options.process)
    comm = comm + ' -t ' + options.tracer
    comm = comm + ' -S ' + options.sif_t_unit
    comm = comm + ' -K ' + options.kf
    comm = comm + ' -T ' + str(options.thr)
    logdir = os.path.join(os.environ["PROJECT_DIR"], "clulogs")
    createDir(logdir)
    name = "SVCA_" + subject["Id"] + "_" + subject["Visit"]
    etime = "00:30:00"
    ID = qsub3(comm, name, "normal", [], etime, "8G", True, logdir, '4')
    print(" **    Job ID:", ID)


MSG = "Extract reference region and its TAC based on the SVCA"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    p0 = parser.add_argument_group("Mandatory Options")
    p0.add_argument('-s', '--subjects', dest='subjects',
                    help='Subjects list, if none choices will be provided')
    p0.add_argument("-r", "--raw", dest="raw",
                    help="Raw directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-p", "--process", dest="process",
                    help="Process directory, if none deduced from PROJECT_DIR")
    p0.add_argument("-t", "--tracer", dest="tracer", default="dpa",
                    help="Tracer used (dpa/pib/...)")
    p0.add_argument("-q", "--qsub", action="store_true", dest="qsub")
    p1 = parser.add_argument_group("Advanced Options")
    p1.add_argument("-S", "--sif-time-unit", default='s', dest='sif_t_unit',
                    help="Time unit of the sif, can only be s/m/h ")
    p1.add_argument("-K", "--cluster-file", default=False, dest="kf",
                    help="Cluster file to use to fit PET on for the SVCA")
    p1.add_argument("-T", "--threshold", default=0.90, dest='thr', type=float,
                    help="Threshold determining the affiliation of a voxel to "
                    "a class. The higher the less voxels in the class")
    options = parser.parse_args()
    init = init_script(options)
    raw = init['raw']
    process = init['process']
    subjects = init['subjects']
    tracer = options.tracer.lower()
    print(colored("| Tracer used:", "blue", attrs=["bold"]), tracer.upper())
# --------------------------- Clusters to fit on  -----------------------------
    if not options.kf:
        print(colored('|', 'blue', attrs=['bold']),
              colored('ERROR: MUST GIVE kinetics file', 'red',
                      attrs=['bold']))
        sys.exit()
    elif isinstance(options.kf, str) and os.path.isfile(options.kf):
        pass
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('ERROR: Unknown input for kinetics file', 'red',
                      attrs=['bold']))
        sys.exit()
    print(colored("| Cluster File:", "blue", attrs=["bold"]), options.kf)
# --------------------------------- cluster  ----------------------------------
    if options.qsub:
        options.raw = raw
        options.process = process
        for s in subjects:
            send_cluster(s, options)
        sys.exit()
# ------------------------ Starting Matlab Engine  ----------------------------
    print(colored('|', 'blue', attrs=['bold']),
          colored('| Starting Matlab Engine', 'magenta', attrs=['reverse',
                                                                'bold']))
    matlab = me.start_matlab()
    matlab.matsep_setup(nargout=0)
# ------------------------------- processing  ---------------------------------
    for i, s in enumerate(subjects):
        cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + s['Id'] + ' ' + s['Visit'] + ': ', 'green',
                      attrs=['bold', 'reverse']))
# --------------------- Defining path to petpreproc ------------------------
        raw_pet_dir = os.path.join(raw, s['Id'], s['Visit'], tracer)
        if not os.path.isdir(raw_pet_dir):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + tracer + ' raw not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + ' raw: ', 'green', attrs=['bold']),
              raw_pet_dir)
        test = get_regex(raw_pet_dir, tracer + '.sif', False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, SIF file not found', 'red', attrs=['bold']))
            continue
        s['sif'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| SIF file :', 'green', attrs=['bold']), s['sif'])
# --------------------- Defining path to petpreproc ------------------------
        petpreproc = os.path.join(process, s['Id'], s['Visit'], tracer +
                                  'preproc')
        if not os.path.isdir(petpreproc):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + petpreproc + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + 'preproc: ', 'green', attrs=['bold']),
              petpreproc)
        test = get_regex(petpreproc, '_movcorr.nii.gz', False)
        if not test:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('Error, Motion corrected not found', 'red',
                          attrs=['bold']))
            continue
        s['mov_corr'] = test[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Motion Corrected ' + tracer + ' :', 'green',
                      attrs=['bold']), s['mov_corr'])
# --------------------------- Retrieving Brain Mask ---------------------------
# =============================================================================
#         b_mask = get_regex(petpreproc, '_brain_mask.*.nii.gz', False)
#         if not b_mask:
#             print(colored('|', 'blue', attrs=['bold']),
#                   colored('|', 'green', attrs=['bold']),
#                   colored('ERROR: brain mask not found in ' + petpreproc,
#                           'red', attrs=['bold']))
#             continue
#         b_mask = b_mask[0]
#         print(colored('|', 'blue', attrs=['bold']),
#               colored('| Found Brain Mask: ', 'green', attrs=['bold']),
#               b_mask)
# =============================================================================
# -------------------------- Retrieving cortex Mask ---------------------------
        petroi = os.path.join(process, s['Id'], s['Visit'], tracer + 'ROI')
        if not os.path.isdir(petroi):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ' + petroi + ' not found', 'red',
                          attrs=['bold']))
            print(colored('|', 'blue', attrs=['bold']),
                  colored('L', 'green', attrs=['bold']))
            continue
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + 'ROI: ', 'green', attrs=['bold']),
              petroi)
        ctx_mask = get_regex(petroi, 'ribbon_ctx.*.nii.gz', False)
        if not ctx_mask:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: ctx mask not found in ' + ctx_mask,
                          'red', attrs=['bold']))
            continue
        ctx_mask = ctx_mask[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found ctx Mask: ', 'green', attrs=['bold']),
              ctx_mask)
        b_mask = get_regex(petroi, '_brain_mask.*.nii.gz', False)
        if not b_mask:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('ERROR: brain mask not found in ' + petroi,
                          'red', attrs=['bold']))
            continue
        b_mask = b_mask[0]
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found Brain Mask: ', 'green', attrs=['bold']),
              b_mask)
# -------------------------- Retrieving SVCA ROIS -----------------------------
# ----------------------- Defining path to petsuper ---------------------------
        petsuper = os.path.join(process, s['Id'], s['Visit'],
                                tracer + 'super_young_H')
        createDir(petsuper)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| ' + tracer + 'super:', 'green', attrs=['bold']),
              petsuper)
        refer_file = changeName(s['mov_corr'], '_refregion', petsuper)
        ratio_file = changeName(s['mov_corr'], '_ratios', petsuper)
        resid_file = changeName(s['mov_corr'], '_residuals', petsuper)
        if not os.path.isfile(refer_file):
            time, start_time, end_time, delta = readSIF(s['sif'],
                                                        options.sif_t_unit)
            ref, rat, res = superX_referExtraction(s['mov_corr'], options.kf,
                                                   b_mask, time, ctx_mask,
                                                   options.thr)
            # ref, rat, res = superX_referExtraction(s['mov_corr'], options.kf,
            #                                        b_mask, time, b_mask,
            #                                        options.thr)
            pet_affine = nib.load(b_mask).affine
            ref_img = nib.Nifti1Image(ref, pet_affine)
            rat_img = nib.Nifti1Image(rat, pet_affine)
            res_img = nib.Nifti1Image(res, pet_affine)
            nib.save(ref_img, refer_file)
            nib.save(rat_img, ratio_file)
            nib.save(res_img, resid_file)
            reffile = changeName(refer_file, newext='.mat')
            matlab.create_mat_tac(s['mov_corr'], s['sif'], refer_file,
                                  reffile, nargout=0)
