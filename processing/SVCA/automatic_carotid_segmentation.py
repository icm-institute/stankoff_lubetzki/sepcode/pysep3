#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 27 14:09:42 2021

@author: arya.yazdan-panah
"""

import os
import numpy as np
import nibabel as nib
import pandas as pd
import matplotlib.pyplot as plt
from scipy import integrate
from scipy.interpolate import interp1d
from scipy.optimize import nnls
from scipy.ndimage import gaussian_filter
from common3 import readSubjectsLists, changeName, createDir
from tools_svca import readSIF, dynPET_normalization, TAC, autoCAROTID
from tools_c3 import biggest_components
from tools_fsl import fslroi, fslmaths, fslstats
import pandas as pd


subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/All_extra_frame_reconstruction.list')
process = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
raw = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw'
out_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats4'
createDir(out_dir)
stats_file = os.path.join(out_dir, 'auto_idif_3_4.csv')
df = pd.DataFrame()
for i, s in enumerate(subjects):
    print('    ', s['Id'])
    pet = os.path.join(process, s['Id'], s['Visit'], 'dpapreproc',
                       '%s_%s_dpa_movcorr.nii.gz' % (s['Id'], s['Visit']))
    blood_mask = autoCAROTID(pet, [0], 3.4, 1000)
    blood_mask = nib.load(blood_mask).get_fdata().astype(bool)
    if i == 0:
        sif = os.path.join(raw, s['Id'], s['Visit'], 'dpa',
                           '%s_%s_dpa.sif' % (s['Id'], s['Visit']))
        time, start_time, end_time, delta = readSIF(sif, timeunit='s')
    PET = nib.load(pet).get_fdata()
    mask = os.path.join(process, s['Id'], s['Visit'], 'dpapreproc',
                        '%s_V01_t1_N4_brain_mask_2dpa.nii.gz' % (s['Id']))
    WB = nib.load(mask).get_fdata().astype(bool)
    PET_size = PET.shape
    n_frames = PET_size[3]
    normIDIF = np.zeros((n_frames,))
    PET_norm = dynPET_normalization(PET, WB)
    df[s['Id']] = np.mean(PET_norm[blood_mask], 0)
df.to_csv(stats_file, index=False)
