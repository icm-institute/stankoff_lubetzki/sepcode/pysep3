#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 10:38:33 2021

@author: arya.yazdan-panah
"""
import os
from tools_fsl import fslmaths
from common3 import changeName
dsts = ['/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/SVCA/pour_Arya/BG005_V01_blood_mask.nii.gz',
'/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/SVCA/pour_Arya/BR005_V01_blood_mask.nii.gz',
'/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/SVCA/pour_Arya/CC021_V01_blood_mask.nii.gz',
'/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/SVCA/pour_Arya/DC019_V01_blood_mask.nii.gz',
'/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/SVCA/pour_Arya/DT009_V01_blood_mask.nii.gz',
'/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/SVCA/pour_Arya/GI003_V01_blood_mask.nii.gz',
'/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/SVCA/pour_Arya/GS012_V01_blood_mask.nii.gz',
'/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/SVCA/pour_Arya/MM013_V02_blood_mask.nii.gz',
'/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/SVCA/pour_Arya/MN018_V01_blood_mask.nii.gz',
'/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/SVCA/pour_Arya/SB002_V01_blood_mask.nii.gz']

for mask in dsts:
    fslmaths(mask, '-dilM', '-ero', changeName(mask, '_closed'))