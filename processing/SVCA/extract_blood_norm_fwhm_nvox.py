#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 19 16:03:25 2021

@author: arya.yazdan-panah
"""

import os
import os
import numpy as np
import nibabel as nib
import pandas as pd
import matplotlib.pyplot as plt
from scipy import integrate
from scipy.interpolate import interp1d
from scipy.optimize import nnls
from scipy.ndimage import gaussian_filter
from tqdm import tqdm
from scipy import interpolate
from common3 import readSubjectsLists, get_regex
from tools_svca import readSIF
import pandas as pd

def dynPET_normalization(DYN, mask):
    DIM = DYN.shape
    normDYN = np.zeros(DIM)
    for frame in range(DIM[3]):
        mean = np.mean(DYN[:, :, :, frame][mask.astype(bool)])
        std = np.std(DYN[:, :, :, frame][mask.astype(bool)])
        normDYN[:, :, :, frame] = (DYN[:, :, :, frame] - mean) / std
    return normDYN


subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/All_extra_frame_reconstruction.list')
process = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
raw = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw'


out_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats2'

for FWHM in [6, 7, 8, 9, 10]:
    for nVoxel in [100, 200, 300, 400, 500]:
        stat_file = os.path.join(out_dir, 'FWHM_' + str(FWHM) + '_nvox_' + str(nVoxel) + '.csv')
        df = pd.DataFrame()
        print('FWHM:', FWHM, 'nVoxel:', nVoxel)
        for i, s in enumerate(subjects):
            print('    ', s['Id'])
            pet = os.path.join(process, s['Id'], s['Visit'], 'dpapreproc', '%s_%s_dpa_movcorr.nii.gz' % (s['Id'], s['Visit']))
            mask = os.path.join(process, s['Id'], s['Visit'], 'dpapreproc', '%s_V01_t1_N4_brain_mask_2dpa.nii.gz' % (s['Id']))
            if i == 0:
                sif = os.path.join(raw, s['Id'], s['Visit'], 'dpa', '%s_%s_dpa.sif' % (s['Id'], s['Visit']))
                time, start_time, end_time, delta = readSIF(sif, timeunit='s')
            PET = nib.load(pet).get_fdata()
            WB = nib.load(mask).get_fdata().astype(bool)
            PET_size = PET.shape
            n_frames = PET_size[3]
            normIDIF = np.zeros((n_frames,))
            PET_norm = dynPET_normalization(PET, WB)
            wanted_frames = np.zeros(len(time), bool)
            wanted_frames[0] = True
            # Smoothing PET with FWHM formula
            # FWHM = 3
            # nVoxel = 200
            pixdim = 1.218750
            sigma = FWHM / (2 * np.sqrt(2 * np.log(2))) / pixdim
            PETsm = gaussian_filter(PET_norm, sigma=sigma)
            sumPET = np.sum(PETsm[:, :, :, wanted_frames], axis=3)
            # Finding nVoxel most active voxels
            indices = np.argpartition(sumPET.flatten(), -nVoxel)[-nVoxel:]
            indices = np.vstack(np.unravel_index(indices, sumPET.shape)).T
            # calculating IDIF
            finalIDIF = np.zeros(delta.shape)
            for ind in indices:
                finalIDIF = finalIDIF + PET_norm[ind[0], ind[1], ind[2], :]
            finalIDIF = finalIDIF / nVoxel
            df[s['Id']] = finalIDIF
        df.to_csv(stat_file, index=False)
