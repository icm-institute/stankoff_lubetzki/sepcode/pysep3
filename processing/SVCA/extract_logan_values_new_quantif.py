#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 17:27:47 2021

@author: arya.yazdan-panah
"""

import os
import nibabel as nib
from common3 import readSubjectsLists, get_regex
from tools_svca import TAC
import csv
sub_list = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/TR_old.list'
subjects = readSubjectsLists(sub_list)
p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/rois_good.csv'
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'Visit',
              'ROI',
              'logan',
              'Age']

names = ['thalamus',
         'cbgm',
         'cbwm',
         'wm',
         'lobes_frontal',
         'lobes_insula',
         'lobes_limbic',
         'lobes_occipital',
         'lobes_parietal',
         'lobes_temporal',
         'ctx']


Ages = {'AL059_V01': 77,
        'AL059_V02': 79,
        'AM062_V01': 62,
        'AM062_V02': 64,
        'FN003_V01': 74,
        'FN003_V02': 76,
        'GM067_V01': 67,
        'GM067_V02': 69,
        'LC093_V01': 79,
        'LC093_V02': 80,
        'MC001_V01': 72,
        'MC001_V02': 74,
        'MG011_V01': 71,
        'MG011_V02': 73,
        'MP046_V01': 79,
        'MP046_V02': 81,
        'NV058_V01': 63,
        'NV058_V02': 65,
        'PC090_V01': 71,
        'PC090_V02': 73,
        'PJ091_V01': 70,
        'PJ091_V02': 71,
        'TL066_V01': 71,
        'TL066_V02': 73,
        'TM096_V01': 65,
        'TM096_V02': 67}


stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
for s in subjects:
    print(s['Id'], s['Visit'])
    sdir = os.path.join(p, s['Id'], s['Visit'])
    logdir = os.path.join(sdir, 'dpalogan_old_classes')
    logan = nib.load(get_regex(logdir, '_vt')[0]).get_fdata()
    roidir = os.path.join(sdir, 'dpaROI')
    if s['Visit'] == 'V02':
        roidir = os.path.join(sdir, 'dpaROI_V02')
    rois = get_regex(roidir, '_(cbgm|cbwm|lobes_frontal|lobes_insula|lobes_limbic|lobes_occipital|lobes_parietal|lobes_temporal|TH|wm|ribbon_ctx)_2dpa.nii.gz', False, False, full=False)
    for i, r in enumerate(rois):
        print('   ', names[i])
        mean_logan = TAC(logan, r)
        stats.writerow({'Subject': s['Id'],
                        'Visit': s['Visit'],
                        'ROI': names[i],
                        'logan': mean_logan,
                        'Age': Ages[s['Id'] + '_' + s['Visit']]})

f.close()
# %%
import os
import nibabel as nib
from common3 import readSubjectsLists, get_regex
from tools_svca import TAC
from scipy.stats import ttest_ind
import csv

sub_list = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/TR_old.list'
subjects = readSubjectsLists(sub_list)
p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/rois_tr_old_classes.csv'
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'ROI',
              'logan1',
              'logan2',
              'TRlogan']
names = ['thalamus',
         'cbgm',
         'cbwm',
         'wm',
         'lobes_frontal',
         'lobes_insula',
         'lobes_limbic',
         'lobes_occipital',
         'lobes_parietal',
         'lobes_temporal',
         'ctx']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
for s in subjects:
    if s['Visit'] == 'V02':
        continue
    print(s['Id'])
    sdir1 = os.path.join(p, s['Id'], 'V01')
    sdir2 = os.path.join(p, s['Id'], 'V02')
    logdir1 = os.path.join(sdir1, 'dpalogan_old_classes')
    logdir2 = os.path.join(sdir2, 'dpalogan_old_classes')
    logan1 = nib.load(get_regex(logdir1, '_vt')[0]).get_fdata()
    logan2 = nib.load(get_regex(logdir2, '_vt')[0]).get_fdata()
    roidir1 = os.path.join(sdir1, 'dpaROI')
    roidir2 = os.path.join(sdir2, 'dpaROI')
    rois1 = get_regex(roidir1, '_(cbgm|cbwm|lobes_frontal|lobes_insula|lobes_limbic|lobes_occipital|lobes_parietal|lobes_temporal|TH|wm|ribbon_ctx)_2dpa.nii.gz', False, False, full=False)
    rois2 = get_regex(roidir2, '_(cbgm|cbwm|lobes_frontal|lobes_insula|lobes_limbic|lobes_occipital|lobes_parietal|lobes_temporal|TH|wm|ribbon_ctx)_2dpa.nii.gz', False, False, full=False)

    for i, (r1, r2) in enumerate(zip(rois1, rois2)):
        print(names[i])
        mean_logan1 = TAC(logan1, r1)
        mean_logan2 = TAC(logan2, r2)
        stats.writerow({'Subject': s['Id'],
                        'ROI': names[i],
                        'logan1': mean_logan1,
                        'logan2': mean_logan2,
                        'TRlogan': 200 * abs(mean_logan1 - mean_logan2) / (mean_logan1 + mean_logan2)})
f.close()
# %%

import os
import nibabel as nib
from common3 import readSubjectsLists, get_regex
from tools_svca import TAC
import csv
sub_list = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/TR_inflasep.list'
subjects = readSubjectsLists(sub_list)
p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/rois_inflasep_oldclasses.csv'
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'Visit',
              'ROI',
              'logan']
names = ['thalamus',
         'cbgm',
         'cbwm',
         'wm',
         'lobes_frontal',
         'lobes_insula',
         'lobes_limbic',
         'lobes_occipital',
         'lobes_parietal',
         'lobes_temporal',
         'ctx']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
for s in subjects:
    print(s['Id'], s['Visit'])
    sdir = os.path.join(p, s['Id'], s['Visit'])
    logdir = os.path.join(sdir, 'dpalogan_old_classes')
    logan = nib.load(get_regex(logdir, '_vt')[0]).get_fdata()
    roidir = os.path.join(sdir, 'dpaROI')
    rois = get_regex(roidir, '_(cbgm|cbwm|lobes_frontal|lobes_insula|lobes_limbic|lobes_occipital|lobes_parietal|lobes_temporal|TH|wm|ribbon_ctx)_2dpa.nii.gz', False, False, full=False)
    for i, r in enumerate(rois):
        print(names[i])
        mean_logan = TAC(logan, r)
        print(mean_logan)
        stats.writerow({'Subject': s['Id'],
                        'Visit': s['Visit'],
                        'ROI': names[i],
                        'logan': mean_logan})
f.close()


# %% Axtract all WMparc regions

import os
import nibabel as nib
import numpy as np
from common3 import readSubjectsLists, get_regex, changeName
from tools_svca import TAC
import csv

sub_list = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/TR_old.list'
subjects = readSubjectsLists(sub_list)

p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/ALLrois.csv'
f = open(stats_file, 'w', newline='')

fieldnames = ['Subject',
              'Visit',
              'ROI',
              'ROIvol',
              'logan',
              'Age']

Ages = {'AL059_V01': 77,
        'AL059_V02': 79,
        'AM062_V01': 62,
        'AM062_V02': 64,
        'FN003_V01': 74,
        'FN003_V02': 76,
        'GM067_V01': 67,
        'GM067_V02': 69,
        'LC093_V01': 79,
        'LC093_V02': 80,
        'MC001_V01': 72,
        'MC001_V02': 74,
        'MG011_V01': 71,
        'MG011_V02': 73,
        'MP046_V01': 79,
        'MP046_V02': 81,
        'NV058_V01': 63,
        'NV058_V02': 65,
        'PC090_V01': 71,
        'PC090_V02': 73,
        'PJ091_V01': 70,
        'PJ091_V02': 71,
        'TL066_V01': 71,
        'TL066_V02': 73,
        'TM096_V01': 65,
        'TM096_V02': 67}

stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
for s in subjects:
    print(s['Id'], s['Visit'])
    sdir = os.path.join(p, s['Id'], s['Visit'])
    logdir = os.path.join(sdir, 'dpalogan_old_classes')
    logan = nib.load(get_regex(logdir, '_vt')[0]).get_fdata()
    roidir = os.path.join(sdir, 'dpaROI')
    if s['Visit'] == 'V02':
        roidir = os.path.join(sdir, 'dpaROI_V02')
    WMdir = get_regex(roidir, '_individual', deep_search=False)[0]
    rois = get_regex(WMdir, '.')
    for i, r in enumerate(rois):
        to_remove = s['Id'] + '_' + s['Visit'] + '_wmparc_'
        roiname = changeName(r, '', '', '', '', to_remove)
        print('   ', roiname)
        M = nib.load(r).get_fdata().astype(bool)
        Mvol = np.sum(M)
        if Mvol == 0:
            mean_logan = np.nan
        else:
            mean_logan = TAC(logan, M)
        stats.writerow({'Subject': s['Id'],
                        'Visit': s['Visit'],
                        'ROI': roiname,
                        'ROIvol': Mvol,
                        'logan': mean_logan,
                        'Age': Ages[s['Id'] + '_' + s['Visit']]})
f.close()
