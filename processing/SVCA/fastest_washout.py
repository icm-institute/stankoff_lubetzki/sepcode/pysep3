#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 24 15:40:23 2021

@author: arya.yazdan-panah
"""

import os
import matplotlib.pyplot as plt
from tools_svca import TAC, readSIF
from common3 import readSubjectsLists
import numpy as np

slist = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/TR_HAB.list'

S = readSubjectsLists(slist)
p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
time, start_time, end_time, delta = readSIF('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw/AL059/V01/dpa/AL059_V01_dpa.sif')
# plt.figure(1)

reftac = np.zeros((len(time), len(S)))
ctxtac = np.zeros((len(time), len(S)))

for i, s in enumerate(S):
    # plt.clf()
    print(s)
    pet = os.path.join(p, s['Id'], s['Visit'], 'dpapreproc',
                       s['Id'] + '_' + s['Visit'] + '_dpa_movcorr.nii.gz')
    ref = os.path.join(p, s['Id'], s['Visit'], 'dpasuper_new_classes_arya',
                       s['Id'] + '_' + s['Visit'] + '_dpa_movcorr_refregion.nii.gz')
    ctx = os.path.join(p, s['Id'], s['Visit'], 'dpaROI',
                       s['Id'] + '_V01_ribbon_ctx_2dpa.nii.gz')
    reftac[:, i] = TAC(pet, ref)
    ctxtac[:, i] = TAC(pet, ctx)
    # plt.plot(time, TAC(pet, ref))
    # plt.plot(time, TAC(pet, ctx))
    # plt.show()
    # plt.pause(0.1)