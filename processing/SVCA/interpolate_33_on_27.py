#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 14:35:36 2021

@author: arya.yazdan-panah
"""

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from tools_svca import readSIF




in_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats4'

df33 = pd.read_csv(os.path.join(in_dir, 'norm_blood.csv'))
sif33 = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw/BG005/'\
'V01/dpa2/BG005_V01_dpa2.sif'
time33, start33, end33, delta33 = readSIF(sif33)
tac33 = df33.median(axis=1, skipna=True)


df27 = pd.read_csv(os.path.join(in_dir, 'norm_blood_carotmask.csv'))
sif27 = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw/BG005/'\
'V01/dpa/BG005_V01_dpa.sif'
time27, start27, end27, delta27 = readSIF(sif27)
tac27 = df27.median(axis=1, skipna=True)

interp_blood = interp1d(time33, tac33, 'linear', fill_value="extrapolate")
itac33 = interp_blood(time27)

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.spines['bottom'].set_position('zero')
ax.spines['top'].set_color('none')
ax.spines['right'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
plt.xlabel('time [m]')
plt.ylabel('Normalized Activity')
plt.plot(time27, tac27, label="TAC extracted on 27 frames")
plt.plot(time27, itac33, label="interp TAC33 on 27")
plt.plot(time27, np.divide(tac27, itac33), label="Ratio of the 2 TACs")
plt.plot(time27, np.multiply(itac33, np.mean(np.divide(tac27, itac33))), label="iTAC33 multiplied by the ratio")
plt.legend()