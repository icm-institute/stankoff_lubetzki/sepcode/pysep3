#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 14:38:38 2021

@author: arya.yazdan-panah
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter
from scipy.interpolate import interp1d

kinetics_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/kinetic_classes/HAB_MAB_try_young.csv'

kf = pd.read_csv(kinetics_file, header=None)
ref_time = kf[0].to_numpy().astype(float)
if ref_time[-1] < 150:
    ref_time = ref_time * 60
kin_GM = kf[1].to_numpy().astype(float)
kin_BV = kf[2].to_numpy().astype(float)
kin_WM = kf[3].to_numpy().astype(float)
kin_HB = kf[4].to_numpy().astype(float)

fig = plt.figure(1)
fig.clear()
ax = fig.add_subplot(1, 1, 1)

# Move left y-axis and bottim x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('zero')
ax.spines['bottom'].set_position('zero')

# Eliminate upper and right axes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

# Show ticks in the left and lower axes only
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

ax.scatter(ref_time, kin_GM, color='darkblue', label='GM (cerebellum)')
ax.scatter(ref_time, kin_BV, color='red', label='Blood')
ax.scatter(ref_time, kin_WM, color='cyan', label='WM (eroded)')
ax.scatter(ref_time, kin_HB, color='darkorange', label='HB (thalamus)')
ax.legend()

savgol_kin_GM = savgol_filter(kin_GM, 5, 3)
savgol_kin_BV = savgol_filter(kin_BV, 5, 3)
savgol_kin_WM = savgol_filter(kin_WM, 5, 3)
savgol_kin_HB = savgol_filter(kin_HB, 5, 3)

ax.plot(ref_time, savgol_kin_GM, color='darkblue', label='GM (cerebellum)')
ax.plot(ref_time, savgol_kin_BV, color='red', label='Blood')
ax.plot(ref_time, savgol_kin_WM, color='cyan', label='WM (eroded)')
ax.plot(ref_time, savgol_kin_HB, color='darkorange', label='HB (thalamus)')

# new_time = np.linspace(0, np.max(ref_time), len(ref_time)*10)

# spline_kin_GM = interp1d(ref_time, kin_GM, kind='cubic')(new_time)
# spline_kin_BV = interp1d(ref_time, kin_BV, kind='cubic')(new_time)
# spline_kin_WM = interp1d(ref_time, kin_WM, kind='cubic')(new_time)
# spline_kin_HB = interp1d(ref_time, kin_HB, kind='cubic')(new_time)

# ax.plot(new_time, spline_kin_GM, color='darkblue', label='GM (cerebellum)')
# ax.plot(new_time, spline_kin_BV, color='red', label='Blood')
# ax.plot(new_time, spline_kin_WM, color='cyan', label='WM (eroded)')
# ax.plot(new_time, spline_kin_HB, color='darkorange', label='HB (thalamus)')