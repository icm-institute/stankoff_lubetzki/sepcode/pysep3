#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 17 16:11:49 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from lmfit import Model

plt.close()
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2)

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats_final_carotide_mask/HAB_visit1.csv'
df = pd.read_csv(CLP, header=None)
time = df[0].to_numpy()
GM = df[1].to_numpy()
WM = df[3].to_numpy()
BL = df[2].to_numpy()
HB = df[4].to_numpy()

ax = ax1
ax.set_title('HAB')
ax.plot(time, GM, 'b', label='Gray Matter', marker='s')
ax.plot(time, WM, 'c', label='White Matter', marker='s')
ax.plot(time, BL, 'r', label='Blood', marker='s')
ax.plot(time, HB, 'orange', label='High Binding', marker='s')
ax.legend()
ax.set_xlabel('Time')
ax.xaxis.set_label_coords(1, 0.38)
ax.set_ylabel('normTAC')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.spines['left'].set_position('zero')


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats_final_carotide_mask/MAB_visit1.csv'
df = pd.read_csv(CLP, header=None)
time = df[0].to_numpy()
GM = df[1].to_numpy()
WM = df[3].to_numpy()
BL = df[2].to_numpy()
HB = df[4].to_numpy()

ax = ax2
ax.set_title('MAB')
ax.plot(time, GM, 'b', label='Gray Matter', marker='s')
ax.plot(time, WM, 'c', label='White Matter', marker='s')
ax.plot(time, BL, 'r', label='Blood', marker='s')
ax.plot(time, HB, 'orange', label='High Binding', marker='s')
ax.legend()
ax.set_xlabel('Time')
ax.xaxis.set_label_coords(1, 0.38)
ax.set_ylabel('normTAC')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.spines['left'].set_position('zero')


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats_final_carotide_mask/HAB_MAB_visit1.csv'
df = pd.read_csv(CLP, header=None)
time = df[0].to_numpy()
GM = df[1].to_numpy()
WM = df[3].to_numpy()
BL = df[2].to_numpy()
HB = df[4].to_numpy()

ax = ax3
ax.set_title('HAB and MAB merged')
ax.plot(time, GM, 'b', label='Gray Matter', marker='s')
ax.plot(time, WM, 'c', label='White Matter', marker='s')
ax.plot(time, BL, 'r', label='Blood', marker='s')
ax.plot(time, HB, 'orange', label='High Binding', marker='s')
ax.legend()
ax.set_xlabel('Time')
ax.xaxis.set_label_coords(1, 0.38)
ax.set_ylabel('normTAC')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.spines['left'].set_position('zero')


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats_final_carotide_mask/LAB_visit1.csv'
df = pd.read_csv(CLP, header=None)
time = df[0].to_numpy()
GM = df[1].to_numpy()
WM = df[3].to_numpy()
BL = df[2].to_numpy()
HB = df[4].to_numpy()

ax = ax4
ax.set_title('LAB')
ax.plot(time, GM, 'b', label='Gray Matter', marker='s')
ax.plot(time, WM, 'c', label='White Matter', marker='s')
ax.plot(time, BL, 'r', label='Blood', marker='s')
ax.plot(time, HB, 'orange', label='High Binding', marker='s')
ax.legend()
ax.set_xlabel('Time')
ax.xaxis.set_label_coords(1, 0.38)
ax.set_ylabel('normTAC')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.spines['left'].set_position('zero')