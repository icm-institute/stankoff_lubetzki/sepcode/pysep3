#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 11:14:17 2021

@author: arya.yazdan-panah
"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import nibabel as nib
from pprint import pprint
from common3 import get_regex, cmdWoutput
from tools_nibabel import flip_nii
from tools_svca import TAC, readSIF
import scipy as sp
from scipy import interpolate
import matplotlib.cm as cm

df = pd.read_csv('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/IFs.csv')
df1 = pd.read_csv('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/Image_derived_IFs.csv')
df2 = pd.read_csv('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/blood_with_max_3sigma_1frame.csv')
time1, no, no, no = readSIF('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw/BG005/V01/dpa/BG005_V01_dpa.sif')
time, start_time, end_time, delta = readSIF('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw/BG005/V01/dpa2/BG005_V01_dpa2.sif')
plt.figure(1)
plt.clf()
colors = cm.rainbow(np.linspace(0, 1, 10))

def interpolate_blood_tac(blood_time, blood_input_function, start_time,
                          end_time, delay=0):
    interpolated_blood = np.zeros(start_time.shape)
    blood_time = blood_time - delay
    ici = np.where(np.diff(blood_input_function) == 0)
    blood_input_function[ici] += 1e-6
    for i in range(len(start_time)):
        idx = np.where((blood_time >= start_time[i]) &
                       (blood_time <= end_time[i]))
        time_frame = blood_time[idx]
        blood_frame = blood_input_function[idx]
        if not(len(idx[0])) or blood_time[idx[0][0]] != start_time[i]:

            interp_blood = interpolate.interp1d(blood_time,
                                                blood_input_function,
                                                'linear',
                                                fill_value="extrapolate")
            blood_interped1 = interp_blood(start_time[i])
            time_frame = np.append(start_time[i], time_frame)
            blood_frame = np.append(blood_interped1, blood_frame)
        if not(len(idx[0])) or blood_time[idx[0][-1]] != end_time[i]:
            interp_blood = interpolate.interp1d(blood_time,
                                                blood_input_function,
                                                'linear',
                                                fill_value="extrapolate")
            blood_interped2 = interp_blood(end_time[i])
            time_frame = np.append(time_frame, end_time[i])
            blood_frame = np.append(blood_frame, blood_interped2)
        interpolated_blood[i] = np.trapz(blood_frame, time_frame) / (end_time[i] - start_time[i])
    return interpolated_blood

ICM = np.zeros((33, 10))
ICM_max = np.zeros((27, 10))

IF = np.zeros(21)
iIF = np.zeros((33, 10))
for i in range(10):
    # plt.figure()
    tac_mask_1 = df1.iloc[:, i].to_numpy()
    tac_mask_2 = df2.iloc[:, i + 1].to_numpy()
    # plt.plot(time, tac_mask, c=colors[i], linestyle='--', label='Image input function')
    # plt.plot(time, tac_cea, c=colors[i], linestyle=':', label='Image input function')
    Input_function = df.iloc[:, 2*i + 1].to_numpy()
    timeif = df.iloc[:, 2*i].to_numpy()

    iInput_function = interpolate_blood_tac(timeif, Input_function, start_time, end_time, 0.5)
    to_fit = np.c_[ iInput_function, np.ones(len(iInput_function)) ]
    try:
        x, res, rank, s = sp.linalg.lstsq(to_fit, tac_mask_1)
    except ValueError:
        x, res, rank, s  = sp.linalg.lstsq(to_fit[:-1], tac_mask_1[:-1])
    print(x, res)
    # plt.plot(time, Input_function, c=colors[i], label='arterial input function')
    # plt.plot(time, iInput_function - tac_mask)
    ICM[:,i] = tac_mask_1
    ICM_max[:,i] = tac_mask_2
    IF += Input_function
    iIF[:,i] = iInput_function
    # plt.legend()


# plt.plot(time, ICM/10, label='TAC inside the carotid masks')
# plt.plot(time, iIF/10, label='TAC of the non metabolite corrected input function')


plt.clf()

plt.plot(time, np.nanmean(ICM, 1), 'r', label='TAC inside the carotid masks')
# plt.fill_between(time, np.nanmean(ICM, 1) - np.nanstd(ICM, 1),
#                   np.nanmean(ICM, 1) + np.nanstd(ICM, 1),
#                   color='r', alpha=0.2)

plt.plot(time1, np.nanmean(ICM_max, 1), 'g',
          label='TAC extracetd with 200 max voxels and $\sigma$ = 3mm')
# plt.fill_between(time, np.nanmean(ICM_ero2, 1) - np.nanstd(ICM_ero2, 1),
#                  np.nanmean(ICM_ero2, 1) + np.nanstd(ICM_ero2, 1),
#                  color='g', alpha=0.2)


plt.plot(time, np.nanmean(iIF,1), 'k',
          label='TAC of the non metabolite corrected input function')
# plt.fill_between(time, np.nanmean(iIF,1) - np.nanstd(iIF,1), np.nanmean(iIF,1) + np.nanstd(iIF,1),
#                  color='k', alpha=0.2)
plt.legend()
plt.xlabel('time [m]')
plt.ylabel('Activity')


