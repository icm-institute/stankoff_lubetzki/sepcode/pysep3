#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 18:26:13 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.optimize import curve_fit
from lmfit import Model


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/ALLrois.csv'
df = pd.read_csv(CLP)

plt.close()
plt.style.use('default')
fig, ax = plt.subplots()
sns.boxplot(ax=ax, x="ROI", y="logan", hue="Visit", data=df)


# %%

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.optimize import curve_fit
from lmfit import Model


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/rois_tr.csv'
df = pd.read_csv(CLP)

plt.close()
fig, ax = plt.subplots()
sns.boxplot(ax=ax, x="ROI", y="TRlogan", data=df)


# %%
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.optimize import curve_fit
from lmfit import Model
CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/rois_good.csv'
df = pd.read_csv(CLP)

plt.style.use('default')
names = ['thalamus',
         'cbgm',
         'cbwm',
         'wm',
         'lobes_frontal',
         'lobes_insula',
         'lobes_limbic',
         'lobes_occipital',
         'lobes_parietal',
         'lobes_temporal',
         'ctx']
for ROI in names:
    fig, ax = plt.subplots()
    sns.scatterplot(ax=ax, x="Age", y="logan", hue="Visit", data=df[df['ROI'] == ROI])
    for s in df['Subject'].unique():
        sns.lineplot(ax=ax, x="Age", y="logan", data=df.loc[(df['ROI'] == ROI) & (df['Subject'] == s)], color='grey')
    ax.set_title(ROI)


# %% wilcoxxxxxxxx

import numpy as np
import pandas as pd
from scipy.stats import wilcoxon

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats/ALLrois.csv'
df = pd.read_csv(CLP)

for ROI in df['ROI'].unique():
    stat = 1
    V01 = df.loc[(df['ROI'] == ROI) & (df['Visit'] == 'V01')]['logan']
    V02 = df.loc[(df['ROI'] == ROI) & (df['Visit'] == 'V02')]['logan']
    try:
        test = wilcoxon(V01, V02)
        print(ROI, 'stat:', stat * test.statistic, 'p-val:', test.pvalue)
    except ValueError:
        print(ROI)
        print(V01)
        print(V02)

    # if ROI == 'wm':
    #     break
