#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 11:14:17 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from lmfit import Model

plt.close()
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, True, True)

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats_final/HAB_visit1.csv'
df = pd.read_csv(CLP)
time = df["Time"].to_numpy()
GM = df["Gray Matter"].to_numpy()
WM = df["White Matter"].to_numpy()
BL = df["Blood"].to_numpy()
HB = df["High Binding"].to_numpy()

ax = ax1
ax.set_title('HAB')
ax.plot(time, GM, 'b', label='Gray Matter', marker='s')
ax.plot(time, WM, 'c', label='White Matter', marker='s')
ax.plot(time, BL, 'r', label='Blood', marker='s')
ax.plot(time, HB, 'orange', label='High Binding', marker='s')
ax.legend()
ax.set_xlabel('Time')
ax.xaxis.set_label_coords(1, 0.38)
ax.set_ylabel('normTAC')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.spines['left'].set_position('zero')


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats_final/MAB_visit1.csv'
df = pd.read_csv(CLP)
time = df["Time"].to_numpy()
GM = df["Gray Matter"].to_numpy()
WM = df["White Matter"].to_numpy()
BL = df["Blood"].to_numpy()
HB = df["High Binding"].to_numpy()

ax = ax2
ax.set_title('MAB')
ax.plot(time, GM, 'b', label='Gray Matter', marker='s')
ax.plot(time, WM, 'c', label='White Matter', marker='s')
ax.plot(time, BL, 'r', label='Blood', marker='s')
ax.plot(time, HB, 'orange', label='High Binding', marker='s')
ax.legend()
ax.set_xlabel('Time')
ax.xaxis.set_label_coords(1, 0.38)
ax.set_ylabel('normTAC')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.spines['left'].set_position('zero')


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats_final/merged_HAB_MAB_visit1.csv'
df = pd.read_csv(CLP)
time = df["Time"].to_numpy()
GM = df["Gray Matter"].to_numpy()
WM = df["White Matter"].to_numpy()
BL = df["Blood"].to_numpy()
HB = df["High Binding"].to_numpy()

ax = ax3
ax.set_title('HAB and MAB merged')
ax.plot(time, GM, 'b', label='Gray Matter', marker='s')
ax.plot(time, WM, 'c', label='White Matter', marker='s')
ax.plot(time, BL, 'r', label='Blood', marker='s')
ax.plot(time, HB, 'orange', label='High Binding', marker='s')
ax.legend()
ax.set_xlabel('Time')
ax.xaxis.set_label_coords(1, 0.38)
ax.set_ylabel('normTAC')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.spines['left'].set_position('zero')


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats_final/LAB_visit1.csv'
df = pd.read_csv(CLP)
time = df["Time"].to_numpy()
GM = df["Gray Matter"].to_numpy()
WM = df["White Matter"].to_numpy()
BL = df["Blood"].to_numpy()
HB = df["High Binding"].to_numpy()

ax = ax4
ax.set_title('LAB')
ax.plot(time, GM, 'b', label='Gray Matter', marker='s')
ax.plot(time, WM, 'c', label='White Matter', marker='s')
ax.plot(time, BL, 'r', label='Blood', marker='s')
ax.plot(time, HB, 'orange', label='High Binding', marker='s')
ax.legend()
ax.set_xlabel('Time')
ax.xaxis.set_label_coords(1, 0.38)
ax.set_ylabel('normTAC')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.spines['left'].set_position('zero')
