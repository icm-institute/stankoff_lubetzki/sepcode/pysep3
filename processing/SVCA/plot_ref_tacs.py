#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 14 15:39:18 2021

@author: arya.yazdan-panah
"""

import os
import numpy as np
from common3 import readSubjectsLists, get_regex
from tools_svca import TAC, readSIF
import matplotlib.pyplot as plt

subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/TR_old.list')
p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
N = len(subjects)
super_dir = 'dpasuper_young'
tacsctx = np.zeros((27,N))
tacsref = np.zeros((27,N))
for i, s in enumerate(subjects):
    print(s['Id'], s['Visit'])
    tep = get_regex(os.path.join(p, s['Id'], s['Visit'], 'dpapreproc'), '_dpa_movcorr.nii.gz', 0)[0]
    ref_mask = get_regex(os.path.join(p, s['Id'], s['Visit'], super_dir), '_dpa_movcorr_refregion.nii.gz', 0)[0]
    ctx_mask = get_regex(os.path.join(p, s['Id'], s['Visit'], 'dpaROI'), '_ribbon_ctx_2dpa.nii.gz', 0)[0]
    tacsref[:,i] = TAC(tep, ref_mask)
    tacsctx[:,i] = TAC(tep, ctx_mask)
# %
time, no, no, no = readSIF('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw/BG005/V01/dpa/BG005_V01_dpa.sif')
# plt.figure(1)
# plt.plot(time, tacs)

# # %
# subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/TR_HAB_MAB.list')
# p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
# N = len(subjects)
# super_dir = 'dpasuper_try_young_savgol'
# tacs2 = np.zeros((27,N))
# for i, s in enumerate(subjects):
#     print(s['Id'], s['Visit'])
#     tep = get_regex(os.path.join(p, s['Id'], s['Visit'], 'dpapreproc'), '_dpa_movcorr.nii.gz', 0)[0]
#     ref_mask = get_regex(os.path.join(p, s['Id'], s['Visit'], super_dir), '_dpa_movcorr_refregion.nii.gz', 0)[0]
#     tacs2[:,i] = TAC(tep, ref_mask)

# %%

# plt.figure(1)
# plt.plot(time, tacs)