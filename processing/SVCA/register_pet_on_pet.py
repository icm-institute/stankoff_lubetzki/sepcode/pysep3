#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 22 14:50:52 2021

@author: arya.yazdan-panah
"""

import os
from common3 import get_regex, readSubjectsLists, changeName
from termcolor import cprint, colored
from tools_ants import AntsRegistrationSyn, ApplyRegistration


process = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/All_extra_frame_reconstruction.list')
tracer = 'dpa'
print(colored("| Tracer used:", "blue", attrs=["bold"]), tracer.upper())
N = len(subjects)
use_carotid = True
im = '2'
# for fwhm in range(9):
for i, s in enumerate(subjects):
    cprint('|', 'blue', attrs=['bold'])
    print(s['Id'], s['Visit'])
    petpreproc2 = os.path.join(process, s['Id'], s['Visit'], tracer +
                               '2preproc')
    if not os.path.isdir(petpreproc2):
        continue
    test = get_regex(petpreproc2, '_movcorr_mean.nii.gz', False)
    if not test:
        continue
    s['mov_corr2'] = test[0]
    print('Found Motion Corrected2', s['mov_corr2'])
    petpreproc1 = os.path.join(process, s['Id'], s['Visit'], tracer +
                               'preproc')
    if not os.path.isdir(petpreproc1):
        continue
    test = get_regex(petpreproc1, '_movcorr_mean.nii.gz', False)
    if not test:
        continue
    s['mov_corr1'] = test[0]
    print('Found Motion Corrected1', s['mov_corr1'])

    warp = changeName(s['mov_corr2'], '_2dpa1', petpreproc1)
    invwarp = changeName(s['mov_corr1'], '_2dpa2', petpreproc1)
    reg = AntsRegistrationSyn(s['mov_corr2'], s['mov_corr1'], warp, invwarp,
                              'r')
    dpa2dpa = reg['forward_transforms']
    roisvc = os.path.join(process, s['Id'], s['Visit'],
                          tracer + '2ROI', 'roi_svca')
    if not os.path.isdir(roisvc):
        continue
    rois = get_regex(roisvc, '_blood_mask_closed')[0]
    warp = changeName(rois, '_2dpa1', petpreproc1)
    ApplyRegistration(rois, s['mov_corr1'], dpa2dpa, warp,
                          interp='NearestNeighbor')