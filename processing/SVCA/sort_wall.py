#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 23 14:42:03 2021

@author: arya.yazdan-panah
"""

import numpy as np

rectangles = [(180, 255),   #Turkish
              (180, 255),   #Turkish
              (180, 255),   #Turkish
              (180, 255),   #Turkish
              (180, 255),   #Turkish
              (180, 255),   #Turkish
              (180, 255),   #Turkish
              (150, 260),   #Austriansmall
              (209, 269),   #ANAgreen
              (200, 220),   #Aegan
              (200, 220),   #Aegan
              (235, 225),   #transavia
              (100, 210),   #AF
              (100, 210),
              (100, 210),
              (100, 210)]

from rectpack import (newPacker, PackingMode, SORT_RATIO, SORT_PERI, SORT_AREA,
                      SORT_DIFF, SORT_SSIDE, SORT_LSIDE)

bins = [(860, 2050)]

packer = newPacker(mode=PackingMode.Offline,sort_algo=SORT_RATIO,rotation=False)

# Add the rectangles to packing queue
for r in rectangles:
	packer.add_rect(*r)

# Add the bins where the rectangles will be placed
for b in bins:
	packer.add_bin(*b)

# Start packing
packer.pack()

import matplotlib.pyplot as plt
from matplotlib import patches

output = []
for index, abin in enumerate(packer):
    bw, bh  = abin.width, abin.height
    print('bin', bw, bh, "nr of rectangles in bin", len(abin))
    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    for rect in abin:
        x, y, w, h = rect.x, rect.y, rect.width, rect.height
        output.append([x,y,w,h])
        plt.axis([0,bw,0,bh])
        print('rectangle', w,h)
        ax.add_patch(patches.Rectangle((x, y),  # (x,y)
                                       w,          # width
                                       h,          # height
                                       facecolor="#00ffff",
                                       edgecolor="black",
                                       linewidth=3))
    plt.show()
    # fig.savefig("rect_%(index)s.png" % locals(), dpi=144, bbox_inches='tight')

# printing the rectangle coordinates to plot them in P5JS
print(output)

