#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 11:35:03 2021

@author: arya.yazdan-panah
"""

import nibabel as nib
import os
import matplotlib.pyplot as plt
from tools_svca import TAC, dynPET_normalization,readSIF


pet = ["/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/AM062/V01/dpapreproc/AM062_V01_dpa_movcorr.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/AM062/V02/dpapreproc/AM062_V02_dpa_movcorr.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BA055/V01/dpapreproc/BA055_V01_dpa_movcorr.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpapreproc/BG005_V01_dpa_movcorr.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V02/dpapreproc/BG005_V02_dpa_movcorr.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BP002/V01/dpapreproc/BP002_V01_dpa_movcorr.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/CA036/V01/dpapreproc/CA036_V01_dpa_movcorr.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/CC021/V01/dpapreproc/CC021_V01_dpa_movcorr.nii.gz"]

mask = ["/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/AM062/V01/dpapreproc/AM062_V01_t1_N4_brain_mask_2dpa.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/AM062/V02/dpapreproc/AM062_V01_t1_N4_brain_mask_2dpa.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BA055/V01/dpapreproc/BA055_V01_t1_N4_brain_mask_2dpa.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpapreproc/BG005_V01_t1_N4_brain_mask_2dpa.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V02/dpapreproc/BG005_V01_t1_N4_brain_mask_2dpa.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BP002/V01/dpapreproc/BP002_V01_t1_N4_brain_mask_2dpa.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/CA036/V01/dpapreproc/CA036_V01_t1_N4_brain_mask_2dpa.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/CC021/V01/dpapreproc/CC021_V01_t1_N4_brain_mask_2dpa.nii.gz"]


carot = ["/network/lustre/iss01/lubetzki-stankoff/clinic/users/Theodore/mission/carotides_seg/AM062_V01_dpa_movcorr_frame_1_sm_4_norm_seg.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/users/Theodore/mission/carotides_seg/AM062_V02_dpa_movcorr_frame_1_sm_4_norm_seg.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/users/Theodore/mission/carotides_seg/BA055_V01_dpa_movcorr_frame_1_sm_4_norm_seg.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/users/Theodore/mission/carotides_seg/BG005_V01_dpa_movcorr_frame_1_sm_4_norm_seg2.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/users/Theodore/mission/carotides_seg/BG005_V02_dpa_movcorr_frame_1_sm_4_norm_seg.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/users/Theodore/mission/carotides_seg/BP002_V01_dpa_movcorr_frame_1_sm_4_norm_seg.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/users/Theodore/mission/carotides_seg/CA036_V01_dpa_movcorr_frame_1_sm_4_norm_seg.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/users/Theodore/mission/carotides_seg/CC021_V01_dpa_movcorr_frame_1_sm_4_norm_seg.nii.gz"]


ctx = ["/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/AM062/V01/dpaROI/roi_svca/AM062_V01_gm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/AM062/V02/dpaROI/roi_svca/AM062_V01_gm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BA055/V01/dpaROI/roi_svca/BA055_V01_gm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpaROI/roi_svca/BG005_V01_gm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V02/dpaROI/roi_svca/BG005_V01_gm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BP002/V01/dpaROI/roi_svca/BP002_V01_gm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/CA036/V01/dpaROI/roi_svca/CA036_V01_gm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/CC021/V01/dpaROI/roi_svca/CC021_V01_gm_ero.nii.gz"]


wm = ["/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/AM062/V01/dpaROI/roi_svca/AM062_V01_wm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/AM062/V02/dpaROI/roi_svca/AM062_V01_wm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BA055/V01/dpaROI/roi_svca/BA055_V01_wm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpaROI/roi_svca/BG005_V01_wm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V02/dpaROI/roi_svca/BG005_V01_wm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BP002/V01/dpaROI/roi_svca/BP002_V01_wm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/CA036/V01/dpaROI/roi_svca/CA036_V01_wm_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/CC021/V01/dpaROI/roi_svca/CC021_V01_wm_ero.nii.gz"]


th = ["/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/AM062/V01/dpaROI/roi_svca/AM062_V01_TH_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/AM062/V02/dpaROI/roi_svca/AM062_V01_TH_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BA055/V01/dpaROI/roi_svca/BA055_V01_TH_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpaROI/roi_svca/BG005_V01_TH_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V02/dpaROI/roi_svca/BG005_V01_TH_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BP002/V01/dpaROI/roi_svca/BP002_V01_TH_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/CA036/V01/dpaROI/roi_svca/CA036_V01_TH_ero.nii.gz",
"/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/CC021/V01/dpaROI/roi_svca/CC021_V01_TH_ero.nii.gz"]


time, start_time, end_time, delta = readSIF("/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw/BG005/V01/dpa/BG005_V01_dpa.sif")

index = 0

for i, (p, m, a, b, c, d) in enumerate(zip(pet, mask, carot, ctx, wm, th)):
    norm_pet = dynPET_normalization(p, m)
    if i==0:
        carotac = TAC(norm_pet, a)
        gmtac = TAC(norm_pet, b)
        wmtac = TAC(norm_pet, c)
        thtac = TAC(norm_pet, d)
    carotac = carotac + TAC(norm_pet, a)
    gmtac = gmtac + TAC(norm_pet, b)
    wmtac = wmtac + TAC(norm_pet, c)
    thtac = thtac + TAC(norm_pet, d)

plt.close()
fig, ax = plt.subplots(1, 1, True, True)

ax.plot(time, gmtac/9, 'b', label='Gray Matter', marker='s')
ax.plot(time, wmtac/9, 'c', label='White Matter', marker='s')
ax.plot(time, carotac/9, 'r', label='Blood', marker='s')
ax.plot(time, thtac/9, 'orange', label='High Binding', marker='s')
ax.legend()
ax.set_xlabel('Time')
ax.xaxis.set_label_coords(1, 0.38)
ax.set_ylabel('normTAC')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.spines['left'].set_position('zero')
