#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 18 12:07:57 2021

@author: arya.yazdan-panah
"""




import os
import numpy as np
import nibabel as nib
import pandas as pd
import matplotlib.pyplot as plt

from scipy import integrate
from scipy.interpolate import interp1d
from scipy.optimize import nnls, lsq_linear
from scipy.ndimage import gaussian_filter
from tqdm import tqdm
from scipy import interpolate
from common3 import changeName
from tools_fsl import fslroi, fslmaths, fslstats


def readSIF(siffile, timeunit='s'):
    if not os.path.isfile(siffile):
        return False
    A = open(siffile, 'r')
    lines = A.readlines()
    nlines = len(lines)
    A.close()
    start_time = np.zeros((nlines - 1,))
    end_time = np.zeros((nlines - 1,))
    for i, line in enumerate(lines):
        if not i:
            continue
        line = line.split()
        start_time[i - 1] = float(line[0])
        end_time[i - 1] = float(line[1])
    delta = end_time - start_time
    time = start_time + (0.5 * delta)
    if timeunit == 's':
        return time / 60, start_time / 60, end_time / 60, delta / 60
    elif timeunit == 'm':
        return time, start_time, end_time, delta
    elif timeunit == 'h':
        return time * 60, start_time * 60, end_time * 60, delta * 60
    else:
        return False


def dynPET_normalization(DYN, mask):
    DIM = DYN.shape
    normDYN = np.zeros(DIM)
    for frame in range(DIM[3]):
        mean = np.mean(DYN[:, :, :, frame][mask.astype(bool)])
        std = np.std(DYN[:, :, :, frame][mask.astype(bool)])
        normDYN[:, :, :, frame] = (DYN[:, :, :, frame] - mean) / std
    return normDYN

pet_file = "/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpapreproc/BG005_V01_dpa_movcorr.nii.gz"
brain_mask = "/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpapreproc/BG005_V01_t1_N4_brain_mask_2dpa.nii.gz"
kinetics_file = "/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/stats_final/HAB_visit1.csv"
time, start_time, end_time, delta = readSIF("/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw/BG005/V01/dpa/BG005_V01_dpa.sif")
# reference_mask_file = "/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpaROI/BG005_V01_aparc+aseg_cbgm_2dpa.nii.gz"
reference_mask_file = "/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpaROI/roi_svca/BG005_V01_gm_ero.nii.gz"
petsuper = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpasuper'
sm_pet = changeName(pet_file, '_sm' + str(2), petsuper)
# fslmaths(pet_file, '-s', 2, sm_pet)
# pet_file = sm_pet

pet = nib.load(pet_file).get_fdata()
WB = nib.load(brain_mask).get_fdata().astype(bool)
RM = nib.load(reference_mask_file).get_fdata().astype(bool)
kf = pd.read_csv(kinetics_file)
ref_time = kf['Time'].values
kin_GM = kf['Gray Matter'].values
kin_WM = kf['White Matter'].values
kin_BV = kf['Blood'].values
kin_HB = kf['High Binding'].values
# %%
for max_time, moment in reversed(list(enumerate(time))):
    if moment <= ref_time[-1]:
        break
limited_time = time[:max_time]
kinetics = np.column_stack((kin_GM, kin_WM, kin_BV, kin_HB))
kinetics_interped = interp1d(ref_time, kinetics, axis=0)(limited_time)
PET_norm = dynPET_normalization(pet, WB)
PET_norm_flat = PET_norm.reshape(-1, PET_norm.shape[-1])
RM_flat = RM.ravel()
Ratios = np.zeros((PET_norm_flat.shape[0], 4))
Residuals = np.zeros(PET_norm_flat.shape[0])
for vox in tqdm(range(len(PET_norm_flat))):
    if not RM_flat[vox]:
        continue
    Ratios[vox, :], Residuals[vox] = nnls(kinetics_interped,
                                          PET_norm_flat[vox, 0:max_time])
    if sum(Ratios[vox, :]) > 0:
        Ratios[vox, :] = Ratios[vox, :] / sum(Ratios[vox, :])

Ratios = Ratios.reshape(WB.shape + (4,))
Residuals = Residuals.reshape(WB.shape)
ref_mask = np.zeros(WB.shape)
thr = 0.9
ref_mask[(RM == 1) & (Ratios[..., 0] > thr)] = 1
reference_mask_file = "/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpaROI/roi_svca/BG005_V01_gm_ero.nii.gz"
RM = nib.load(reference_mask_file).get_fdata().astype(bool)
plt.figure(1)
plt.clf()
plt.plot(time, np.mean(pet[ref_mask.astype(bool)], 0), label="extracted reference in CTX")
plt.plot(time, np.mean(pet[RM], 0), label="TAC in cortex")
plt.legend()
refer_file = "/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpasuper/BG005_V01_dpa_refregion_09.nii.gz"
ratio_file = "/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process/BG005/V01/dpasuper/BG005_V01_dpa_ratios.nii.gz"
pet_affine = nib.load(brain_mask).affine
ref_img = nib.Nifti1Image(ref_mask, pet_affine)
nib.save(ref_img, refer_file)
ratios_img = nib.Nifti1Image(Ratios, pet_affine)
nib.save(ratios_img, ratio_file)

# =============================================================================
#     result = lsq_linear(kinetics_interped, PET_norm_flat[vox, 0:max_time],
#                         bounds=(0, np.inf))
#     Ratios[vox, :] = result.x
#     Residuals[vox] = np.mean(result.fun)
# =============================================================================
# sum_Ratios = np.sum(Ratios, len(Ratios.shape) - 1)
# for k in range(4):
#     Ratios[..., k] = np.divide(Ratios[..., k], sum_Ratios)