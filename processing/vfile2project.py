#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
from common3 import changeName, command, createDir
from common3 import *
from optparse import OptionParser


def createSIF(minffile, siffile):
    print(" -- Creating SIF file")
    att = "".join(open(minffile).readlines())
    exec(att, globals())
    durtime = None
    strtime = None
    if 'duration_time' in attributes and 'start_time' in attributes:
        durtime = attributes['duration_time']
        strtime = attributes['start_time']
    if durtime and strtime and not os.path.exists(siffile):
        import datetime
        hoy = datetime.datetime.now()
        hoy.strftime("%d/%m/%y %H:%M:%S")
        p = open(siffile, "w")
        p.write(hoy.strftime("%d/%m/%y %H:%M:%S") + " " + str(len(strtime)) +
                " 4 2 DGL F-18\n")
        for i in range(len(strtime)):
            p.write(str(strtime[i]/1000.0) + "\t" + str((float(strtime[i]) +
                                                         float(durtime[i])) /
                                                        1000.0) + "\t0\t0\n")
        p.close()
    else:
        print(" -- MISSING INFO OR FILE EXISTS")


def convertPET(vfile, output):
    if os.path.exists(output):
        return
    # Convert (in the same folder) the v file to nifti (if necessary)
    niifile = ""
    try:
        [head, tail] = os.path.split(output)
        siffile = changeName(output, newext=".sif")
        niifile = changeName(vfile, "_tmp", outdir=head, newext=".nii.gz")
#        niifile = vfile[:-2] + "_tmp.nii.gz"
        minf = niifile + ".minf"
        if vfile.endswith(".v"):
            print(" -- Converting intput in Nifti")
            comm = ["AimsFileConvert", "-i", vfile, "-o", niifile]
            if command(comm, vfile, niifile):
                print(" ** ERROR in AimsFileConvert: Please verify the file is"
                      " supported by this tool")
                sys.exit(1)
            print(minf)
            if os.path.exists(minf) and not os.path.exists(siffile):
                createSIF(minf, siffile)
            elif not os.path.exists(minf):
                print(" ** minf not found!!")
        else:
            print(" -- Input file already in Nifti")
            niifile = vfile
        # Reorient to RPI and move to the good foler
        print(" -- Converting image to RPI ")
        comm = ["toRPI.py", niifile, output]
        if command(comm, niifile, output) == 0:
            print(" -- Finish!")
            print(" -- Image is found in")
            print(output)
        else:
            print(" ** ERROR in toRPI.py")
            sys.exit(1)
    finally:
        if not niifile == vfile:
            print(" -- Erasing intermediary files")
            comm = ['rm', '-f', niifile, minf]
            command(comm)


if __name__ == '__main__':
    usage = "usage: %prog petfile.v PatientID/Visit"
    if len(sys.argv) < 3:
        print("")
        print(" -- Usage: "+sys.argv[0] + " petfile.v PatientID/Visit")
        print("                       By default the nameOfsequence is pib")
        print("                       If no visit is given V01 is used")
        print("")
        print("    or   : "+sys.argv[0]+" petfile.v PatientID/Visit nameOfsequ"
              "ence")
        print("                       To precise the name of the sequence")
        print("")
        print("    or   : "+sys.argv[0]+" petfile.v PatientID/Visit nameOfsequ"
              "ence WRITE")
        print("                       Avoid checking for existing PatientID an"
              "d Visit")
        print("                       Be careful with this option")
        print("")
        print("  Only one image at a time")
        print("")
        sys.exit(1)
    # Parse input arguments
    parser = OptionParser(usage=usage)
    parser.add_option("-o", "--output", dest="output",
                      help=" Avoid project setup, only give output image")
    (options, args) = parser.parse_args()
    petfile = sys.argv[1]
    if not os.path.exists(petfile):
        print(" ** ERROR: input petfile does not exists")
        sys.exit(1)
    if options.output is not None:
        convertPET(petfile, options.output)
        sys.exit(1)
    if "PROJECT_DIR" not in os.environ:
        print(" -- You must set the PROJECT_DIR variable")
        print("   export PROJECT_DIR=path_to_the_data_structure")
        print("   for example: /home/bodini/Bureau/SHADOWTEP_renaissance/data")
        sys.exit(1)
    projectdir = os.environ["PROJECT_DIR"]
    rawdir = projectdir + "/raw/"
    utillog = projectdir + "/../utils/vfile2project.log"
    target = sys.argv[2]
    if len(sys.argv) > 3:
        seqname = sys.argv[3]
    else:
        seqname = "pib"
    print(" -- The sequence name is " + seqname)
    if len(sys.argv) == 5 and sys.argv[4] == "WRITE":
        print(" -- Trying to write PatientID/Visit")
        if target.count("/") == 1:
            createDir(rawdir + target.split()[0])
            createDir(rawdir + target)
    pid = None
    visit = None
    if target.count("/") == 0:
        " -- Not visit found, using default: V01"
        pid = target
        visit = "V01"
    elif target.count("/") == 1:
        pid = target.split("/")[0]
        visit = target.split("/")[1]
    else:
        print(" ** ERROR more than one / detected in the target")
        print("        please verify the PatientID/Visit")
        print("        you wrote         " + target)
        print(" The PatientID/Visit was not correct")
        sys.exit(1)
    if not os.path.isdir(rawdir + pid):
        print(" ** ERROR: PatientID does not exists in the PROJECT_DIR")
        print(" - PROJECT_DIR=" + projectdir)
        print("")
        print(" Possible IDS:")
        for i in os.listdir(rawdir):
            print("       - " + i)
        sys.exit(1)
    # Chosen folder
    pdir = rawdir + pid + os.sep
    if not os.path.isdir(pdir + visit):
        print(" ** ERROR: Visit not found in patient " + pid)
        print("")
        print(" Possible Visits:")
        for i in os.listdir(pdir):
            print("       - " + i)
        sys.exit(1)
    # Check existing
    seqdir = pdir + visit + os.sep + seqname + os.sep
    ima = seqdir + pid + "_" + visit + "_" + seqname + ".nii.gz"
    if os.path.exists(ima) or os.path.exists(ima[:-3]):
        print(" -- Output image already exists!!")
        if os.path.exists(ima):
            print(ima)
        else:
            print(ima[:-3])
        sys.exit(0)
    else:
        createDir(seqdir)
        if not convertPET(petfile, ima):
            p = open(utillog, "a")
            p.write(pid + "," + visit + "," + seqname + "," +
                    os.path.basename(petfile) + "," + ima + "\n")
            p.close()
