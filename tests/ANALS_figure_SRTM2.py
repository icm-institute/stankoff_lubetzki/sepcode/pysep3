#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 14:09:04 2020

@author: arya.yazdan-panah
"""

import os
import nibabel as nib
import numpy as np
import skimage.morphology as sm
import csv
from common3 import get_regex, createDir, changeName, readSubjectsLists
from tools_ants import ApplyRegistration
from tools_fsl import fslmaths, fslstats
from importlib import import_module
from scipy.io import loadmat
from scipy.ndimage.filters import convolve
from pprint import pprint


# =============================================================================
suj_list = "/network/lustre/iss01/lubetzki-stankoff/clinic/SHADOWTEP/utils/All"
suj_list = suj_list + "_patients_SRTM2_TRV.list"
subjects = readSubjectsLists(suj_list)
stats_file = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_re"
stats_file = stats_file + "test/data/process/TR_MNI/stats/patients.csv"
proc = "/network/lustre/iss01/lubetzki-stankoff/clinic/SHADOWTEP/data/process"
suj_list = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/utils/All.list"
subjects2 = readSubjectsLists(suj_list)
proc2 = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process"
with open(stats_file, 'w', newline='') as f:
    fieldnames = ['Subject', 'Visit', 'ROI', 'volume', 'mR1', 'mDVR', 'mlogan']
    stats = csv.DictWriter(f, fieldnames)
    stats.writeheader()
    for s in subjects:
        Id = os.path.join(proc, s['Id'])
        Visit = os.path.join(Id, s['Visit'])
        if not os.path.isdir(Visit):
            print(s['Id'], s['Visit'], os.path.isdir(Visit))
            continue
        print(s['Id'], s['Visit'])
        SRTM2 = get_regex(Visit, 'pibSRTM2/.*(DVR|R1).nii.gz', False)
        if len(SRTM2) < 2:
            continue
        SRTM2.sort()
        DVR = SRTM2[0]
        R1 = SRTM2[1]
        logan = get_regex(Visit, 'piblogan/.*_pib_logan_vt.nii.gz', False)
        if not logan:
            continue
        logan = logan[0]
        pibroi = os.path.join(Visit, 'pibROI')
        if not os.path.isdir(pibroi):
            continue
        nawm = get_regex(pibroi, '_NAWM_p0.90_2pib$')
        ctx = get_regex(pibroi, '_cortex_p0.90_2pib$')
        R1stat = np.nan
        DVRstat = np.nan
        loganstat = np.nan
        vol = np.nan
        if nawm and ctx:
            ctx = ctx[0]
            nawm = nawm[0]
            nawm_ero = changeName(nawm, '_ero_ctx')
            fslmaths(ctx, '-kernel', 'sphere', 2, '-dilM', '-binv', '-mas',
                     nawm, nawm_ero)
            R1stat = float(fslstats(R1, '-k', nawm_ero, '-n', '-m'))
            DVRstat = float(fslstats(DVR, '-k', nawm_ero, '-n', '-m'))
            loganstat = float(fslstats(logan, '-k', nawm_ero, '-n', '-m'))
            vol = float(fslstats(nawm_ero, '-V').split()[1])
        stats.writerow({'Subject': s['Id'],
                        'Visit': s['Visit'],
                        'ROI': 'NAWM',
                        'volume': vol,
                        'mR1': R1stat,
                        'mDVR': DVRstat,
                        'mlogan': loganstat})
        peri = get_regex(pibroi, '_PERI_p0.90_2pib$')
        R1stat = np.nan
        DVRstat = np.nan
        loganstat = np.nan
        vol = np.nan
        if peri:
            peri = peri[0]
            R1stat = float(fslstats(R1, '-k', peri, '-n', '-m'))
            DVRstat = float(fslstats(DVR, '-k', peri, '-n', '-m'))
            loganstat = float(fslstats(logan, '-k', peri, '-n', '-m'))
            vol = float(fslstats(peri, '-V').split()[1])
        stats.writerow({'Subject': s['Id'],
                        'Visit': s['Visit'],
                        'ROI': 'PERI',
                        'volume': vol,
                        'mR1': R1stat,
                        'mDVR': DVRstat,
                        'mlogan': loganstat})
        blackholes = get_regex(pibroi, '_t1se_blackholes_manual_2t1_2pib$')
        R1stat = np.nan
        DVRstat = np.nan
        loganstat = np.nan
        vol = np.nan
        if blackholes:
            blackholes = blackholes[0]
            R1stat = float(fslstats(R1, '-k', blackholes, '-n', '-m'))
            DVRstat = float(fslstats(DVR, '-k', blackholes, '-n', '-m'))
            loganstat = float(fslstats(logan, '-k', blackholes, '-n', '-m'))
            vol = float(fslstats(blackholes, '-V').split()[1])
        stats.writerow({'Subject': s['Id'],
                        'Visit': s['Visit'],
                        'ROI': 'BLACKHOLES',
                        'volume': vol,
                        'mR1': R1stat,
                        'mDVR': DVRstat,
                        'mlogan': loganstat})
        les = get_regex(pibroi, '_t2_t2les_manual_t1space_2pib$')
        R1stat = np.nan
        DVRstat = np.nan
        loganstat = np.nan
        vol = np.nan
        if les:
            les = les[0]
            R1stat = float(fslstats(R1, '-k', les, '-n', '-m'))
            DVRstat = float(fslstats(DVR, '-k', les, '-n', '-m'))
            loganstat = float(fslstats(logan, '-k', les, '-n', '-m'))
            vol = float(fslstats(les, '-V').split()[1])
        stats.writerow({'Subject': s['Id'],
                        'Visit': s['Visit'],
                        'ROI': 'LESIONS',
                        'volume': vol,
                        'mR1': R1stat,
                        'mDVR': DVRstat,
                        'mlogan': loganstat})
        gado = get_regex(pibroi, '_t1gd_GDles_manual_2t1_2pib$')
        R1stat = np.nan
        DVRstat = np.nan
        loganstat = np.nan
        vol = np.nan
        if gado:
            gado = gado[0]
            R1stat = float(fslstats(R1, '-k', gado, '-n', '-m'))
            DVRstat = float(fslstats(DVR, '-k', gado, '-n', '-m'))
            loganstat = float(fslstats(logan, '-k', gado, '-n', '-m'))
            vol = float(fslstats(gado, '-V').split()[1])
        stats.writerow({'Subject': s['Id'],
                        'Visit': s['Visit'],
                        'ROI': 'GADO',
                        'volume': vol,
                        'mR1': R1stat,
                        'mDVR': DVRstat,
                        'mlogan': loganstat})
    proc = proc2
    for s in subjects2:
        Id = os.path.join(proc, s['Id'])
        Visit = os.path.join(Id, s['Visit'])
        if not os.path.isdir(Visit):
            print(s['Id'], s['Visit'], os.path.isdir(Visit))
            continue
        print(s['Id'], s['Visit'])
        SRTM2 = get_regex(Visit, 'pibSRTM2/.*(DVR|R1).nii.gz', False)
        if len(SRTM2) < 2:
            continue
        SRTM2.sort()
        DVR = SRTM2[0]
        R1 = SRTM2[1]
        logan = get_regex(Visit, 'piblogan/.*_pib_logan_vt.nii.gz', False)
        if not logan:
            continue
        logan = logan[0]
        pibroi = os.path.join(Visit, 'pibROI')
        if not os.path.isdir(pibroi):
            continue
        wm = get_regex(pibroi, '_aparc.aseg_wm_2pib$')
        ctx = get_regex(pibroi, '_ribbon_ctx_2pib$')
        R1stat = np.nan
        DVRstat = np.nan
        loganstat = np.nan
        vol = np.nan
        if wm and ctx:
            ctx = ctx[0]
            wm = wm[0]
            wm_ero = changeName(wm, '_ero_ctx')
            fslmaths(ctx, '-kernel', 'sphere', 2, '-dilM', '-binv', '-mas',
                     wm, wm_ero)
            R1stat = float(fslstats(R1, '-k', wm_ero, '-n', '-m'))
            DVRstat = float(fslstats(DVR, '-k', wm_ero, '-n', '-m'))
            loganstat = float(fslstats(logan, '-k', wm_ero, '-n', '-m'))
            vol = float(fslstats(wm_ero, '-V').split()[1])
        stats.writerow({'Subject': s['Id'],
                        'Visit': s['Visit'],
                        'ROI': 'WM HC',
                        'volume': vol,
                        'mR1': R1stat,
                        'mDVR': DVRstat,
                        'mlogan': loganstat})
