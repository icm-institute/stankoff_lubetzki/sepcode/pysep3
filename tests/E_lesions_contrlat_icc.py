#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 17 14:00:05 2021

@author: arya.yazdan-panah
"""

import os
import csv
import numpy as np
import nibabel as nib
import matlab.engine
from common3 import get_regex

m = matlab.engine.start_matlab()
# C'est une résolution formelle j'ai le droit... cheh
np.seterr(divide='ignore', invalid='ignore')
# =============================================================================
les_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retes\
t/data/process/labelled_inflasep_lesions'
# Lesions of the patients in MNI
Lesions = get_regex(les_dir, 'P_...../P_....._.*nii.gz', False, )
print("Number of Lesions:", len(Lesions))
# =============================================================================
subjects = ["C_ARIIS",
            "C_ARIIS",
            "C_BENFL",
            "C_BENFL",
            "C_BENMA",
            "C_BENMA",
            "C_CHRKE",
            "C_CHRKE",
            "C_DUBAN",
            "C_DUBAN",
            "C_GUECO",
            "C_GUECO",
            "C_LEBJO",
            "C_LEBJO",
            "C_LEVVA",
            "C_LEVVA"]
# =============================================================================
k2primes = [0.06338108263,
            0.05934295106,
            0.06786313224,
            0.06633747349,
            0.06166487042,
            0.06185613334,
            0.05867357518,
            0.0574208377,
            0.06154755673,
            0.0662559161,
            0.06014615976,
            0.06281320825,
            0.06083698382,
            0.05798011997,
            0.06063100159,
            0.05972947848]
# =============================================================================
input_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_ret\
est/data/process/TR_new_MNI'
# =============================================================================
stats_dir = os.path.join(input_dir, 'stats')

convs_dir = os.path.join(input_dir, 'convolutions')

masks_dir = os.path.join(input_dir, 'masks')
# =============================================================================
stats_file = os.path.join(stats_dir, 'inflasep_lesions_contrlat_icc.csv')
wm_file = os.path.join(masks_dir, 'all_subjects_WM.nii.gz')
WM = nib.load(wm_file).get_fdata()
wm_file = os.path.join(masks_dir, 'flipped', 'all_subjects_WM_flipped.nii.gz')
WM_flip = nib.load(wm_file).get_fdata()
WM = np.multiply(WM, WM_flip)

R1 = os.path.join(convs_dir, 'flipped', 'R1.nii.gz')
R1contr = os.path.join(convs_dir, 'flipped', 'R1_contr.nii.gz')
k2 = os.path.join(convs_dir, 'flipped', 'k2.nii.gz')
k2contr = os.path.join(convs_dir, 'flipped', 'k2_contr.nii.gz')
log = os.path.join(convs_dir, 'flipped', 'logan.nii.gz')
logcontr = os.path.join(convs_dir, 'flipped', 'logan_contr.nii.gz')
suvr18 = os.path.join(convs_dir, 'flipped', 'suvr1to8.nii.gz')
suvr18contr = os.path.join(convs_dir, 'flipped', 'suvr1to8_contr.nii.gz')
suvr0120 = os.path.join(convs_dir, 'flipped', 'suvr0to120.nii.gz')
suvr0120contr = os.path.join(convs_dir, 'flipped', 'suvr0to120_contr.nii.gz')

suvr4090 = os.path.join(convs_dir, 'flipped', 'suvr5070.nii.gz')
suvr4090contr = os.path.join(convs_dir, 'flipped', 'suvr5070_contr.nii.gz')

R1 = nib.load(R1).get_fdata()
R1contr = nib.load(R1contr).get_fdata()
k2 = nib.load(k2).get_fdata()
k2contr = nib.load(k2contr).get_fdata()
log = nib.load(log).get_fdata()
logcontr = nib.load(logcontr).get_fdata()
suvr18 = nib.load(suvr18).get_fdata()
suvr18contr = nib.load(suvr18contr).get_fdata()
suvr0120 = nib.load(suvr0120).get_fdata()
suvr0120contr = nib.load(suvr0120contr).get_fdata()
suvr4090 = nib.load(suvr4090).get_fdata()
suvr4090contr = nib.load(suvr4090contr).get_fdata()
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'Lesion Name',
              'Lesion Volume',
              'ICC R1',
              'ICC DVR (SRTM2)',
              'ICC DVR (logan)',
              'ICC SUVR1to8m',
              'ICC SUVR0to120s',
              'ICC SUVR50to70m']

stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
for les in Lesions:
    ldata = np.multiply(nib.load(les).get_fdata(), WM)
    LV = int(np.sum(ldata))
    if LV == 0:
        continue
    print('Lesion Volume:', LV)
    les_name = os.path.basename(les)
    mR1 = np.zeros((16, 2))
    mk2 = np.zeros((16, 2))
    DVR = np.zeros((16, 2))
    mSUVR0to120 = np.zeros((16, 2))
    mSUVR1to8 = np.zeros((16, 2))
    mSUVR40to90 = np.zeros((16, 2))
    mlogan = np.zeros((16, 2))
    for i, s in enumerate(subjects):
        print('    ', s)
        # retrieving images
        R1_norm = R1[:, :, :, i]
        R1_contr = R1contr[:, :, :, i]
        k2_norm = k2[:, :, :, i]
        k2_contr = k2contr[:, :, :, i]
        log_norm = log[:, :, :, i]
        log_contr = logcontr[:, :, :, i]
        suvr18_norm = suvr18[:, :, :, i]
        suvr18_contr = suvr18contr[:, :, :, i]
        suvr0120_norm = suvr0120[:, :, :, i]
        suvr0120_contr = suvr0120contr[:, :, :, i]
        suvr4090_norm = suvr4090[:, :, :, i]
        suvr4090_contr = suvr4090contr[:, :, :, i]
        k2prime = k2primes[i]

        mR1[i, 0] = np.sum(np.multiply(ldata, R1_norm))/LV
        mR1[i, 1] = np.sum(np.multiply(ldata, R1_contr))/LV
        mk2[i, 0] = np.sum(np.multiply(ldata, k2_norm))/LV
        mk2[i, 1] = np.sum(np.multiply(ldata, k2_contr))/LV
        mlogan[i, 0] = np.sum(np.multiply(ldata, log_norm))/LV
        mlogan[i, 1] = np.sum(np.multiply(ldata, log_contr))/LV
        # DVR after mean calculation
        try:
            DVR[i, 0] = mR1[i, 0] * k2prime / mk2[i, 0]
        except ZeroDivisionError:
            DVR[i, 0] = np.nan
        try:
            DVR[i, 1] = mR1[i, 1] * k2prime / mk2[i, 1]
        except ZeroDivisionError:
            DVR[i, 1] = np.nan

        mSUVR1to8[i, 0] = np.sum(np.multiply(ldata, suvr18_norm))/LV
        mSUVR1to8[i, 1] = np.sum(np.multiply(ldata, suvr18_contr))/LV
        mSUVR0to120[i, 0] = np.sum(np.multiply(ldata, suvr0120_norm))/LV
        mSUVR0to120[i, 1] = np.sum(np.multiply(ldata, suvr0120_contr))/LV
        mSUVR40to90[i, 0] = np.sum(np.multiply(ldata, suvr4090_norm))/LV
        mSUVR40to90[i, 1] = np.sum(np.multiply(ldata, suvr4090_contr))/LV
    XR1 = matlab.double(mR1.tolist())
    XDVR = matlab.double(DVR.tolist())
    Xlogan = matlab.double(mlogan.tolist())
    Xsuvr1 = matlab.double(mSUVR0to120.tolist())
    Xsuvr2 = matlab.double(mSUVR1to8.tolist())
    Xsuvr3 = matlab.double(mSUVR40to90.tolist())
    ICCR1 = m.IPN_icc(XR1, 2, 'single')
    ICCDVR = m.IPN_icc(XDVR, 2, 'single')
    ICCLogan = m.IPN_icc(Xlogan, 2, 'single')
    ICCsuvr1 = m.IPN_icc(Xsuvr1, 2, 'single')
    ICCsuvr2 = m.IPN_icc(Xsuvr2, 2, 'single')
    ICCsuvr3 = m.IPN_icc(Xsuvr3, 2, 'single')
    stats.writerow({'Lesion Name': les_name,
                    'Lesion Volume': LV,
                    'ICC R1': ICCR1,
                    'ICC DVR (SRTM2)': ICCDVR,
                    'ICC DVR (logan)': ICCLogan,
                    'ICC SUVR1to8m': ICCsuvr2,
                    'ICC SUVR0to120s': ICCsuvr1,
                    'ICC SUVR50to70m': ICCsuvr3})
