#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 14:33:45 2021

@author: arya.yazdan-panah
"""

import os
import csv
import numpy as np
import nibabel as nib
from common3 import get_regex

# C'est une résolution formelle j'ai le droit... cheh
np.seterr(divide='ignore', invalid='ignore')
# =============================================================================
les_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retes\
t/data/process/labelled_inflasep_lesions'
# Lesions of the patients in MNI
Lesions = get_regex(les_dir, 'P_...../P_....._.*nii.gz', False, )
print("Number of Lesions:", len(Lesions))
# =============================================================================
subjects = ["C_ARIIS",
            "C_BENFL",
            "C_BENMA",
            "C_CHRKE",
            "C_DUBAN",
            "C_GUECO",
            "C_LEBJO",
            "C_LEVVA",
            "C_ARIIS",
            "C_BENFL",
            "C_BENMA",
            "C_CHRKE",
            "C_DUBAN",
            "C_GUECO",
            "C_LEBJO",
            "C_LEVVA"]
# =============================================================================
k2primes = [0.053878788121064200,
            0.046619715640901090,
            0.045820297526742140,
            0.048443731269552974,
            0.046589148459084940,
            0.045751284218846620,
            0.046603704896474266,
            0.045126620880579976,
            0.053623538821938740,
            0.054505496393880190,
            0.048248964142092890,
            0.048854877386098390,
            0.046176992356509360,
            0.041633532203379910,
            0.050695694990241624,
            0.052164938619052520]
# =============================================================================
input_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_ret\
est/data/process/TR_new_MNI'
# =============================================================================
stats_dir = os.path.join(input_dir, 'stats')
convs_dir = os.path.join(input_dir, 'convolutions_smoothed')
masks_dir = os.path.join(input_dir, 'masks')
# =============================================================================
stats_file = os.path.join(stats_dir, 'lesions_contrlat_smooth.csv')
wm_file = os.path.join(masks_dir, 'all_subjects_WM.nii.gz')
WM = nib.load(wm_file).get_fdata()
wm_file = os.path.join(masks_dir, 'flipped', 'all_subjects_WM_flipped.nii.gz')
WM_flip = nib.load(wm_file).get_fdata()
WM = np.multiply(WM, WM_flip)

R1 = os.path.join(convs_dir, 'flipped', 'R1.nii.gz')
R1contr = os.path.join(convs_dir, 'flipped', 'R1_contr.nii.gz')
k2 = os.path.join(convs_dir, 'flipped', 'k2.nii.gz')
k2contr = os.path.join(convs_dir, 'flipped', 'k2_contr.nii.gz')
log = os.path.join(convs_dir, 'flipped', 'logan.nii.gz')
logcontr = os.path.join(convs_dir, 'flipped', 'logan_contr.nii.gz')
suvr18 = os.path.join(convs_dir, 'flipped', 'suvr1to8.nii.gz')
suvr18contr = os.path.join(convs_dir, 'flipped', 'suvr1to8_contr.nii.gz')
suvr0120 = os.path.join(convs_dir, 'flipped', 'suvr0to120.nii.gz')
suvr0120contr = os.path.join(convs_dir, 'flipped', 'suvr0to120_contr.nii.gz')
suvr4090 = os.path.join(convs_dir, 'flipped', 'suvr5070.nii.gz')
suvr4090contr = os.path.join(convs_dir, 'flipped', 'suvr5070_contr.nii.gz')

R1 = nib.load(R1).get_fdata()
R1contr = nib.load(R1contr).get_fdata()
k2 = nib.load(k2).get_fdata()
k2contr = nib.load(k2contr).get_fdata()
log = nib.load(log).get_fdata()
logcontr = nib.load(logcontr).get_fdata()
suvr18 = nib.load(suvr18).get_fdata()
suvr18contr = nib.load(suvr18contr).get_fdata()
suvr0120 = nib.load(suvr0120).get_fdata()
suvr0120contr = nib.load(suvr0120contr).get_fdata()
suvr4090 = nib.load(suvr4090).get_fdata()
suvr4090contr = nib.load(suvr4090contr).get_fdata()
f = open(stats_file, 'w', newline='')

fieldnames = ['Subject',
              'Lesion Name',
              'Lesion Volume',
              'mR1',
              'mR1contr',
              'mk2',
              'mk2contr',
              'k2prime',
              'DVR',
              'DVRcontr',
              'mlogan',
              'mlogancontr',
              'mSUVR18',
              'mSUVR18contr',
              'mSUVR0120',
              'mSUVR0120contr',
              'mSUVR5070',
              'mSUVR5070contr',
              'TRDVRSRTM2',
              'TRR1',
              'TRDVRlogan',
              'TRSUVR18',
              'TRSUVR0120',
              'TRSUVR5070']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
for les in Lesions:
    ldata = np.multiply(nib.load(les).get_fdata(), WM)
    LV = int(np.sum(ldata))
    if LV == 0:
        continue
    print('Lesion Volume:', LV)
    les_name = os.path.basename(les)
    for i, s in enumerate(subjects):
        print('    ', s)
        # retrieving images
        R1_norm = R1[:, :, :, i]
        R1_contr = R1contr[:, :, :, i]
        k2_norm = k2[:, :, :, i]
        k2_contr = k2contr[:, :, :, i]
        log_norm = log[:, :, :, i]
        log_contr = logcontr[:, :, :, i]
        suvr18_norm = suvr18[:, :, :, i]
        suvr18_contr = suvr18contr[:, :, :, i]
        suvr0120_norm = suvr0120[:, :, :, i]
        suvr0120_contr = suvr0120contr[:, :, :, i]
        suvr4090_norm = suvr4090[:, :, :, i]
        suvr4090_contr = suvr4090contr[:, :, :, i]
        k2prime = k2primes[i]
        # setting default values for .csv file
        mR1 = np.nan
        mR1contr = np.nan
        mk2 = np.nan
        mk2contr = np.nan

        mR1 = np.sum(np.multiply(ldata, R1_norm))/LV
        mR1contr = np.sum(np.multiply(ldata, R1_contr))/LV

        mk2 = np.sum(np.multiply(ldata, k2_norm))/LV
        mk2contr = np.sum(np.multiply(ldata, k2_contr))/LV

        mlogan = np.sum(np.multiply(ldata, log_norm))/LV
        mlogancontr = np.sum(np.multiply(ldata, log_contr))/LV

        msuvr18 = np.sum(np.multiply(ldata, suvr18_norm))/LV
        msuvr18_contr = np.sum(np.multiply(ldata, suvr18_contr))/LV
        msuvr0120 = np.sum(np.multiply(ldata, suvr0120_norm))/LV
        msuvr0120_contr = np.sum(np.multiply(ldata, suvr0120_contr))/LV
        msuvr4090 = np.sum(np.multiply(ldata, suvr4090_norm))/LV
        msuvr4090_contr = np.sum(np.multiply(ldata, suvr4090_contr))/LV
        # DVR after mean calculation
        try:
            DVR = mR1 * k2prime / mk2
        except ZeroDivisionError:
            DVR = np.nan
        try:
            DVRcontr = mR1contr * k2prime / mk2contr
        except ZeroDivisionError:
            DVRcontr = np.nan
        # Calculating TRV
        try:
            TRDVRSRTM2 = 2 * abs(DVR - DVRcontr) / (DVR + DVRcontr)
        except ZeroDivisionError:
            TRDVRSRTM2 = np.nan
        try:
            TRDVRlogan = 2 * abs(mlogan - mlogancontr) / (mlogan + mlogancontr)
        except ZeroDivisionError:
            TRDVRlogan = np.nan
        try:
            TRR1 = 2 * abs(mR1 - mR1contr) / (mR1 + mR1contr)
        except ZeroDivisionError:
            TRR1 = np.nan
        try:
            TRSUVR18 = 2 * abs(msuvr18 - msuvr18_contr) / (msuvr18 + msuvr18_contr)
        except ZeroDivisionError:
            TRSUVR18 = np.nan
        try:
            TRSUVR0120 = 2 * abs(msuvr0120 - msuvr0120_contr) / (msuvr0120 + msuvr0120_contr)
        except ZeroDivisionError:
            TRSUVR0120 = np.nan
        try:
            TRSUVR4090 = 2 * abs(msuvr4090 - msuvr4090_contr) / (msuvr4090 + msuvr4090_contr)
        except ZeroDivisionError:
            TRSUVR4090 = np.nan
        stats.writerow({'Subject': s,
                        'Lesion Name': les_name,
                        'Lesion Volume': LV,
                        'mR1': mR1,
                        'mR1contr': mR1contr,
                        'mk2': mk2,
                        'mk2contr': mk2contr,
                        'k2prime': k2prime,
                        'DVR': DVR,
                        'DVRcontr': DVRcontr,
                        'mlogan': mlogan,
                        'mlogancontr': mlogancontr,
                        'mSUVR18': msuvr18,
                        'mSUVR18contr': msuvr18_contr,
                        'mSUVR0120': msuvr0120,
                        'mSUVR0120contr': msuvr0120_contr,
                        'mSUVR5070': msuvr4090,
                        'mSUVR5070contr': msuvr4090_contr,
                        'TRDVRSRTM2': TRDVRSRTM2,
                        'TRR1': TRR1,
                        'TRDVRlogan': TRDVRlogan,
                        'TRSUVR18': TRSUVR18,
                        'TRSUVR0120': TRSUVR0120,
                        'TRSUVR5070': TRSUVR4090})
