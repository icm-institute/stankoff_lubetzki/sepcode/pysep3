#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 11 11:49:27 2020

@author: arya.yazdan-panah
"""


import os
import csv
import numpy as np
import nibabel as nib
from common3 import get_regex
from tools_fsl import fslmerge


# C'est une résolution formelle j'ai le droit... cheh
np.seterr(divide='ignore', invalid='ignore')
# =============================================================================
les_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retes\
t/data/process/labelled_inflasep_lesions'
# Lesions of the patients in MNI
Lesions = get_regex(les_dir, 'P_...../P_....._.*nii.gz', False, )
print("Number of Lesions:", len(Lesions))

# %%
# =============================================================================
subjects = ["C_ARIIS",
            "C_BENFL",
            "C_BENMA",
            "C_CHRKE",
            "C_DUBAN",
            "C_GUECO",
            "C_LEBJO",
            "C_LEVVA"]
# =============================================================================
k2primes = [[0.06338108263, 0.05934295106],
            [0.06786313224, 0.06633747349],
            [0.06166487042, 0.06185613334],
            [0.05867357518, 0.0574208377],
            [0.06154755673, 0.0662559161],
            [0.06014615976, 0.06281320825],
            [0.06083698382, 0.05798011997],
            [0.06063100159, 0.05972947848]]
k2prime = [0, 0]
# =============================================================================
input_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_ret\
est/data/process/TR_new_MNI'
# =============================================================================
stats_dir = os.path.join(input_dir, 'stats')

convs_dir = os.path.join(input_dir, 'convolutions')

masks_dir = os.path.join(input_dir, 'masks')
# =============================================================================
stats_file = os.path.join(stats_dir, 'lesions.csv')
wm_file = os.path.join(masks_dir, 'all_subjects_WM.nii.gz')
WM = nib.load(wm_file).get_fdata()

# R1 = ['', '']
# k2 = ['', '']
# DVR = ['', '']
# Logs = ['', '']
# SUVRs1 = ['', '']
# SUVRs2 = ['', '']
# R1[0] = get_regex(convs_dir, 'V01_R1_2MNI.nii.gz', False)
# R1[1] = get_regex(convs_dir, 'V02_R1_2MNI.nii.gz', False)
# k2[0] = get_regex(convs_dir, 'V01_k2_2MNI.nii.gz', False)
# k2[1] = get_regex(convs_dir, 'V02_k2_2MNI.nii.gz', False)
# Logs[0] = get_regex(convs_dir, 'V01_pib_logan_vt_2MNI.nii.gz', False)
# Logs[1] = get_regex(convs_dir, 'V02_pib_logan_vt_2MNI.nii.gz', False)
# SUVRs1[0] = get_regex(convs_dir, 'V01_SUVR_0_to_120s_2MNI.nii.gz', False)
# SUVRs1[1] = get_regex(convs_dir, 'V02_SUVR_0_to_120s_2MNI.nii.gz', False)
# SUVRs2[0] = get_regex(convs_dir, 'V01_SUVR_1_to_8m_2MNI.nii.gz', False)
# SUVRs2[1] = get_regex(convs_dir, 'V02_SUVR_1_to_8m_2MNI.nii.gz', False)
R1V01 = os.path.join(convs_dir, 'R1_V01.nii.gz')
R1V02 = os.path.join(convs_dir, 'R1_V02.nii.gz')
k2V01 = os.path.join(convs_dir, 'k2_V01.nii.gz')
k2V02 = os.path.join(convs_dir, 'k2_V02.nii.gz')
logV01 = os.path.join(convs_dir, 'logan_V01.nii.gz')
logV02 = os.path.join(convs_dir, 'logan_V02.nii.gz')
SUVRs1V01 = os.path.join(convs_dir, 'SUVR_0_to_120s_V01.nii.gz')
SUVRs1V02 = os.path.join(convs_dir, 'SUVR_0_to_120s_V02.nii.gz')
SUVRs2V01 = os.path.join(convs_dir, 'SUVR_1_to_8m_V01.nii.gz')
SUVRs2V02 = os.path.join(convs_dir, 'SUVR_1_to_8m_V02.nii.gz')
# SUVRs3V01 = os.path.join(convs_dir, 'SUVR_40_to_90m_V01.nii.gz')
# SUVRs3V02 = os.path.join(convs_dir, 'SUVR_40_to_90m_V02.nii.gz')
SUVRs3V01 = os.path.join(convs_dir, 'SUVR_50_to_70m_V01.nii.gz')
SUVRs3V02 = os.path.join(convs_dir, 'SUVR_50_to_70m_V02.nii.gz')
# lesions_merged = os.path.join(les_dir, 'patients_lesions.nii.gz')

# fslmerge(R1[0], R1V01)
# fslmerge(R1[1], R1V02)
# fslmerge(k2[0], k2V01)
# fslmerge(k2[1], k2V02)
# fslmerge(Logs[0], logV01)
# fslmerge(Logs[1], logV02)
# fslmerge(SUVRs1[0], SUVRs1V01)
# fslmerge(SUVRs1[1], SUVRs1V02)
# fslmerge(SUVRs2[0], SUVRs2V01)
# fslmerge(SUVRs2[1], SUVRs2V02)
# fslmerge(Lesions, lesions_merged)


R1V01 = nib.load(R1V01).get_fdata()
R1V02 = nib.load(R1V02).get_fdata()
k2V01 = nib.load(k2V01).get_fdata()
k2V02 = nib.load(k2V02).get_fdata()
logV01 = nib.load(logV01).get_fdata()
logV02 = nib.load(logV02).get_fdata()
SUVRs1V01 = nib.load(SUVRs1V01).get_fdata()
SUVRs1V02 = nib.load(SUVRs1V02).get_fdata()
SUVRs2V01 = nib.load(SUVRs2V01).get_fdata()
SUVRs2V02 = nib.load(SUVRs2V02).get_fdata()
SUVRs3V01 = nib.load(SUVRs3V01).get_fdata()
SUVRs3V02 = nib.load(SUVRs3V02).get_fdata()

f = open(stats_file, 'w', newline='')

fieldnames = ['Subject',
              'Lesion Name',
              'Lesion Volume',
              'mR1V01',
              'mR1V02',
              'mk2V01',
              'mk2V02',
              'k2primeV01',
              'k2primeV02',
              'DVRV01',
              'DVRV02',
              'mloganV01',
              'mloganV02',
              'mSUVR0to120V01',
              'mSUVR0to120V02',
              'mSUVR1to8V01',
              'mSUVR1to8V02',
              'mSUVR50to70V01',
              'mSUVR50to70V02',
              'TRDVRSRTM2',
              'TRR1',
              'TRDVRlogan',
              'TRSUVR0to120',
              'TRSUVR1to8',
              'TRSUVR50to70']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
for les in Lesions:
    ldata = np.multiply(nib.load(les).get_fdata(), WM)
    LV = int(np.sum(ldata))
    if LV == 0:
        continue
    print('Lesion Volume:', LV)
    les_name = os.path.basename(les)
    for i, s in enumerate(subjects):
        print('    ', s)
        # retrieving images
        R1_V01 = R1V01[:, :, :, i]
        R1_V02 = R1V02[:, :, :, i]
        k2_V01 = k2V01[:, :, :, i]
        k2_V02 = k2V02[:, :, :, i]
        log_V01 = logV01[:, :, :, i]
        log_V02 = logV02[:, :, :, i]
        SUVRs1_V01 = SUVRs1V01[:, :, :, i]
        SUVRs1_V02 = SUVRs1V02[:, :, :, i]
        SUVRs2_V01 = SUVRs2V01[:, :, :, i]
        SUVRs2_V02 = SUVRs2V02[:, :, :, i]
        SUVRs3_V01 = SUVRs3V01[:, :, :, i]
        SUVRs3_V02 = SUVRs3V02[:, :, :, i]
        k2primeV01 = k2primes[i][0]
        k2primeV02 = k2primes[i][1]
        mR1V01 = np.sum(np.multiply(ldata, R1_V01))/LV
        mR1V02 = np.sum(np.multiply(ldata, R1_V02))/LV
        mk2V01 = np.sum(np.multiply(ldata, k2_V01))/LV
        mk2V02 = np.sum(np.multiply(ldata, k2_V02))/LV
        mloganV01 = np.sum(np.multiply(ldata, log_V01))/LV
        mloganV02 = np.sum(np.multiply(ldata, log_V02))/LV
        mSUVR0to120V01 = np.sum(np.multiply(ldata, SUVRs1_V01))/LV
        mSUVR0to120V02 = np.sum(np.multiply(ldata, SUVRs1_V02))/LV
        mSUVR1to8V01 = np.sum(np.multiply(ldata, SUVRs2_V01))/LV
        mSUVR1to8V02 = np.sum(np.multiply(ldata, SUVRs2_V02))/LV
        mSUVR40to90V01 = np.sum(np.multiply(ldata, SUVRs3_V01))/LV
        mSUVR40to90V02 = np.sum(np.multiply(ldata, SUVRs3_V02))/LV
        # DVR after mean calculation
        try:
            DVRV01 = mR1V01 * k2primeV01 / mk2V01
        except ZeroDivisionError:
            DVRV01 = np.nan
        try:
            DVRV02 = mR1V02 * k2primeV02 / mk2V02
        except ZeroDivisionError:
            DVRV02 = np.nan
        # Calculating TRV
        try:
            TRDVRSRTM2 = 2 * abs(DVRV01 - DVRV02) / (DVRV01 + DVRV02)
        except ZeroDivisionError:
            TRDVRSRTM2 = np.nan
        try:
            TRDVRlogan = 2 * abs(mloganV01 - mloganV02) / (mloganV01 + mloganV02)
        except ZeroDivisionError:
            TRDVRlogan = np.nan
        try:
            TRR1 = 2 * abs(mR1V01 - mR1V02) / (mR1V01 + mR1V02)
        except ZeroDivisionError:
            TRR1 = np.nan
        try:
            TRSUVR0120 = 2 * abs(mSUVR0to120V01 - mSUVR0to120V02) / (mSUVR0to120V01 + mSUVR0to120V02)
        except ZeroDivisionError:
            TRSUVR0120 = np.nan
        try:
            TRSUVR1to8 = 2 * abs(mSUVR1to8V01 - mSUVR1to8V02) / (mSUVR1to8V01 + mSUVR1to8V02)
        except ZeroDivisionError:
            TRSUVR1to8 = np.nan
        try:
            TRSUVR40to90 = 2 * abs(mSUVR40to90V01 - mSUVR40to90V02) / (mSUVR40to90V01 + mSUVR40to90V02)
        except ZeroDivisionError:
            TRSUVR40to90 = np.nan
        stats.writerow({'Subject': s,
                        'Lesion Name': les_name,
                        'Lesion Volume': LV,
                        'mR1V01': mR1V01,
                        'mR1V02': mR1V02,
                        'mk2V01': mk2V01,
                        'mk2V02': mk2V02,
                        'k2primeV01': k2primeV01,
                        'k2primeV02': k2primeV02,
                        'DVRV01': DVRV01,
                        'DVRV02': DVRV02,
                        'mloganV01': mloganV01,
                        'mloganV02': mloganV02,
                        'mSUVR0to120V01': mSUVR0to120V01,
                        'mSUVR0to120V02': mSUVR0to120V02,
                        'mSUVR1to8V01': mSUVR1to8V01,
                        'mSUVR1to8V02': mSUVR1to8V02,
                        'mSUVR50to70V01': mSUVR40to90V01,
                        'mSUVR50to70V02': mSUVR40to90V02,
                        'TRDVRSRTM2': TRDVRSRTM2,
                        'TRR1': TRR1,
                        'TRDVRlogan': TRDVRlogan,
                        'TRSUVR0to120': TRSUVR0120,
                        'TRSUVR1to8': TRSUVR1to8,
                        'TRSUVR50to70': TRSUVR40to90})

# =============================================================================
# _V01_aparc+aseg_cbgm_2MNI$
# _V01_aparc+aseg_cbwm_2MNI$
# _V01_aparc+aseg_wm_ero_2MNI$
# _V01_lobes_frontal_2MNI$
# _V01_lobes_insula_2MNI$
# _V01_lobes_limbic_2MNI$
# _V01_lobes_occipital_2MNI$
# _V01_lobes_parietal_2MNI$
# _V01_lobes_temporal_2MNI$
# _V01_t1_pre_first_all_fast_firstseg_OnlyStr_2MNI$
# _V01_t1_pre_first_all_fast_firstseg_OnlyTH_2MNI$
# =============================================================================
