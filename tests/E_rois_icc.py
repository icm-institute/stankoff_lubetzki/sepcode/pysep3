#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 25 15:02:50 2021

@author: arya.yazdan-panah
"""

import os
import csv
import numpy as np
import nibabel as nib
from common3 import get_regex
from pprint import pprint
import matlab.engine

# C'est une résolution formelle j'ai le droit... cheh
np.seterr(divide='ignore', invalid='ignore')
m = matlab.engine.start_matlab()
# =============================================================================
subjects = ["C_ARIIS",
            "C_BENFL",
            "C_BENMA",
            "C_CHRKE",
            "C_DUBAN",
            "C_GUECO",
            "C_LEBJO",
            "C_LEVVA"]
# =============================================================================
k2primes = [[0.06338108263, 0.05934295106],
            [0.06786313224, 0.06633747349],
            [0.06166487042, 0.06185613334],
            [0.05867357518, 0.0574208377],
            [0.06154755673, 0.0662559161],
            [0.06014615976, 0.06281320825],
            [0.06083698382, 0.05798011997],
            [0.06063100159, 0.05972947848]]
k2prime = [0, 0]
# =============================================================================
input_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_ret\
est/data/process/TR_new_MNI'
# =============================================================================
stats_dir = os.path.join(input_dir, 'stats')
convs_dir = os.path.join(input_dir, 'convolutions')
masks_dir = os.path.join(input_dir, 'masks')
# =============================================================================
stats_file = os.path.join(stats_dir, 'rois_icc.csv')
# =============================================================================
R1V01 = os.path.join(convs_dir, 'R1_V01.nii.gz')
R1V02 = os.path.join(convs_dir, 'R1_V02.nii.gz')
k2V01 = os.path.join(convs_dir, 'k2_V01.nii.gz')
k2V02 = os.path.join(convs_dir, 'k2_V02.nii.gz')
logV01 = os.path.join(convs_dir, 'logan_V01.nii.gz')
logV02 = os.path.join(convs_dir, 'logan_V02.nii.gz')
SUVRs1V01 = os.path.join(convs_dir, 'SUVR_0_to_120s_V01.nii.gz')
SUVRs1V02 = os.path.join(convs_dir, 'SUVR_0_to_120s_V02.nii.gz')
SUVRs2V01 = os.path.join(convs_dir, 'SUVR_1_to_8m_V01.nii.gz')
SUVRs2V02 = os.path.join(convs_dir, 'SUVR_1_to_8m_V02.nii.gz')
SUVRs3V01 = os.path.join(convs_dir, 'SUVR_50_to_70m_V01.nii.gz')
SUVRs3V02 = os.path.join(convs_dir, 'SUVR_50_to_70m_V02.nii.gz')
R1V01 = nib.load(R1V01).get_fdata()
R1V02 = nib.load(R1V02).get_fdata()
k2V01 = nib.load(k2V01).get_fdata()
k2V02 = nib.load(k2V02).get_fdata()
logV01 = nib.load(logV01).get_fdata()
logV02 = nib.load(logV02).get_fdata()
SUVRs1V01 = nib.load(SUVRs1V01).get_fdata()
SUVRs1V02 = nib.load(SUVRs1V02).get_fdata()
SUVRs2V01 = nib.load(SUVRs2V01).get_fdata()
SUVRs2V02 = nib.load(SUVRs2V02).get_fdata()
SUVRs3V01 = nib.load(SUVRs3V01).get_fdata()
SUVRs3V02 = nib.load(SUVRs3V02).get_fdata()
# =============================================================================
# =============================================================================
ROIs = get_regex(masks_dir, '^all_subjects_(cbgm|cbwm|lobes_frontal|lobes_insula|lobes_limbic|lobes_occipital|lobes_parietal|lobes_temporal|striatum|thalamus|WM).nii.gz', False, False, full=False)
ROIs.extend(get_regex(masks_dir, '^all_subjects_wm_lobes_(cingulate|frontal|insula|occipital|parietal|temporal).nii.gz', False, False, full=False))
pprint(ROIs)
pprint(ROIs)
names = ['wm',
         'cbgm',
         'cbwm',
         'frontal_ctx',
         'insula',
         'limbic_ctx',
         'occipital_ctx',
         'parietal_ctx',
         'temporal_ctx',
         'striatum',
         'thalamus',
         "cingulate_wm",
         "frontal_wm",
         "insula_wm",
         "occipital_wm",
         "parietal_wm",
         "temporal_wm"]
# =============================================================================
f = open(stats_file, 'w', newline='')
fieldnames = ['ROI Name',
              'ROI Volume',
              'ICC R1',
              'ICC DVR (SRTM2)',
              'ICC DVR (logan)',
              'ICC SUVR1to8m',
              'ICC SUVR0to120s',
              'ICC SUVR50to70m']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
for j, roi in enumerate(ROIs):
    roidata = nib.load(roi).get_fdata()
    roiV = int(np.sum(roidata))
    roi_name = names[j]
    print('ROI Volume:', roiV)
    mR1 = np.zeros((8, 2))
    mk2 = np.zeros((8, 2))
    DVR = np.zeros((8, 2))
    mSUVR0to120 = np.zeros((8, 2))
    mSUVR1to8 = np.zeros((8, 2))
    mSUVR40to90 = np.zeros((8, 2))
    mlogan = np.zeros((8, 2))
    for i, s in enumerate(subjects):
        print('    ', s)
        # retrieving images
        R1_V01 = R1V01[:, :, :, i]
        R1_V02 = R1V02[:, :, :, i]
        k2_V01 = k2V01[:, :, :, i]
        k2_V02 = k2V02[:, :, :, i]
        log_V01 = logV01[:, :, :, i]
        log_V02 = logV02[:, :, :, i]
        SUVRs1_V01 = SUVRs1V01[:, :, :, i]
        SUVRs1_V02 = SUVRs1V02[:, :, :, i]
        SUVRs2_V01 = SUVRs2V01[:, :, :, i]
        SUVRs2_V02 = SUVRs2V02[:, :, :, i]
        SUVRs3_V01 = SUVRs3V01[:, :, :, i]
        SUVRs3_V02 = SUVRs3V02[:, :, :, i]
        k2prime = k2primes[i]
        k2primeV01 = k2prime[0]
        k2primeV02 = k2prime[1]
        mR1[i, 0] = np.sum(np.multiply(roidata, R1_V01))/roiV
        mR1[i, 1] = np.sum(np.multiply(roidata, R1_V02))/roiV
        mk2[i, 0] = np.sum(np.multiply(roidata, k2_V01))/roiV
        mk2[i, 1] = np.sum(np.multiply(roidata, k2_V02))/roiV
        mlogan[i, 0] = np.sum(np.multiply(roidata, log_V01))/roiV
        mlogan[i, 1] = np.sum(np.multiply(roidata, log_V02))/roiV
        mSUVR0to120[i, 0] = np.sum(np.multiply(roidata, SUVRs1_V01))/roiV
        mSUVR0to120[i, 1] = np.sum(np.multiply(roidata, SUVRs1_V02))/roiV
        mSUVR1to8[i, 0] = np.sum(np.multiply(roidata, SUVRs2_V01))/roiV
        mSUVR1to8[i, 1] = np.sum(np.multiply(roidata, SUVRs2_V02))/roiV
        mSUVR40to90[i, 0] = np.sum(np.multiply(roidata, SUVRs3_V01))/roiV
        mSUVR40to90[i, 1] = np.sum(np.multiply(roidata, SUVRs3_V02))/roiV
        # DVR after mean calculation
        try:
            DVR[i, 0] = mR1[i, 0] * k2primeV01 / mk2[i, 0]
        except ZeroDivisionError:
            DVR[i, 0] = np.nan
        try:
            DVR[i, 1] = mR1[i, 1] * k2primeV02 / mk2[i, 1]
        except ZeroDivisionError:
            DVR[i, 1] = np.nan
    XR1 = matlab.double(mR1.tolist())
    XDVR = matlab.double(DVR.tolist())
    Xlogan = matlab.double(mlogan.tolist())
    Xsuvr1 = matlab.double(mSUVR0to120.tolist())
    Xsuvr2 = matlab.double(mSUVR1to8.tolist())
    Xsuvr3 = matlab.double(mSUVR40to90.tolist())
    ICCR1 = m.IPN_icc(XR1, 2, 'single')
    ICCDVR = m.IPN_icc(XDVR, 2, 'single')
    ICCLogan = m.IPN_icc(Xlogan, 2, 'single')
    ICCsuvr1 = m.IPN_icc(Xsuvr1, 2, 'single')
    ICCsuvr2 = m.IPN_icc(Xsuvr2, 2, 'single')
    ICCsuvr3 = m.IPN_icc(Xsuvr3, 2, 'single')
    stats.writerow({'ROI Name': roi_name,
                    'ROI Volume': roiV,
                    'ICC R1': ICCR1,
                    'ICC DVR (SRTM2)': ICCDVR,
                    'ICC DVR (logan)': ICCLogan,
                    'ICC SUVR1to8m': ICCsuvr2,
                    'ICC SUVR0to120s': ICCsuvr1,
                    'ICC SUVR50to70m': ICCsuvr3})
f.close()
