#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 16:02:00 2021

@author: arya.yazdan-panah
"""

import os
import csv
import numpy as np
import nibabel as nib
from common3 import get_regex
from tools_fsl import fslmerge
from pprint import pprint

np.seterr(divide='ignore', invalid='ignore')
# =============================================================================
subjects = ["C_ARIIS",
            "C_BENFL",
            "C_BENMA",
            "C_CHRKE",
            "C_DUBAN",
            "C_GUECO",
            "C_LEBJO",
            "C_LEVVA"]
# =============================================================================
k2primes = [[0.053878788121064200, 0.046619715640901090],
            [0.045820297526742140, 0.048443731269552974],
            [0.046589148459084940, 0.045751284218846620],
            [0.046603704896474266, 0.045126620880579976],
            [0.053623538821938740, 0.054505496393880190],
            [0.048248964142092890, 0.048854877386098390],
            [0.046176992356509360, 0.041633532203379910],
            [0.050695694990241624, 0.052164938619052520]]


k2prime = [0, 0]
# =============================================================================
input_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_ret\
est/data/process/TR_new_MNI'
# =============================================================================
stats_dir = os.path.join(input_dir, 'stats')
convs_dir = os.path.join(input_dir, 'convolutions_smoothed')
masks_dir = os.path.join(input_dir, 'masks')
# =============================================================================
stats_file = os.path.join(stats_dir, 'rois_smooth.csv')
# =============================================================================
# R1 = ['', '']
# k2 = ['', '']
# DVR = ['', '']
# Logs = ['', '']
# suvr1 = ['', '']
# suvr2 = ['', '']
# suvr3 = ['', '']
# R1[0] = get_regex(convs_dir, 'V01_R1_2MNI.nii.gz', False)
# R1[1] = get_regex(convs_dir, 'V02_R1_2MNI.nii.gz', False)
# k2[0] = get_regex(convs_dir, 'V01_k2_2MNI.nii.gz', False)
# k2[1] = get_regex(convs_dir, 'V02_k2_2MNI.nii.gz', False)
# Logs[0] = get_regex(convs_dir, 'V01_pib_logan_vt_2MNI.nii', False)
# Logs[1] = get_regex(convs_dir, 'V02_pib_logan_vt_2MNI.nii', False)
# suvr1[0] = get_regex(convs_dir, '_V01_SUVR_sm2_5_0_to_120_2MNI.nii.gz', False)
# suvr1[1] = get_regex(convs_dir, '_V02_SUVR_sm2_5_0_to_120_2MNI.nii.gz', False)
# suvr2[0] = get_regex(convs_dir, '_V01_SUVR_sm2_5_1_to_8_2MNI.nii.gz', False)
# suvr2[1] = get_regex(convs_dir, '_V02_SUVR_sm2_5_1_to_8_2MNI.nii.gz', False)
# suvr3[0] = get_regex(convs_dir, 'V01_SUVR_sm2_5_50_to_70_2MNI.nii.gz', False)
# suvr3[1] = get_regex(convs_dir, 'V02_SUVR_sm2_5_50_to_70_2MNI.nii.gz', False)
R1V01 = os.path.join(convs_dir, 'sm_R1_V01.nii.gz')
R1V02 = os.path.join(convs_dir, 'sm_R1_V02.nii.gz')
k2V01 = os.path.join(convs_dir, 'sm_k2_V01.nii.gz')
k2V02 = os.path.join(convs_dir, 'sm_k2_V02.nii.gz')
logV01 = os.path.join(convs_dir, 'sm_logan_V01.nii.gz')
logV02 = os.path.join(convs_dir, 'sm_logan_V02.nii.gz')
SUVRs1V01 = os.path.join(convs_dir, 'sm_SUVR_0_to_120s_V01.nii.gz')
SUVRs1V02 = os.path.join(convs_dir, 'sm_SUVR_0_to_120s_V02.nii.gz')
SUVRs2V01 = os.path.join(convs_dir, 'sm_SUVR_1_to_8m_V01.nii.gz')
SUVRs2V02 = os.path.join(convs_dir, 'sm_SUVR_1_to_8m_V02.nii.gz')
SUVRs3V01 = os.path.join(convs_dir, 'sm_SUVR_50_to_70m_V01.nii.gz')
SUVRs3V02 = os.path.join(convs_dir, 'sm_SUVR_50_to_70m_V02.nii.gz')
# fslmerge(R1[0], R1V01)
# fslmerge(R1[1], R1V02)
# fslmerge(k2[0], k2V01)
# fslmerge(k2[1], k2V02)
# fslmerge(Logs[0], logV01)
# fslmerge(Logs[1], logV02)
# fslmerge(suvr1[0], SUVRs1V01)
# fslmerge(suvr1[1], SUVRs1V02)
# fslmerge(suvr2[0], SUVRs2V01)
# fslmerge(suvr2[1], SUVRs2V02)
# fslmerge(suvr3[0], SUVRs3V01)
# fslmerge(suvr3[1], SUVRs3V02)
R1V01 = nib.load(R1V01).get_fdata()
R1V02 = nib.load(R1V02).get_fdata()
k2V01 = nib.load(k2V01).get_fdata()
k2V02 = nib.load(k2V02).get_fdata()
logV01 = nib.load(logV01).get_fdata()
logV02 = nib.load(logV02).get_fdata()
SUVRs1V01 = nib.load(SUVRs1V01).get_fdata()
SUVRs1V02 = nib.load(SUVRs1V02).get_fdata()
SUVRs2V01 = nib.load(SUVRs2V01).get_fdata()
SUVRs2V02 = nib.load(SUVRs2V02).get_fdata()
SUVRs3V01 = nib.load(SUVRs3V01).get_fdata()
SUVRs3V02 = nib.load(SUVRs3V02).get_fdata()
# =============================================================================
ROIs = get_regex(masks_dir, '^all_subjects_(cbgm|cbwm|lobes_frontal|lobes_insula|lobes_limbic|lobes_occipital|lobes_parietal|lobes_temporal|striatum|thalamus|WM).nii.gz', False, False, full=False)
ROIs.extend(get_regex(masks_dir, '^all_subjects_wm_lobes_(cingulate|frontal|insula|occipital|parietal|temporal).nii.gz', False, False, full=False))
pprint(ROIs)
names = ['wm',
         'cbgm',
         'cbwm',
         'frontal_ctx',
         'insula',
         'limbic_ctx',
         'occipital_ctx',
         'parietal_ctx',
         'temporal_ctx',
         'striatum',
         'thalamus',
         "cingulate_wm",
         "frontal_wm",
         "insula_wm",
         "occipital_wm",
         "parietal_wm",
         "temporal_wm"]
# =============================================================================
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'ROI Name',
              'ROI Volume',
              'mR1V01',
              'mR1V02',
              'mk2V01',
              'mk2V02',
              'k2primeV01',
              'k2primeV02',
              'DVRV01',
              'DVRV02',
              'mloganV01',
              'mloganV02',
              'mSUVR0to120V01',
              'mSUVR0to120V02',
              'mSUVR1to8V01',
              'mSUVR1to8V02',
              'mSUVR50to70V01',
              'mSUVR50to70V02',
              'TRDVRSRTM2',
              'TRR1',
              'TRDVRlogan',
              'TRSUVR0to120',
              'TRSUVR1to8',
              'TRSUVR50to70']

stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
for i, roi in enumerate(ROIs):
    roidata = nib.load(roi).get_fdata()
    if not i:
        save_wm = roidata
    if i > 10:
        roisdata = np.multiply(roidata, save_wm)
    roiV = int(np.sum(roidata))
    roi_name = names[i]
    print('ROI Volume:', roiV)
    for i, s in enumerate(subjects):
        print('    ', s)
        # retrieving images
        R1_V01 = R1V01[:, :, :, i]
        R1_V02 = R1V02[:, :, :, i]
        k2_V01 = k2V01[:, :, :, i]
        k2_V02 = k2V02[:, :, :, i]
        log_V01 = logV01[:, :, :, i]
        log_V02 = logV02[:, :, :, i]
        SUVRs1_V01 = SUVRs1V01[:, :, :, i]
        SUVRs1_V02 = SUVRs1V02[:, :, :, i]
        SUVRs2_V01 = SUVRs2V01[:, :, :, i]
        SUVRs2_V02 = SUVRs2V02[:, :, :, i]
        SUVRs3_V01 = SUVRs3V01[:, :, :, i]
        SUVRs3_V02 = SUVRs3V02[:, :, :, i]
        k2prime = k2primes[i]
        # setting default values for .csv file
        mR1V01 = np.nan
        mR1V02 = np.nan
        mk2V01 = np.nan
        mk2V02 = np.nan
        k2primeV01 = k2prime[0]
        k2primeV02 = k2prime[1]
        # DVR before mean
        bmDVRV01 = np.nan
        bmDVRV02 = np.nan
        # DVR after mean
        DVRV01 = np.nan
        DVRV02 = np.nan
        # Mean Logan
        mloganV01 = np.nan
        mloganV02 = np.nan
        # Results
        TRDVRSRTM2 = np.nan
        TRDVRlogan = np.nan

        mR1V01 = np.sum(np.multiply(roidata, R1_V01))/roiV
        mR1V02 = np.sum(np.multiply(roidata, R1_V02))/roiV
        mk2V01 = np.sum(np.multiply(roidata, k2_V01))/roiV
        mk2V02 = np.sum(np.multiply(roidata, k2_V02))/roiV
        mloganV01 = np.sum(np.multiply(roidata, log_V01))/roiV
        mloganV02 = np.sum(np.multiply(roidata, log_V02))/roiV
        mSUVR0to120V01 = np.sum(np.multiply(roidata, SUVRs1_V01))/roiV
        mSUVR0to120V02 = np.sum(np.multiply(roidata, SUVRs1_V02))/roiV
        mSUVR1to8V01 = np.sum(np.multiply(roidata, SUVRs2_V01))/roiV
        mSUVR1to8V02 = np.sum(np.multiply(roidata, SUVRs2_V02))/roiV
        mSUVR40to90V01 = np.sum(np.multiply(roidata, SUVRs3_V01))/roiV
        mSUVR40to90V02 = np.sum(np.multiply(roidata, SUVRs3_V02))/roiV
        # DVR after mean calculation
        try:
            DVRV01 = mR1V01 * k2primeV01 / mk2V01
        except ZeroDivisionError:
            DVRV01 = np.nan

        try:
            DVRV02 = mR1V02 * k2primeV02 / mk2V02
        except ZeroDivisionError:
            DVRV02 = np.nan

        # Calculating TRV
        try:
            TRDVRSRTM2 = 2 * abs(DVRV01 - DVRV02) / (DVRV01 + DVRV02)
        except ZeroDivisionError:
            TRDVRSRTM2 = np.nan
        try:
            TRDVRlogan = 2 * abs(mloganV01 - mloganV02) / (mloganV01 + mloganV02)
        except ZeroDivisionError:
            TRDVRlogan = np.nan

        try:
            TRR1 = 2 * abs(mR1V01 - mR1V02) / (mR1V01 + mR1V02)
        except ZeroDivisionError:
            TRR1 = np.nan
        try:
            TRSUVR0120 = 2 * abs(mSUVR0to120V01 - mSUVR0to120V02) / (mSUVR0to120V01 + mSUVR0to120V02)
        except ZeroDivisionError:
            TRSUVR0120 = np.nan
        try:
            TRSUVR1to8 = 2 * abs(mSUVR1to8V01 - mSUVR1to8V02) / (mSUVR1to8V01 + mSUVR1to8V02)
        except ZeroDivisionError:
            TRSUVR1to8 = np.nan
        try:
            TRSUVR40to90 = 2 * abs(mSUVR40to90V01 - mSUVR40to90V02) / (mSUVR40to90V01 + mSUVR40to90V02)
        except ZeroDivisionError:
            TRSUVR40to90 = np.nan
        stats.writerow({'Subject': s,
                        'ROI Name': roi_name,
                        'ROI Volume': roiV,
                        'mR1V01': mR1V01,
                        'mR1V02': mR1V02,
                        'mk2V01': mk2V01,
                        'mk2V02': mk2V02,
                        'k2primeV01': k2primeV01,
                        'k2primeV02': k2primeV02,
                        'DVRV01': DVRV01,
                        'DVRV02': DVRV02,
                        'mloganV01': mloganV01,
                        'mloganV02': mloganV02,
                        'mSUVR0to120V01': mSUVR0to120V01,
                        'mSUVR0to120V02': mSUVR0to120V02,
                        'mSUVR1to8V01': mSUVR1to8V01,
                        'mSUVR1to8V02': mSUVR1to8V02,
                        'mSUVR50to70V01': mSUVR40to90V01,
                        'mSUVR50to70V02': mSUVR40to90V02,
                        'TRDVRSRTM2': TRDVRSRTM2,
                        'TRR1': TRR1,
                        'TRDVRlogan': TRDVRlogan,
                        'TRSUVR0to120': TRSUVR0120,
                        'TRSUVR1to8': TRSUVR1to8,
                        'TRSUVR50to70': TRSUVR40to90})
