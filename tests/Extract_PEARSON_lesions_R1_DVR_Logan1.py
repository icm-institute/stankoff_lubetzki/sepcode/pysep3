#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 17:26:12 2021

@author: arya.yazdan-panah
"""

import os
import csv
import time
import numpy as np
import nibabel as nib
from scipy.stats import pearsonr
from common3 import get_regex

# C'est une résolution formelle j'ai le droit... cheh
np.seterr(divide='ignore', invalid='ignore')
# =============================================================================
les_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retes\
t/data/process/labelled_inflasep_lesions'
# Lesions of the patients in MNI
Lesions = get_regex(les_dir, 'P_...../P_....._.*nii.gz', False, )
print("Number of Lesions:", len(Lesions))
# =============================================================================
subjects = ["C_ARIIS",
            "C_BENFL",
            "C_BENMA",
            "C_CHRKE",
            "C_DUBAN",
            "C_GUECO",
            "C_LEBJO",
            "C_LEVVA"]
# =============================================================================
k2primes = [[0.06338108263, 0.05934295106],
            [0.06786313224, 0.06633747349],
            [0.06166487042, 0.06185613334],
            [0.05867357518, 0.0574208377],
            [0.06154755673, 0.0662559161],
            [0.06014615976, 0.06281320825],
            [0.06083698382, 0.05798011997],
            [0.06063100159, 0.05972947848]]
k2prime = [0, 0]
# =============================================================================
input_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_ret\
est/data/process/TR_new_MNI'
# =============================================================================
stats_dir = os.path.join(input_dir, 'stats')

convs_dir = os.path.join(input_dir, 'convolutions')

masks_dir = os.path.join(input_dir, 'masks')
# =============================================================================
stats_file = os.path.join(stats_dir, 'inflasep_lesions_pearson_suvr_2.csv')
wm_file = os.path.join(masks_dir, 'all_subjects_WM.nii.gz')
WM = nib.load(wm_file).get_fdata()

R1V01 = os.path.join(convs_dir, 'R1_V01.nii.gz')
R1V02 = os.path.join(convs_dir, 'R1_V02.nii.gz')
k2V01 = os.path.join(convs_dir, 'k2_V01.nii.gz')
k2V02 = os.path.join(convs_dir, 'k2_V02.nii.gz')
logV01 = os.path.join(convs_dir, 'logan_V01.nii.gz')
logV02 = os.path.join(convs_dir, 'logan_V02.nii.gz')
SUVRs1V01 = os.path.join(convs_dir, 'SUVR_0_to_120s_V01.nii.gz')
SUVRs1V02 = os.path.join(convs_dir, 'SUVR_0_to_120s_V02.nii.gz')
SUVRs2V01 = os.path.join(convs_dir, 'SUVR_1_to_8m_V01.nii.gz')
SUVRs2V02 = os.path.join(convs_dir, 'SUVR_1_to_8m_V02.nii.gz')

R1V01 = nib.load(R1V01).get_fdata()
R1V02 = nib.load(R1V02).get_fdata()
k2V01 = nib.load(k2V01).get_fdata()
k2V02 = nib.load(k2V02).get_fdata()
logV01 = nib.load(logV01).get_fdata()
logV02 = nib.load(logV02).get_fdata()
SUVRs1V01 = nib.load(SUVRs1V01).get_fdata()
SUVRs1V02 = nib.load(SUVRs1V02).get_fdata()
SUVRs2V01 = nib.load(SUVRs2V01).get_fdata()
SUVRs2V02 = nib.load(SUVRs2V02).get_fdata()

f = open(stats_file, 'w', newline='')

fieldnames = ['Lesion Name',
              'Lesion Volume',
              'R1',
              'DVR (SRTM2)',
              'DVR (logan)',
              'SUVR1',
              'SUVR2']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()

then = time.time()
for les in Lesions:
    ldata = np.multiply(nib.load(les).get_fdata(), WM)
    LV = int(np.sum(ldata))
    if LV == 0:
        continue
    print('Lesion Volume:', LV)
    les_name = os.path.basename(les)
    # setting default values for .csv file
    mR1 = np.zeros((8, 2))
    mk2 = np.zeros((8, 2))
    DVR = np.zeros((8, 2))
    SUVR1 = np.zeros((8, 2))
    SUVR2 = np.zeros((8, 2))
    mlogan = np.zeros((8, 2))
    # Results
    ICCR1 = np.nan
    ICCDVR = np.nan
    ICCLogan = np.nan
    ICCSUVR1 = np.nan
    ICCSUVR2 = np.nan
    for i, s in enumerate(subjects):
        print('    ', s)
        # retrieving images and K2prime for the subject
        R1_V01 = R1V01[:, :, :, i]
        R1_V02 = R1V02[:, :, :, i]
        k2_V01 = k2V01[:, :, :, i]
        k2_V02 = k2V02[:, :, :, i]
        log_V01 = logV01[:, :, :, i]
        log_V02 = logV02[:, :, :, i]
        SUVRs1_V01 = SUVRs1V01[:, :, :, i]
        SUVRs1_V02 = SUVRs1V02[:, :, :, i]
        SUVRs2_V01 = SUVRs2V01[:, :, :, i]
        SUVRs2_V02 = SUVRs2V02[:, :, :, i]
        k2primeV01 = k2primes[i][0]
        k2primeV02 = k2primes[i][1]

        # Finding mean values of R1, k2, DVR, logan for the lesion
        mR1[i, 0] = np.sum(np.multiply(ldata, R1_V01)) / LV
        mR1[i, 1] = np.sum(np.multiply(ldata, R1_V02)) / LV
        mk2[i, 0] = np.sum(np.multiply(ldata, k2_V01)) / LV
        mk2[i, 1] = np.sum(np.multiply(ldata, k2_V02)) / LV
        mlogan[i, 0] = np.sum(np.multiply(ldata, log_V01)) / LV
        mlogan[i, 1] = np.sum(np.multiply(ldata, log_V02)) / LV
        SUVR1[i, 0] = np.sum(np.multiply(ldata, SUVRs1_V01))/LV
        SUVR1[i, 1] = np.sum(np.multiply(ldata, SUVRs1_V02))/LV
        SUVR2[i, 0] = np.sum(np.multiply(ldata, SUVRs2_V01))/LV
        SUVR2[i, 1] = np.sum(np.multiply(ldata, SUVRs2_V02))/LV
        # DVR after mean calculation
        try:
            DVR[i, 0] = mR1[i, 0] * k2primeV01 / mk2[i, 0]
        except ZeroDivisionError:
            DVR[i, 0] = np.nan
        try:
            DVR[i, 1] = mR1[i, 1] * k2primeV02 / mk2[i, 1]
        except ZeroDivisionError:
            DVR[i, 1] = np.nan
    ICCR1 = pearsonr(mR1[:, 0], mR1[:, 1])[0]
    ICCDVR = pearsonr(DVR[:, 0], DVR[:, 1])[0]
    ICCLogan = pearsonr(mlogan[:, 0], mlogan[:, 1])[0]
    ICCSUVR1 = pearsonr(SUVR1[:, 0], SUVR1[:, 1])[0]
    ICCSUVR2 = pearsonr(SUVR2[:, 0], SUVR2[:, 1])[0]
    stats.writerow({'Lesion Name': les_name,
                    'Lesion Volume': LV,
                    'R1': ICCR1,
                    'DVR (SRTM2)': ICCDVR,
                    'DVR (logan)': ICCLogan,
                    'SUVR1': ICCSUVR1,
                    'SUVR2': ICCSUVR2})
duration = time.time() - then
mins, secs = divmod(duration, 60)
hours, mins = divmod(mins, 60)
print(hours, mins, secs)
