#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 15:43:44 2021

@author: arya.yazdan-panah
"""

import os
import csv
import numpy as np
import nibabel as nib
from common3 import get_regex
from tools_fsl import fslmerge
from scipy.stats import ttest_ind

# C'est une résolution formelle j'ai le droit... cheh
np.seterr(divide='ignore', invalid='ignore')
# =============================================================================
les_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retes\
t/data/process/labelled_inflasep_lesions'
# Lesions of the patients in MNI
Lesions = get_regex(les_dir, 'P_...../P_....._.*nii.gz', False, )
print("Number of Lesions:", len(Lesions))
# =============================================================================
subjects = ["C_ARIIS",
            "C_ARIIS",
            "C_BENFL",
            "C_BENFL",
            "C_BENMA",
            "C_BENMA",
            "C_CHRKE",
            "C_CHRKE",
            "C_DUBAN",
            "C_DUBAN",
            "C_GUECO",
            "C_GUECO",
            "C_LEBJO",
            "C_LEBJO",
            "C_LEVVA",
            "C_LEVVA"]
# =============================================================================
k2primes = [0.06338108263,
            0.05934295106,
            0.06786313224,
            0.06633747349,
            0.06166487042,
            0.06185613334,
            0.05867357518,
            0.0574208377,
            0.06154755673,
            0.0662559161,
            0.06014615976,
            0.06281320825,
            0.06083698382,
            0.05798011997,
            0.06063100159,
            0.05972947848]
# =============================================================================
input_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_ret\
est/data/process/TR_new_MNI'
# =============================================================================
stats_dir = os.path.join(input_dir, 'stats')
convs_dir = os.path.join(input_dir, 'convolutions')
masks_dir = os.path.join(input_dir, 'masks')
# =============================================================================
wm_file = os.path.join(masks_dir, 'all_subjects_WM.nii.gz')
WM = nib.load(wm_file).get_fdata()
wm_file = os.path.join(masks_dir, 'flipped', 'all_subjects_WM_flipped.nii.gz')
WM_flip = nib.load(wm_file).get_fdata()
WM = np.multiply(WM, WM_flip)
# =============================================================================
R1 = os.path.join(convs_dir, 'flipped', 'R1.nii.gz')
R1contr = os.path.join(convs_dir, 'flipped', 'R1_contr.nii.gz')
k2 = os.path.join(convs_dir, 'flipped', 'k2.nii.gz')
k2contr = os.path.join(convs_dir, 'flipped', 'k2_contr.nii.gz')
log = os.path.join(convs_dir, 'flipped', 'logan.nii.gz')
logcontr = os.path.join(convs_dir, 'flipped', 'logan_contr.nii.gz')
suvr18 = os.path.join(convs_dir, 'flipped', 'suvr1to8.nii.gz')
suvr18contr = os.path.join(convs_dir, 'flipped', 'suvr1to8_contr.nii.gz')
suvr0120 = os.path.join(convs_dir, 'flipped', 'suvr0to120.nii.gz')
suvr0120contr = os.path.join(convs_dir, 'flipped', 'suvr0to120_contr.nii.gz')
suvr4090 = os.path.join(convs_dir, 'flipped', 'suvr5070.nii.gz')
suvr4090contr = os.path.join(convs_dir, 'flipped', 'suvr5070_contr.nii.gz')
# =============================================================================
R1 = nib.load(R1).get_fdata()
R1contr = nib.load(R1contr).get_fdata()
k2 = nib.load(k2).get_fdata()
k2contr = nib.load(k2contr).get_fdata()
log = nib.load(log).get_fdata()
logcontr = nib.load(logcontr).get_fdata()
suvr18 = nib.load(suvr18).get_fdata()
suvr18contr = nib.load(suvr18contr).get_fdata()
suvr0120 = nib.load(suvr0120).get_fdata()
suvr0120contr = nib.load(suvr0120contr).get_fdata()
suvr4090 = nib.load(suvr4090).get_fdata()
suvr4090contr = nib.load(suvr4090contr).get_fdata()
# =============================================================================
# %%
stats_file = os.path.join(stats_dir, 'ttset_inflasep_lesions_contrlat.csv')
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'Lesion Name',
              'Lesion Volume',
              'tR1',
              'pR1',
              'tk2',
              'pk2',
              'tDVR',
              'pDVR',
              'tlogan',
              'plogan',
              'tSUVR18',
              'pSUVR18',
              'tSUVR0120',
              'pSUVR0120',
              'tSUVR5070',
              'pSUVR5070']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
# =============================================================================
for les in Lesions:
    ldata = np.multiply(nib.load(les).get_fdata(), WM)
    LV = int(np.sum(ldata))
    if LV == 0:
        continue
    print('Lesion Volume:', LV)
    les_name = os.path.basename(les)
    for i, s in enumerate(subjects):
        print('    ', s)
# =============================================================================
        R1_norm = R1[:, :, :, i]
        R1_contr = R1contr[:, :, :, i]
        k2_norm = k2[:, :, :, i]
        k2_contr = k2contr[:, :, :, i]
        log_norm = log[:, :, :, i]
        log_contr = logcontr[:, :, :, i]
        suvr18_norm = suvr18[:, :, :, i]
        suvr18_contr = suvr18contr[:, :, :, i]
        suvr0120_norm = suvr0120[:, :, :, i]
        suvr0120_contr = suvr0120contr[:, :, :, i]
        suvr4090_norm = suvr4090[:, :, :, i]
        suvr4090_contr = suvr4090contr[:, :, :, i]
        k2prime = k2primes[i]
# =============================================================================
        vox_R1 = R1_norm[ldata == 1]
        vox_R1contr = R1_contr[ldata == 1]
        vox_k2 = k2_norm[ldata == 1]
        vox_k2contr = k2_contr[ldata == 1]
        vox_logan = log_norm[ldata == 1]
        vox_logancontr = log_contr[ldata == 1]
        vox_suvr18 = suvr18_norm[ldata == 1]
        vox_suvr18_contr = suvr18_contr[ldata == 1]
        vox_suvr0120 = suvr0120_norm[ldata == 1]
        vox_suvr0120_contr = suvr0120_contr[ldata == 1]
        vox_suvr4090 = suvr4090_norm[ldata == 1]
        vox_suvr4090_contr = suvr4090_contr[ldata == 1]
        vox_DVR = vox_R1 * k2prime / vox_k2
        vox_DVRcontr = vox_R1contr * k2prime / vox_k2contr

        stat_R1 = ttest_ind(vox_R1, vox_R1contr)
        stat_k2 = ttest_ind(vox_k2, vox_k2contr)
        stat_DVR = ttest_ind(vox_DVR, vox_DVRcontr)
        stat_logan = ttest_ind(vox_logan, vox_logancontr)
        stat_suvr18 = ttest_ind(vox_suvr18, vox_suvr18_contr)
        stat_suvr0120 = ttest_ind(vox_suvr0120, vox_suvr0120_contr)
        stat_suvr4090 = ttest_ind(vox_suvr4090, vox_suvr4090_contr)

        # Calculating ttests
        stats.writerow({'Subject': s,
                        'Lesion Name': les_name,
                        'Lesion Volume': LV,
                        'tR1': stat_R1[0],
                        'pR1': stat_R1[1],
                        'tk2': stat_k2[0],
                        'pk2': stat_k2[1],
                        'tDVR': stat_DVR[0],
                        'pDVR': stat_DVR[1],
                        'tlogan': stat_logan[0],
                        'plogan': stat_logan[1],
                        'tSUVR18': stat_suvr18[0],
                        'pSUVR18': stat_suvr18[1],
                        'tSUVR0120': stat_suvr0120[0],
                        'pSUVR0120': stat_suvr0120[1],
                        'tSUVR5070': stat_suvr4090[0],
                        'pSUVR5070': stat_suvr4090[1]})
f.close()