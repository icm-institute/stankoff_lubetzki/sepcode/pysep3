#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 14:33:23 2021

@author: arya.yazdan-panah
"""

import os
import csv
import numpy as np
import nibabel as nib
from common3 import get_regex
from pprint import pprint
from tools_fsl import fslmerge


# C'est une résolution formelle j'ai le droit... cheh
np.seterr(divide='ignore', invalid='ignore')
# =============================================================================
les_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retes\
t/data/process/labelled_inflasep_lesions'
# Lesions of the patients in MNI
Lesions = get_regex(les_dir, 'P_...../P_....._.*nii.gz', False, )
print("Number of Lesions:", len(Lesions))
# =============================================================================
subjects = ["C_ARIIS",
            "C_BENFL",
            "C_BENMA",
            "C_CHRKE",
            "C_DUBAN",
            "C_GUECO",
            "C_LEBJO",
            "C_LEVVA"]
# =============================================================================
k2primes = [[0.06338108263, 0.05934295106],
            [0.06786313224, 0.06633747349],
            [0.06166487042, 0.06185613334],
            [0.05867357518, 0.0574208377],
            [0.06154755673, 0.0662559161],
            [0.06014615976, 0.06281320825],
            [0.06083698382, 0.05798011997],
            [0.06063100159, 0.05972947848]]
k2prime = [0, 0]
# =============================================================================
input_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_ret\
est/data/process/TR_new_MNI'
# =============================================================================
stats_dir = os.path.join(input_dir, 'stats')

convs_dir = os.path.join(input_dir, 'convolutions')

masks_dir = os.path.join(input_dir, 'masks')
# =============================================================================
stats_file = os.path.join(stats_dir,
                          'inflasep_lesions_contrlat_longitudinal.csv')
wm_file = os.path.join(masks_dir, 'all_subjects_WM.nii.gz')
WM = nib.load(wm_file).get_fdata()
wm_file = os.path.join(masks_dir, 'flipped', 'all_subjects_WM_flipped.nii.gz')
WM_flip = nib.load(wm_file).get_fdata()
WM = np.multiply(WM, WM_flip)
# R1 = ['', '']
# k2 = ['', '']
# DVR = ['', '']
# Logs = ['', '']
# SUVR18 = ['', '']
# SUVR0120 = ['', '']
# R1[0] = get_regex(convs_dir, 'C_...../.*_R1_2MNI.nii.gz', False)
# R1[1] = get_regex(convs_dir, 'flipped/C_...../.*_R1_2MNI_flipped.nii.gz', False)
# k2[0] = get_regex(convs_dir, 'C_...../.*_k2_2MNI.nii.gz', False)
# k2[1] = get_regex(convs_dir, 'flipped/C_...../.*_k2_2MNI_flipped.nii.gz', False)
# Logs[0] = get_regex(convs_dir, 'C_...../.*_pib_logan_vt_2MNI.nii.gz', False)
# Logs[1] = get_regex(convs_dir, 'flipped/C_...../.*_pib_logan_vt_2MNI_flipped.nii.gz', False)
# SUVR18[0] = get_regex(convs_dir, 'C_...../.*_SUVR_1_to_8m_2MNI.nii.gz', False)
# SUVR18[1] = get_regex(convs_dir, 'flipped/C_...../.*_SUVR_1_to_8m_2MNI_flipped.nii.gz', False)
# SUVR0120[0] = get_regex(convs_dir, 'C_...../.*_SUVR_0_to_120s_2MNI.nii.gz', False)
# SUVR0120[1] = get_regex(convs_dir, 'flipped/C_...../.*_SUVR_0_to_120s_2MNI_flipped.nii.gz', False)


R1 = os.path.join(convs_dir, 'flipped', 'R1.nii.gz')
R1contr = os.path.join(convs_dir, 'flipped', 'R1_contr.nii.gz')
k2 = os.path.join(convs_dir, 'flipped', 'k2.nii.gz')
k2contr = os.path.join(convs_dir, 'flipped', 'k2_contr.nii.gz')
log = os.path.join(convs_dir, 'flipped', 'logan.nii.gz')
logcontr = os.path.join(convs_dir, 'flipped', 'logan_contr.nii.gz')
suvr18 = os.path.join(convs_dir, 'flipped', 'suvr1to8.nii.gz')
suvr18contr = os.path.join(convs_dir, 'flipped', 'suvr1to8_contr.nii.gz')
suvr0120 = os.path.join(convs_dir, 'flipped', 'suvr0to120.nii.gz')
suvr0120contr = os.path.join(convs_dir, 'flipped', 'suvr0to120_contr.nii.gz')
# fslmerge(R1[0], R1)
# fslmerge(R1[1], R1contr)
# fslmerge(k2[0], k2)
# fslmerge(k2[1], k2contr)
# fslmerge(Logs[0], log)
# fslmerge(Logs[1], logcontr)
# fslmerge(SUVR18[0], suvr18)
# fslmerge(SUVR18[1], surv18contr)
# fslmerge(SUVR0120[0], suvr0120)
# fslmerge(SUVR0120[1], suvr0120contr)


R1 = nib.load(R1).get_fdata()
R1contr = nib.load(R1contr).get_fdata()
k2 = nib.load(k2).get_fdata()
k2contr = nib.load(k2contr).get_fdata()
log = nib.load(log).get_fdata()
logcontr = nib.load(logcontr).get_fdata()
suvr18 = nib.load(suvr18).get_fdata()
suvr18contr = nib.load(suvr18contr).get_fdata()
suvr0120 = nib.load(suvr0120).get_fdata()
suvr0120contr = nib.load(suvr0120contr).get_fdata()

f = open(stats_file, 'w', newline='')

fieldnames = ['Subject',
              'Lesion Name',
              'Lesion Volume',
              'TRR1_V01',
              'TRDVRSRTM2_V01',
              'TRDVRlogan_V01',
              'TRR1_V02',
              'TRDVRSRTM2_V02',
              'TRDVRlogan_V02',
              'TRSUVR18_V01',
              'TRSUVR18_V02',
              'TRSUVR0120_V01',
              'TRSUVR0120_V02']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
for les in Lesions:
    ldata = np.multiply(nib.load(les).get_fdata(), WM)
    LV = int(np.sum(ldata))
    if LV == 0:
        continue
    print('Lesion Volume:', LV)
    les_name = os.path.basename(les)
    for j in range(8):
        s = subjects[j]
        print('    ', s)
        ind_V01 = 2*j
        ind_V02 = ind_V01 + 1
        # retrieving images V01
        R1_norm_V01 = R1[:, :, :, ind_V01]
        R1_contr_V01 = R1contr[:, :, :, ind_V01]
        k2_norm_V01 = k2[:, :, :, ind_V01]
        k2_contr_V01 = k2contr[:, :, :, ind_V01]
        log_norm_V01 = log[:, :, :, ind_V01]
        log_contr_V01 = logcontr[:, :, :, ind_V01]
        suvr18_norm_V01 = suvr18[:, :, :, ind_V01]
        suvr18_contr_V01 = suvr18contr[:, :, :, ind_V01]
        suvr0120_norm_V01 = suvr0120[:, :, :, ind_V01]
        suvr0120_contr_V01 = suvr0120contr[:, :, :, ind_V01]
        k2prime_V01 = k2primes[j][0]
        # retrieving images V02
        R1_norm_V02 = R1[:, :, :, ind_V02]
        R1_contr_V02 = R1contr[:, :, :, ind_V02]
        k2_norm_V02 = k2[:, :, :, ind_V02]
        k2_contr_V02 = k2contr[:, :, :, ind_V02]
        log_norm_V02 = log[:, :, :, ind_V02]
        log_contr_V02 = logcontr[:, :, :, ind_V02]
        suvr18_norm_V02 = suvr18[:, :, :, ind_V02]
        suvr18_contr_V02 = suvr18contr[:, :, :, ind_V02]
        suvr0120_norm_V02 = suvr0120[:, :, :, ind_V02]
        suvr0120_contr_V02 = suvr0120contr[:, :, :, ind_V02]
        k2prime_V02 = k2primes[j][1]
        # V01 values
        R1_V01 = np.sum(np.multiply(ldata, R1_norm_V01))/LV
        R1_V01_contr = np.sum(np.multiply(ldata, R1_contr_V01))/LV
        k2_V01 = np.sum(np.multiply(ldata, k2_norm_V01))/LV
        k2_V01_contr = np.sum(np.multiply(ldata, k2_contr_V01))/LV
        logan_V01 = np.sum(np.multiply(ldata, log_norm_V01))/LV
        logan_V01_contr = np.sum(np.multiply(ldata, log_contr_V01))/LV
        suvr18_V01 = np.sum(np.multiply(ldata, suvr18_norm_V01))/LV
        suvr18_V01_contr = np.sum(np.multiply(ldata, suvr18_contr_V01))/LV
        suvr0120_V01 = np.sum(np.multiply(ldata, suvr0120_norm_V01))/LV
        suvr0120_V01_contr = np.sum(np.multiply(ldata, suvr0120_contr_V01))/LV
        # DVR V01 contr vs normal
        try:
            DVR_V01 = R1_V01 * k2prime_V01 / k2_V01
        except ZeroDivisionError:
            DVR_V01 = np.nan
        try:
            DVR_V01_contr = R1_V01_contr * k2prime_V01 / k2_V01_contr
        except ZeroDivisionError:
            DVR_V01_contr = np.nan
        try:
            TRR1_V01 = 2 * (R1_V01 - R1_V01_contr) / (R1_V01 + R1_V01_contr)
        except ZeroDivisionError:
            TRR1_V01 = np.nan
        try:
            TRDVRSRTM2_V01 = 2 * (DVR_V01 - DVR_V01_contr) / (DVR_V01 + DVR_V01_contr)
        except ZeroDivisionError:
            TRDVRSRTM2_V01 = np.nan
        try:
            TRDVRlogan_V01 = 2 * (logan_V01 - logan_V01_contr) / (logan_V01 + logan_V01_contr)
        except ZeroDivisionError:
            TRDVRlogan_V01 = np.nan
        try:
            TRSUVR18_V01 = 2 * (suvr18_V01 - suvr18_V01_contr) / (suvr18_V01 + suvr18_V01_contr)
        except ZeroDivisionError:
            TRSUVR18_V01 = np.nan
        try:
            TRSUVR0120_V01 = 2 * (suvr0120_V01 - suvr0120_V01_contr) / (suvr0120_V01 + suvr0120_V01_contr)
        except ZeroDivisionError:
            TRSUVR0120_V01 = np.nan
        # V02 values
        R1_V02 = np.sum(np.multiply(ldata, R1_norm_V02))/LV
        R1_V02_contr = np.sum(np.multiply(ldata, R1_contr_V02))/LV
        k2_V02 = np.sum(np.multiply(ldata, k2_norm_V02))/LV
        k2_V02_contr = np.sum(np.multiply(ldata, k2_contr_V02))/LV
        logan_V02 = np.sum(np.multiply(ldata, log_norm_V02))/LV
        logan_V02_contr = np.sum(np.multiply(ldata, log_contr_V02))/LV
        suvr18_V02 = np.sum(np.multiply(ldata, suvr18_norm_V02))/LV
        suvr18_V02_contr = np.sum(np.multiply(ldata, suvr18_contr_V02))/LV
        suvr0120_V02 = np.sum(np.multiply(ldata, suvr0120_norm_V02))/LV
        suvr0120_V02_contr = np.sum(np.multiply(ldata, suvr0120_contr_V02))/LV
        # DVR V02 contr vs normal
        try:
            DVR_V02 = R1_V02 * k2prime_V02 / k2_V02
        except ZeroDivisionError:
            DVR_V02 = np.nan
        try:
            DVR_V02_contr = R1_V02_contr * k2prime_V02 / k2_V02_contr
        except ZeroDivisionError:
            DVR_V02_contr = np.nan
        try:
            TRR1_V02 = 2 * (R1_V02 - R1_V02_contr) / (R1_V02 + R1_V02_contr)
        except ZeroDivisionError:
            TRR1_V02 = np.nan
        try:
            TRDVRSRTM2_V02 = 2 * (DVR_V02 - DVR_V02_contr) / (DVR_V02 + DVR_V02_contr)
        except ZeroDivisionError:
            TRDVRSRTM2_V02 = np.nan
        try:
            TRDVRlogan_V02 = 2 * (logan_V02 - logan_V02_contr) / (logan_V02 + logan_V02_contr)
        except ZeroDivisionError:
            TRDVRlogan_V02 = np.nan
        try:
            TRSUVR18_V02 = 2 * (suvr18_V02 - suvr18_V02_contr) / (suvr18_V02 + suvr18_V02_contr)
        except ZeroDivisionError:
            TRSUVR18_V02 = np.nan
        try:
            TRSUVR0120_V02 = 2 * (suvr0120_V02 - suvr0120_V02_contr) / (suvr0120_V02 + suvr0120_V02_contr)
        except ZeroDivisionError:
            TRSUVR0120_V02 = np.nan
        # Writing CSV
        stats.writerow({'Subject': s,
                        'Lesion Name': les_name,
                        'Lesion Volume': LV,
                        'TRR1_V01': TRR1_V01,
                        'TRDVRSRTM2_V01': TRDVRSRTM2_V01,
                        'TRDVRlogan_V01': TRDVRlogan_V01,
                        'TRR1_V02': TRR1_V02,
                        'TRDVRSRTM2_V02': TRDVRSRTM2_V02,
                        'TRDVRlogan_V02': TRDVRlogan_V02,
                        'TRSUVR18_V01':TRSUVR18_V01,
                        'TRSUVR18_V02': TRSUVR18_V02,
                        'TRSUVR0120_V01':TRSUVR0120_V01,
                        'TRSUVR0120_V02':TRSUVR0120_V02})
