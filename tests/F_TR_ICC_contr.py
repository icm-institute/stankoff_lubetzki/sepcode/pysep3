#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  8 15:21:16 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from lmfit import Model


def groupedAvg(myArray, N=16):
    result = np.cumsum(myArray, 0)[N - 1::N] / float(N)
    result[1:] = result[1:] - result[:-1]
    return result


def sigmoid(x, L, x0, k, b):
    y = L / (1 + np.exp(-k * (np.log(x) - x0))) + b
    return y


def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w


# =============================================================================
# =============================================================================
# # PLOTTING THE TRVS
# =============================================================================
# =============================================================================
CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/inflasep_lesions_contrlat.csv'
df = pd.read_csv(CLP)
df = df.sort_values("Lesion Volume")
X = df["Lesion Volume"].to_numpy()
print(len(X))
Xm = groupedAvg(X)
Xu, indexes, counts = np.unique(Xm, return_inverse=True, return_counts=True)


def fit_sigmoid(df, Xu, column):
    Y = df[column].to_numpy()
    Ym = groupedAvg(Y)
    Yu = np.zeros(Xu.shape)
    for i in range(len(Xu)):
        Yu[i] = np.mean(Ym[np.where(indexes == i)])
    p0 = [max(Yu), np.median(Yu), 1, min(Yu)]
    ps, res = curve_fit(sigmoid, Xu, Yu, p0)
    sigmodel = Model(sigmoid)
    params = sigmodel.make_params(L=ps[0], x0=ps[1], k=ps[2], b=ps[3])
    results = sigmodel.fit(Yu, params, x=Xu)
    try:
        threshold = Xu[np.where(results.best_fit > 0.1)[-1][-1]]
    except IndexError:
        threshold = -10
    return Yu, results, threshold


# =============================================================================
# Fitting sigmoid models on the TRV(lesions on controls)
# =============================================================================
Yu1, results1, threshold1 = fit_sigmoid(df, Xu, "TRR1")
Yu2, results2, threshold2 = fit_sigmoid(df, Xu, "TRSUVR0120")
Yu3, results3, threshold3 = fit_sigmoid(df, Xu, "TRSUVR18")
# =============================================================================
# Plotting Figure
# =============================================================================
plt.close()
plt.rcParams.update({'font.size': 25})
fig, ax = plt.subplots(2, 2, sharex='col', sharey='row')
# Top Figure
a = ax[0, 0]
a.set_ylabel('Test Retest Variability [%]')
a.set_xscale('log')
a.set_xlim(Xu[0], Xu[-1])
a.set_ylim(0, np.max(Yu2) * 100)
a.scatter(Xu, Yu1 * 100, color='r', alpha=0.2)
a.scatter(Xu, Yu2 * 100, color='g', alpha=0.2)
a.scatter(Xu, Yu3 * 100, color='b', alpha=0.2)
a.plot(Xu, results1.best_fit * 100, 'r', label='SRTM2 R1')
a.plot(Xu, results2.best_fit * 100, 'g', label='SUVR (0-2m)')
a.plot(Xu, results3.best_fit * 100, 'b', label='SUVR (1-8m)')
a.grid()
a.legend(loc=1)
# =============================================================================
# Fitting sigmoid models on the TRV(lesions on controls)
# =============================================================================
Yu1, results1, threshold1 = fit_sigmoid(df, Xu, "TRDVRSRTM2")
Yu2, results2, threshold2 = fit_sigmoid(df, Xu, "TRDVRlogan")
Yu3, results3, threshold3 = fit_sigmoid(df, Xu, "TRSUVR5070")
# =============================================================================
# Plotting Figure
# =============================================================================
# Top Figure
a = ax[0, 1]
a.set_xscale('log')
a.set_xlim(Xu[0], Xu[-1])
a.set_ylim(0, np.max(Yu3) * 100)
a.scatter(Xu, Yu1 * 100, color='r', alpha=0.2)
a.scatter(Xu, Yu2 * 100, color='g', alpha=0.2)
a.scatter(Xu, Yu3 * 100, color='b', alpha=0.2)
a.plot(Xu, results1.best_fit * 100, 'r', label='DVR (SRTM2)')
a.plot(Xu, results2.best_fit * 100, 'g', label='DVR (LOGAN)')
a.plot(Xu, results3.best_fit * 100, 'b', label='SUVR (50-70m)')
a.grid()
a.legend(loc=1)

# =============================================================================
# =============================================================================
# # PLOTTING THE ICCS
# =============================================================================
# =============================================================================
CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/inflasep_lesions_contrlat_icc.csv'
df = pd.read_csv(CLP)
X = df[["Lesion Volume"]].to_numpy()
Xu, indexes, counts = np.unique(X, return_inverse=True, return_counts=True)


def fit_sigmoid(df, Xu, column):
    Y = df[column].to_numpy()
    Yu = np.zeros(Xu.shape)
    for i in range(len(Xu)):
        Yu[i] = np.mean(Y[np.where(indexes == i)])
    p0 = [max(Yu), np.median(Yu), 1, min(Yu)]
    ps, res = curve_fit(sigmoid, Xu, Yu, p0)
    sigmodel = Model(sigmoid)
    params = sigmodel.make_params(L=ps[0], x0=ps[1], k=ps[2], b=ps[3])
    results = sigmodel.fit(Yu, params, x=Xu)
    try:
        threshold = Xu[np.where(results.best_fit > 0.8)[0][0]]
    except IndexError:
        threshold = -10
    return Yu, results, threshold


# =============================================================================
# # Fitting sigmoid models on the ICC(lesions on controls)
# =============================================================================
Yu1, results1, threshold1 = fit_sigmoid(df, Xu, "ICC R1")
Yu2, results2, threshold2 = fit_sigmoid(df, Xu, "ICC SUVR0to120s")
Yu3, results3, threshold3 = fit_sigmoid(df, Xu, "ICC SUVR1to8m")
# =============================================================================
# Plotting Figure
# =============================================================================
# Top Figure
a = ax[1, 0]
a.set_xlabel('Lesion Volume [$mm^3$]')
a.set_ylabel('ICC')
a.set_xscale('log')
a.set_xlim(Xu[0], Xu[-1])
a.set_ylim(-1, 1)
a.scatter(Xu, Yu1, color='r', alpha=0.2)
a.scatter(Xu, Yu2, color='g', alpha=0.2)
a.scatter(Xu, Yu3, color='b', alpha=0.2)
a.plot(Xu, results1.best_fit, 'r', label='SRTM2 R1')
a.plot(Xu, results2.best_fit, 'g', label='SUVR (0-2m)')
a.plot(Xu, results3.best_fit, 'b', label='SUVR (1-8m)')
a.grid()
a.legend(loc=3)
# =============================================================================
# # Fitting sigmoid models on the ICC(lesions on controls)
# =============================================================================
Yu1, results1, threshold1 = fit_sigmoid(df, Xu, "ICC DVR (SRTM2)")
Yu2, results2, threshold2 = fit_sigmoid(df, Xu, "ICC DVR (logan)")
Yu3, results3, threshold3 = fit_sigmoid(df, Xu, "ICC SUVR50to70m")
# =============================================================================
# Plotting Figure
# =============================================================================
# Bottom Figure
a = ax[1, 1]
a.set_xlabel('Lesion Volume [$mm^3$]')
a.set_xscale('log')
a.set_xlim(Xu[0], Xu[-1])
a.set_ylim(-1, 1)
a.scatter(Xu, Yu1, color='r', alpha=0.2)
a.scatter(Xu, Yu2, color='g', alpha=0.2)
a.scatter(Xu, Yu3, color='b', alpha=0.2)
a.plot(Xu, results1.best_fit, 'r', label='DVR (SRTM2)')
a.plot(Xu, results2.best_fit, 'g', label='DVR (LOGAN)')
a.plot(Xu, results3.best_fit, 'b', label='SUVR (50-70m)')
a.grid()
a.legend(loc=3)
