#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 15:17:12 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/rois_smooth.csv'
df = pd.read_csv(CLP)

plt.style.use('default')

df_pair_V01 = df[['ROI Name', 'mR1V01', 'DVRV01', 'mloganV01','mSUVR0to120V01', 'mSUVR1to8V01', 'mSUVR50to70V01']].rename(columns={'mR1V01': 'mR1', 'DVRV01': 'DVR', 'mloganV01': 'mlogan', 'mSUVR0to120V01': 'mSUVR0to120', 'mSUVR1to8V01': 'mSUVR1to8', 'mSUVR50to70V01':'mSUVR50to70'})
df_pair_V02 = df[['ROI Name', 'mR1V02', 'DVRV02', 'mloganV02', 'mSUVR0to120V02', 'mSUVR1to8V02', 'mSUVR50to70V02']].rename(columns={'mR1V02': 'mR1', 'DVRV02': 'DVR', 'mloganV02': 'mlogan', 'mSUVR0to120V02': 'mSUVR0to120', 'mSUVR1to8V02': 'mSUVR1to8', 'mSUVR50to70V02':'mSUVR50to70'})

df_pair_V01['Visit'] = "V01"
df_pair_V02['Visit'] = "V02"

frames = [df_pair_V01, df_pair_V02]
df_pair = pd.concat(frames)

# =============================================================================
# Plotting the figure
# =============================================================================
fig, ax = plt.subplots(3, 2, sharex=False, sharey='row')
sns.boxplot(ax=ax[0, 0], y=df_pair["ROI Name"], x=df_pair["mR1"],
            hue=df_pair["Visit"], orient="h", fliersize=2)
sns.boxplot(ax=ax[1, 0], y=df_pair["ROI Name"], x=df_pair["mSUVR1to8"],
            hue=df_pair["Visit"], orient="h", fliersize=2)
sns.boxplot(ax=ax[2, 0], y=df_pair["ROI Name"], x=df_pair["mSUVR0to120"],
            hue=df_pair["Visit"], orient="h", fliersize=2)
sns.boxplot(ax=ax[0, 1], y=df_pair["ROI Name"], x=df_pair["DVR"],
            hue=df_pair["Visit"], orient="h", fliersize=2)
sns.boxplot(ax=ax[1, 1], y=df_pair["ROI Name"], x=df_pair["mlogan"],
            hue=df_pair["Visit"], orient="h", fliersize=2)
sns.boxplot(ax=ax[2, 1], y=df_pair["ROI Name"], x=df_pair["mSUVR50to70"],
            hue=df_pair["Visit"], orient="h", fliersize=2)
labels = ["WM",
          "Cerebellar GM",
          "Cerebellar WM",
          "Frontal Cortex",
          "Insula",
          "Limbic Cortex",
          "Occipital Cortex",
          "Parietal Cortex",
          "Temporal Cortex",
          "Striatum",
          "Thalamus",
          "Cingulate WM",
          "Frontal WM",
          "Insular WM",
          "Occipital WM",
          "Parietal WM",
          "Temporal WM"]

plot_titles = ["R1", "SRTM2 DVR",
               "SUVR1-8", "Logan DVR",
               "SUVR0-2", "SUVR50-70"]

for i, a in enumerate(ax.flatten()):
    a.tick_params(axis='both', which='both', length=0)
    a.set_xlabel('')
    a.set_yticklabels(labels)
    a.set_ylabel('')
    a.set_title(plot_titles[i])
    a.xaxis.grid(True, linestyle='--')
    a.tick_params(axis='x', labelrotation=0)
