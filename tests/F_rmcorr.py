#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  8 10:25:02 2021

@author: arya.yazdan-panah
"""


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import pearsonr
from scipy.optimize import curve_fit
from lmfit import Model
from pprint import pprint
# =============================================================================
# USEFULL FUNCTIONS
# =============================================================================


def groupedAvg(myArray, N=8):
    result = np.cumsum(myArray, 0)[N - 1::N] / float(N)
    result[1:] = result[1:] - result[:-1]
    return result


def groupedSTD(myArray, N=8):
    result = np.zeros(len(myArray) // N)
    for i in range(len(myArray) // N):
        result[i] = np.std(myArray[i * N: (i+1)*N - 1 ])
    return result


def groupedPearson(A1, A2, N=8):
    out_len = int(len(A1) / N)
    out_coeffs = np.zeros(out_len)
    for i in range(out_len):
        A = np.array(A1[N * i: N * i + N])
        B = np.array(A2[N * i: N * i + N])
        out_coeffs[i] = pearsonr(A, B)[0]
    return out_coeffs


def sigmoid(x, L, x0, k, b):
    y = L / (1 + np.exp(-k*(np.log(x)-x0)))+b
    return y


# =============================================================================
# AT the ROI LEVEL FIRST
# =============================================================================

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/rois.csv'
df = pd.read_csv(CLP)

dfV01 = df[['ROI Name', 'mR1V01', 'DVRV01', 'mloganV01', 'mSUVR0to120V01',
            'mSUVR1to8V01',
            'mSUVR50to70V01']].rename(columns={'mR1V01': 'R1',
                                               'mSUVR0to120V01': 'SUVR02',
                                               'mSUVR1to8V01': 'SUVR18',
                                               'DVRV01': 'DVR',
                                               'mloganV01': 'LOG',
                                               'mSUVR50to70V01': 'SUVR4090'})
dfV02 = df[['ROI Name', 'mR1V02', 'DVRV02', 'mloganV02', 'mSUVR0to120V02',
            'mSUVR1to8V02',
            'mSUVR50to70V02']].rename(columns={'mR1V02': 'R1',
                                               'mSUVR0to120V02': 'SUVR02',
                                               'mSUVR1to8V02': 'SUVR18',
                                               'DVRV02': 'DVR',
                                               'mloganV02': 'LOG',
                                               'mSUVR50to70V02': 'SUVR4090'})
m = 'SUVR4090'
mean = groupedAvg(dfV01[m].to_numpy(), N=8)
std = groupedSTD(dfV01[m].to_numpy(), N=8)
mean2 = groupedAvg(dfV02[m].to_numpy(), N=8)
std2 = groupedSTD(dfV02[m].to_numpy(), N=8)
for i, j in enumerate(mean):
    # print('{0:.6f}'.format(i))
    print(j, std[i], mean2[i], std2[i])

# %% TRV print the means fo tables
CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/rois.csv'
df = pd.read_csv(CLP)

m = 'TRSUVR50to70'
mean = groupedAvg(df[m].to_numpy(), N=8)*100
std = groupedSTD(df[m].to_numpy(), N=8)*100
for m, s in zip(mean, std):
    print('{0:.2f}'.format(m), "±", '{0:.2f}'.format(s))

# %% Metrics correlations
import pandas as pd
import pingouin as pg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import pearsonr
from scipy.optimize import curve_fit
from lmfit import Model
from pprint import pprint

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/rois.csv'

df = pd.read_csv(CLP)
dfV01 = df[['Subject', 'ROI Name', 'mR1V01', 'DVRV01', 'mloganV01', 'mSUVR0to120V01',
            'mSUVR1to8V01',
            'mSUVR50to70V01']].rename(columns={'mR1V01': 'R1',
                                               'mSUVR0to120V01': 'SUVR02',
                                               'mSUVR1to8V01': 'SUVR18',
                                               'DVRV01': 'DVR',
                                               'mloganV01': 'LOG',
                                               'mSUVR50to70V01': 'SUVR50to70'})
dfV02 = df[['Subject', 'ROI Name', 'mR1V02', 'DVRV02', 'mloganV02', 'mSUVR0to120V02',
            'mSUVR1to8V02',
            'mSUVR50to70V02']].rename(columns={'mR1V02': 'R1',
                                               'mSUVR0to120V02': 'SUVR02',
                                               'mSUVR1to8V02': 'SUVR18',
                                               'DVRV02': 'DVR',
                                               'mloganV02': 'LOG',
                                               'mSUVR50to70V02': 'SUVR50to70'})
V01V02 = [dfV01, dfV02]
df_pair = pd.concat(V01V02)
A = []

for roi in ["WM","cbgm","cbwm","lobes_frontal","lobes_insula","lobes_limbic",
            "lobes_occipital","lobes_parietal","lobes_temporal","striatum",
            "thalamus","wm_lobes_cingulate","wm_lobes_frontal",
            "wm_lobes_insula","wm_lobes_occipital","wm_lobes_parietal",
            "wm_lobes_temporal"]:
    A.append(pg.rm_corr(data=df_pair.loc[df['ROI Name'] == roi], x='R1',
                        y='SUVR50to70', subject='Subject'))
A = pd.concat(A)
# %%
# =============================================================================
# LOAD DATA
# =============================================================================
CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/inflasep_lesions.csv'
df = pd.read_csv(CLP)

dfV01 = df[['Lesion Volume', 'mR1V01', 'DVRV01', 'mloganV01', 'mSUVR0to120V01',
            'mSUVR1to8V01',
            'mSUVR50to70V01']].rename(columns={'mR1V01': 'R1',
                                               'mSUVR0to120V01': 'SUVR02',
                                               'mSUVR1to8V01': 'SUVR18',
                                               'DVRV01': 'DVR',
                                               'mloganV01': 'LOG',
                                               'mSUVR50to70V01': 'SUVR4090'})
dfV02 = df[['Lesion Volume', 'mR1V02', 'DVRV02', 'mloganV02', 'mSUVR0to120V02',
            'mSUVR1to8V02',
            'mSUVR50to70V02']].rename(columns={'mR1V02': 'R1',
                                               'mSUVR0to120V02': 'SUVR02',
                                               'mSUVR1to8V02': 'SUVR18',
                                               'DVRV02': 'DVR',
                                               'mloganV02': 'LOG',
                                               'mSUVR50to70V02': 'SUVR4090'})

X = df["Lesion Volume"].to_numpy()
Xp = groupedAvg(X)
corr_V01_R1_SUVR18 = groupedPearson(dfV01["R1"].to_numpy(),
                                    dfV01["SUVR18"].to_numpy(), N=8)
corr_V01_R1_SUVR02 = groupedPearson(dfV01["R1"].to_numpy(),
                                    dfV01["SUVR02"].to_numpy(), N=8)
corr_V01_SUVR02_SUVR18 = groupedPearson(dfV01["SUVR02"].to_numpy(),
                                        dfV01["SUVR18"].to_numpy(), N=8)

# plt.scatter(Xu, corr_V01_R1_SUVR18)


def rescale_linear(array, new_min, new_max):
    """Rescale an arrary linearly."""
    minimum, maximum = np.min(array), np.max(array)
    m = (new_max - new_min) / (maximum - minimum)
    b = new_min - m * minimum
    return m * array + b


def fit_sigmoid(Xp, Yp):
    Xu, indexes, cnts = np.unique(Xp, return_inverse=True, return_counts=True)
    Yu = np.zeros(Xu.shape)
    for i in range(len(Xu)):
        Yu[i] = np.mean(Yp[np.where(indexes == i)])
    p0 = [max(Yu), np.median(Yu), 1, min(Yu)]
    ps, res = curve_fit(sigmoid, Xu, Yu, p0)
    sigmodel = Model(sigmoid)
    params = sigmodel.make_params(L=ps[0], x0=ps[1], k=ps[2], b=ps[3])
    # params = sigmodel.make_params(L=max(Yu), x0=np.median(Yu), k=1, b=min(Yu))
    rXu = rescale_linear(Xu, np.min(cnts), np.max(cnts))
    weights = np.multiply(cnts, rXu)
# =============================================================================
#     weights = np.square(np.linspace(-100, 1000, len(Xu)))
# =============================================================================
# =============================================================================
#     weights = np.zeros(len(Xu))
#     weights[0] = 1
#     weights[-1] = 1
# =============================================================================
    # plt.plot(weights)
    results = sigmodel.fit(Yu, params, x=Xu, weights=weights)
    try:
        threshold = Xu[np.where(results.best_fit >= 0.8)[0][0]]
    except IndexError:
        threshold = -10
    return Xu, Yu, results, threshold


Xu, Yu1, results1, threshold1 = fit_sigmoid(Xp, corr_V01_R1_SUVR18)
Xu, Yu2, results2, threshold2 = fit_sigmoid(Xp, corr_V01_R1_SUVR02)
Xu, Yu3, results3, threshold3 = fit_sigmoid(Xp, corr_V01_SUVR02_SUVR18)

plt.close()
plt.style.use('default')
fig, ax = plt.subplots(2, 1, sharex=True, sharey=True)
ax[0].set_xscale('log')
ax[0].set_xlim(Xu[0], Xu[-1])
ax[0].set_ylim(-1, 1)
ax[0].set_ylabel('Pearson R')
ax[1].set_ylabel('Pearson R')
ax[1].set_xlabel('Lesion Volume [#voxels]')
ax[0].scatter(Xu, Yu1, c='r', alpha=0.2, label='Average Pearson(R1, SUVR18)')
ax[0].scatter(Xu, Yu2, c='g', alpha=0.2, label='Average Pearson(R1, SUVR02)')
# ax[0].scatter(Xu, Yu3, c='b', alpha=0.2, label='Average Pearson(LOG, SUVR)')
ax[0].plot(Xu, results1.best_fit, c='r', label='Fit Pearson(R1, SUVR18)')
ax[0].plot(Xu, results2.best_fit, c='g', label='Fit Pearson(R1, SUVR18)')
# ax[0].plot(Xu, results3.best_fit, c='b', label='Fit Pearson(LOG, SUVR)')

ax[0].legend()
ax[0].set_title('Pearson R between CBF proxies')
# ax[0].axhline(0.8, Xu[0], Xu[-1], label='R = 0.8')
# ax[0].axvline(threshold1, ymax=0.9, c='r')


corr_V01_DVR_LOG = groupedPearson(dfV01["DVR"].to_numpy(), dfV01["LOG"].to_numpy(), N=8)
corr_V01_DVR_SUVR4090 = groupedPearson(dfV01["DVR"].to_numpy(), dfV01["SUVR4090"].to_numpy(), N=8)
corr_V01_LOG_SUVR4090 = groupedPearson(dfV01["LOG"].to_numpy(), dfV01["SUVR4090"].to_numpy(), N=8)

Xu, Yu4, results4, threshold4 = fit_sigmoid(Xp, corr_V01_DVR_LOG)
Xu, Yu5, results5, threshold5 = fit_sigmoid(Xp, corr_V01_DVR_SUVR4090)
Xu, Yu6, results6, threshold6 = fit_sigmoid(Xp, corr_V01_LOG_SUVR4090)

ax[1].scatter(Xu, Yu4, c='r', alpha=0.2, label='Average Pearson(SRTM2, LOG)')
ax[1].scatter(Xu, Yu5, c='g', alpha=0.2, label='Average Pearson(SRTM2, SUVR)')
ax[1].scatter(Xu, Yu6, c='b', alpha=0.2, label='Average Pearson(LOG, SUVR)')
ax[1].plot(Xu, results4.best_fit, c='r', label='Fit Pearson(SRTM2, LOG)')
ax[1].plot(Xu, results5.best_fit, c='g', label='Fit Pearson(SRTM2, SUVR)')
ax[1].plot(Xu, results6.best_fit, c='b', label='Fit Pearson(LOG, SUVR)')
ax[1].legend()
ax[1].set_title('Pearson R between DVRs proxies')
ax[1].axhline(0.8, Xu[0], Xu[-1], label='R = 0.8')
ax[1].axvline(threshold4, ymax=0.9, c='r')
ax[1].axvline(threshold5, ymax=0.9, c='g')
ax[1].axvline(threshold6, ymax=0.9, c='b')

# V01V02 = [dfV01, dfV02]
# df_pair = pd.concat(V01V02)
# X = df_pair["Lesion Volume"].to_numpy()
# Xu, indexes, counts = np.unique(X, return_inverse=True, return_counts=True)
# Xm = groupedAvg(X)


# Xu, indexes, counts = np.unique(Xm, return_inverse=True, return_counts=True)

# plt.figure(1)
# plt.clf()
# plt.scatter(Xu, Yu, label='Pearson Corr Coeff for each ROI')
# plt.xscale('log')
# plt.plot(Xu, results.best_fit, 'r', label='Fit')
# plt.legend(loc=4)
# plt.title('Evolution of the Pearson R correlation coeff with\nincreasing leasion size between R1 and SUVR 1 to 8 min')
# plt.hlines(0.8, Xu[0] - 1, Xu[-1] + 1, label='R = 0.8')
# plt.vlines(threshold, -1, 0.8)
# plt.xlim(Xu[0], Xu[-1])
# plt.ylim(bottom=min(Yu))
# plt.xlabel('Lesion Volume [#voxels]')
# plt.ylabel('Pearson R')

# =============================================================================
# USELESS STUFF
# =============================================================================
# =============================================================================
# Y1 = output_shape
# Yu1 = np.zeros(Xu.shape)
# for i in range(len(Xu)):
#     Yu1[i] = np.mean(Y1[np.where(indexes == i)])
# ps, res = curve_fit(sigmoid, Xu, Yu1)
# sigmodel = Model(exp_func)
# params = sigmodel.make_params(k=ps[0])
# results1 = sigmodel.fit(Yu1, params, x=Xu)
# threshold1 = Xu[np.where(results1.best_fit > 0.8)[0][0]]
# =============================================================================
# from scipy.stats import zprob
# def z_transform(r, n=8):
#     z = np.log((1 + r) / (1 - r)) * (np.sqrt(n - 3) / 2)
#     p = zprob(-z)
#     return p
# def exp_func(x, k):
#     y = 1 - np.exp(-k*(np.log(x)))
#     return y
# corr = df.corr()
# corr.style.background_gradient(cmap='coolwarm')