#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  9 16:44:47 2020

@author: arya.yazdan-panah
"""

import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from scipy.optimize import curve_fit
from scipy import stats
from lmfit import Model

# =============================================================================
# ICC = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da'
# ICC = ICC + 'ta/process/TR_MNI/stats/09_12_2020_ICC.csv'
# df = pd.read_csv(ICC)
#
#
# def plot_ICC(df, name, metric='mean(ICC)'):
#     df_ICC = df.loc[df['Image'] == name]
#     rois = ['brain', 'thalamus', 'striatum', 'cortex', 'cerebellum (ALL)',
#             'cerebellum (GM)', 'cerebellum (WM)', 'cerebro-spinal fluid',
#             'white matter']
#     colors = plt.cm.rainbow(np.linspace(0, 1, 9))
#     plt.figure()
#     for roi, c in zip(rois, colors):
#         df_ICC_roi = df_ICC.loc[df_ICC['ROI'] == roi]
#         plt.plot('Lesion volume', metric, data=df_ICC_roi, c=c,
#                  marker='o')
#     plt.title('Evolution of the ICC of the %s according to lesion size and ROI'
#               % name)
#     plt.legend(rois)
#     plt.xlim(0, 10)
#     plt.ylim(-0.1, 1)
#     plt.axhline(0, color='green')
#
# plot_ICC(df, 'R1 (SRTM2)')
# plot_ICC(df, 'DVR (SRTM2)')
# plot_ICC(df, 'k2 (SRTM2)')
# plot_ICC(df, 'DVR (Logan)')
# =============================================================================


# TRV = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da'
# TRV = TRV + 'ta/process/TR_MNI/stats/09_12_2020_TRV.csv'
# df = pd.read_csv(TRV)
# hue_order = ['striatum',
#               'thalamus',
#               'cortex',
#               'cerebellum (GM)',
#               'cerebellum (ALL)',
#               'brain',
#               'cerebellum (WM)',
#               'white matter',
#               'cerebro-spinal fluid']
# # hue_order = ['thalamus', 'white matter', 'brain', 'cerebellum (WM)',
# #              'striatum', 'cortex', 'cerebellum (ALL)', 'cerebellum (GM)',
# #              'cerebro-spinal fluid']
# def plot_TRV(df, name, metric='sd(RD)', horder=hue_order):
#     df_TRV = df.loc[df['Image'] == name]
#     plt.figure()
#     sns.boxplot(x="Lesion volume", y=metric, data=df_TRV, hue='ROI',
#                 hue_order=horder)
#     plt.title('%s of the %s according to lesion size and ROI'
#               % (metric, name))
#     plt.grid()
#     plt.ylim(ymin=0, ymax=0.3)
#     plt.grid
# plt.close()
# plot_TRV(df, 'R1 (SRTM2)', 'mean(RD)')
# plot_TRV(df, 'DVR (SRTM2)', 'median(RD)')
# plot_TRV(df, 'k2 (SRTM2)')
# plot_TRV(df, 'DVR (Logan)', 'mean(RD)')
# plot_TRV(df, 'DVR post-conv (SRTM2)', 'median(RD)')

# =============================================================================
# PAT = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da'
# PAT = PAT + 'ta/process/TR_MNI/stats/patients.csv'
# df = pd.read_csv(PAT)
# # Remove V02 of patients
# df = df.loc[~(df.Subject.str.startswith('P') & (df.Visit == 'V02'))]
#
# def plot_annals(df, name):
#     plt.figure()
#     sns.boxplot(x="ROI", y=name, data=df, order=['WM HC',
#                                                  'NAWM',
#                                                  'PERI',
#                                                  'LESIONS',
#                                                  'BLACKHOLES',
#                                                  'GADO'], hue='Visit')
#     # plt.ylim(ymin=0.6, ymax=2.5)
#     plt.title('%s according to ROI' % name)
#
# plt.close()
# plot_annals(df, 'mR1')  # effet des ventricules trop visibles dans R1 WM HC
# plot_annals(df, 'mDVR')
# plot_annals(df, 'mlogan')
# # blackhole - gad
#
# A = df[(df.ROI == 'WM HC') & (df.Visit == 'V01')].mean()
# B = df[(df.ROI == 'WM HC') & (df.Visit == 'V01')].std()
# CV = B / A
# print('WM HC:')
# print(CV)
# A = df[(df.ROI == 'NAWM') & (df.Visit == 'V01')].mean()
# B = df[(df.ROI == 'NAWM') & (df.Visit == 'V01')].std()
# CV = B / A
# print('NAWM:')
# print(CV)
# A = df[(df.ROI == 'PERI') & (df.Visit == 'V01')].mean()
# B = df[(df.ROI == 'PERI') & (df.Visit == 'V01')].std()
# CV = B / A
# print('PERI:')
# print(CV)
# A = df[(df.ROI == 'BLACKHOLES') & (df.Visit == 'V01')].mean()
# B = df[(df.ROI == 'BLACKHOLES') & (df.Visit == 'V01')].std()
# CV = B / A
# print('BLACKHOLES:')
# print(CV)
# A = df[(df.ROI == 'LESIONS') & (df.Visit == 'V01')].mean()
# B = df[(df.ROI == 'LESIONS') & (df.Visit == 'V01')].std()
# CV = B / A
# print('LESIONS:')
# print(CV)
# A = df[(df.ROI == 'BLACKHOLES') & (df.Visit == 'V01')].mean()
# B = df[(df.ROI == 'BLACKHOLES') & (df.Visit == 'V01')].std()
# CV = B / A
# print('BLACKHOLES:')
# print(CV)
# A = df[(df.ROI == 'GADO') & (df.Visit == 'V01')].mean()
# B = df[(df.ROI == 'GADO') & (df.Visit == 'V01')].std()
# CV = B / A
# print('GADO:')
# print(CV)
# =============================================================================


# CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da'
# CLP = CLP + 'ta/process/TR_MNI/stats/controls_les_patients_no_wm_ero.csv'
# df = pd.read_csv(CLP)
# df1 = df.dropna()
# def plot_les_controls(df, name):
#     plt.figure()
#     sns.scatterplot(x="Lesion Volume", y=name, data=df, hue='Subject')
#     plt.title('%s according to Lesion Volume' % name)
#     plt.ylim(ymin=0, ymax=0.5)
#     plt.xscale('log')
# plt.close()
# # plot_les_controls(df, 'TRDVRSRTM2bm')
# # plot_les_controls(df, 'TRDVRSRTM2am')

# X = df1["Lesion Volume"].to_numpy(float)
# Y = df1["TRR1"].to_numpy(float)
# # plot_les_controls(df, 'TRR1')
# reg = LinearRegression()
# reg.fit(np.log(df1[["Lesion Volume"]]), df1["TRR1"])
# # sns.regplot(np.log(df1[["Lesion Volume"]]), df1["TRR1"])
# # slope, intercept, r_value, p_value, std_err = stats.linregress(X, np.log(Y))
# # # reg.score(X, Y)
# print(reg.score(np.log(df1[["Lesion Volume"]]), df1["TRR1"]))
# print(reg.coef_)
# print(reg.predict(np.array([[50]])))

# %% Fitting 3 models on the TRV(lesions on controls)

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da'
CLP = CLP + 'ta/process/TR_new_MNI/stats/inflasep_lesions.csv'
df = pd.read_csv(CLP)


def plot_les_controls(df, name):
    plt.figure()
    sns.scatterplot(x="Lesion Volume", y=name, data=df, hue='Subject')
    plt.title('%s according to Lesion Volume' % name)
    plt.ylim(ymin=0, ymax=0.5)
    plt.xlim(df[["Lesion Volume"]].to_numpy().min(),
             df[["Lesion Volume"]].to_numpy().max())
    plt.xscale('log')


# Polynomial Regression
def polyfit(x, y, degree):
    results = {}
    coeffs = np.polyfit(x, y, degree)
    # Polynomial Coefficients
    results['polynomial'] = coeffs.tolist()
    correlation = np.corrcoef(x, y)[0, 1]
    # r
    results['correlation'] = correlation
    # r-squared
    results['determination'] = correlation**2
    return results


Y = df[["TRR1"]].to_numpy()
X = df[["Lesion Volume"]].to_numpy()


def groupedAvg(myArray, N=8):
    result = np.cumsum(myArray, 0)[N-1::N]/float(N)
    result[1:] = result[1:] - result[:-1]
    return result


Xm = groupedAvg(X)
Ym = groupedAvg(Y)
plt.clf()
plt.scatter(Xm, Ym, color='silver', alpha=0.3, label='TRV of a lesion averaged on all subjects')
plt.xscale('log')
Xu, indexes, counts = np.unique(Xm, return_inverse=True, return_counts=True)
Yu = np.zeros(Xu.shape)
for i in range(len(Xu)):
    Yu[i] = np.mean(Ym[np.where(indexes == i)])

ordonnee_origine = np.mean(Yu[:5])
ordonnee_inf = np.mean(Yu[-5:])


def func2exp(X, b, c, alpha, beta):
    return 0.02185 + b * np.exp(-alpha*X) + c * np.exp(-beta*X)


p2, res = curve_fit(func2exp, Xu, Yu)
exp2model = Model(func2exp)
print("First Model with 2 exponential")
print('parameter names: {}'.format(exp2model.param_names))
print('independent variables: {}'.format(exp2model.independent_vars))
params = exp2model.make_params(b=p2[0], c=p2[1], alpha=p2[2], beta=p2[3])
result2 = exp2model.fit(Yu, params, X=Xu, )
print(result2.fit_report())


def func1exp(X, b, alpha):
    return 0.02185 + b * np.exp(-alpha*X)


p1, res = curve_fit(func1exp, Xu, Yu)
exp1model = Model(func1exp)
print()
print("Second Model with only 1 exponential")
print('parameter names: {}'.format(exp1model.param_names))
print('independent variables: {}'.format(exp1model.independent_vars))
params = exp1model.make_params(b=p1[0], alpha=p1[1])
result1 = exp1model.fit(Yu, params, X=Xu, )
print(result1.fit_report())


def sigmoid(x, L, x0, k, b):
    y = L / (1 + np.exp(-k*(x-x0)))+b
    return (y)


p0 = [max(Yu), np.median(Yu), 1, min(Yu)]
ps, res = curve_fit(sigmoid, np.log(Xu), Yu, p0)
sigmodel = Model(sigmoid)
print()
print("Second Model with only 1 exponential")
print('parameter names: {}'.format(sigmodel.param_names))
print('independent variables: {}'.format(sigmodel.independent_vars))
params = sigmodel.make_params(L=ps[0], x0=ps[1], k=ps[2], b=ps[3])
results = sigmodel.fit(Yu, params, x=np.log(Xu))
print(results.fit_report())

plt.scatter(Xu, Yu, color='gray', label='Average TRV of all lesions of the same size')
plt.plot(Xu, result2.best_fit, 'r', label='2 exponential Fit [AIC=%d]' % result2.aic, linewidth=2)
plt.plot(Xu, result1.best_fit, 'b', label='1 exponential Fit [AIC=%d]' % result1.aic, linewidth=2)
plt.plot(Xu, results.best_fit, 'g', label='Sigmoid Fit [AIC=%d]' % results.aic, linewidth=2)
plt.legend(loc='best')
plt.xscale('log')
plt.xlabel('Lesion Volume (#voxels)')
plt.ylabel('Test Retest Variability (%)')
plt.title('Choice of Model for Test Retest Variability as a Function of Lesion Volume')
plt.show()
