#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 15:17:12 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.optimize import curve_fit
from lmfit import Model


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/rois.csv'
df = pd.read_csv(CLP)

plt.close()
plt.style.use('dark_background')
fig, axs = plt.subplots(6,1, sharex=True, sharey=False)
sns.boxplot(ax=axs[0], x=df["ROI Name"], y=df["TRR1"]*100)
axs[0].set_ylabel('R1 SRTM2 TR')
axs[0].set_xlabel('')
sns.boxplot(ax=axs[1], x=df["ROI Name"], y=df["TRSUVR0to120"]*100)
axs[1].set_ylabel('0-120s SUVR TR')
axs[1].set_xlabel('')
sns.boxplot(ax=axs[2], x=df["ROI Name"], y=df["TRSUVR1to8"]*100)
axs[2].set_ylabel('1-8m SUVR TR')
axs[2].set_xlabel('')
sns.boxplot(ax=axs[3], x=df["ROI Name"], y=df["TRDVRSRTM2"]*100)
axs[3].set_ylabel('DVR SRTM2 TR')
axs[3].set_xlabel('')
sns.boxplot(ax=axs[4], x=df["ROI Name"], y=df["TRDVRlogan"]*100)
axs[4].set_ylabel('DVR Logan TR')
axs[4].set_xlabel('Region of interest')
sns.boxplot(ax=axs[5], x=df["ROI Name"], y=df["TRSUVR50to70"]*100)
axs[5].set_ylabel('50-70m SUVR')
axs[5].set_xlabel('')
plt.suptitle('ROI dependant Test-ReTest Variability [%] comparison')
