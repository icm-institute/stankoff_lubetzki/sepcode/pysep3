#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 17 17:46:01 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from lmfit import Model


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/inflasep_lesions.csv'
df = pd.read_csv(CLP)
df = df.sort_values("Lesion Volume")


def groupedAvg(myArray, N=8):
    result = np.cumsum(myArray, 0)[N - 1::N] / float(N)
    result[1:] = result[1:] - result[:-1]
    return result


def sigmoid(x, L, x0, k, b):
    y = L / (1 + np.exp(-k * (np.log(x) - x0))) + b
    return y


X = df["Lesion Volume"].to_numpy()
Xm = groupedAvg(X)
Xu, indexes, counts = np.unique(Xm, return_inverse=True, return_counts=True)


def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w


def fit_sigmoid(df, Xu, column):
    Y = df[column].to_numpy()
    Ym = groupedAvg(Y)
    Yu = np.zeros(Xu.shape)
    for i in range(len(Xu)):
        Yu[i] = np.mean(Ym[np.where(indexes == i)])
    p0 = [max(Yu), np.median(Yu), 1, min(Yu)]
    ps, res = curve_fit(sigmoid, Xu, Yu, p0)
    sigmodel = Model(sigmoid)
    params = sigmodel.make_params(L=ps[0], x0=ps[1], k=ps[2], b=ps[3])
    results = sigmodel.fit(Yu, params, x=Xu)
    try:
        threshold = Xu[np.where(results.best_fit > 0.1)[-1][-1]]
    except IndexError:
        threshold = -10
    return Yu, results, threshold


# =============================================================================
# Fitting sigmoid models on the TRV(lesions on controls)
# =============================================================================
Yu1, results1, threshold1 = fit_sigmoid(df, Xu, "TRR1")
Yu2, results2, threshold2 = fit_sigmoid(df, Xu, "TRSUVR0to120")
Yu3, results3, threshold3 = fit_sigmoid(df, Xu, "TRSUVR1to8")
# =============================================================================
# Plotting Figure
# =============================================================================
# =============================================================================
# plt.figure(1)
# plt.clf()
# plt.scatter(X, df["TRR1"] * 100, color='r', alpha=0.2, label='R1')
# w = 200
# B = moving_average(df["TRR1"] * 100, w)
# plt.plot(X[w//2-1:-w//2], B)
# plt.xscale('log')
# =============================================================================
plt.figure(1)
plt.clf()
plt.rcParams.update({'font.size': 15})
# fig, (ax1, ax2) = plt.subplots(nrows=2, sharex=True)
# plt.style.use('dark_background')
plt.suptitle('Longitudinal Test-Retest Variability/ICC for CBF \
according to ROI size in the white matter')

# Top Figure
plt.subplot(211)
plt.xlabel('Lesion Volume [$mm^3$]')
plt.ylabel('Test Retest Variability [%]')
plt.xscale('log')
plt.xlim(Xu[0], Xu[-1])
plt.ylim(0, np.max(Yu2) * 100)
plt.scatter(Xu, Yu1 * 100, color='r', alpha=0.2, label='R1')
plt.scatter(Xu, Yu2 * 100, color='g', alpha=0.2, label='SUVR (0-2m)')
plt.scatter(Xu, Yu3 * 100, color='b', alpha=0.2, label='SUVR (1-8m)')
plt.plot(Xu, results1.best_fit * 100, 'r')
plt.plot(Xu, results2.best_fit * 100, 'g')
plt.plot(Xu, results3.best_fit * 100, 'b')
# plt.hlines(10, Xu[0] - 1, Xu[-1] + 1, 'gray', label='TRV = 10%')
# plt.vlines(threshold1, 0, 10, 'r', '--')
# plt.vlines(threshold2, 0, 10, 'g', '--')
# plt.vlines(threshold3, 0, 10, 'b', '--')
plt.legend(loc=1)

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/inflasep_lesions_icc.csv'
df = pd.read_csv(CLP)


def fit_sigmoid(df, Xu, column):
    Y = df[column].to_numpy()
    Yu = np.zeros(Xu.shape)
    for i in range(len(Xu)):
        Yu[i] = np.mean(Y[np.where(indexes == i)])
    p0 = [max(Yu), np.median(Yu), 1, min(Yu)]
    ps, res = curve_fit(sigmoid, Xu, Yu, p0)
    sigmodel = Model(sigmoid)
    params = sigmodel.make_params(L=ps[0], x0=ps[1], k=ps[2], b=ps[3])
    results = sigmodel.fit(Yu, params, x=Xu)
    try:
        threshold = Xu[np.where(results.best_fit > 0.8)[0][0]]
    except IndexError:
        threshold = -10
    return Yu, results, threshold


X = df[["Lesion Volume"]].to_numpy()
Xu, indexes, counts = np.unique(X, return_inverse=True, return_counts=True)

# =============================================================================
# # Fitting sigmoid models on the ICC(lesions on controls)
# =============================================================================
Yu1, results1, threshold1 = fit_sigmoid(df, Xu, "ICC R1")
Yu2, results2, threshold2 = fit_sigmoid(df, Xu, "ICC SUVR0to120s")
Yu3, results3, threshold3 = fit_sigmoid(df, Xu, "ICC SUVR1to8m")

plt.subplot(212)
plt.xlabel('Lesion Volume [$mm^3$]')
plt.ylabel('ICC')
plt.xscale('log')
plt.xlim(Xu[0], Xu[-1])
plt.ylim(-1, 1)
plt.scatter(Xu, Yu1, color='r', alpha=0.2, label='R1')
plt.scatter(Xu, Yu2, color='g', alpha=0.2, label='SUVR (0-2m)')
plt.scatter(Xu, Yu3, color='b', alpha=0.2, label='SUVR (1-8m)')
plt.plot(Xu, results1.best_fit, 'r')
plt.plot(Xu, results2.best_fit, 'g')
plt.plot(Xu, results3.best_fit, 'b')
# plt.hlines(0.8, Xu[0] - 1, Xu[-1] + 1, 'gray', label='ICC = 0.8')
# plt.vlines(threshold1, -1, 0.8, 'r', '--')
# plt.vlines(threshold2, -1, 0.8, 'g', '--')
# plt.vlines(threshold3, -1, 0.8, 'b', '--')
plt.legend(loc=3)
