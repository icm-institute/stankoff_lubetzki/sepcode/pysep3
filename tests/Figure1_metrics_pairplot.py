#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  8 10:25:02 2021

@author: arya.yazdan-panah
"""


import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/inflasep_lesions.csv'
df = pd.read_csv(CLP)
df_pair_V01 = df[['mR1V01',
                  'DVRV01',
                  'mloganV01',
                  'mSUVR0to120V01',
                  'mSUVR1to8V01',
                  'mSUVR40to90V01']].rename(columns={'mR1V01': 'mR1',
                                                   'DVRV01': 'DVR',
                                                   'mloganV01': 'mlogan',
                                                   'mSUVR0to120V01': 'mSUVR0to120',
                                                   'mSUVR1to8V01': 'mSUVR1to8',
                                                   'mSUVR40to90V01':'mSUVR40to90'})
df_pair_V02 = df[['mR1V02',
                  'DVRV02',
                  'mloganV02',
                  'mSUVR0to120V02',
                  'mSUVR1to8V02',
                  'mSUVR40to90V02']].rename(columns={'mR1V02': 'mR1',
                                                   'DVRV02': 'DVR',
                                                   'mloganV02': 'mlogan',
                                                   'mSUVR0to120V02': 'mSUVR0to120',
                                                   'mSUVR1to8V02': 'mSUVR1to8',
                                                   'mSUVR40to90V02':'mSUVR40to90'})
frames = [df_pair_V01, df_pair_V02]
df_pair = pd.concat(frames)
plt.style.use('dark_background')
sns.pairplot(df_pair)
corr = df_pair.corr()
