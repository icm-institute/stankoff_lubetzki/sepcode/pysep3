#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 16:56:40 2021

@author: arya.yazdan-panah
"""


import matplotlib.pyplot as plt
import matplotlib.patches as ptc
from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from lmfit import Model


def confidence_ellipse(x, y, ax, n_std=3.0, facecolor='none', **kwargs):
    """
    Create a plot of the covariance confidence ellipse of *x* and *y*.

    Parameters
    ----------
    x, y : array-like, shape (n, )
        Input data.

    ax : matplotlib.axes.Axes
        The axes object to draw the ellipse into.

    n_std : float
        The number of standard deviations to determine the ellipse's radiuses.

    **kwargs
        Forwarded to `~matplotlib.patches.Ellipse`

    Returns
    -------
    matplotlib.patches.Ellipse
    """
    if x.size != y.size:
        raise ValueError("x and y must be the same size")

    cov = np.cov(x.T, y.T)
    pearson = cov[0, 1]/np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensionl dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0), width=ell_radius_x * 2, height=ell_radius_y * 2,
                      facecolor=facecolor, **kwargs)

    # Calculating the stdandard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = np.sqrt(cov[0, 0]) * n_std
    mean_x = np.mean(x)

    # calculating the stdandard deviation of y ...
    scale_y = np.sqrt(cov[1, 1]) * n_std
    mean_y = np.mean(y)

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/inflasep_lesions_contrlat_longitudinal_right.csv'
df = pd.read_csv(CLP)

X = df[["TRR1_V01"]].to_numpy()
Y = df[["TRR1_V02"]].to_numpy()
plt.clf()
fig = plt.figure(1)
ax = fig.add_subplot(1, 1, 1)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.set_aspect('equal')
ax.set_xlim([-1, 1])
ax.set_ylim([-1, 1])
ax.set_xlabel('V01 contralateral\nTest-ReTest')
ax.set_ylabel('V02 contralateral\nTest-ReTest', rotation=0)
ax.xaxis.set_label_coords(1, 0.46)
ax.yaxis.set_label_coords(0.5, 1.015)
ax.set_aspect('equal')
ax.plot([-0.75, 0.75], [-0.75, 0.75], 'r--')
# ax.text(-1.2, -1.4, 'normal < contralateral')
# ax.text(0.3, 1.4, 'normal > contralateral')
ax.scatter(X,Y, zorder=1, alpha=0.15)
confidence_ellipse(X, Y, ax, n_std=1, label='$1\sigma$', edgecolor='lime')
confidence_ellipse(X, Y, ax, n_std=2, label='$2\sigma$', edgecolor='limegreen')
confidence_ellipse(X, Y, ax, n_std=3, label='$3\sigma$', edgecolor='darkgreen')
ax.legend()
ax.set_title('Logitudinal Follow-up of the R1 contralateral Test-ReTest', y=1.07, fontsize=20)
#%%
X = df[["TRDVRSRTM2_V01"]].to_numpy()
Y = df[["TRDVRSRTM2_V02"]].to_numpy()
plt.clf()
fig = plt.figure(1)
ax = fig.add_subplot(1, 1, 1)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.set_aspect('equal')
ax.set_xlim([-1, 1])
ax.set_ylim([-1, 1])
ax.set_xlabel('V01 contralateral\nTest-ReTest')
ax.set_ylabel('V02 contralateral\nTest-ReTest', rotation=0)
ax.xaxis.set_label_coords(1, 0.46)
ax.yaxis.set_label_coords(0.5, 1.015)
ax.set_aspect('equal')
ax.plot([-0.75, 0.75], [-0.75, 0.75], 'r--')
# ax.text(-1.2, -1.4, 'normal < contralateral')
# ax.text(0.3, 1.4, 'normal > contralateral')
ax.scatter(X,Y, zorder=1, alpha=0.15)
confidence_ellipse(X, Y, ax, n_std=1, label='$1\sigma$', edgecolor='lime')
confidence_ellipse(X, Y, ax, n_std=2, label='$2\sigma$', edgecolor='limegreen')
confidence_ellipse(X, Y, ax, n_std=3, label='$3\sigma$', edgecolor='darkgreen')
ax.legend()
ax.set_title('Logitudinal Follow-up of the DVR contralateral Test-ReTest', y=1.07, fontsize=20)
X = df[["TRDVRlogan_V01"]].to_numpy()
Y = df[["TRDVRlogan_V02"]].to_numpy()
#%%
plt.clf()
fig = plt.figure(1)
ax = fig.add_subplot(1, 1, 1)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.set_aspect('equal')
ax.set_xlim([-1, 1])
ax.set_ylim([-1, 1])
ax.set_xlabel('V01 contralateral\nTest-ReTest')
ax.set_ylabel('V02 contralateral\nTest-ReTest', rotation=0)
ax.xaxis.set_label_coords(1, 0.46)
ax.yaxis.set_label_coords(0.5, 1.015)
ax.set_aspect('equal')
ax.plot([-0.75, 0.75], [-0.75, 0.75], 'r--')
# ax.text(-1.2, -1.4, 'normal < contralateral')
# ax.text(0.3, 1.4, 'normal > contralateral')
ax.scatter(X,Y, zorder=1, alpha=0.15)
confidence_ellipse(X, Y, ax, n_std=1, label='$1\sigma$', edgecolor='lime')
confidence_ellipse(X, Y, ax, n_std=2, label='$2\sigma$', edgecolor='limegreen')
confidence_ellipse(X, Y, ax, n_std=3, label='$3\sigma$', edgecolor='darkgreen')
ax.legend()
ax.set_title('Logitudinal Follow-up of the Logan contralateral Test-ReTest', y=1.07, fontsize=20)