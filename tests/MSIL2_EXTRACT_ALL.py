#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 19 13:57:41 2021

@author: arya.yazdan-panah
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 10:19:36 2021

@author: arya.yazdan-panah
"""

import os
import nibabel as nib
import numpy as np
import csv
from tqdm import tqdm
from common3 import get_regex, readSubjectsLists
from pprint import pprint
from scipy.ndimage.morphology import binary_dilation

slist = '/network/lustre/iss01/lubetzki-stankoff/clinic/MSIL2/utils/All.list'
subjects = readSubjectsLists(slist)
process = '/network/lustre/iss01/lubetzki-stankoff/clinic/MSIL2/data/process'
stfile1 = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/all_MSIL22.csv'
# stfile4 = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/prelesion_MSIL2_V04.csv'
f1 = open(stfile1, 'w', newline='')
# f4 = open(stfile4, 'w', newline='')
fieldnames = ['subj',
              'MRI visit',
              'les visit',
              'Gd',
              'les vol',
              'T1',
              'FLAIR',
              'FA',
              'MD',
              'AD',
              'RD',
              'MTR',
              'ICVF',
              'ISOVF',
              'OD',
              'qMPF',
              'qMTR',
              'qT1',
              'qT2',
              'cles vol',
              'cT1',
              'cFLAIR',
              'cFA',
              'cMD',
              'cAD',
              'cRD',
              'cMTR',
              'cICVF',
              'cISOVF',
              'cOD',
              'cqMPF',
              'cqMTR',
              'cqT1',
              'cqT2']
statsV01 = csv.DictWriter(f1, fieldnames)
statsV01.writeheader()
# statsV04 = csv.DictWriter(f4, fieldnames)
# statsV04.writeheader()

keys = ['T1', 'FLAIR', 'FA', 'MD', 'AD', 'RD', 'MTR', 'ICVF', 'ISOVF', 'OD',
        'qMPF', 'qMTR', 'qT1', 'qT2']
ckeys = ['cT1', 'cFLAIR', 'cFA', 'cMD', 'cAD', 'cRD', 'cMTR',
         'cICVF', 'cISOVF', 'cOD', 'cqMPF', 'cqMTR', 'cqT1', 'cqT2']

for s in subjects:
    print(s)
    hws = os.path.join(process, s['Id'], s['Id'] + '_hwf_fs_V01V04')
    # print(hws)
    subj = s['Id']
    # print(subj)
    # Anatomical MRIs
    t1_V01 = get_regex(hws, '_V01_t1_to_hws_V01V04.nii.gz', False)
    t1_V04 = get_regex(hws, '_V04_t1_to_hws_V01V04.nii.gz', False)
    flair_V01 = get_regex(hws + '/flair2', '_V01_flair_2hws_V01V04.nii.gz',
                          False)
    flair_V04 = get_regex(hws + '/flair2', '_V04_flair_2hws_V01V04.nii.gz',
                          False)
    # DTI
    fa_V01 = get_regex(hws, '_V01_dti_FA_2hws_V01V04.nii.gz', False)
    fa_V04 = get_regex(hws, '_V04_dti_FA_2hws_V01V04.nii.gz', False)
    md_V01 = get_regex(hws, '_V01_dti_MD_2hws_V01V04.nii.gz', False)
    md_V04 = get_regex(hws, '_V04_dti_MD_2hws_V01V04.nii.gz', False)
    ad_V01 = get_regex(hws, '_V01_dti_L1_2hws_V01V04.nii.gz', False)
    ad_V04 = get_regex(hws, '_V04_dti_L1_2hws_V01V04.nii.gz', False)
    l2_V01 = get_regex(hws, '_V01_dti_L2_2hws_V01V04.nii.gz', False)
    l2_V04 = get_regex(hws, '_V04_dti_L2_2hws_V01V04.nii.gz', False)
    l3_V01 = get_regex(hws, '_V01_dti_L3_2hws_V01V04.nii.gz', False)
    l3_V04 = get_regex(hws, '_V04_dti_L3_2hws_V01V04.nii.gz', False)
    # MT
    mtr_V01 = get_regex(hws, '_V01_mtr_2hws_V01V04.nii.gz', False)
    mtr_V04 = get_regex(hws, '_V04_mtr_2hws_V01V04.nii.gz', False)
    # NODDI
    icvf_V01 = get_regex(hws, 'V1/rFIT_ICVF.nii', False)
    icvf_V04 = get_regex(hws, 'V4/rFIT_ICVF.nii', False)
    isovf_V01 = get_regex(hws, 'V1/rFIT_ISOVF.nii', False)
    isovf_V04 = get_regex(hws, 'V4/rFIT_ISOVF.nii', False)
    OD_V01 = get_regex(hws, 'V1/rFIT_OD.nii', False)
    OD_V04 = get_regex(hws, 'V4/rFIT_OD.nii', False)
    # qMT
    rmpf_V01 = get_regex(hws, 'V1/rMPF_map.nii', False)
    rmpf_V04 = get_regex(hws, 'V4/rMPF_map.nii', False)
    rmtr_V01 = get_regex(hws, 'V1/rMTR_map.nii', False)
    rmtr_V04 = get_regex(hws, 'V4/rMTR_map.nii', False)
    rT1_V01 = get_regex(hws, 'V1/rT1_map.nii', False)
    rT1_V04 = get_regex(hws, 'V4/rT1_map.nii', False)
    rT2_V01 = get_regex(hws, 'V1/rT2_map.nii', False)
    rT2_V04 = get_regex(hws, 'V4/rT2_map.nii', False)
    Visit01 = [t1_V01, flair_V01, fa_V01, md_V01, ad_V01, l2_V01, l3_V01,
               mtr_V01, icvf_V01, isovf_V01, OD_V01, rmpf_V01, rmtr_V01,
               rT1_V01, rT2_V01]
    Visit01_data = []
    for ind, img in enumerate(Visit01):
        if img:
            to_append = nib.load(img[0]).get_fdata()
        else:
            to_append = False
        Visit01_data.append(to_append)
    L3 = Visit01_data.pop(6)
    Visit01_data[5] = np.divide(np.add(Visit01_data[5], L3), 2)
    Visit01_data = dict(zip(keys, Visit01_data))

    Visit04 = [t1_V04, flair_V04, fa_V04, md_V04, ad_V04, l2_V04, l3_V04,
               mtr_V04, icvf_V04, isovf_V04, OD_V04, rmpf_V04, rmtr_V04,
               rT1_V04, rT2_V04]
    Visit04_data = []
    for ind, img in enumerate(Visit04):
        if img:
            to_append = nib.load(img[0]).get_fdata()
        else:
            to_append = False
        Visit04_data.append(to_append)
    L3 = Visit04_data.pop(6)
    Visit04_data[5] = np.divide(np.add(Visit04_data[5], L3), 2)
    Visit04_data = dict(zip(keys, Visit04_data))

    nawm1 = nib.load(get_regex(hws, '_V01_NAWM_2hws_V01V04.nii.gz',
                               False)[0]).get_fdata().astype(bool)
    nawm4 = nib.load(get_regex(hws, '_V04_NAWM_2hws_V01V04.nii.gz',
                               False)[0]).get_fdata().astype(bool)
    manual_lesion_dir = os.path.join(hws, 'manual_lesion')
    lesions = get_regex(manual_lesion_dir, '_(gado|ngado).label_2hws_'
                        'V01V04_labelled_.*.nii.gz', False)
    contrlesions = get_regex(manual_lesion_dir, '_(gado|ngado).label_'
                             '2hws_V01V04_contr_labelled_.*.nii.gz', False)
    les_contr = list(zip(lesions, contrlesions))
    # pprint(les_contr)
    # break
    for les_couple in tqdm(les_contr):
        les_name = os.path.basename(les_couple[0])
        name_list = os.path.basename(les_couple[0]).split('_')
        if name_list[2][0] == 'V':
            les_visit = name_list[2]
        else:
            les_visit = name_list[1]
        # V01
        to_write_V01 = dict(zip(fieldnames, [np.nan] * len(fieldnames)))
        to_write_V01['MRI visit'] = 'V01'
        to_write_V01['subj'] = subj
        to_write_V01['les visit'] = les_visit
        if 'ngado' in les_couple[0]:
            to_write_V01['Gd'] = 0
        else:
            to_write_V01['Gd'] = 1
        for i, les in enumerate(les_couple):
            lesdata = nib.load(les).get_fdata().astype(bool)
            if i == 1:
                lesdata_nawm = np.multiply(nawm1, lesdata)
            else:
                lesdata_nawm = lesdata
            les_vol = np.sum(lesdata_nawm)
            while les_vol < 8:
                lesdata = binary_dilation(lesdata)
                lesdata_nawm = np.multiply(nawm1, lesdata)
                les_vol = np.sum(lesdata_nawm)
            if i == 0:
                KEYS = keys
                to_write_V01['les vol'] = les_vol
            else:
                KEYS = ckeys
                to_write_V01['cles vol'] = les_vol
            for j, k in enumerate(KEYS):
                if not isinstance(Visit01_data[keys[j]], bool):
                    to_write_V01[k] = np.mean(Visit01_data[keys[j]][lesdata_nawm])
        statsV01.writerow(to_write_V01)
        # V04
        to_write_V04 = dict(zip(fieldnames, [np.nan] * len(fieldnames)))
        to_write_V04['MRI visit'] = 'V04'
        to_write_V04['subj'] = subj
        to_write_V04['les visit'] = les_visit
        if 'ngado' in les_couple[0]:
            to_write_V04['Gd'] = 0
        else:
            to_write_V04['Gd'] = 1
        for i, les in enumerate(les_couple):
            lesdata = nib.load(les).get_fdata().astype(bool)
            if i == 1:
                lesdata_nawm = np.multiply(nawm4, lesdata)
            else:
                lesdata_nawm = lesdata
            les_vol = np.sum(lesdata_nawm)
            while les_vol < 8:
                lesdata = binary_dilation(lesdata)
                lesdata_nawm = np.multiply(nawm4, lesdata)
                les_vol = np.sum(lesdata_nawm)
            if i == 0:
                KEYS = keys
                to_write_V04['les vol'] = les_vol
            else:
                KEYS = ckeys
                to_write_V04['cles vol'] = les_vol
            for j, k in enumerate(KEYS):
                if not isinstance(Visit04_data[keys[j]], bool):
                    to_write_V04[k] = np.mean(Visit04_data[keys[j]][lesdata_nawm])
        statsV01.writerow(to_write_V04)
