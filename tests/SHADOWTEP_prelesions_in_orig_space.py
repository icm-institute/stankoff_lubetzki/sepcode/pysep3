#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 14:05:41 2021

@author: arya.yazdan-panah
"""

import os
import nibabel as nib
import csv
import numpy as np
from scipy.ndimage.morphology import binary_dilation
from common3 import get_regex
from scipy.stats import ttest_ind


# %%
subjects = ["P_ABIAB", "P_BARLA", "P_BENMO", "P_BOUBR", "P_BOUJE", "P_DILAN",
            "P_ELONA", "P_ISSLO", "P_LAANA", "P_LABDU", "P_MARAU", "P_NICVA",
            "P_SIFIR", "P_TAZKA", "P_TROAR", "P_VIDVI", "P_ZEMSA"]

preles_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELE\
SIONS/SHADOWTEP/data'
shad = '/network/lustre/iss01/lubetzki-stankoff/clinic/SHADOWTEP/data/process'
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELES\
IONS/SHADOWTEP/prelesion_masks_untouched.csv'
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'preles_vol',
              'R1',
              'contr_vol',
              'cR1']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()

for s in subjects:
    mydir = os.path.join(preles_dir, s)
    for visit in ['V01']:
        mydir_vis = os.path.join(mydir, visit)
        preles_mask = nib.load(get_regex(mydir_vis, 'COR_roimask', 0)[0]).get_fdata().astype(bool)
        contr_mask = nib.load(get_regex(mydir_vis, 'controlatNAWM', 0)[0]).get_fdata().astype(bool)
        R1 = nib.load(get_regex(mydir_vis, 'R1.nii.gz', 0)[0]).get_fdata()
        VC = np.ones(preles_mask.shape)
        preles_vol = int(np.sum(VC[preles_mask]))
        contr_vol = int(np.sum(VC[contr_mask]))
        mR1 = np.mean(R1[preles_mask])
        mcR1 = np.mean(R1[contr_mask])
        stats.writerow({'Subject': s,
                        'preles_vol': preles_vol,
                        'R1': mR1,
                        'contr_vol': contr_vol,
                        'cR1': mcR1})
# %%
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELES\
IONS/SHADOWTEP/prelesion_contr_masks_eroded_by_cortex.csv'
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'preles_vol',
              'R1',
              'contr_vol',
              'cR1']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()

for s in subjects:
    mydir = os.path.join(preles_dir, s)
    for visit in ['V01']:
        mydir_vis = os.path.join(mydir, visit)
        preles_mask = nib.load(get_regex(mydir_vis, 'COR_roimask', 0)[0]).get_fdata().astype(bool)
        contr_mask = nib.load(get_regex(mydir_vis, 'controlatNAWM', 0)[0]).get_fdata().astype(bool)
        cortex_mask = nib.load(get_regex(mydir_vis, '_ribbon_ctx_2R1_V01', 0)[0]).get_fdata().astype(bool)
        ctx_dil = binary_dilation(cortex_mask, iterations=3)
        contr_mask = np.logical_xor(contr_mask, np.logical_and(contr_mask, ctx_dil))
        R1 = nib.load(get_regex(mydir_vis, 'R1.nii.gz', 0)[0]).get_fdata()
        VC = np.ones(preles_mask.shape)
        preles_vol = int(np.sum(VC[preles_mask]))
        contr_vol = int(np.sum(VC[contr_mask]))
        mR1 = np.mean(R1[preles_mask])
        mcR1 = np.mean(R1[contr_mask])
        stats.writerow({'Subject': s,
                        'preles_vol': preles_vol,
                        'R1': mR1,
                        'contr_vol': contr_vol,
                        'cR1': mcR1})
# %%
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELES\
IONS/SHADOWTEP/prelesion_masks_eroded_by_cortex.csv'
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'preles_vol',
              'R1',
              'contr_vol',
              'cR1']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()

for s in subjects:
    mydir = os.path.join(preles_dir, s)
    for visit in ['V01']:
        mydir_vis = os.path.join(mydir, visit)
        preles_mask = nib.load(get_regex(mydir_vis, 'COR_roimask', 0)[0]).get_fdata().astype(bool)
        contr_mask = nib.load(get_regex(mydir_vis, 'controlatNAWM', 0)[0]).get_fdata().astype(bool)
        cortex_mask = nib.load(get_regex(mydir_vis, '_ribbon_ctx_2R1_V01', 0)[0]).get_fdata().astype(bool)
        ctx_dil = binary_dilation(cortex_mask, iterations=3)
        contr_mask = np.logical_xor(contr_mask, np.logical_and(contr_mask, ctx_dil))
        preles_mask = np.logical_xor(preles_mask, np.logical_and(preles_mask, ctx_dil))
        R1 = nib.load(get_regex(mydir_vis, 'R1.nii.gz', 0)[0]).get_fdata()
        VC = np.ones(preles_mask.shape)
        preles_vol = int(np.sum(VC[preles_mask]))
        contr_vol = int(np.sum(VC[contr_mask]))
        mR1 = np.mean(R1[preles_mask])
        mcR1 = np.mean(R1[contr_mask])
        stats.writerow({'Subject': s,
                        'preles_vol': preles_vol,
                        'R1': mR1,
                        'contr_vol': contr_vol,
                        'cR1': mcR1})

# %% Emanuele's masks eroded by FS cortex
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELES\
IONS/SHADOWTEP/EMAs_prelesion_masks_eroded_by_cortex.csv'
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'les_num',
              'preles_vol',
              'R1',
              'contr_vol',
              'cR1']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()

for s in subjects:
    mydir = os.path.join(preles_dir, s)
    visit = 'V01'
    mydir_vis = os.path.join(mydir, visit)
    preles_mask = nib.load(get_regex(mydir_vis, 'COR_roimask_fuzzy_2label_last_EXP3_2R1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    contr_mask = nib.load(get_regex(mydir_vis, '_COR_controlatNAWM_2label_final_EXP3_ero1_2R1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    cortex_mask = nib.load(get_regex(mydir_vis, '_ribbon_ctx_2R1_V01', 0)[0]).get_fdata().astype(int)
    ctx_dil = binary_dilation(cortex_mask, iterations=1)
    contr_mask = np.subtract(contr_mask, np.multiply(contr_mask, ctx_dil))
    preles_mask = np.subtract(preles_mask, np.multiply(preles_mask, ctx_dil))
    R1 = nib.load(get_regex(mydir_vis, 'R1.nii.gz', 0)[0]).get_fdata()
    maxpre = np.max(preles_mask)
    maxcontr = np.max(contr_mask)
    VC = np.ones(preles_mask.shape)
    if maxcontr == 0 or maxpre == 0:
        print("maxcontr = ", maxcontr)
        print("maxpre = ", maxpre)
        print()
        continue
    for label in range(1, max([maxcontr, maxpre])):
        print(s, label)
        preles_vol = int(np.sum(VC[preles_mask == label]))
        contr_vol = int(np.sum(VC[contr_mask == label]))
        if preles_vol == 0 or contr_vol == 0:
            continue
        print("Prelesion volume", preles_vol)
        print("Contr volume", contr_vol)
        mR1 = np.mean(R1[preles_mask == label])
        mcR1 = np.mean(R1[contr_mask == label])
        stats.writerow({'Subject': s,
                        'les_num': label,
                        'preles_vol': preles_vol,
                        'R1': mR1,
                        'contr_vol': contr_vol,
                        'cR1': mcR1})
f.close()

# %% Emanuele's masks non eroded but with t-test for each prelesion

preles_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELE\
SIONS/SHADOWTEP/data'
shad = '/network/lustre/iss01/lubetzki-stankoff/clinic/SHADOWTEP/data/process'
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELES\
IONS/SHADOWTEP/EMAs_prelesion_masks_individual_ttest.csv'
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'les_num',
              'preles_vol',
              'R1',
              'contr_vol',
              'cR1',
              'tval',
              'pval']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()

for s in subjects:
    mydir = os.path.join(preles_dir, s)
    visit = 'V01'
    mydir_vis = os.path.join(mydir, visit)
    preles_mask = nib.load(get_regex(mydir_vis, 'COR_roimask_fuzzy_2label_last_EXP3_2R1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    contr_mask = nib.load(get_regex(mydir_vis, '_COR_controlatNAWM_2label_final_EXP3_ero1_2R1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    cortex_mask = nib.load(get_regex(mydir_vis, '_ribbon_ctx_2R1_V01', 0)[0]).get_fdata().astype(int)
    R1 = nib.load(get_regex(mydir_vis, 'R1.nii.gz', 0)[0]).get_fdata()
    maxpre = np.max(preles_mask)
    maxcontr = np.max(contr_mask)
    VC = np.ones(preles_mask.shape)
    if maxcontr == 0 or maxpre == 0:
        print("maxcontr = ", maxcontr)
        print("maxpre = ", maxpre)
        print()
        continue
    for label in range(1, max([maxcontr, maxpre])):
        print(s, label)
        preles_vol = int(np.sum(VC[preles_mask == label]))
        contr_vol = int(np.sum(VC[contr_mask == label]))
        if preles_vol == 0 or contr_vol == 0:
            continue
        print("Prelesion volume", preles_vol)
        print("Contr volume", contr_vol)
        R1_preles = R1[preles_mask == label]
        R1_contrlat = R1[contr_mask == label]
        mR1 = np.mean(R1_preles)
        mcR1 = np.mean(R1_contrlat)
        tval, pval = ttest_ind(R1_preles, R1_contrlat)
        stats.writerow({'Subject': s,
                        'les_num': label,
                        'preles_vol': preles_vol,
                        'R1': mR1,
                        'contr_vol': contr_vol,
                        'cR1': mcR1,
                        'tval': tval,
                        'pval': pval})

f.close()

# %% Emanuele's masks non eroded but with t-test for each prelesion and DM added

preles_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELE\
SIONS/SHADOWTEP/data'
shad = '/network/lustre/iss01/lubetzki-stankoff/clinic/SHADOWTEP/data/process'
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELES\
IONS/SHADOWTEP/EMAs_prelesion_masks_individual_ttest_with_dm.csv'
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'les_num',
              'preles_vol',
              'R1',
              'dm_vt',
              'dm_cx',
              'dm_wm',
              'contr_vol',
              'cR1',
              'cdm_vt',
              'cdm_cx',
              'cdm_wm',
              'tval',
              'pval']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()

for s in subjects:
    mydir = os.path.join(preles_dir, s)
    visit = 'V01'
    mydir_vis = os.path.join(mydir, visit)
    preles_mask = nib.load(get_regex(mydir_vis, 'COR_roimask_fuzzy_2label_last_EXP3_2R1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    contr_mask = nib.load(get_regex(mydir_vis, '_COR_controlatNAWM_2label_final_EXP3_ero1_2R1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    cortex_mask = nib.load(get_regex(mydir_vis, '_ribbon_ctx_2R1_V01', 0)[0]).get_fdata().astype(int)
    R1 = nib.load(get_regex(mydir_vis, 'R1.nii.gz', 0)[0]).get_fdata()
    DM_cx = nib.load(get_regex(mydir_vis, '_DM_cx_2R1_V01', 0)[0]).get_fdata()
    DM_vt = nib.load(get_regex(mydir_vis, '_DM_ventri_2R1_V01', 0)[0]).get_fdata()
    DM_wm = nib.load(get_regex(mydir_vis, '_DM_wm_2R1_V01', 0)[0]).get_fdata()
    maxpre = np.max(preles_mask)
    maxcontr = np.max(contr_mask)
    VC = np.ones(preles_mask.shape)
    if maxcontr == 0 or maxpre == 0:
        print("maxcontr = ", maxcontr)
        print("maxpre = ", maxpre)
        print()
        continue
    for label in range(1, max([maxcontr, maxpre])):
        print(s, label)
        preles_vol = int(np.sum(VC[preles_mask == label]))
        contr_vol = int(np.sum(VC[contr_mask == label]))
        if preles_vol == 0 or contr_vol == 0:
            continue
        print("Prelesion volume", preles_vol)
        print("Contr volume", contr_vol)
        R1_preles = R1[preles_mask == label]
        R1_contrlat = R1[contr_mask == label]
        DM_cx_preles = DM_cx[preles_mask == label]
        DM_vt_preles = DM_vt[preles_mask == label]
        DM_wm_preles = DM_wm[preles_mask == label]
        DM_cx_contrlat = DM_cx[contr_mask == label]
        DM_vt_contrlat = DM_vt[contr_mask == label]
        DM_wm_contrlat = DM_wm[contr_mask == label]
        mR1 = np.mean(R1_preles)
        mcR1 = np.mean(R1_contrlat)
        mDM_cx_preles = np.mean(DM_cx_preles)
        mDM_vt_preles = np.mean(DM_vt_preles)
        mDM_wm_preles = np.mean(DM_wm_preles)
        mDM_cx_contrlat = np.mean(DM_cx_contrlat)
        mDM_vt_contrlat = np.mean(DM_vt_contrlat)
        mDM_wm_contrlat = np.mean(DM_wm_contrlat)
        tval, pval = ttest_ind(R1_preles, R1_contrlat)
        stats.writerow({'Subject': s,
                        'les_num': label,
                        'preles_vol': preles_vol,
                        'R1': mR1,
                        'dm_vt': mDM_vt_preles,
                        'dm_cx': mDM_cx_preles,
                        'dm_wm': mDM_wm_preles,
                        'contr_vol': contr_vol,
                        'cR1': mcR1,
                        'cdm_vt': mDM_vt_contrlat,
                        'cdm_cx': mDM_cx_contrlat,
                        'cdm_wm': mDM_wm_contrlat,
                        'tval': tval,
                        'pval': pval})

f.close()

# %% Emanuele's masks non eroded but with t-test for each prelesion and DM added and GD type added

subjects = ["P_ABIAB", "P_BARLA", "P_BENMO", "P_BOUBR", "P_BOUJE", "P_DILAN",
            "P_ELONA", "P_LAANA", "P_LABDU", "P_MARAU", "P_NICVA",
            "P_SIFIR", "P_TAZKA", "P_TROAR", "P_VIDVI", "P_ZEMSA"]

preles_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELE\
SIONS/SHADOWTEP/data'
shad = '/network/lustre/iss01/lubetzki-stankoff/clinic/SHADOWTEP/data/process'
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELES\
IONS/SHADOWTEP/EMAs_prelesion_masks_individual_ttest_with_dm_with_gd.csv'
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'les_num',
              'preles_vol',
              'R1',
              'dm_vt',
              'dm_cx',
              'dm_wm',
              'contr_vol',
              'cR1',
              'cdm_vt',
              'cdm_cx',
              'cdm_wm',
              'gd_rim1_nod_2',
              'tval',
              'pval']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()

for s in subjects:
    mydir = os.path.join(preles_dir, s)
    visit = 'V01'
    mydir_vis = os.path.join(mydir, visit)
    preles_mask = nib.load(get_regex(mydir_vis, 'COR_roimask_fuzzy_2label_last_EXP3_2R1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    contr_mask = nib.load(get_regex(mydir_vis, '_COR_controlatNAWM_2label_final_EXP3_ero1_2R1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    cortex_mask = nib.load(get_regex(mydir_vis, '_ribbon_ctx_2R1_V01', 0)[0]).get_fdata().astype(int)
    R1 = nib.load(get_regex(mydir_vis, 'R1.nii.gz', 0)[0]).get_fdata()
    DM_cx = nib.load(get_regex(mydir_vis, '_DM_cx_2R1_V01', 0)[0]).get_fdata()
    DM_vt = nib.load(get_regex(mydir_vis, '_DM_ventri_2R1_V01', 0)[0]).get_fdata()
    DM_wm = nib.load(get_regex(mydir_vis, '_DM_wm_2R1_V01', 0)[0]).get_fdata()
    rim = binary_dilation(nib.load(get_regex(mydir_vis, 'rim', 0)[0]).get_fdata().astype(int))
    nodule = binary_dilation(nib.load(get_regex(mydir_vis, 'nodule', 0)[0]).get_fdata().astype(int))
    maxpre = np.max(preles_mask)
    maxcontr = np.max(contr_mask)
    VC = np.ones(preles_mask.shape)
    if maxcontr == 0 or maxpre == 0:
        print("maxcontr = ", maxcontr)
        print("maxpre = ", maxpre)
        print()
        continue
    for label in range(1, max([maxcontr, maxpre])):
        print(s, label)
        preles_vol = int(np.sum(VC[preles_mask == label]))
        contr_vol = int(np.sum(VC[contr_mask == label]))
        if preles_vol == 0 or contr_vol == 0:
            continue
        print("Prelesion volume", preles_vol)
        print("Contr volume", contr_vol)
        if np.sum(rim[preles_mask == label]) != 0:
            gd_val = 1
        elif np.sum(nodule[preles_mask == label]) != 0:
            gd_val = 2
        else:
            gd_val = 0
        R1_preles = R1[preles_mask == label]
        R1_contrlat = R1[contr_mask == label]
        DM_cx_preles = DM_cx[preles_mask == label]
        DM_vt_preles = DM_vt[preles_mask == label]
        DM_wm_preles = DM_wm[preles_mask == label]
        DM_cx_contrlat = DM_cx[contr_mask == label]
        DM_vt_contrlat = DM_vt[contr_mask == label]
        DM_wm_contrlat = DM_wm[contr_mask == label]
        mR1 = np.mean(R1_preles)
        mcR1 = np.mean(R1_contrlat)
        mDM_cx_preles = np.mean(DM_cx_preles)
        mDM_vt_preles = np.mean(DM_vt_preles)
        mDM_wm_preles = np.mean(DM_wm_preles)
        mDM_cx_contrlat = np.mean(DM_cx_contrlat)
        mDM_vt_contrlat = np.mean(DM_vt_contrlat)
        mDM_wm_contrlat = np.mean(DM_wm_contrlat)
        tval, pval = ttest_ind(R1_preles, R1_contrlat)
        stats.writerow({'Subject': s,
                        'les_num': label,
                        'preles_vol': preles_vol,
                        'R1': mR1,
                        'dm_vt': mDM_vt_preles,
                        'dm_cx': mDM_cx_preles,
                        'dm_wm': mDM_wm_preles,
                        'contr_vol': contr_vol,
                        'cR1': mcR1,
                        'cdm_vt': mDM_vt_contrlat,
                        'cdm_cx': mDM_cx_contrlat,
                        'cdm_wm': mDM_wm_contrlat,
                        'gd_rim1_nod_2': gd_val,
                        'tval': tval,
                        'pval': pval})

f.close()

# %% Emanuele's masks non eroded but with t-test for each prelesion and DM added and GD type added + MTR and DWI

subjects = ["P_ABIAB", "P_BARLA", "P_BENMO", "P_BOUBR", "P_BOUJE", "P_DILAN",
            "P_ELONA", "P_LAANA", "P_LABDU", "P_MARAU", "P_NICVA",
            "P_SIFIR", "P_TAZKA", "P_TROAR", "P_VIDVI", "P_ZEMSA"]

preles_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELE\
SIONS/SHADOWTEP/data'
shad = '/network/lustre/iss01/lubetzki-stankoff/clinic/SHADOWTEP/data/process'
stats_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELES\
IONS/SHADOWTEP/EMAs_prelesion_masks_individual_ttest_with_dm_with_gd_with_MDR_RD_MTR_FA_L1.csv'
f = open(stats_file, 'w', newline='')
fieldnames = ['Subject',
              'les_num',
              'preles_vol',
              'R1',
              'MTR',
              'FA',
              'L1',
              'MD',
              'RD',
              'dm_vt',
              'dm_cx',
              'dm_wm',
              'contr_vol',
              'cR1',
              'cMTR',
              'cFA',
              'cL1',
              'cMD',
              'cRD',
              'cdm_vt',
              'cdm_cx',
              'cdm_wm',
              'gd_rim1_nod_2',
              'R1tval',
              'R1pval',
              'MTRtval',
              'MTRpval',
              'FAtval',
              'FApval',
              'L1tval',
              'L1pval',
              'MDtval',
              'MDpval',
              'RDtval',
              'RDpval']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()

for s in subjects:
    # Directories
    mydir = os.path.join(preles_dir, s)
    visit = 'V01'
    mydir_vis = os.path.join(mydir, visit)
    shad_mtr = os.path.join(shad, s, 'V01', 'prepareMTR')
    shad_dwi = os.path.join(shad, s, 'V01', 'dwi2t1')
    # Masks
    pre = nib.load(get_regex(mydir_vis, 'COR_roimask_fuzzy_2label_last_EXP3_2R1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    contr = nib.load(get_regex(mydir_vis, '_COR_controlatNAWM_2label_final_EXP3_ero1_2R1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    pret1 = nib.load(get_regex(mydir_vis, 'COR_roimask_fuzzy_2label_last_EXP3_2t1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    contrt1 = nib.load(get_regex(mydir_vis, '_COR_controlatNAWM_2label_final_EXP3_ero1_2t1_V01.nii.gz', 0)[0]).get_fdata().astype(int)
    # Gadolinium masks
    rim = binary_dilation(nib.load(get_regex(mydir_vis, 'rim_2R1', 0)[0]).get_fdata().astype(int))
    nodule = binary_dilation(nib.load(get_regex(mydir_vis, 'nodule_2R1', 0)[0]).get_fdata().astype(int))
    # cortex_mask = nib.load(get_regex(mydir_vis, '_ribbon_ctx_2R1_V01', 0)[0]).get_fdata().astype(int)
    # Quantitative Images
    R1 = nib.load(get_regex(mydir_vis, 'R1.nii.gz', 0)[0]).get_fdata()

    FA = nib.load(get_regex(shad_dwi, '_dwi_FA_2t1.nii.gz', 0)[0]).get_fdata()
    L1 = nib.load(get_regex(shad_dwi, '_dwi_L1_2t1.nii.gz', 0)[0]).get_fdata()
    RD = nib.load(get_regex(shad_dwi, '_dwi_RD_2t1.nii.gz', 0)[0]).get_fdata()
    if s == 'P_BOUJE' or s == 'P_SIFIR':
        MTR = np.empty(FA.shape)
        MTR[:] = np.NaN
    else:
        MTR = nib.load(get_regex(shad_mtr, '_mtr_2t1.nii.gz', 0)[0]).get_fdata()

    if s == 'P_BOUJE' or s == 'P_NICVA' or s == 'P_SIFIR':
        MD = np.empty(FA.shape)
        MD[:] = np.NaN
    else:
        MD = nib.load(get_regex(shad_dwi, '_dwi_MD_2t1.nii.gz', 0)[0]).get_fdata()
    # Distance Maps
    DM_cx = nib.load(get_regex(mydir_vis, '_DM_cx_2R1_V01', 0)[0]).get_fdata()
    DM_vt = nib.load(get_regex(mydir_vis, '_DM_ventri_2R1_V01', 0)[0]).get_fdata()
    DM_wm = nib.load(get_regex(mydir_vis, '_DM_wm_2R1_V01', 0)[0]).get_fdata()

    maxpre = np.max(pre)
    maxcontr = np.max(contr)
    VC = np.ones(pre.shape)
    if maxcontr == 0 or maxpre == 0:
        print("maxcontr = ", maxcontr)
        print("maxpre = ", maxpre)
        print()
        continue
    for label in range(1, max([maxcontr, maxpre])):
        print(s, label)
        preles_vol = int(np.sum(VC[pre == label]))
        contr_vol = int(np.sum(VC[contr == label]))
        if preles_vol == 0 or contr_vol == 0:
            continue
        print("Prelesion volume", preles_vol)
        print("Contr volume", contr_vol)
        if np.sum(rim[pre == label]) != 0:
            gd_val = 1
        elif np.sum(nodule[pre == label]) != 0:
            gd_val = 2
        else:
            gd_val = 0
        R1_preles = R1[pre == label]
        R1_contrlat = R1[contr == label]

        MTR_preles = MTR[pret1 == label]
        MTR_contrlat = MTR[contrt1 == label]

        FA_preles = FA[pret1 == label]
        FA_contrlat = FA[contrt1 == label]

        L1_preles = L1[pret1 == label]
        L1_contrlat = L1[contrt1 == label]

        MD_preles = MD[pret1 == label]
        MD_contrlat = MD[contrt1 == label]

        RD_preles = RD[pret1 == label]
        RD_contrlat = RD[contrt1 == label]

        DM_cx_preles = DM_cx[pre == label]
        DM_vt_preles = DM_vt[pre == label]
        DM_wm_preles = DM_wm[pre == label]
        DM_cx_contrlat = DM_cx[contr == label]
        DM_vt_contrlat = DM_vt[contr == label]
        DM_wm_contrlat = DM_wm[contr == label]

        mcR1 = np.mean(R1_contrlat)
        mDM_cx_preles = np.mean(DM_cx_preles)
        mDM_vt_preles = np.mean(DM_vt_preles)
        mDM_wm_preles = np.mean(DM_wm_preles)
        mDM_cx_contrlat = np.mean(DM_cx_contrlat)
        mDM_vt_contrlat = np.mean(DM_vt_contrlat)
        mDM_wm_contrlat = np.mean(DM_wm_contrlat)

        R1tval, R1pval = ttest_ind(R1_preles, R1_contrlat)
        MTRtval, MTRpval = ttest_ind(MTR_preles, MTR_contrlat)
        FAtval, FApval = ttest_ind(FA_preles, FA_contrlat)
        L1tval, L1pval = ttest_ind(L1_preles, L1_contrlat)
        MDtval, MDpval = ttest_ind(MD_preles, MD_contrlat)
        RDtval, RDpval = ttest_ind(RD_preles, RD_contrlat)

        mR1 = np.mean(R1_preles)
        mcR1 = np.mean(R1_contrlat)
        mMTR = np.mean(MTR_preles)
        mcMTR = np.mean(MTR_contrlat)
        mFA = np.mean(FA_preles)
        mcFA = np.mean(FA_contrlat)
        mL1 = np.mean(L1_preles)
        mcL1 = np.mean(L1_contrlat)
        mMD = np.mean(MD_preles)
        mcMD = np.mean(MD_contrlat)
        mRD = np.mean(RD_preles)
        mcRD = np.mean(RD_contrlat)

        stats.writerow({'Subject': s,
                        'les_num': label,
                        'preles_vol': preles_vol,
                        'R1': mR1,
                        'MTR': mMTR,
                        'FA': mFA,
                        'L1': mL1,
                        'MD': mMD,
                        'RD': mRD,
                        'dm_vt': mDM_vt_preles,
                        'dm_cx': mDM_cx_preles,
                        'dm_wm': mDM_wm_preles,
                        'contr_vol': contr_vol,
                        'cR1': mcR1,
                        'cMTR': mcMTR,
                        'cFA': mcFA,
                        'cL1': mcL1,
                        'cMD': mcMD,
                        'cRD': mcRD,
                        'cdm_vt': mDM_vt_contrlat,
                        'cdm_cx': mDM_cx_contrlat,
                        'cdm_wm': mDM_wm_contrlat,
                        'gd_rim1_nod_2': gd_val,
                        'R1tval': R1tval,
                        'R1pval': R1pval,
                        'MTRtval': MTRtval,
                        'MTRpval': MTRpval,
                        'FAtval': FAtval,
                        'FApval': FApval,
                        'L1tval': L1tval,
                        'L1pval': L1pval,
                        'MDtval': MDtval,
                        'MDpval': MDpval,
                        'RDtval': RDtval,
                        'RDpval': RDpval})

f.close()
