#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  5 14:45:04 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from statsmodels.stats import weightstats
from scipy.stats import ttest_ind
from scipy.optimize import curve_fit
from lmfit import Model
import seaborn as sns
from scipy.stats import ttest_ind

# %%



TR = {'P_BENMO': 37,
      'P_BARLA': 42,
      'P_BOUJE': 47,
      'P_LABDU': 49,
      'P_ISSLO': 50,
      'P_NICVA': 56,
      'P_ABIAB': 70,
      'P_ZEMSA': 83,
      'P_BOUBR': 84,
      'P_DILAN': 89,
      'P_VIDVI': 91,
      'P_TAZKA': 97,
      'P_TROAR': 98,
      'P_LAANA': 99,
      'P_SIFIR': 100,
      'P_ELONA': 104,
      'P_MARAU': 120}
subjects = ["P_ABIAB", "P_BARLA", "P_BENMO", "P_BOUBR", "P_BOUJE", "P_DILAN",
            "P_ELONA", "P_ISSLO", "P_LAANA", "P_LABDU", "P_MARAU", "P_NICVA",
            "P_SIFIR", "P_TAZKA", "P_TROAR", "P_VIDVI", "P_ZEMSA"]

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELESIONS/SH\
ADOWTEP/EMAs_prelesion_masks_individual_ttest.csv'
df = pd.read_csv(CLP)
TRs = np.zeros(len(df))
diffs = np.zeros(len(df))
vols = df['preles_vol'].to_numpy()
pvals = df['pval'].to_numpy()
tvals = df['tval'].to_numpy()
les_subject = df['Subject'].to_list()
les_nums = df['les_num'].to_list()
les_les = [m + '_' + str(n) for m, n in zip(les_subject, les_nums)]
for idx, lesion in df.iterrows():
    s = lesion['Subject']
    les_num = lesion['les_num']
    test_retest = TR[s]
    TRs[idx] = test_retest
    diffs[idx] = lesion['R1'] - lesion['cR1']
    vols[idx] = lesion['preles_vol']

fig = plt.figure(1)
fig.clear()
ax = fig.add_subplot(111)

ax.scatter(TRs[pvals>0.05], tvals[pvals>0.05], c='b', label='p>0.05')
ax.scatter(TRs[pvals<0.05], tvals[pvals<0.05], c='r', label='p<0.05')
# for i in range(len(les_les)):
#     ax.text(vols[i], tvals[i], les_les[i], color='k')
ax.legend()
ax.set_xlabel('Test Retest time [days]')
ax.set_ylabel('t stat')
ax.set_title('t-stat of prelesions in function of test_retest')




# %% Cool figure but a little bit much
# =============================================================================
# from mpl_toolkits.mplot3d import Axes3d
# fig = plt.figure(1)
# fig.clear()
# ax = fig.add_subplot(111, projection='3d')
#
# ax.scatter(vols[pvals>0.05], TRs[pvals>0.05], tvals[pvals>0.05], c='b', label='p>0.05')
# ax.scatter(vols[pvals<0.05], TRs[pvals<0.05], tvals[pvals<0.05], c='r', label='p<0.05')
# for i in range(len(les_les)):
#     ax.text(vols[i], TRs[i], tvals[i], les_les[i], size=3, zorder=1, color='k')
# ax.legend()
# ax.set_xlabel('Prelesion volume [vox]')
# ax.set_ylabel('TR delay [days]')
# ax.set_zlabel('t stat')
# ax.set_title('t-stat of prelesions in function of size and ')
#
# for angle in range(0, 360):
#     ax.view_init(0, angle)
#     plt.draw()
#     plt.pause(.001)
#     if angle % 90 == 0:
#         plt.pause(2)
#
# =============================================================================


# %% Adding distance maps


TR = {'P_BENMO': 37,
      'P_BARLA': 42,
      'P_BOUJE': 47,
      'P_LABDU': 49,
      'P_ISSLO': 50,
      'P_NICVA': 56,
      'P_ABIAB': 70,
      'P_ZEMSA': 83,
      'P_BOUBR': 84,
      'P_DILAN': 89,
      'P_VIDVI': 91,
      'P_TAZKA': 97,
      'P_TROAR': 98,
      'P_LAANA': 99,
      'P_SIFIR': 100,
      'P_ELONA': 104,
      'P_MARAU': 120}
subjects = ["P_ABIAB", "P_BARLA", "P_BENMO", "P_BOUBR", "P_BOUJE", "P_DILAN",
            "P_ELONA", "P_ISSLO", "P_LAANA", "P_LABDU", "P_MARAU", "P_NICVA",
            "P_SIFIR", "P_TAZKA", "P_TROAR", "P_VIDVI", "P_ZEMSA"]

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELESIONS/SH\
ADOWTEP/EMAs_prelesion_masks_individual_ttest_with_dm.csv'
df = pd.read_csv(CLP)
TRs = np.zeros(len(df))
diffs = np.zeros(len(df))
vols = df['preles_vol'].to_numpy()
pvals = df['pval'].to_numpy()
tvals = df['tval'].to_numpy()
dm_wm = df['dm_wm'].to_numpy()
dm_cx = df['dm_cx'].to_numpy()
dm_vt = df['dm_vt'].to_numpy()
les_subject = df['Subject'].to_list()
les_nums = df['les_num'].to_list()
les_les = [m + '_' + str(n) for m, n in zip(les_subject, les_nums)]
for idx, lesion in df.iterrows():
    s = lesion['Subject']
    les_num = lesion['les_num']
    test_retest = TR[s]
    TRs[idx] = test_retest
    diffs[idx] = lesion['R1'] - lesion['cR1']
    vols[idx] = lesion['preles_vol']

fig = plt.figure(1)
fig.clear()
ax = fig.add_subplot(111)

ax.scatter(dm_cx[pvals>0.05], tvals[pvals>0.05], c='b', label='p>0.05')
ax.scatter(dm_cx[pvals<0.05], tvals[pvals<0.05], c='r', label='p<0.05')
# for i in range(len(les_les)):
#     ax.text(vols[i], tvals[i], les_les[i], color='k')
ax.legend()
ax.set_xlabel('distance from cortex [mm]')
ax.set_ylabel('t stat')
ax.set_title('t-stat of prelesions in function of cortical distance')



# %% Adding distance maps adding gado


TR = {'P_BENMO': 37,
      'P_BARLA': 42,
      'P_BOUJE': 47,
      'P_LABDU': 49,
      'P_ISSLO': 50,
      'P_NICVA': 56,
      'P_ABIAB': 70,
      'P_ZEMSA': 83,
      'P_BOUBR': 84,
      'P_DILAN': 89,
      'P_VIDVI': 91,
      'P_TAZKA': 97,
      'P_TROAR': 98,
      'P_LAANA': 99,
      'P_SIFIR': 100,
      'P_ELONA': 104,
      'P_MARAU': 120}
subjects = ["P_ABIAB", "P_BARLA", "P_BENMO", "P_BOUBR", "P_BOUJE", "P_DILAN",
            "P_ELONA", "P_ISSLO", "P_LAANA", "P_LABDU", "P_MARAU", "P_NICVA",
            "P_SIFIR", "P_TAZKA", "P_TROAR", "P_VIDVI", "P_ZEMSA"]

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELESIONS/SH\
ADOWTEP/EMAs_prelesion_masks_individual_ttest_with_dm_with_gd.csv'
df = pd.read_csv(CLP)
TRs = np.zeros(len(df))
diffs = np.zeros(len(df))
vols = df['preles_vol'].to_numpy()
pvals = df['pval'].to_numpy()
gd = df['gd_rim1_nod_2'].to_numpy()
tvals = df['tval'].to_numpy()
dm_wm = df['dm_wm'].to_numpy()
dm_cx = df['dm_cx'].to_numpy()
dm_vt = df['dm_vt'].to_numpy()
les_subject = df['Subject'].to_list()
les_nums = df['les_num'].to_list()
les_les = [m + '_' + str(n) for m, n in zip(les_subject, les_nums)]
for idx, lesion in df.iterrows():
    s = lesion['Subject']
    les_num = lesion['les_num']
    test_retest = TR[s]
    TRs[idx] = test_retest
    diffs[idx] = lesion['R1'] - lesion['cR1']
    vols[idx] = lesion['preles_vol']

fig = plt.figure(1)
fig.clear()
ax = fig.add_subplot(111)


metric = dm_wm


ax.scatter(metric[(pvals>0.05) & (gd==0)], tvals[(pvals>0.05) & (gd==0)], c='b', label='gd- & p>0.05', marker='x')
ax.scatter(metric[(pvals<0.05) & (gd==0)], tvals[(pvals<0.05) & (gd==0)], c='r', label='gd- & p<0.05', marker='x')
ax.scatter(metric[(pvals>0.05) & (gd==1)], tvals[(pvals>0.05) & (gd==1)], label='rim & p>0.05', marker='o', facecolor='none', edgecolors='b')
ax.scatter(metric[(pvals<0.05) & (gd==1)], tvals[(pvals<0.05) & (gd==1)], label='rim & p<0.05', marker='o', facecolor='none', edgecolors='r')
ax.scatter(metric[(pvals>0.05) & (gd==2)], tvals[(pvals>0.05) & (gd==2)], c='b', label='nodule & p>0.05', marker='o')
ax.scatter(metric[(pvals<0.05) & (gd==2)], tvals[(pvals<0.05) & (gd==2)], c='r', label='nodule & p<0.05', marker='o')
# for i in range(len(les_les)):
#     ax.text(vols[i], tvals[i], les_les[i], color='k')
ax.legend()
ax.set_xlabel('DM white matter [mm]')
ax.set_ylabel('t stat')


# %% Adding distance maps adding gado adding mtr and diffusion


TR = {'P_BENMO': 37,
      'P_BARLA': 42,
      'P_BOUJE': 47,
      'P_LABDU': 49,
      'P_ISSLO': 50,
      'P_NICVA': 56,
      'P_ABIAB': 70,
      'P_ZEMSA': 83,
      'P_BOUBR': 84,
      'P_DILAN': 89,
      'P_VIDVI': 91,
      'P_TAZKA': 97,
      'P_TROAR': 98,
      'P_LAANA': 99,
      'P_SIFIR': 100,
      'P_ELONA': 104,
      'P_MARAU': 120}
subjects = ["P_ABIAB", "P_BARLA", "P_BENMO", "P_BOUBR", "P_BOUJE", "P_DILAN",
            "P_ELONA", "P_ISSLO", "P_LAANA", "P_LABDU", "P_MARAU", "P_NICVA",
            "P_SIFIR", "P_TAZKA", "P_TROAR", "P_VIDVI", "P_ZEMSA"]

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELESIONS/SH\
ADOWTEP/EMAs_prelesion_masks_individual_ttest_with_dm_with_gd_with_MDR_RD_MTR_FA_L1.csv'
df = pd.read_csv(CLP)
TRs = np.zeros(len(df))
diffs = np.zeros(len(df))
vols = df['preles_vol'].to_numpy()
pvals = df['R1pval'].to_numpy()
gd = df['gd_rim1_nod_2'].to_numpy()
tvals = df['R1tval'].to_numpy()
dm_wm = df['dm_wm'].to_numpy()
dm_cx = df['dm_cx'].to_numpy()
dm_vt = df['dm_vt'].to_numpy()
les_subject = df['Subject'].to_list()
les_nums = df['les_num'].to_list()
les_les = [m + '_' + str(n) for m, n in zip(les_subject, les_nums)]
for idx, lesion in df.iterrows():
    s = lesion['Subject']
    les_num = lesion['les_num']
    test_retest = TR[s]
    TRs[idx] = test_retest
    diffs[idx] = lesion['R1'] - lesion['cR1']
    vols[idx] = lesion['preles_vol']

fig = plt.figure(1)
fig.clear()
ax = fig.add_subplot(111)


metric = dm_wm

metric_name1 = 'R1tval'
metric_name2 = 'FAtval'
metric1 = df[metric_name1]
metric2 = df[metric_name2]
sign_hypo_gd0 = (pvals < 0.05) & (gd == 0) & (tvals < 0)
sign_hypo_gd1 = (pvals < 0.05) & (gd == 1) & (tvals < 0)
sign_hypo_gd2 = (pvals < 0.05) & (gd == 2) & (tvals < 0)
sign_hyper_gd0 = (pvals < 0.05) & (gd == 0) & (tvals > 0)
sign_hyper_gd1 = (pvals < 0.05) & (gd == 1) & (tvals > 0)
sign_hyper_gd2 = (pvals < 0.05) & (gd == 2) & (tvals > 0)
not_sign_hypo_gd0 = (pvals > 0.05) & (gd == 0) & (tvals < 0)
not_sign_hypo_gd1 = (pvals > 0.05) & (gd == 1) & (tvals < 0)
not_sign_hypo_gd2 = (pvals > 0.05) & (gd == 2) & (tvals < 0)
not_sign_hyper_gd0 = (pvals > 0.05) & (gd == 0) & (tvals > 0)
not_sign_hyper_gd1 = (pvals > 0.05) & (gd == 1) & (tvals > 0)
not_sign_hyper_gd2 = (pvals > 0.05) & (gd == 2) & (tvals > 0)

ax.scatter(metric1[sign_hypo_gd0], metric2[sign_hypo_gd0], c='b', label='sign_hypo_gd0', marker='x')
ax.scatter(metric1[sign_hypo_gd1], metric2[sign_hypo_gd1], label='sign_hypo_gd1', marker='o', facecolor='none', edgecolors='b')
ax.scatter(metric1[sign_hypo_gd2], metric2[sign_hypo_gd2], c='b', label='sign_hypo_gd2', marker='o')
ax.scatter(metric1[sign_hyper_gd0], metric2[sign_hyper_gd0], c='r', label='sign_hyper_gd0', marker='x')
ax.scatter(metric1[sign_hyper_gd1], metric2[sign_hyper_gd1], label='sign_hyper_gd1', marker='o', facecolor='none', edgecolors='r')
ax.scatter(metric1[sign_hyper_gd2], metric2[sign_hyper_gd2], c='r', label='sign_hyper_gd2', marker='o')
ax.scatter(metric1[not_sign_hypo_gd0], metric2[not_sign_hypo_gd0], c='grey', label='not_sign_hypo_gd0', marker='x')
ax.scatter(metric1[not_sign_hypo_gd1], metric2[not_sign_hypo_gd1], label='not_sign_hypo_gd1', marker='o', facecolor='none', edgecolors='grey')
ax.scatter(metric1[not_sign_hypo_gd2], metric2[not_sign_hypo_gd2], c='grey', label='not_sign_hypo_gd2', marker='o')
ax.scatter(metric1[not_sign_hyper_gd0], metric2[not_sign_hyper_gd0], c='grey', label='not_sign_hyper_gd0', marker='x')
ax.scatter(metric1[not_sign_hyper_gd1], metric2[not_sign_hyper_gd1], label='not_sign_hyper_gd1', marker='o', facecolor='none', edgecolors='grey')
ax.scatter(metric1[not_sign_hyper_gd2], metric2[not_sign_hyper_gd2], c='grey', label='not_sign_hyper_gd2', marker='o')




# ax.scatter(metric[(pvals>0.05) & (gd==0)], tvals[(pvals>0.05) & (gd==0)], c='b', label='gd- & p>0.05', marker='x')
# ax.scatter(metric[(pvals<0.05) & (gd==0)], tvals[(pvals<0.05) & (gd==0)], c='r', label='gd- & p<0.05', marker='x')
# ax.scatter(metric[(pvals>0.05) & (gd==1)], tvals[(pvals>0.05) & (gd==1)], label='rim & p>0.05', marker='o', facecolor='none', edgecolors='b')
# ax.scatter(metric[(pvals<0.05) & (gd==1)], tvals[(pvals<0.05) & (gd==1)], label='rim & p<0.05', marker='o', facecolor='none', edgecolors='r')
# ax.scatter(metric[(pvals>0.05) & (gd==2)], tvals[(pvals>0.05) & (gd==2)], c='b', label='nodule & p>0.05', marker='o')
# ax.scatter(metric[(pvals<0.05) & (gd==2)], tvals[(pvals<0.05) & (gd==2)], c='r', label='nodule & p<0.05', marker='o')
# for i in range(len(les_les)):
#     ax.text(vols[i], tvals[i], les_les[i], color='k')
ax.legend()
ax.set_xlabel(metric_name1)
ax.set_ylabel(metric_name2)

# %% Adding distance maps adding gado adding mtr and diffusion boxplots

TR = {'P_BENMO': 37,
      'P_BARLA': 42,
      'P_BOUJE': 47,
      'P_LABDU': 49,
      'P_ISSLO': 50,
      'P_NICVA': 56,
      'P_ABIAB': 70,
      'P_ZEMSA': 83,
      'P_BOUBR': 84,
      'P_DILAN': 89,
      'P_VIDVI': 91,
      'P_TAZKA': 97,
      'P_TROAR': 98,
      'P_LAANA': 99,
      'P_SIFIR': 100,
      'P_ELONA': 104,
      'P_MARAU': 120}
subjects = ["P_ABIAB", "P_BARLA", "P_BENMO", "P_BOUBR", "P_BOUJE", "P_DILAN",
            "P_ELONA", "P_ISSLO", "P_LAANA", "P_LABDU", "P_MARAU", "P_NICVA",
            "P_SIFIR", "P_TAZKA", "P_TROAR", "P_VIDVI", "P_ZEMSA"]

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELESIONS/SH\
ADOWTEP/EMAs_prelesion_masks_individual_ttest_with_dm_with_gd_with_MDR_RD_MTR_FA_L1.csv'
df = pd.read_csv(CLP)
TRs = np.zeros(len(df))
diffs = np.zeros(len(df))
vols = df['preles_vol'].to_numpy()
pvals = df['R1pval'].to_numpy()
gd = df['gd_rim1_nod_2'].to_numpy()
tvals = df['R1tval'].to_numpy()
dm_wm = df['dm_wm'].to_numpy()
dm_cx = df['dm_cx'].to_numpy()
dm_vt = df['dm_vt'].to_numpy()
les_subject = df['Subject'].to_list()
les_nums = df['les_num'].to_list()
les_les = [m + '_' + str(n) for m, n in zip(les_subject, les_nums)]
for idx, lesion in df.iterrows():
    s = lesion['Subject']
    TRs[idx] = TR[s]

# fig = plt.figure(1)
# fig.clear()
# ax = fig.add_subplot(111)

hyper_df = df[(df['R1tval'] < 0)]

def mscatter(x,y,ax=None, m=None, **kw):
    import matplotlib.markers as mmarkers
    if not ax: ax=plt.gca()
    sc = ax.scatter(x,y,**kw)
    if (m is not None) and (len(m)==len(x)):
        paths = []
        for marker in m:
            if isinstance(marker, mmarkers.MarkerStyle):
                marker_obj = marker
            else:
                marker_obj = mmarkers.MarkerStyle(marker)
            path = marker_obj.get_path().transformed(
                        marker_obj.get_transform())
            paths.append(path)
        sc.set_paths(paths)
    return sc


def plot_df(df, metric_name1, metric_name2, significance='R1pval'):
    fig = plt.figure(1)
    fig.clear()
    ax = fig.add_subplot(111)
    metric1 = df[metric_name1]
    metric2 = df[metric_name2]
    markers = np.where(df['gd_rim1_nod_2'] == 0, 'X', np.where(df['gd_rim1_nod_2'] == 1, 'o', 'P'))
    colors = np.where((df[significance] < 0.05)&(df['R1tval'] < 0), 'b', np.where((df[significance] < 0.05)&(df['R1tval'] > 0), 'r', 'none'))
    mscatter(metric1, metric2, ax, c=colors, m=markers)
    ax.set_xlabel(metric_name1)
    ax.set_ylabel(metric_name2)

# plot_df(df, 'R1', 'FA')


def boxplot_df(df, metric_name):
    fig = plt.figure(2)
    fig.clear()
    ax = fig.add_subplot(111)
    df_hyper = df[(df['R1tval'] > 0) & (df['R1pval'] < 0.05)]
    df_hyper['type'] = 'hyper'
    df_hypo = df[(df['R1tval'] < 0) & (df['R1pval'] < 0.05)]
    df_hypo['type'] = 'hypo'
    df = pd.concat([df_hypo, df_hyper])
    sns.boxplot(x='type', y=metric_name, data=df)
    g = sns.stripplot(x='type', y=metric_name, data=df, color='k')
    g.set(xlabel=None)
    tval, pval = ttest_ind(df_hyper[metric_name], df_hypo[metric_name], nan_policy='omit')
    plt.title('pval={:.3f}, tval={:+.3f}'.format(pval, tval))
    plt.savefig('/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELESIONS/SHADOWTEP/images/hypo_vs_hyper/' + metric_name + '.png')

for m in ['MD', 'R1', 'MTR', 'FA', 'L1', 'RD', 'dm_vt', 'dm_cx', 'dm_wm']:
    boxplot_df(df, m)

# boxplot_df(df, 'gd_rim1_nod_2')

# metric_name='MD'
# fig = plt.figure(2)
# fig.clear()
# ax = fig.add_subplot(111)
# df_hyper = df[(df['R1tval'] > 0) & (df['R1pval'] < 0.05)]
# df_hyper['type'] = 'hyper'
# df_hypo = df[(df['R1tval'] < 0) & (df['R1pval'] < 0.05)]
# df_hypo['type'] = 'hypo'
# df = pd.concat([df_hypo, df_hyper])
# sns.boxplot(x='type', y=metric_name, data=df)
# g = sns.stripplot(x='type', y=metric_name, data=df, color='k')
# g.set(xlabel=None)
# tval, pval = ttest_ind(df_hyper[metric_name], df_hypo[metric_name])
# plt.title('pval={:.3f}, tval={:+.2e}'.format(pval, tval))