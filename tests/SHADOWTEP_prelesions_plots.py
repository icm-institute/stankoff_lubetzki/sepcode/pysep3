#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 28 11:21:37 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from statsmodels.stats import weightstats
from scipy.stats import ttest_ind
from scipy.optimize import curve_fit
from lmfit import Model


# %% whole masks
CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELESIONS/SH\
ADOWTEP/prelesion_masks_untouched.csv'
df = pd.read_csv(CLP)

plt.figure(1)
plt.clf()
plt.title("R1 in prelesions vs controlateral NAWM")
plt.plot(df.index, df.R1, marker='s', label='R1 in prelesions')
plt.plot(df.index, df.cR1, marker='s', label='R1 in controlateral NAWM')
plt.xticks(df.index, df.Subject, rotation=45)
plt.legend()


# %% whole mask eroded by dilated cortex
CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELESIONS/SH\
ADOWTEP/prelesion_masks_eroded_by_cortex.csv'
df = pd.read_csv(CLP)

plt.figure(2)
plt.clf()
plt.title("R1 in prelesions vs controlateral NAWM")
plt.plot(df.index, df.R1, marker='s', label='R1 in prelesions')
plt.plot(df.index, df.cR1, marker='s', label='R1 in controlateral NAWM')
plt.xticks(df.index, df.Subject, rotation=45)
plt.legend()

# %% whole contr mask eroded by dilated cortex
CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELESIONS/SH\
ADOWTEP/prelesion_contr_masks_eroded_by_cortex.csv'
df = pd.read_csv(CLP)

plt.figure(3)
plt.clf()
plt.title("R1 in prelesions vs controlateral NAWM")
plt.plot(df.index, df.R1, marker='s', label='R1 in prelesions')
plt.plot(df.index, df.cR1, marker='s', label='R1 in controlateral NAWM')
plt.xticks(df.index, df.Subject, rotation=45)
plt.legend()

# %% whole contr mask eroded by dilated cortex
CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELESIONS/SH\
ADOWTEP/EMAs_prelesion_masks_eroded_by_cortex.csv'
df = pd.read_csv(CLP)
# df = df.drop(df[df.contr_vol < 40].index)
# df = df.drop(df[df.preles_vol < 40].index)
plt.figure(4)
plt.clf()
plt.title("R1 in prelesions vs controlateral NAWM")
plt.plot(df.index, df.R1, marker='s', label='R1 in prelesions')
plt.plot(df.index, df.cR1, marker='s', label='R1 in controlateral NAWM')
plt.xticks(df.index, df.Subject, rotation=45)
# plt.figure(3)
# df.boxplot(['R1', 'cR1'])
print(ttest_ind(df.R1, df.cR1))
# print(weightstats.ttest_ind(df.R1, df.cR1, weights=(df.preles_vol, df.preles_vol)))
# print(weightstats.ttest_ind(df.R1, df.cR1, weights=(df.preles_vol/np.sum(df.preles_vol), df.preles_vol/np.sum(df.preles_vol))))
# plt.legend()


# import statsmodels.api as sm
# import statsmodels.formula.api as smf

# md = smf.mixedlm("contr ~ R1 + vol ", df, groups=df["Subject"])
# mdf = md.fit()
# print(mdf.summary())