#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 25 15:03:08 2021

@author: arya.yazdan-panah
"""

import os
import shutil
import numpy as np
import nibabel as nib
from pprint import pprint
from common3 import createDir, get_regex, changeName
from tools_ants import ApplyRegistration
from tools_c3 import fsl2itk
from scipy.ndimage import distance_transform_edt as dist

subjects = ["P_ABIAB", "P_BARLA", "P_BENMO", "P_BOUBR", "P_BOUJE", "P_DILAN",
            "P_ELONA", "P_ISSLO", "P_LAANA", "P_LABDU", "P_MARAU", "P_NICVA",
            "P_SIFIR", "P_TAZKA", "P_TROAR", "P_VIDVI", "P_ZEMSA"]
# subjects = ["P_ISSLO"]
preles_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELE\
SIONS/SHADOWTEP/data'
ema = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Emanuele/PRELESION\
2/data/process'
theo = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Theodore/data2022'
shad = '/network/lustre/iss01/lubetzki-stankoff/clinic/SHADOWTEP/data/process'
gado_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Theodore/for_arya/gado_shadow_rim_nodule'
mat_gado_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Theodore/for_arya/matrices_gd_V02_shadow'

# def alternative_distance_map(inimage,outimage):
#     indata=nib.load(inimage).get_fdata()
#     affine=nib.load(inimage).affine
#     outdata=dist(indata)
#     data_creat = nib.Nifti1Image(outdata, affine)
#     nib.save(data_creat, outimage)


for s in subjects:
    print(s)
    if s == 'P_ISSLO':
        continue
    mydir = os.path.join(preles_dir, s)
    ema_dir = os.path.join(ema, s)
    ema_vis = os.path.join(ema_dir, 'V01', 'lesions')
    theo_dir = os.path.join(theo, s)
    shadowtep_t1preproc = os.path.join(shad, s, 'V01', 't1preproc')
    # Masks/image to register
    # preles_mask = get_regex(ema_vis, '_COR_roimask_fuzzy_2label_last_EXP3.nii.gz', 0)[0]
    # contr_mask = get_regex(ema_vis, '_COR_controlatNAWM_2label_final_EXP3_ero1.nii.gz', 0)[0]
    # rim_mask = get_regex(gado_dir, s + '_V02_rim')[0]
    # nodule_mask = get_regex(gado_dir, s + '_V02_nodule')[0]
    V02_gd = get_regex(mat_gado_dir, s + '_V02_t1gd_in_t1')[0]
    # Matrices
    V01_t1_2hws = get_regex(mydir, 'V01_t1_pre_2hws_ITK')[0]
    t1_pre = get_regex(shadowtep_t1preproc, 't1_pre$')[0]
    V02_t1_2hws = get_regex(mydir, 'V02_t1_pre_2hws_ITK')[0]
    gd2t1_V02 = get_regex(mat_gado_dir, s + '_V02_t1gd_in_t10GenericAffine')[0]
    for visit in ['V01']:
        mydir_vis = os.path.join(mydir, visit)
        # theodir_vis = os.path.join(theo_dir, visit, 'recalages_mni', 'trans_mat')
        # t1_mni = get_regex(theodir_vis, '(0GenericAffine|1InverseWarp)', 0)
        # if not t1_mni:
        #     continue
        # t1_mni_mat = t1_mni[0]
        # t1_mni_warp = t1_mni[1]
        # pib_t1 = get_regex(mydir_vis, visit + '_pib_movcorr_2t1_itk.mat', 0)[0]
        # R1 = get_regex(mydir_vis, 'R1.nii.gz', 0)[0]

        warp_t1gd = changeName(V02_gd, '_2t1_' + visit, mydir_vis)
        ApplyRegistration(V02_gd, t1_pre, [gd2t1_V02, V02_t1_2hws, V01_t1_2hws],
                          warp_t1gd, [False, False, True], 'Linear', over=True)

        # warp_rim = changeName(rim_mask, '_2t1_' + visit, mydir_vis)
        # ApplyRegistration(rim_mask, t1_pre, [gd2t1_V02, V02_t1_2hws, V01_t1_2hws],
        #                   warp_rim, [False, False, True], 'NearestNeighbor', over=True)
        # warp_nodule = changeName(nodule_mask, '_2t1_' + visit, mydir_vis)
        # ApplyRegistration(nodule_mask, t1_pre, [gd2t1_V02, V02_t1_2hws, V01_t1_2hws],
        #                   warp_nodule, [False, False, True], 'NearestNeighbor', over=True)

        # warp_pre = changeName(preles_mask, '_2t1_' + visit, mydir_vis)
        # ApplyRegistration(preles_mask, t1_pre, V01_t1_2hws, warp_pre, [True], 'NearestNeighbor')
        # warp_con = changeName(contr_mask, '_2t1_' + visit, mydir_vis)
        # ApplyRegistration(contr_mask, t1_pre, V01_t1_2hws, warp_con, [True], 'NearestNeighbor')
