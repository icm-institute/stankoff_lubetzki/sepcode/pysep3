#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 16:47:25 2020

@author: arya.yazdan-panah
"""

import os
import numpy as np
import matlab.engine as me
from common3 import get_regex, createDir, readSubjectsLists

np.seterr(divide='ignore', invalid='ignore')
# =============================================================================
subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/u'
                             'sers/Arya/PIB_retest/utils/All_V01.list')
proc = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/d'
proc = proc + 'ata/process'
# =============================================================================
# Output Directories
out_general = os.path.join(proc, 'TR_new_MNI')
conv_dir = os.path.join(out_general, 'convolutions')
IMSV01 = []
IMSV02 = []
for s in subjects:
    simages = os.path.join(conv_dir, s['Id'])
    images1 = get_regex(simages, 'V01')
    images1.sort()
    IMSV01.append(images1)
    images2 = get_regex(simages, 'V02')
    images2.sort()
    IMSV02.append(images2)
icc_dir = os.path.join(out_general, 'ICCs')
createDir(icc_dir)
IMSV01 = list(zip(*IMSV01))
IMSV02 = list(zip(*IMSV02))
IMS = list(zip(IMSV01, IMSV02))
matlab = me.start_matlab()
for ims in IMS:
    name = ims[0][0][121:]
    out = os.path.join(icc_dir, 'ICC' + name)
    if not os.path.isfile(out):
        matlab.ICC3D(ims[0], ims[1], out, nargout=0)
