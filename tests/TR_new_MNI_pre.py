#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 14 15:46:27 2020

@author: arya.yazdan-panah
"""


import os
from importlib import import_module
from pprint import pprint
from common3 import get_regex, createDir, changeName, readSubjectsLists
from tools_ants import ApplyRegistration


# =============================================================================
subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/u'
                             'sers/Arya/PIB_retest/utils/All.list')
proc = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/d'
proc = proc + 'ata/process'
# =============================================================================
# Retrieving mni just in case
MNI = getattr(import_module("atlases"), "mni152_sym09c")
MNI = MNI.brain
# =============================================================================
# What image to process and in which folder is it ?
in_dirname = 'pibSRTM2_post_sm'
# =============================================================================
# Output Directories
out_general = os.path.join(proc, 'TR_new_MNI')
createDir(out_general)
conv_dir = os.path.join(out_general, 'convolutions_smoothed')
createDir(conv_dir)
rela_dir = os.path.join(out_general, 'relative_differences')
createDir(rela_dir)
masks_dir = os.path.join(out_general, 'masks')
createDir(masks_dir)
mmask = os.path.join(masks_dir, 'mean')
createDir(mmask)
icc_dir = os.path.join(out_general, 'ICCs')
# =============================================================================
# Defining all convolution shapes
shapes = []
# =============================================================================
for s in subjects:
    print(s['Id'])
    V01 = os.path.join(proc, s['Id'], 'V01')
    Vis = os.path.join(proc, s['Id'], s['Visit'])
    IMs = []
    SRTM_dir = os.path.join(Vis, in_dirname)
    if not os.path.isdir(SRTM_dir):
        continue
    IMs = get_regex(SRTM_dir, '(k2.nii|R1.nii)', False, False)
    IMs.sort()
    logan_dir = os.path.join(Vis, 'piblogan_sm2_5')
    logan = get_regex(logan_dir, '_pib_logan_vt')[0]
    IMs.append(logan)
    suv_dir = os.path.join(Vis, 'pibearlySUVR')
    suvr = get_regex(suv_dir, '_SUVR_sm2_5_0_to_120')
    IMs.extend(suvr)
    suvr = get_regex(suv_dir, '_SUVR_sm2_5_1_to_8')
    IMs.extend(suvr)
    suvr = get_regex(suv_dir, '_SUVR_sm2_5_50_to_70')
    IMs.extend(suvr)
# =============================================================================
    # TRANSFORMATIONS PET to T1
    mat_2t1 = get_regex(Vis, '_pib_movcorr_mean_2t1_ITK')[0]
    reg2SS = os.path.join(V01, 't1Ants2SS')
    # TRANSFORMATIONS T1 to MNI
    T1_2mni_affine = get_regex(reg2SS, '_t1_pre_brain_2mni0GenericAffine')[0]
    T1_2mni_warp = get_regex(reg2SS, '_t1_pre_brain_2mni1Warp')[0]
    pib_2mni = [T1_2mni_warp, T1_2mni_affine, mat_2t1]
    t1_2mni = [T1_2mni_warp, T1_2mni_affine]
# =============================================================================
    # reg and convolve all images
    ims_2mni = os.path.join(conv_dir, s['Id'])
    createDir(ims_2mni)
    for im in IMs:
        print(os.path.basename(im))
        im2mni = changeName(im, '_2MNI', ims_2mni)
        if not os.path.isfile(im2mni):
            ApplyRegistration(im, MNI, pib_2mni, im2mni, interp='Linear')
# =============================================================================
    # Finding brain masks and other ROIS and convolving them
    # t1ROI = os.path.join(Vis, 't1ROI')
    # all_masks = get_regex(t1ROI, '.nii.gz', False)
    # masks_2mni = os.path.join(masks_dir, s['Id'])
    # createDir(masks_2mni)
    # for m in all_masks:
    #     print('    ', os.path.basename(m))
    #     mask2mni = changeName(m, '_2MNI', masks_2mni)
    #     if not os.path.isfile(mask2mni):
    #         ApplyRegistration(m, MNI, t1_2mni, mask2mni,
    #                           interp='NearestNeighbor')
