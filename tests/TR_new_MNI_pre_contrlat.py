#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 17:51:46 2021

@author: arya.yazdan-panah
"""


import os
from importlib import import_module
from common3 import get_regex, createDir, changeName, readSubjectsLists
from tools_ants import ApplyRegistration, flip_nii_reg


# =============================================================================
subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/u'
                             'sers/Arya/PIB_retest/utils/All.list')
proc = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/d'
proc = proc + 'ata/process'
# =============================================================================
# Retrieving mni just in case
MNI = getattr(import_module("atlases"), "mni152_sym09c")
MNI = MNI.brain
# =============================================================================
# What image to process and in which folder is it ?
in_dirname = 'pibSRTM2final'
# =============================================================================
# Output Directories
out_general = os.path.join(proc, 'TR_new_MNI')
createDir(out_general)
conv_dir = os.path.join(out_general, 'convolutions')
createDir(conv_dir)
flip_dir = os.path.join(conv_dir, 'flipped')
createDir(flip_dir)
masks_dir = os.path.join(out_general, 'masks')
createDir(masks_dir)
# =============================================================================
for s in subjects:
    print(s['Id'])
    V01 = os.path.join(proc, s['Id'], 'V01')
    Vis = os.path.join(proc, s['Id'], s['Visit'])
    SRTM_dir = os.path.join(Vis, in_dirname)
    if not os.path.isdir(SRTM_dir):
        continue
    IMs = get_regex(SRTM_dir, '(k2.nii|R1.nii)', False, False)
    IMs.sort()
    logan_dir = os.path.join(Vis, 'piblogan')
    logan = get_regex(logan_dir, '_pib_logan_vt')[0]
    IMs.append(logan)
    suv_dir = os.path.join(Vis, 'pibearlySUVR')
    suvr = get_regex(suv_dir, '_SUVR_.._to')
    IMs.extend(suvr)
# =============================================================================
    reg2SS = os.path.join(V01, 't1Ants2SS')
    T1 = get_regex(reg2SS, 't1_pre_brain_2mni.nii.gz', False)[0]
    T1_flipped = changeName(T1, '_mirrored')
    if not os.path.isfile(T1_flipped):
        T1_flipped, trans_flip = flip_nii_reg(T1, T1_flipped)
    else:
        trans_flip = get_regex(reg2SS,
                               '_t1_pre_brain_2mni_mirrored0GenericAffine.mat',
                               False)[0]
# =============================================================================
    # TRANSFORMATIONS PET to T1
    mat_2t1 = get_regex(Vis, '_pib_movcorr_mean_2t1_ITK')[0]
    reg2SS = os.path.join(V01, 't1Ants2SS')
    # TRANSFORMATIONS T1 to MNI
    T1_2mni_affine = get_regex(reg2SS, '_t1_pre_brain_2mni0GenericAffine')[0]
    T1_2mni_warp = get_regex(reg2SS, '_t1_pre_brain_2mni1Warp')[0]
    pib_2mni = [T1_2mni_warp, T1_2mni_affine, mat_2t1]
    t1_2mni = [T1_2mni_warp, T1_2mni_affine]
# =============================================================================
    # reg and convolve all images
    ims_2mni = os.path.join(conv_dir, 'flipped', s['Id'])
    createDir(ims_2mni)
    for im in IMs:
        print(os.path.basename(im))
        im2mni = changeName(im, '_2MNI_flipped', ims_2mni)
        im2mnitmp = changeName(im, '_2MNI_flipped_tmp', ims_2mni)
        if not os.path.isfile(im2mni):
            ApplyRegistration(im, MNI, pib_2mni, im2mnitmp, interp='Linear')
            flip_nii_reg(im2mnitmp, im2mni, trans_flip)
            os.remove(im2mnitmp)
# =============================================================================
    # Finding brain masks and other ROIS and convolving them
    t1ROI = os.path.join(Vis, 't1ROI')
    all_masks = get_regex(t1ROI, '.nii.gz', False)
    masks_2mni = os.path.join(masks_dir, 'flipped', s['Id'])
    createDir(masks_2mni)
    for m in all_masks:
        print('    ', os.path.basename(m))
        mask2mni = changeName(m, '_2MNI_flipped', masks_2mni)
        mask2mnitmp = changeName(m, '_2MNI_flipped_tmp', masks_2mni)
        if not os.path.isfile(mask2mni):
            ApplyRegistration(m, MNI, t1_2mni, mask2mnitmp,
                              interp='NearestNeighbor')
            flip_nii_reg(mask2mnitmp, mask2mni, trans_flip,
                         interp='NearestNeighbor')
            os.remove(mask2mnitmp)
