# -*- coding: utf-8 -*-

"""

Created on Thu Aug 26 14:43:59 2021



@author: theodore

"""



from optparse import OptionParser,OptionGroup

import nibabel as nib

import numpy as np
from common3 import changeName
import argparse



control_perfusion = ["/home/arya.yazdan-panah/Bureau/anna_controls/C_ARIIS_V01_R1_2MNI.nii.gz",
"/home/arya.yazdan-panah/Bureau/anna_controls/C_BENFL_V01_R1_2MNI.nii.gz",
"/home/arya.yazdan-panah/Bureau/anna_controls/C_BENMA_V01_R1_2MNI.nii.gz",
"/home/arya.yazdan-panah/Bureau/anna_controls/C_CHRKE_V01_R1_2MNI.nii.gz",
"/home/arya.yazdan-panah/Bureau/anna_controls/C_DUBAN_V01_R1_2MNI.nii.gz",
"/home/arya.yazdan-panah/Bureau/anna_controls/C_GUECO_V01_R1_2MNI.nii.gz",
"/home/arya.yazdan-panah/Bureau/anna_controls/C_LEBJO_V01_R1_2MNI.nii.gz",
"/home/arya.yazdan-panah/Bureau/anna_controls/C_LEVVA_V01_R1_2MNI.nii.gz"]

for perf in control_perfusion:
# load options & images
    input_path = perf
    mask = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_new_MNI/masks/all_subjects_WM.nii.gz"
    output_name = changeName(perf, '_norm', '/home/arya.yazdan-panah/Bureau/anna_controls/norm')
    dataload=nib.load(input_path)
    data_in=dataload.get_fdata()
    affine=dataload.affine
    mask_data=nib.load(mask).get_fdata().astype(bool)
# extracts the mean and std from reference region
    mask_quantit = data_in[mask_data]
    mean = np.mean(mask_quantit)
    std  = np.std(mask_quantit)
    print("mean : ",mean)
    print("std  : ",std)
# recomputes the whole image and saves it :
    data_out=data_in-mean
    data_out=data_out/std
    outimage=nib.Nifti1Image(data_out,affine)
    nib.save(outimage,output_name)
    print("image saved as ",output_name)
