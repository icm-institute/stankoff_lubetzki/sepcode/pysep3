#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 14 17:22:00 2021

@author: arya.yazdan-panah
"""

import os
from subprocess import call


def changeName(filename, suffix=None, outdir=None, newext=None, prefix=None,
               remove=None):
    # do fileparts
    name = os.path.basename(filename)
    path = os.path.dirname(filename)
    ext1 = os.path.splitext(name)[1]
    ext2 = os.path.splitext(os.path.splitext(name)[0])[1]
    name = os.path.splitext(os.path.splitext(name)[0])[0]
    if newext is not None:
        ext = newext
        if not ext.startswith('.') and ext != '':
            ext = '.' + ext
    elif ext2:
        ext = ext2 + ext1
    elif ext1:
        ext = ext1
    else:
        ext = ''
    if remove:
        start = name.find(remove)
        if 0 <= start:
            end = start + len(remove)
            name = name[:start] + name[end:]
    if prefix:
        name = prefix + name
    if suffix:
        name = name + suffix
    if outdir is not None:
        path = outdir
    return os.path.join(path, name + ext)


def command(commandline, inputs=None, outputs=None, clfile=None,
            logfile=None, verbose=True, timecheck=False):
    strline = ' '.join(commandline)
    if verbose:
        print(strline)
    # Check newer input file
    itime = -1  # numer of seconds since epoch
    inputs_exist = True
    if inputs is not None:
        # check if input is only string and not list
        if isinstance(inputs, str):
            if not os.path.exists(inputs):
                inputs_exist = False
                print(' ** Error: Input does not exist! :: ' + str(inputs))
            else:
                itime = os.path.getmtime(inputs)
        else:
            for i in inputs:
                if not os.path.exists(i):
                    inputs_exist = False
                    print(' ** Error: One input does not exist! :: ' + i)
                else:
                    timer = os.path.getmtime(i)
                    if timer < itime or itime < 0:
                        itime = timer
    # Check if outputs exist AND is newer than inputs
    outExists = False
    otime = -1
    if outputs is not None:
        if isinstance(outputs, str):
            outExists = os.path.exists(outputs)
            if outExists:
                otime = os.path.getmtime(outputs)
        else:
            for o in outputs:
                outExists = os.path.exists(o)
                if outExists:
                    timer = os.path.getmtime(o)
                    if timer > otime:
                        otime = timer
                if not outExists:
                    break
    if outExists:
        if timecheck and itime > 0 and otime > 0 and otime < itime:
            if verbose:
                print(' -- Warning: Output exists but older than input! Redoin'
                      'g command')
                print('     otime ' + str(otime) + ' < itime ' + str(itime))
        else:
            if verbose:
                print(' -- Skipping: Output Exists')
            return 0
    # Check if inputs exist
    if inputs is not None and inputs_exist is False:
        print(' ** Error  in ' + commandline[0] + ':The input does not exists!\
              :: ' + str(inputs))
        return -1
    # run command
    # open comand line file
    if clfile is not None:
        f = open(clfile, 'a')
        f.write(strline + '\n')
        f.close()
    try:
        if logfile is not None:
            f = open(logfile, 'a')
            outvalue = call(commandline, stdout=f)
            f.close()
        else:
            outvalue = call(commandline)
    except OSError:
        print(' XX ERROR: unable to find executable ' + commandline[0])
        return -1
    if not outvalue == 0:
        print(' ** Error  in ' + commandline[0] + ': Executable output was',
              str(outvalue))
        return outvalue
    outExists = False
    if outputs is None:
        outExists = True
    elif isinstance(outputs, str):
        outExists = os.path.exists(outputs)
    else:
        for o in outputs:
            outExists = os.path.exists(o)
            if not outExists:
                break
    if not outExists:
        if verbose:
            print(' ** Error  in ' + commandline[0] + ': Error: output does no'
                  't exist!')
            return -1
    # return command output
    return outvalue


# change paths here to those of the file you downloaded
mean_perf_map = "/home/arya.yazdan-panah/Bureau/anna_controls/norm/mean_R1_2MNI_norm.nii.gz"
std_perf_map = "/home/arya.yazdan-panah/Bureau/anna_controls/norm/std_R1_2MNI_norm.nii.gz"

# change to output of theodore's script
input_image_to_zscore = "path/to/normalized/image.nii.gz"
output_path = changeName(input_image_to_zscore, '_zscore')

cmd = ['fslmaths', input_image_to_zscore, '-sub', mean_perf_map, '-div',
       std_perf_map]
command(cmd)