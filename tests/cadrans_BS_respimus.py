#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 15 14:52:53 2021

@author: arya.yazdan-panah
"""

from common3 import get_regex, readSubjectsLists
import os
import nibabel as nib
import numpy as np

process = "/network/lustre/iss01/lubetzki-stankoff/clinic/RESPIMUS/data/process"
subject_list = "/network/lustre/iss01/lubetzki-stankoff/clinic/RESPIMUS/utils/Brainstem.list"
sids = readSubjectsLists(subject_list)


for s in sids:
    # get your mask (brainstem)
    doss_suj = os.path.join(process, s['Id'], s['Visit'], 't1ROI')
    mask = get_regex(doss_suj, "brainstemSsLabels1.v12")[0]
    print(s["Id"])
    mat_mask = nib.load(mask).get_fdata()
    posterior = mat_mask * 0
    anterior = mat_mask * 0
    aff_mask = nib.load(mask).affine
    vox_tronc = np.where(mat_mask > 0)
    x_median = int(np.median(vox_tronc[0]))
    y_median = int(np.median(vox_tronc[1]))
    z_median = int(np.median(vox_tronc[2]))
