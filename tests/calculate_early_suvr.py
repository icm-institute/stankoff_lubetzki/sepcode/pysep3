#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 13:49:24 2021

@author: arya.yazdan-panah
"""

import os
import nibabel as nib
import numpy as np
from tools_fsl import fslmean, fslroi
from tools_svca import TAC
from common3 import get_regex, changeName
from pprint import pprint

p = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process'
A = get_regex(p, 'pibearlySUVR$', False)
pprint(A)

B = get_regex(p, '_SUVR.nii.gz', False)
pprint(B)


# %%
for a, b in zip(A, B):
    pet = get_regex(os.path.dirname(a), '_pib_movcorr_sm2_5.nii.gz', 0)[0]
    ref_reg = get_regex(os.path.join(os.path.dirname(a), 'pibsuper_sm2_5'), '_pib_refregion.nii', 0)[0]
    tep = nib.load(pet)
    info = tep.affine
    tep = tep.get_fdata()
    tac = TAC(tep, ref_reg)
    data = np.zeros(tep.shape)
    for i in range(len(tac)):
        data[:,:,:,i]  = tep[:,:,:,i] / tac[i]
    nift1 = nib.Nifti1Image(data, info)
    out_name = changeName(b, '_sm2_5')
    nib.save(nift1, out_name)

# %%
import os
import nibabel as nib
import numpy as np
from tools_fsl import fslmean, fslroi
from tools_svca import TAC
from common3 import get_regex, changeName
from pprint import pprint


p = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process'
B = get_regex(p, '_SUVR_sm2_5.nii.gz', False)
pprint(B)

for b in B:
    dst = changeName(b, '_50_to_70')
    fslroi(b, dst, 20, 3)
    fslmean(dst, dst)
    dst = changeName(b, '_1_to_8')
    fslroi(b, dst, 1, 6)
    fslmean(dst, dst)
    dst = changeName(b, '_0_to_120')
    fslroi(b, dst, 0, 2)
    fslmean(dst, dst)
