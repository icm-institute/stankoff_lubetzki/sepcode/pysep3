#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 21 11:51:28 2021

@author: arya.yazdan-panah
"""

import os
import numpy as np
import nibabel as nib
from common3 import get_regex, readSubjectsLists

p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/genotypes.list')
for genotype in ['LAB']:
    N = 0
    A = get_regex('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data', 'suv_' + genotype)[0]
    Mean = nib.load(A)
    affine = Mean.affine
    Mean = Mean.get_fdata()
    Std = np.zeros(Mean.shape)
    for s in subjects:
        print(s['Id'], s['Visit'])
        if s['extra'] == genotype:
            sdir = os.path.join(p, s['Id'], s['Visit'], 't1Ants2SS')
            suv = get_regex(sdir, 'suv_2mni.nii.gz', False)
            SUV = nib.load(suv[0]).get_fdata()
            tmp = np.square(np.subtract(SUV, Mean))
            Std = np.add(Std, tmp)
            N += 1
        else:
            continue
    Std = np.sqrt(np.divide(Std, N))
    IMG = nib.Nifti1Image(Std, affine)
    nib.save(IMG, '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/std_' + genotype + '.nii.gz')

