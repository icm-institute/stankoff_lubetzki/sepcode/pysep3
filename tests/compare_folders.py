#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 22 11:13:52 2020

@author: arya.yazdan-panah
"""


from common3 import get_regex
import os
import filecmp

process1 = "/home/arya.yazdan-panah/Images/INNMS/data/process"
A = get_regex(process1, "V0.$")
A.sort()
process2 = "/network/lustre/iss01/lubetzki-stankoff/clinic/INNMS/data/process"
B = get_regex(process2, "V0.$")
B.sort()
for i, j in enumerate(A):
    print("")
    print(j)
    print(B[i])
    comp = filecmp.dircmp(j,B[i])
    print("LEFT")
    print(comp.left_only)
    print("RIGHT")
    print(comp.right_only)