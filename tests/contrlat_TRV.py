#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 14:58:26 2021

@author: arya.yazdan-panah
"""

import os
import sys
import shutil
from tools_fsl import fslmaths, fslstats
from tools_ants import flip_nii_reg
from tools_nibabel import relative_difference
from common3 import get_regex, createDir, init_script, changeName
from termcolor import cprint, colored
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

MSG = "Quantify PET into relative delivery (perfusion) maps and DVR using \
the SRTM2 model"

if __name__ == '__main__':
    cprint(os.path.basename(sys.argv[0]), attrs=['reverse', 'bold'])
# ---------------------------- Options definitions ----------------------------
    usage = "usage: %prog [opts]"
    parser = ArgumentParser(description=MSG,
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-s', '--subjects', dest='subjects',
                        help='Subjects list, if none choices will be provided')
    parser.add_argument("-p", "--process", dest="process",
                        help="Process directory, if none deduced from "
                        "PROJECT_DIR")
    parser.add_argument("-t", "--tracer", dest="tracer", default="pib",
                        help="Tracer used (dpa/pib/...)")
    options = parser.parse_args()
    init = init_script(options)
    process = init['process']
    subjects = init['subjects']
    tracer = options.tracer.lower()
    print(colored("| Tracer used:", "blue", attrs=["bold"]),
          tracer.upper())
# ------------------------------- processing  ---------------------------------
    for s in subjects:
        petSRTM2 = os.path.join(process, s['Id'], s['Visit'], tracer +
                                  'SRTM2final')
        test = get_regex(petSRTM2, 'R1.nii.gz', False, False)
        R1 = test[0]
        contr = os.path.join(process, s['Id'], s['Visit'], 'contrlat')
        # createDir(contr)
        # shutil.copy(R1, contr)
        R1 = changeName(R1, outdir=contr)
        R1_flip = changeName(R1, '_flipped_reg')
        # null, trans = flip_nii_reg(R1, R1_flip)
# ------------------------ Defining path to petSRTM ---------------------------
        TRV = changeName(R1, '_contr_TRV')
        # relative_difference(R1, R1_flip, TRV)
# --------------------- Defining path to petpreproc ------------------------
        petROI = os.path.join(process, s['Id'], s['Visit'], tracer + 'ROI')
        test = get_regex(petROI, 'aseg_wm_2pib_ero_ctx.nii.gz', False, False)
        WM = test[0]
        # shutil.copy(WM, contr)
        WM = changeName(WM, outdir=contr)
        WM_flip = changeName(WM, '_flipped_reg')
        # flip_nii_reg(WM, WM_flip, trans, interp='NearestNeighbor')
        WM_and = changeName(WM, '_and_flipped')
        # fslmaths(WM, '-mul', WM_flip, WM_and)
        print(fslstats(TRV, '-k', WM_and, '-M'))
