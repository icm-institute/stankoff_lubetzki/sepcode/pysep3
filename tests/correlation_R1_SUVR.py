#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 15:57:26 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from scipy.stats import pearsonr
from lmfit import Model


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/inflasep_lesions.csv'
df = pd.read_csv(CLP)
X = df["mSUVR1to8V02"].to_numpy()
Y = df["mSUVR1to8V01"].to_numpy()
plt.figure(1)
plt.scatter(X, Y)
print(pearsonr(X,Y))