#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 11:49:34 2021

@author: arya.yazdan-panah
"""

from common3 import get_regex
import os
import numpy as np
import pandas as pd
import nibabel as nib
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import ttest_ind
import csv



# %%
proc = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Emanuele/PRELESION2/data/process/'
file = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/prelesion.csv'

f = open(file, 'w', newline='')
fieldnames = ['Subject',
              'Lesion Name',
              'Lesion Volume',
              'Contr Volume',
              'FA',
              'MD',
              'RD',
              'AD',
              'MTR',
              'cFA',
              'cMD',
              'cRD',
              'cAD',
              'cMTR']
stats = csv.DictWriter(f, fieldnames)
stats.writeheader()

for i, name in enumerate(['P_ABIAB', 'P_BARLA', 'P_BENMO',
                          'P_BOUBR', 'P_BOUJE', 'P_DILAN', 'P_ELONA',
                          'P_ISSLO', 'P_LAANA', 'P_LABDU',
                          'P_MARAU', 'P_NICVA', 'P_SIFIR', 'P_TAZKA',
                          'P_TROAR', 'P_VIDVI', 'P_ZEMSA']):
    mA = np.nan
    mB = np.nan
    mC = np.nan
    mD = np.nan
    mE = np.nan
    mAc = np.nan
    mBc = np.nan
    mCc = np.nan
    mDc = np.nan
    mEc = np.nan
    les_dir = os.path.join(proc, name, 'V01', 'lesions')
    a = get_regex(les_dir, '_COR_roimask_fuzzy_2label_last_EXP3$')[0]
    b = get_regex(les_dir, '_COR_controlatNAWM_2label_final_EXP3_ero1$')[0]
    les = nib.load(a).get_fdata().astype(int)
    ctr = nib.load(b).get_fdata().astype(int)

    hws = os.path.join(proc, name, 'HWS_V01V02')
    fa = nib.load(get_regex(hws, '_COR_FA_2hws', False)[0]).get_fdata()
    md = nib.load(get_regex(hws, '_COR_MD_2hws', False)[0]).get_fdata()
    rd = nib.load(get_regex(hws, '_COR_RD_2hws', False)[0]).get_fdata()
    ad = nib.load(get_regex(hws, '_COR_AD_2hws', False)[0]).get_fdata()

    mt = get_regex(hws, '_COR_MTR_2hws', False)
    if mt:
        do_mt = True
        mt = nib.load(mt[0]).get_fdata()
        # mE = MTR x mask
        # mEc = MTR x contr
    else:
        do_mt = False
    for lab in range(1, np.max(les) + 1):
        mask = np.zeros(les.shape).astype(bool)
        cmask = np.zeros(les.shape).astype(bool)
        mask[les == lab] = True
        cmask[ctr == lab] = True
        lv = np.sum(mask)
        if lv < 8:
            continue
        cv = np.sum(cmask)
        mA = np.mean(fa[mask])
        mB = np.mean(md[mask])
        mC = np.mean(rd[mask])
        mD = np.mean(ad[mask])
        mAc = np.mean(fa[cmask])
        mBc = np.mean(md[cmask])
        mCc = np.mean(rd[cmask])
        mDc = np.mean(ad[cmask])
        if do_mt:
            mE = np.mean(mt[mask])
            mEc = np.mean(mt[cmask])
        stats.writerow({'Subject': name,
                        'Lesion Name': str(lab),
                        'Lesion Volume': lv,
                        'Contr Volume': cv,
                        'FA': mA,
                        'MD': mB,
                        'RD': mC,
                        'AD': mD,
                        'MTR': mE,
                        'cFA': mAc,
                        'cMD': mBc,
                        'cRD': mCc,
                        'cAD': mDc,
                        'cMTR': mEc})
f.close()
# %%

file = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/prelesion.csv'
df = pd.read_csv(file)

plt.close()
fig, axs = plt.subplots(3, 2)
sns.boxplot(ax=axs[0, 0], x=df['Lesion = 1 / Contr = 0'], y=df['FA'])
sns.boxplot(ax=axs[0, 1], x=df['Lesion = 1 / Contr = 0'], y=df['MD'])
sns.boxplot(ax=axs[1, 0], x=df['Lesion = 1 / Contr = 0'], y=df['RD'])
sns.boxplot(ax=axs[1, 1], x=df['Lesion = 1 / Contr = 0'], y=df['AD'])
sns.boxplot(ax=axs[2, 0], x=df['Lesion = 1 / Contr = 0'], y=df['MTR'])

print(ttest_ind(df.dropna()['FA'][:79], df.dropna()['FA'][79:]))
print(ttest_ind(df.dropna()['MD'][:79], df.dropna()['MD'][79:]))
print(ttest_ind(df.dropna()['RD'][:79], df.dropna()['RD'][79:]))
print(ttest_ind(df.dropna()['AD'][:79], df.dropna()['AD'][79:]))
print(ttest_ind(df.dropna()['MTR'][:79], df.dropna()['MTR'][79:]))
