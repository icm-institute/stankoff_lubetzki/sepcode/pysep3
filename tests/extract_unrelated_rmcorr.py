#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  8 18:11:38 2021

@author: arya.yazdan-panah
"""

import os
import csv
import numpy as np
import nibabel as nib
import pingouin as pg
import pandas as pd
from common3 import get_regex

# C'est une résolution formelle j'ai le droit... cheh
np.seterr(divide='ignore', invalid='ignore')
# =============================================================================
les_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retes\
t/data/process/labelled_inflasep_lesions'
# Lesions of the patients in MNI
Lesions = get_regex(les_dir, 'P_...../P_....._.*nii.gz', False, )
print("Number of Lesions:", len(Lesions))
# =============================================================================
subjects = ["C_ARIIS",
            "C_BENFL",
            "C_BENMA",
            "C_CHRKE",
            "C_DUBAN",
            "C_GUECO",
            "C_LEBJO",
            "C_LEVVA"]
S2 = ["C_ARIIS", "C_ARIIS",
      "C_BENFL", "C_BENFL",
      "C_BENMA", "C_BENMA",
      "C_CHRKE", "C_CHRKE",
      "C_DUBAN", "C_DUBAN",
      "C_GUECO", "C_GUECO",
      "C_LEBJO", "C_LEBJO",
      "C_LEVVA", "C_LEVVA"]
# =============================================================================
k2primes = [[0.06338108263, 0.05934295106],
            [0.06786313224, 0.06633747349],
            [0.06166487042, 0.06185613334],
            [0.05867357518, 0.0574208377],
            [0.06154755673, 0.0662559161],
            [0.06014615976, 0.06281320825],
            [0.06083698382, 0.05798011997],
            [0.06063100159, 0.05972947848]]
k2prime = [0, 0]
# =============================================================================
input_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_ret\
est/data/process/TR_new_MNI'
# =============================================================================
stats_dir = os.path.join(input_dir, 'stats')

convs_dir = os.path.join(input_dir, 'convolutions')

masks_dir = os.path.join(input_dir, 'masks')
# =============================================================================
stats_file = os.path.join(stats_dir, 'inflasep_lesions_rmcorr.csv')
wm_file = os.path.join(masks_dir, 'all_subjects_WM.nii.gz')
WM = nib.load(wm_file).get_fdata()

R1V01 = os.path.join(convs_dir, 'R1_V01.nii.gz')
R1V02 = os.path.join(convs_dir, 'R1_V02.nii.gz')
k2V01 = os.path.join(convs_dir, 'k2_V01.nii.gz')
k2V02 = os.path.join(convs_dir, 'k2_V02.nii.gz')
logV01 = os.path.join(convs_dir, 'logan_V01.nii.gz')
logV02 = os.path.join(convs_dir, 'logan_V02.nii.gz')
SUVRs1V01 = os.path.join(convs_dir, 'SUVR_0_to_120s_V01.nii.gz')
SUVRs1V02 = os.path.join(convs_dir, 'SUVR_0_to_120s_V02.nii.gz')
SUVRs2V01 = os.path.join(convs_dir, 'SUVR_1_to_8m_V01.nii.gz')
SUVRs2V02 = os.path.join(convs_dir, 'SUVR_1_to_8m_V02.nii.gz')
SUVRs3V01 = os.path.join(convs_dir, 'SUVR_50_to_70m_V01.nii.gz')
SUVRs3V02 = os.path.join(convs_dir, 'SUVR_50_to_70m_V02.nii.gz')

R1V01 = nib.load(R1V01).get_fdata()
R1V02 = nib.load(R1V02).get_fdata()
k2V01 = nib.load(k2V01).get_fdata()
k2V02 = nib.load(k2V02).get_fdata()
logV01 = nib.load(logV01).get_fdata()
logV02 = nib.load(logV02).get_fdata()
SUVRs1V01 = nib.load(SUVRs1V01).get_fdata()
SUVRs1V02 = nib.load(SUVRs1V02).get_fdata()
SUVRs2V01 = nib.load(SUVRs2V01).get_fdata()
SUVRs2V02 = nib.load(SUVRs2V02).get_fdata()
SUVRs3V01 = nib.load(SUVRs3V01).get_fdata()
SUVRs3V02 = nib.load(SUVRs3V02).get_fdata()


f = open(stats_file, 'w', newline='')
fieldnames = ['Lesion Name',
              'Lesion Volume',
              'pR1_suvr18',
              'R1_suvr18',
              'pR1_suvr02',
              'R1_suvr02',
              'psuvr18_suvr02',
              'suvr18_suvr02',
              'pdvr_log',
              'dvr_log',
              'pdvr_suvr5070',
              'dvr_suvr5070',
              'plog_suvr5070',
              'log_suvr5070',
              'pR1_dvr',
              'R1_dvr',
              'pR1_log',
              'R1_log',
              'pR1_suvr5070',
              'R1_suvr5070',
              'psuvr02_dvr',
              'suvr02_dvr',
              'psuvr02_log',
              'suvr02_log',
              'psuvr02_suvr5070',
              'suvr02_suvr5070',
              'psuvr18_dvr',
              'suvr18_dvr',
              'psuvr18_log',
              'suvr18_log',
              'psuvr18_suvr5070',
              'suvr18_suvr5070']

stats = csv.DictWriter(f, fieldnames)
stats.writeheader()
for les in Lesions:
    ldata = np.multiply(nib.load(les).get_fdata(), WM)
    LV = int(np.sum(ldata))
    if LV == 0:
        continue
    print('Lesion Volume:', LV)
    les_name = os.path.basename(les)
    mR1 = np.zeros((8, 2))
    mk2 = np.zeros((8, 2))
    DVR = np.zeros((8, 2))
    mSUVR0to120 = np.zeros((8, 2))
    mSUVR1to8 = np.zeros((8, 2))
    mSUVR40to90 = np.zeros((8, 2))
    mlogan = np.zeros((8, 2))
    for i, s in enumerate(subjects):
        print('    ', s)
        # retrieving images and K2prime for the subject
        R1_V01 = R1V01[:, :, :, i]
        R1_V02 = R1V02[:, :, :, i]
        k2_V01 = k2V01[:, :, :, i]
        k2_V02 = k2V02[:, :, :, i]
        log_V01 = logV01[:, :, :, i]
        log_V02 = logV02[:, :, :, i]
        SUVRs1_V01 = SUVRs1V01[:, :, :, i]
        SUVRs1_V02 = SUVRs1V02[:, :, :, i]
        SUVRs2_V01 = SUVRs2V01[:, :, :, i]
        SUVRs2_V02 = SUVRs2V02[:, :, :, i]
        SUVRs3_V01 = SUVRs3V01[:, :, :, i]
        SUVRs3_V02 = SUVRs3V02[:, :, :, i]
        k2prime = k2primes[i]
        k2primeV01 = k2prime[0]
        k2primeV02 = k2prime[1]
        # Finding mean values of R1, k2, DVR, logan for the lesion
        mR1[i, 0] = np.sum(np.multiply(ldata, R1_V01)) / LV
        mR1[i, 1] = np.sum(np.multiply(ldata, R1_V02)) / LV
        mk2[i, 0] = np.sum(np.multiply(ldata, k2_V01)) / LV
        mk2[i, 1] = np.sum(np.multiply(ldata, k2_V02)) / LV
        mlogan[i, 0] = np.sum(np.multiply(ldata, log_V01)) / LV
        mlogan[i, 1] = np.sum(np.multiply(ldata, log_V02)) / LV
        mSUVR0to120[i, 0] = np.sum(np.multiply(ldata, SUVRs1_V01))/LV
        mSUVR0to120[i, 1] = np.sum(np.multiply(ldata, SUVRs1_V02))/LV
        mSUVR1to8[i, 0] = np.sum(np.multiply(ldata, SUVRs2_V01))/LV
        mSUVR1to8[i, 1] = np.sum(np.multiply(ldata, SUVRs2_V02))/LV
        mSUVR40to90[i, 0] = np.sum(np.multiply(ldata, SUVRs3_V01))/LV
        mSUVR40to90[i, 1] = np.sum(np.multiply(ldata, SUVRs3_V02))/LV
        # DVR after mean calculation
        try:
            DVR[i, 0] = mR1[i, 0] * k2primeV01 / mk2[i, 0]
        except ZeroDivisionError:
            DVR[i, 0] = np.nan
        try:
            DVR[i, 1] = mR1[i, 1] * k2primeV02 / mk2[i, 1]
        except ZeroDivisionError:
            DVR[i, 1] = np.nan
    lesdf = pd.DataFrame(columns=['subj', 'r1', 'suvr02', 'suvr18', 'dvr',
                                  'log', 'suvr5070'])
    lesdf["subj"] = S2
    lesdf["r1"] = mR1.flatten()
    lesdf["suvr02"] = mSUVR0to120.flatten()
    lesdf["suvr18"] = mSUVR1to8.flatten()
    lesdf["log"] = mlogan.flatten()
    lesdf["dvr"] = DVR.flatten()
    lesdf["suvr5070"] = mSUVR40to90.flatten()

    rm_R1_suvr18 = pg.rm_corr(data=lesdf, x='r1', y='suvr18', subject='subj')
    rm_R1_suvr02 = pg.rm_corr(data=lesdf, x='r1', y='suvr02', subject='subj')
    rm_suvr18_suvr02 = pg.rm_corr(data=lesdf, x='suvr18', y='suvr02', subject='subj')
    rm_dvr_log = pg.rm_corr(data=lesdf, x='dvr', y='log', subject='subj')
    rm_dvr_suvr5070 = pg.rm_corr(data=lesdf, x='dvr', y='suvr5070', subject='subj')
    rm_log_suvr5070 = pg.rm_corr(data=lesdf, x='log', y='suvr5070', subject='subj')
    rm_R1_dvr = pg.rm_corr(data=lesdf, x='r1', y='dvr', subject='subj')
    rm_R1_log = pg.rm_corr(data=lesdf, x='r1', y='log', subject='subj')
    rm_R1_suvr5070 = pg.rm_corr(data=lesdf, x='r1', y='suvr5070', subject='subj')
    rm_suvr02_dvr = pg.rm_corr(data=lesdf, x='suvr02', y='dvr', subject='subj')
    rm_suvr02_log = pg.rm_corr(data=lesdf, x='suvr02', y='log', subject='subj')
    rm_suvr02_suvr5070 = pg.rm_corr(data=lesdf, x='suvr02', y='suvr5070', subject='subj')
    rm_suvr18_dvr = pg.rm_corr(data=lesdf, x='suvr18', y='dvr', subject='subj')
    rm_suvr18_log = pg.rm_corr(data=lesdf, x='suvr18', y='log', subject='subj')
    rm_suvr18_suvr5070 = pg.rm_corr(data=lesdf, x='suvr18', y='suvr5070', subject='subj')


    pR1_suvr18 = rm_R1_suvr18['pval'][0]
    R1_suvr18 = rm_R1_suvr18['r'][0]
    pR1_suvr02 = rm_R1_suvr02['pval'][0]
    R1_suvr02 = rm_R1_suvr02['r'][0]
    psuvr18_suvr02 = rm_suvr18_suvr02['pval'][0]
    suvr18_suvr02 = rm_suvr18_suvr02['r'][0]
    pdvr_log = rm_dvr_log['pval'][0]
    dvr_log = rm_dvr_log['r'][0]
    pdvr_suvr5070 = rm_dvr_suvr5070['pval'][0]
    dvr_suvr5070 = rm_dvr_suvr5070['r'][0]
    plog_suvr5070 = rm_log_suvr5070['pval'][0]
    log_suvr5070 = rm_log_suvr5070['r'][0]
    pR1_dvr = rm_R1_dvr['pval'][0]
    R1_dvr = rm_R1_dvr['r'][0]
    pR1_log = rm_R1_log['pval'][0]
    R1_log = rm_R1_log['r'][0]
    pR1_suvr5070 = rm_R1_suvr5070['pval'][0]
    R1_suvr5070 = rm_R1_suvr5070['r'][0]
    psuvr02_dvr = rm_suvr02_dvr['pval'][0]
    suvr02_dvr = rm_suvr02_dvr['r'][0]
    psuvr02_log = rm_suvr02_log['pval'][0]
    suvr02_log = rm_suvr02_log['r'][0]
    psuvr02_suvr5070 = rm_suvr02_suvr5070['pval'][0]
    suvr02_suvr5070 = rm_suvr02_suvr5070['r'][0]
    psuvr18_dvr = rm_suvr18_dvr['pval'][0]
    suvr18_dvr = rm_suvr18_dvr['r'][0]
    psuvr18_log = rm_suvr18_log['pval'][0]
    suvr18_log = rm_suvr18_log['r'][0]
    psuvr18_suvr5070 = rm_suvr18_suvr5070['pval'][0]
    suvr18_suvr5070 = rm_suvr18_suvr5070['r'][0]

    stats.writerow({'Lesion Name': les_name,
                    'Lesion Volume': LV,
                    'pR1_suvr18': pR1_suvr18,
                    'R1_suvr18': R1_suvr18,
                    'pR1_suvr02': pR1_suvr02,
                    'R1_suvr02': R1_suvr02,
                    'psuvr18_suvr02': psuvr18_suvr02,
                    'suvr18_suvr02': suvr18_suvr02,
                    'pdvr_log': pdvr_log,
                    'dvr_log': dvr_log,
                    'pdvr_suvr5070': pdvr_suvr5070,
                    'dvr_suvr5070': dvr_suvr5070,
                    'plog_suvr5070': plog_suvr5070,
                    'log_suvr5070': log_suvr5070,
                    'pR1_dvr': pR1_dvr,
                    'R1_dvr': R1_dvr,
                    'pR1_log': pR1_log,
                    'R1_log': R1_log,
                    'pR1_suvr5070': pR1_suvr5070,
                    'R1_suvr5070': R1_suvr5070,
                    'psuvr02_dvr': psuvr02_dvr,
                    'suvr02_dvr': suvr02_dvr,
                    'psuvr02_log': psuvr02_log,
                    'suvr02_log': suvr02_log,
                    'psuvr02_suvr5070': psuvr02_suvr5070,
                    'suvr02_suvr5070': suvr02_suvr5070,
                    'psuvr18_dvr': psuvr18_dvr,
                    'suvr18_dvr': suvr18_dvr,
                    'psuvr18_log': psuvr18_log,
                    'suvr18_log': suvr18_log,
                    'psuvr18_suvr5070': psuvr18_suvr5070,
                    'suvr18_suvr5070': suvr18_suvr5070})
