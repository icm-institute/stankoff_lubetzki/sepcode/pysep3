#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 24 11:16:50 2021

@author: arya.yazdan-panah
"""


import pingouin as pg
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy.stats import pearsonr
from scipy.optimize import curve_fit
from lmfit import Model
from pprint import pprint

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/da\
ta/process/TR_new_MNI/stats/rois.csv'

df = pd.read_csv(CLP)
dfV01 = df[['Subject', 'ROI Name', 'mR1V01', 'DVRV01', 'mloganV01', 'mSUVR0to120V01',
            'mSUVR1to8V01',
            'mSUVR50to70V01']].rename(columns={'mR1V01': 'R1',
                                               'mSUVR0to120V01': 'SUVR02',
                                               'mSUVR1to8V01': 'SUVR18',
                                               'DVRV01': 'DVR',
                                               'mloganV01': 'LOG',
                                               'mSUVR50to70V01': 'SUVR50to70'})
dfV02 = df[['Subject', 'ROI Name', 'mR1V02', 'DVRV02', 'mloganV02', 'mSUVR0to120V02',
            'mSUVR1to8V02',
            'mSUVR50to70V02']].rename(columns={'mR1V02': 'R1',
                                               'mSUVR0to120V02': 'SUVR02',
                                               'mSUVR1to8V02': 'SUVR18',
                                               'DVRV02': 'DVR',
                                               'mloganV02': 'LOG',
                                               'mSUVR50to70V02': 'SUVR50to70'})
V01V02 = [dfV01, dfV02]
df_pair = pd.concat(V01V02)

# metrics = ['R1', 'SUVR18', 'SUVR02', 'DVR', 'LOG', 'SUVR50to70']
couples = [['R1', 'SUVR18'],
           ['R1', 'SUVR02'],
           ['SUVR18', 'SUVR02'],
           ['DVR', 'LOG'],
           ['DVR', 'SUVR50to70'],
           ['LOG', 'SUVR50to70']]
R = np.zeros((17, 6))
P = np.zeros((17, 6))
for i, c in enumerate(couples):
    for j, roi in enumerate(["WM","cbgm","cbwm","lobes_frontal","lobes_insula","lobes_limbic",
            "lobes_occipital","lobes_parietal","lobes_temporal","striatum",
            "thalamus","wm_lobes_cingulate","wm_lobes_frontal",
            "wm_lobes_insula","wm_lobes_occipital","wm_lobes_parietal",
            "wm_lobes_temporal"]):
        stat = pg.rm_corr(data=df_pair.loc[df['ROI Name'] == roi], x=c[0],
                          y=c[1], subject='Subject')
        R[j, i] = float(stat['r'])
        P[j, i] = float(stat['pval'])
        # A.append(pg.rm_corr(data=df_pair.loc[df['ROI Name'] == roi], x=c[0],
        #                     y=c[1], subject='Subject'))
    # A = pd.concat(A)

# %%


sns.heatmap(R)