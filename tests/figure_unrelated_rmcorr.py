#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 12 14:21:05 2021

@author: arya.yazdan-panah
"""



import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from lmfit import Model


CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_new_MNI/stats/inflasep_lesions_rmcorr.csv'
df = pd.read_csv(CLP)

plt.figure(1)
plt.clf()

plt.subplot(5,3,1)
pvals = df['pR1_suvr18'].to_numpy()
corrs = df['R1_suvr18']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('R1 vs [1-8]SUVR')
plt.xscale("log")
plt.grid()
plt.subplot(5,3,2)
pvals = df['pR1_suvr02'].to_numpy()
corrs = df['R1_suvr02']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('R1 vs [0-2]SUVR')
plt.xscale("log")
plt.grid()
plt.subplot(5,3,3)
pvals = df['psuvr18_suvr02'].to_numpy()
corrs = df['suvr18_suvr02']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('[1-8]SUVR vs [0-2]SUVR')
plt.xscale("log")
plt.grid()


plt.subplot(5,3,4)
pvals = df['pdvr_log'].to_numpy()
corrs = df['dvr_log']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('DVR(SRTM2) vs DVR(logan)')
plt.xscale("log")
plt.grid()
plt.subplot(5,3,5)
pvals = df['pdvr_suvr5070'].to_numpy()
corrs = df['dvr_suvr5070']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('[50-70]SUVR vs DVR(SRTM2)')
plt.xscale("log")
plt.grid()
plt.subplot(5,3,6)
pvals = df['plog_suvr5070'].to_numpy()
corrs = df['log_suvr5070']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('DVR(logan) vs [50-70]SUVR')
plt.xscale("log")
plt.grid()


# =============================================================================
# UNRELATED METRICS
# =============================================================================

plt.subplot(5,3,7)
pvals = df['pR1_log'].to_numpy()
corrs = df['R1_dvr']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('R1 vs DVR(logan)')
plt.xscale("log")
plt.grid()
plt.subplot(5,3,8)
pvals = df['pR1_dvr'].to_numpy()
corrs = df['R1_dvr']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('R1 vs DVR(SRTM2)')
plt.xscale("log")
plt.grid()
plt.subplot(5,3,9)
pvals = df['pR1_suvr5070'].to_numpy()
corrs = df['R1_suvr5070']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('R1 vs [50-70]SUVR')
plt.xscale("log")
plt.grid()


plt.subplot(5,3,10)
pvals = df['psuvr18_log'].to_numpy()
corrs = df['suvr18_dvr']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('[1-8]SUVR vs DVR(logan)')
plt.xscale("log")
plt.grid()
plt.subplot(5,3,11)
pvals = df['psuvr18_dvr'].to_numpy()
corrs = df['suvr18_dvr']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('[1-8]SUVR vs DVR(SRTM2)')
plt.xscale("log")
plt.grid()
plt.subplot(5,3,12)
pvals = df['psuvr18_suvr5070'].to_numpy()
corrs = df['suvr18_suvr5070']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('[1-8]SUVR [50-70]SUVR')
plt.xscale("log")
plt.grid()


plt.subplot(5,3,13)
pvals = df['psuvr02_log'].to_numpy()
corrs = df['suvr02_dvr']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('[0-2]SUVR vs DVR(logan)')
plt.xscale("log")
plt.grid()
plt.subplot(5,3,14)
pvals = df['psuvr02_dvr'].to_numpy()
corrs = df['suvr02_dvr']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('[0-2]SUVR vs DVR(SRTM2)')
plt.xscale("log")
plt.grid()
plt.subplot(5,3,15)
pvals = df['psuvr02_suvr5070'].to_numpy()
corrs = df['suvr02_suvr5070']
les_vol = df['Lesion Volume']
plt.scatter(les_vol[pvals>0.05], corrs[pvals>0.05], c='b', label='p>0.05')
plt.scatter(les_vol[pvals<0.05], corrs[pvals<0.05], c='r', label='p<0.05')
plt.legend()
plt.gca().set_title('[0-2]SUVR [50-70]SUVR')
plt.xscale("log")
plt.grid()

plt.show()