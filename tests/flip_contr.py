#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 20 10:13:45 2021

@author: arya.yazdan-panah
"""

import os
from tools_nibabel import flip_nii
from common3 import get_regex, createDir, changeName
from pprint import pprint
from tools_fsl import fslmerge


d = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_new_MNI/convolutions_smoothed'
# flp = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_new_MNI/convolutions_smoothed/flipped'
# ims = get_regex(d, 'C_...../C_.....')
# pprint(ims)



# for i in ims:
#     name = os.path.basename(i)[:7]
#     odir = os.path.join(flp, name)
#     oim = changeName(i, '_flipped', odir)
#     flip_nii(i, 0, odir, '_flipped')


flp = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_new_MNI/convolutions_smoothed/flipped'

# ims = get_regex(d, '_k2_2MNI_flipped')
# pprint(ims)
# out_im = os.path.join(flp, 'k2_contr.nii.gz')
# fslmerge(ims, out_im)

# ims = get_regex(d, '_pib_logan_vt_2MNI_flipped')
# pprint(ims)
# out_im = os.path.join(flp, 'logan_contr.nii.gz')
# fslmerge(ims, out_im)

# ims = get_regex(d, '_R1_2MNI_flipped')
# pprint(ims)
# out_im = os.path.join(flp, 'R1_contr.nii.gz')
# fslmerge(ims, out_im)

# ims = get_regex(d, '_SUVR_sm2_5_0_to_120_2MNI_flipped')
# pprint(ims)
# out_im = os.path.join(flp, 'suvr0to120_contr.nii.gz')
# fslmerge(ims, out_im)

# ims = get_regex(d, '_SUVR_sm2_5_1_to_8_2MNI_flipped')
# pprint(ims)
# out_im = os.path.join(flp, 'suvr1to8_contr.nii.gz')
# fslmerge(ims, out_im)

# ims = get_regex(d, '_SUVR_sm2_5_50_to_70_2MNI_flipped')
# pprint(ims)
# out_im = os.path.join(flp, 'suvr5070_contr.nii.gz')
# fslmerge(ims, out_im)


# %%


d = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_new_MNI/convolutions_smoothed'
ims = get_regex(d, '_k2_2MNI$')
pprint(ims)
out_im = os.path.join(flp, 'k2.nii.gz')
fslmerge(ims, out_im)

ims = get_regex(d, '_pib_logan_vt_2MNI$')
pprint(ims)
out_im = os.path.join(flp, 'logan.nii.gz')
fslmerge(ims, out_im)

ims = get_regex(d, '_R1_2MNI$')
pprint(ims)
out_im = os.path.join(flp, 'R1.nii.gz')
fslmerge(ims, out_im)

ims = get_regex(d, '_SUVR_sm2_5_0_to_120_2MNI$')
pprint(ims)
out_im = os.path.join(flp, 'suvr0to120.nii.gz')
fslmerge(ims, out_im)

ims = get_regex(d, '_SUVR_sm2_5_1_to_8_2MNI$')
pprint(ims)
out_im = os.path.join(flp, 'suvr1to8.nii.gz')
fslmerge(ims, out_im)

ims = get_regex(d, '_SUVR_sm2_5_50_to_70_2MNI$')
pprint(ims)
out_im = os.path.join(flp, 'suvr5070.nii.gz')
fslmerge(ims, out_im)
