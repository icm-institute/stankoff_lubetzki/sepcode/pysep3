#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  2 14:49:51 2021

@author: arya.yazdan-panah
"""

import os
from common3 import get_regex, createDir
from tools_dicoms import dicom2nifti
from pprint import pprint
import shutil

input_dir = '/network/lustre/iss01/lubetzki-stankoff/clinic/INNMS/data/backup/dicom_SHFJ/incoming/1483_Offline_VPFXS_ZTE'
to_import = '/network/lustre/iss01/lubetzki-stankoff/clinic/INNMS/data/backup/dicom_SHFJ/incoming/to_import'
imported = '/network/lustre/iss01/lubetzki-stankoff/clinic/INNMS/data/backup/dicom_SHFJ/incoming/imported'
createDir(to_import)
for i in range(33):
    shutil.rmtree(to_import)
    createDir(to_import)
    A = get_regex(input_dir, '_bin' + str(i + 1) + '_')
    for a in A:
        shutil.copy(a, to_import)
    dicom2nifti(to_import, imported, 'P_DEDER_V01_dpa_'  + str(i + 1))