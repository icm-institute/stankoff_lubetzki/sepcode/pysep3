#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 10:19:51 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import nibabel as nib



les = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/labelled_inflasep_lesions/merged_les.nii.gz').get_fdata()
wm = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_new_MNI/masks/all_subjects_WM.nii.gz').get_fdata().astype(bool)
r1 = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_new_MNI/R1.nii.gz').get_fdata()
# %%
Y = r1[wm]
X = les[wm]

Xu, indexes, counts = np.unique(X, return_inverse=True, return_counts=True)
Yu = np.zeros(Xu.shape)
for i in range(len(Xu)):
    Yu[i] = np.mean(Y[np.where(indexes == i)])
plt.scatter(Xu,Yu)
plt.xlabel('Probability for a voxel to be lesionnal')
plt.ylabel('Mean perfusion value')

