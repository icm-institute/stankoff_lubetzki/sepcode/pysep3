#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 30 18:34:13 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt

def u(mu, n) :
    # Créer votre fonction u(mu,n) ici. Ne pas oublier d'indenter
    u0 = 0.5
    Us = [u0]
    for i in range(n):
        new_Un = mu * Us[i] * (1 - Us[i])
        Us.append(new_Un)
    return Us

Us = u(3, 10)

def dessiner(mu,n) :
    # Compléter la fonction dessiner(mu,n).
    # Ce qui suit est une proposition de début de code, vous pouvez rajouter des lignes si necessaire.

    X = list(range(n + 1))
    Y = u(mu, n)
    plt.plot(X,Y,".-",linewidth=1)
    plt.show()

# dessiner(1, 1000)

def Mandelbrot(mu):
    N = 200
    u0 = 0.5
    Us = [u0]
    index = 0
    while (abs(Us[-1]) < 1000) and (index < N):
        new_Un = mu * Us[index] * (1 - Us[index])
        Us.append(new_Un)
        index = index + 1
    return index


from PIL import Image, ImageDraw  # Module de gestion des images
import numpy as np

# --------- Les constantes, modifiables
WIDTH,HEIGHT = 900,600
X_MIN,Y_MIN,X_MAX = -2,-2,4 # Valeurs min et max pour les parties reelles et imaginaires
# Autres valeurs intéressantes :
#X_MIN,Y_MIN,X_MAX = 3,-0.35,4 # Zoom sur la point à droite
#X_MIN,Y_MIN,X_MAX = 2.80,-0.35,3.20
#X_MIN,Y_MIN,X_MAX = 3.82,-0.015,3.86 # Zoom sur le petit point noir tout à droite

Y_MAX=Y_MIN+HEIGHT*(X_MAX-X_MIN)/WIDTH #Pour avoir un repère normé


# La fonction pour créer l'image de la courbe de Mandelbrot
def creer_image():
    # Crée une image vierge
    im = Image.new('HSV', (WIDTH, HEIGHT), (255, 255, 255))
    draw = ImageDraw.Draw(im)
    # Créer autant de X et Y que de pixels dans l'image
    X = np.linspace(X_MIN, X_MAX, WIDTH)
    Y = np.linspace(Y_MAX, Y_MIN, HEIGHT)
    for x in range(WIDTH):
        for y in range(HEIGHT):
            n = Mandelbrot(complex(X[x],Y[y]))
            # Colorie le point de coordonnée (x,y) avec une couleur indiquée en HSV (ce qui permet de n'avoir à modifier que le premier coefficient (et le dernier pour obtenir le noir))
            draw.point((x, y), (n%255, 255, n%255 if n < 200 else 0))

    # Converti l'image en RGB pour ensuite la sauvegarder en .png
    im.convert('RGB').save('/home/arya.yazdan-panah/Bureau/output.png', 'PNG')

creer_image()
