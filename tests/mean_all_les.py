#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 10:25:07 2021

@author: arya.yazdan-panah
"""

import shutil
from pprint import pprint
from common3 import get_regex
from tools_fsl import fslmaths
D = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/labelled_inflasep_lesions"

les = get_regex(D, "P_...../")
pprint(les)
outp = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/labelled_inflasep_lesions/merged_les.nii.gz"
outp1 = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/labelled_inflasep_lesions/merged_les_sum.nii.gz"

for i, l in enumerate(les):
    print(i)
    if i == 0:
        shutil.copy(l, outp1)
    else:
        fslmaths(outp1, '-add', l, outp1)

fslmaths(outp1, '-mul', 100, "-div", len(les),  outp)