#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 15:51:35 2021

@author: arya.yazdan-panah
"""

from common3 import readSubjectsLists, get_regex
import os
import nibabel as nib
import numpy as np

mni = '/network/lustre/iss01/lubetzki-stankoff/clinic/tools/Atlas/MNI/mni_icbm152_nlin_sym_09c/mni_icbm152_t1_tal_nlin_sym_09c_brain.nii.gz'
Data = np.zeros(nib.load(mni).shape + (27,))
affine = nib.load(mni).affine
subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/HAB.list')
p = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
for s in subjects:
    sdir = os.path.join(p, s['Id'], s['Visit'], 't1Ants2SS')
    suv = get_regex(sdir, 'suv_2mni.nii.gz', False)
    print(suv)
    Data = np.add(Data, nib.load(suv[0]).get_fdata())
Data = np.divide(Data, len(subjects))
nib.save(nib.Nifti1Image(Data, affine), '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/suv_HAB.nii.gz')