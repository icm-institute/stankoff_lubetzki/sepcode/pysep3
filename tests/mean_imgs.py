#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 10:43:12 2021

@author: arya.yazdan-panah
"""

import os
import csv
import numpy as np
import nibabel as nib
from common3 import get_regex
from tools_fsl import fslmerge, fslmean, fslmaths
from pprint import pprint


regs = ["_V01_wm_lobes_cingulate_2MNI",
        "_V01_wm_lobes_frontal_2MNI",
        "_V01_wm_lobes_insula_2MNI",
        "_V01_wm_lobes_occipital_2MNI",
        "_V01_wm_lobes_parietal_2MNI",
        "_V01_wm_lobes_temporal_2MNI"]
p = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_new_MNI/masks'
A = get_regex(p, regs[2])
names = ["wm_lobes_cingulate",
          "wm_lobes_frontal",
          "wm_lobes_insula",
          "wm_lobes_occipital",
          "wm_lobes_parietal",
          "wm_lobes_temporal"]
for i in range(6):
    A = get_regex(p, regs[i])
    name = names[i]
    mean = os.path.join(p, 'all_subjects_mean_' + name + '.nii.gz')
    out = os.path.join(p, 'all_subjects_' + name + '.nii.gz')
    pprint(A)
    fslmerge(A, mean)
    fslmean(mean, mean)
    fslmaths(mean, '-thr', 0.5, '-bin' ,out)