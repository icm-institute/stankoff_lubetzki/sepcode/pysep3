#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 19 12:31:19 2021

@author: arya.yazdan-panah
"""

from common3 import get_regex, readSubjectsLists
import os
import nibabel as nib
import numpy as np

Lis = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/utils/reconstructed_with_more_frames.list'
subjects = readSubjectsLists(Lis)
proc = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/process'
N = 0
mni = '/network/lustre/iss01/lubetzki-stankoff/clinic/tools/Atlas/MNI/mni_icbm152_nlin_sym_09c/mni_icbm152_t1_tal_nlin_sym_09c_brain.nii.gz'
output_data = np.zeros((nib.load(mni).get_fdata().shape) + (33,))
affine = nib.load(mni).affine
for s in subjects:
    # if s['Visit'] == 'V02':
    #     continue
    N = N + 1
    dpapre = os.path.join(proc, s['Id'], s['Visit'], 't1Ants2SS')
    if not os.path.isdir(dpapre):
        continue
    pet = get_regex(dpapre, '_suv2_2mni.nii.gz', False)
    if not pet:
        continue
    pet = pet[0]
    pet = nib.load(pet).get_fdata()
    print(pet.shape)
    output_data = output_data + pet

output_data = output_data / N
out_img = nib.Nifti1Image(output_data, affine)
nib.save(out_img, '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/suv1.nii.gz')