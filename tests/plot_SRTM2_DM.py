#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 18:02:09 2021

@author: arya.yazdan-panah
"""

import nibabel as nib
import numpy as np
import os
from common3 import get_regex
import seaborn as sns
import matplotlib.pyplot as plt

DM = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_MNI_old/MNI_brain_distance_map.nii.gz"
RD = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_MNI_old/relative_differences/mean_R1_skull_stripped.nii.gz"
WM = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_MNI_old/masks/mean/V01_aparc+aseg_wm_2MNI_c_1.nii.gz"
distance_map = nib.load(DM).get_fdata()
relative_difference = nib.load(RD).get_fdata()
white_matter = nib.load(WM).get_fdata().astype(bool)

X = distance_map[white_matter]
Y = relative_difference[white_matter]
Xu, indexes, counts= np.unique(X,return_inverse=True, return_counts=True)
Yu = np.zeros(Xu.shape)
for i in range(len(Xu)):
    Yu[i] = np.mean(Y[np.where(indexes == i)])

plt.close()
plt.figure()
plt.scatter(Xu, Yu * 100)
plt.title('R1 - Distance map')
plt.xlabel('Distance from CSF')
plt.ylabel('TRV ')

RD = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_MNI_old/relative_differences/mean_DVR_skull_stripped.nii.gz"
relative_difference = nib.load(RD).get_fdata()

X = distance_map[white_matter]
Y = relative_difference[white_matter]
Xu, indexes, counts= np.unique(X,return_inverse=True, return_counts=True)
Yu = np.zeros(Xu.shape)
for i in range(len(Xu)):
    Yu[i] = np.mean(Y[np.where(indexes == i)])

plt.figure()
plt.scatter(Xu, Yu * 100)
plt.title('DVR (SRTM2) - Distance map')
plt.xlabel('Distance from CSF')
plt.ylabel('TRV ')

RD = "/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process/TR_MNI_old/relative_differences/mean_LOG_skull_stripped.nii.gz"
relative_difference = nib.load(RD).get_fdata()

X = distance_map[white_matter]
Y = relative_difference[white_matter]
Xu, indexes, counts= np.unique(X,return_inverse=True, return_counts=True)
Yu = np.zeros(Xu.shape)
for i in range(len(Xu)):
    Yu[i] = np.mean(Y[np.where(indexes == i)])

plt.figure()
plt.scatter(Xu, Yu * 100)
plt.title('DVR (Logan) - Distance map')
plt.xlabel('Distance from CSF')
plt.ylabel('TRV ')
