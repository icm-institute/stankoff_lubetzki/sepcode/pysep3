#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 20 17:09:49 2021

@author: arya.yazdan-panah
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/clusters/cluster_fr27_nobext_fwhm3.csv'
df = pd.read_csv(CLP)

plt.close()
fig = plt.figure(1)
ax = fig.add_subplot(1, 1, 1)
# Move left y-axis and bottim x-axis to centre, passing through (0,0)
ax.spines['bottom'].set_position('zero')
ax.spines['left'].set_position('zero')
# Eliminate upper and right axes
ax.spines['top'].set_color('none')
ax.spines['right'].set_color('none')
ax.plot('Time', 'Gray Matter', data=df, marker='o')
ax.plot('Time', 'White Matter', data=df, marker='o')
ax.plot('Time', 'Blood', data=df, marker='o')
ax.plot('Time', 'High Binding', data=df, marker='o')
# ax.set_ylim(-0.4, 0.4)
ax.legend()

# CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/clusters/HRRT_dpa_K_MAB.csv'
# df = pd.read_csv(CLP)

# # ax = fig.add_subplot(3, 1, 2)
# # # Move left y-axis and bottim x-axis to centre, passing through (0,0)
# ax.spines['bottom'].set_position('zero')
# ax.spines['left'].set_position('zero')
# # Eliminate upper and right axes
# ax.spines['top'].set_color('none')
# ax.spines['right'].set_color('none')
# ax.plot('Time', 'Gray Matter', data=df, marker='o')
# ax.plot('Time', 'White Matter', data=df, marker='o')
# ax.plot('Time', 'Blood', data=df, marker='o')
# ax.plot('Time', 'High Binding', data=df, marker='o')
# ax.set_ylim(-0.6, 0.8)
# ax.legend()

# CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/clusters/HRRT_dpa_K_HAB.csv'
# df = pd.read_csv(CLP)
# plt.style.use('dark_background')

# # ax = fig.add_subplot(3, 1, 3)
# # Move left y-axis and bottim x-axis to centre, passing through (0,0)
# ax.spines['bottom'].set_position('zero')
# ax.spines['left'].set_position('zero')
# # Eliminate upper and right axes
# ax.spines['top'].set_color('none')
# ax.spines['right'].set_color('none')
# ax.plot('Time', 'Gray Matter', data=df, marker='o')
# ax.plot('Time', 'White Matter', data=df, marker='o')
# ax.plot('Time', 'Blood', data=df, marker='o')
# ax.plot('Time', 'High Binding', data=df, marker='o')
# ax.set_ylim(-0.7, 0.8)
# ax.legend()