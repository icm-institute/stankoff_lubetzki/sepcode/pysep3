#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 15:19:01 2021

@author: arya.yazdan-panah
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import pearsonr
from statsmodels.stats.weightstats import ttest_ind
import seaborn as sns

CLP = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PRELESIONS/MSIL2/prelesion_MSIL2_V01.csv'
df = pd.read_csv(CLP)
keys = ['T1', 'FLAIR', 'FA', 'MD', 'AD', 'RD', 'MTR', 'ICVF', 'ISOVF', 'OD',
        'qMPF', 'qMTR', 'qT1', 'qT2']

plt.close()
fig, ax = plt.subplots(2, 7)
line = 0
for i, k in enumerate(keys):
    data_to_plot = pd.melt(df, id_vars=['les visit', 'les vol'], value_vars=[k, 'c' + k])
    wdf = data_to_plot.loc[data_to_plot.index.repeat(data_to_plot['les vol'])].reset_index(drop=True)
    if i == 7:
        line += 1
    sns.violinplot(x="variable", y="value", data=wdf,
                    ax=ax[line][i % 7])
    test = ttest_ind(df.dropna()[k], df.dropna()['c' + k], 'two-sided',
                      'pooled', (df.dropna()['les vol'],
                                df.dropna()['cles vol']), 0)
    ax[line][i % 7].set_title("t=%.2f, p=%.4f" % (test[0], test[1]))

# for i, k in enumerate(keys):
#     fig, ax = plt.subplots(1, 1)
#     data_to_plot = pd.melt(df, id_vars=['les visit'], value_vars=[k, 'c' + k])
#     hue_labels = []
#     for j, visit in enumerate(['V02', 'V03', 'V04', 'V05']):
#         visitdf = df.loc[df['les visit'] == visit].dropna()
#         test = ttest_ind(visitdf[k], visitdf['c' + k], 'two-sided', 'pooled',
#                          (visitdf['les vol'], visitdf['cles vol']), 0)
#         hue_labels.append(visit + ": t=%.2f, p=%.4f" % (test[0], test[1]))
#     g = sns.violinplot(x="variable", y="value", data=data_to_plot, ax=ax,
#                        hue='les visit')
#     # title
#     new_title = 'Visit where Lesion appeared'
#     g.get_legend().set_title(new_title)
#     # replace labels
#     for t, l in zip(g.get_legend().texts, hue_labels):
#         t.set_text(l)
#     ax.set_title(k.upper())
