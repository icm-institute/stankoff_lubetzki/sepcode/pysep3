#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 30 14:11:20 2020

@author: arya.yazdan-panah
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import skimage.morphology as sm
from mpl_toolkits.mplot3d import Axes3D
inc = 1
shapes = []
sizes = []
for size in range(6):
    octa = sm.octahedron(size)
    ball = sm.ball(size)
    shapes.append(octa)
    shapes.append(ball)
    nb_vox_octa = np.sum(octa)
    nb_vox_ball = np.sum(ball)
    sizes.append(nb_vox_octa)
    sizes.append(nb_vox_ball)
    # ax = fig.add_subplot(2, 6, inc, projection=Axes3D.name)
    # ax.voxels(octa)
    # ax.set_title('octahedron size ' + str(size))
    # inc = inc+1
    # if size < 2:
    #     ax = fig.add_subplot(2, 6, inc, projection=Axes3D.name)
    #     ax.voxels(ball)
    #     ax.set_title('ball size ' + str(size))
    #     inc = inc+1
indexes = pd.Index(sizes)
indexes = indexes.duplicated()
indexes = np.invert(indexes).astype(int)
sizes = np.array(sizes)
sizes = sizes[np.where(indexes)]
sizes = sizes.tolist()
shapes = np.array(shapes)
shapes = shapes[np.where(indexes)]
zipped = sorted(list(zip(shapes, sizes)),  key=lambda x: x[1])

for i in range(10):
    fig = plt.figure(figsize=(4.6, 4.92))
    ax = fig.gca(projection=Axes3D.name)
    ax.voxels(zipped[i][0])
    ax.set_title('Number of voxels ' + str(zipped[i][1]))
    plt.show()
