#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 21 15:10:01 2021

@author: arya.yazdan-panah
"""

import os
import nibabel as nib
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from tools_svca import readSIF

gen = 'HAB'

time, ni, no ,no = readSIF('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/raw/AL059/V01/dpa/AL059_V01_dpa.sif')

mask = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/tools/Atlas/MNI/mni_icbm152_nlin_sym_09c/mni_icbm152_t1_tal_nlin_sym_09c_brain.nii.gz').get_fdata().astype(bool)
Mean = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/results/suv_%s.nii.gz' % gen).get_fdata()
Std = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/results/std_%s.nii.gz' % gen).get_fdata()

tmean = np.mean(Mean[mask], 0)
tstd = np.mean(Std[mask], 0) * 1.96

fig, ax = plt.subplots()
ax.plot(time, tmean, 'b')
ax.fill_between(time, (tmean-tstd), (tmean+tstd), color='b', alpha=.1)

gen = 'MAB'

mask = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/tools/Atlas/MNI/mni_icbm152_nlin_sym_09c/mni_icbm152_t1_tal_nlin_sym_09c_brain.nii.gz').get_fdata().astype(bool)
Mean = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/results/suv_%s.nii.gz' % gen).get_fdata()
Std = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/results/std_%s.nii.gz' % gen).get_fdata()
tmean = np.mean(Mean[mask], 0)
tstd = np.mean(Std[mask], 0) * 1.96
ax.plot(time, tmean, 'r')
ax.fill_between(time, (tmean-tstd), (tmean+tstd), color='r', alpha=.1)

gen = 'LAB'

mask = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/tools/Atlas/MNI/mni_icbm152_nlin_sym_09c/mni_icbm152_t1_tal_nlin_sym_09c_brain.nii.gz').get_fdata().astype(bool)
Mean = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/results/suv_%s.nii.gz' % gen).get_fdata()
Std = nib.load('/network/lustre/iss01/lubetzki-stankoff/clinic/DPAHC/data/results/std_%s.nii.gz' % gen).get_fdata()
tmean = np.mean(Mean[mask], 0)
tstd = np.mean(Std[mask], 0) * 1.96
ax.plot(time, tmean, 'g', )
ax.fill_between(time, (tmean-tstd), (tmean+tstd), color='g', alpha=.1)

# Move left y-axis and bottim x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('zero')
ax.spines['bottom'].set_position('zero')

# Eliminate upper and right axes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')