"""
paths to the useful data in the system
only useful in the cenir enviroment
"""


import os
import sys
from termcolor import cprint

class mni152:
    """ stores useful information from the mni152 atlas found in FSL"""
    if "FSLDIR" in os.environ:
        fsldir = os.environ["FSLDIR"]
    else:
        print(" *** ERROR: FSLDIR path not found!!")
        sys.exit(1)
    T12mm = os.path.join(fsldir, "data", "standard", "MNI152_T1_2mm.nii.gz")
    T11mm = os.path.join(fsldir, "data", "standard", "MNI152_T1_1mm.nii.gz")
    T12mm_brain = os.path.join(fsldir, "data", "standard", "MNI152_T1_2mm_brain.nii.gz")
    T12mm_edges = os.path.join(fsldir, "data", "standard", "MNI152_T1_2mm_edges.nii.gz")
    T11mm_brain = os.path.join(fsldir, "data", "standard", "MNI152_T1_1mm_brain.nii.gz")
    FNIRT_MNI2MM = os.path.join(fsldir, "etc", "flirtsch", "T1_2_MNI152_2mm.cnf")
    T12mm_mask = os.path.join(fsldir, "data", "standard", "MNI152_T1_2mm_brain_mask.nii.gz")
    T11mm_mask = os.path.join(fsldir, "data", "standard", "MNI152_T1_1mm_brain_mask.nii.gz")

if 'ATLAS_HOME' not in os.environ:
        cprint(' ** ERROR: ATLAS_HOME was not set, please verify the sour'
               'ce file (.../clinic/PROTOCOL/scripts/source.protocol)',
               'red', attrs=['bold'])
        sys.exit(1)
else:
    Atlashome = os.environ["ATLAS_HOME"]
if not os.path.isdir(Atlashome):
    Atlashome = "/home/ep247664/data/Atlas/"


class mni152_asym09a:
    """ stores non linear atlas from the mni
    THis is the new version of the template using non linear registration
    See Fonov 2006 neuroimage paper
    """
    mnidir = os.path.join(Atlashome, "MNI", "mni_icbm152_nlin_asym_09a")
    if not os.path.isdir(mnidir):
        print(" *** ERROR: mni_atlases path not found!!")
        sys.exit(1)

    t1 = os.path.join(mnidir, "mni_icbm152_t1_tal_nlin_asym_09a.nii.gz")
    t2 = os.path.join(mnidir, "mni_icbm152_t2_tal_nlin_asym_09a.nii.gz")
    pd = os.path.join(mnidir, "mni_icbm152_pd_tal_nlin_asym_09a.nii.gz")
    gm = os.path.join(mnidir, "mni_icbm152_gm_tal_nlin_asym_09a.nii.gz")
    wm = os.path.join(mnidir, "mni_icbm152_wm_tal_nlin_asym_09a.nii.gz")
    csf = os.path.join(mnidir, "mni_icbm152_csf_tal_nlin_asym_09a.nii.gz")
    mask = os.path.join(mnidir, "mni_icbm152_t1_tal_nlin_asym_09a_mask.nii.gz")
    brain = os.path.join(mnidir, "mni_icbm152_t1_tal_nlin_asym_09a_brain.nii.gz")
    skull = os.path.join(mnidir, "mni_bet_skull.nii.gz")


class mni152_asym09b:
    """ stores non linear atlas from the mni
    THis is the new version of the template using non linear registration
    See Fonov 2006 neuroimage paper
    """
    mnidir = os.path.join(Atlashome, "MNI", "mni_icbm152_nlin_asym_09b")
    if not os.path.isdir(mnidir):
        print(" *** ERROR: mni_atlases path not found!!")
        sys.exit(1)
    t1 = os.path.join(mnidir, "mni_icbm152_t1_tal_nlin_asym_09b_hires.nii.*")
    t2 = os.path.join(mnidir, "mni_icbm152_t2_tal_nlin_asym_09b_hires.nii.*")
    pd = os.path.join(mnidir, "mni_icbm152_pd_tal_nlin_asym_09b_hires.nii.*")


class mni152_sym09a:
    """ stores non linear symetrical atlas from the mni
    THis is the new version of the template using non linear registration
    See Fonov 2006 neuroimage paper
    """
    mnidir = os.path.join(Atlashome, "MNI", "mni_icbm152_nlin_sym_09a")
    if not os.path.isdir(mnidir):
        print(" *** ERROR: mni_atlases path not found!!")
        sys.exit(1)
    t1 = os.path.join(mnidir, "mni_icbm152_t1_tal_nlin_sym_09a.nii.*")
    t2 = os.path.join(mnidir, "mni_icbm152_t2_tal_nlin_sym_09a.nii.*")
    pd = os.path.join(mnidir, "mni_icbm152_pd_tal_nlin_sym_09a.nii.*")
    gm = os.path.join(mnidir, "mni_icbm152_gm_tal_nlin_sym_09a.nii.*")
    wm = os.path.join(mnidir, "mni_icbm152_wm_tal_nlin_sym_09a.nii.*")
    csf = os.path.join(mnidir, "mni_icbm152_csf_tal_nlin_sym_09a.nii.*")
    mask = os.path.join(mnidir, "mni_icbm152_t1_tal_nlin_sym_09a_mask.nii.*")
    atlasgrey = os.path.join(mnidir, 'AtlasGrey.nii')
    atlaswhite = os.path.join(mnidir, 'AtlasWhite.nii')


class mni152_sym09c:
    mnidir = os.path.join(Atlashome, "MNI", "mni_icbm152_nlin_sym_09c")
    if not os.path.isdir(mnidir):
        print(" *** ERROR: mni_atlases path not found!!")
        sys.exit(1)
    t1 = os.path.join(mnidir, "fmni_icbm152_t1_tal_nlin_sym_09c.nii.gz")
    brain = os.path.join(mnidir, "mni_icbm152_t1_tal_nlin_sym_09c_brain.nii.gz")


class miccai_2012:
    miccaidir = os.path.join(Atlashome, "MICCAI-2012-Multi-Atlas-Challenge-Data")
    brain = os.path.join(miccaidir, "Brain")
    label = os.path.join(miccaidir, "Label")


class oasis_atropos:
    oasisdir = os.path.join(Atlashome, "OASIS-30_Atropos_template")
    template0 = os.path.join(oasisdir, "T_template0.nii.gz")
    probabilityMask = os.path.join(oasisdir,
                          "T_template0_BrainCerebellumProbabilityMask.nii.gz")
    extractionMask = os.path.join(oasisdir,
                         "T_template0_BrainCerebellumExtractionMask.nii.gz")
    priors = os.path.join(oasisdir, "Priors2", "priors%d.nii.gz")


class ADNINormal:
    ADNINormaldir = os.path.join(Atlashome, "ADNI_Normal")
    template0 = os.path.join(ADNINormaldir, "T_template0.nii.gz")
    probabilityMask = os.path.join(ADNINormaldir,
                          "T_template0_BrainCerebellumProbabilityMask.nii.gz")
    extractionMask = os.path.join(ADNINormaldir,
                         "T_template0_BrainCerebellumExtractionMask.nii.gz")
    priors = os.path.join(ADNINormaldir, "PRIORS", "priors%d.nii.gz")

class fsl_diff_std:
    if "FSLDIR" in os.environ:
        fsldir = os.environ["FSLDIR"]
    else:
        print(" *** ERROR: FSLDIR path not found!!")
        sys.exit(1)
    FA = os.path.join(fsldir, "data", "standard", "FMRIB58_FA_1mm.nii.gz")
    skeleton = os.path.join(fsldir, "data", "standard", "FMRIB58_FA-skeleton_1mm.nii.gz")
