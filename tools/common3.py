#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Wed Jan 15 14:37:40 2020

@author: arya.yazdan-panah
'''


import os
import re
import sys
import shutil
import numpy as np
import gzip
import smtplib
import getpass
import inspect
from pprint import pprint
from termcolor import cprint, colored
from subprocess import call, Popen, PIPE
from email.message import EmailMessage
from email.headerregistry import Address


def get_all_values(d):
    if isinstance(d, dict):
        for v in d.values():
            yield from get_all_values(v)
    elif isinstance(d, list):
        for v in d:
            yield from get_all_values(v)
    else:
        yield d


def isnii(s):
    """
    Return True if s is a nifti or compressed nifti

    Parameters
    ----------
    s : no-TYPE
        any variable.

    Returns
    -------
    bool
        True if s is a string.
    """

    s = os.path.splitext(s)
    if not s[1]:
        return False
    elif s[1] == '.nii':
        return True
    else:
        return isnii(s[0])


def cmd_and_wait(cmd):
    p = Popen(cmd, stdout=PIPE, shell=True)
    (output, err) = p.communicate()
    p_status = p.wait()
    return output, err, p_status


def cmd_str(cmd):
    Popen(cmd, stdout=PIPE, shell=True)
    return

def getFSLExtension(filename=None):
    """ Give the appropriate nifti extension to the filename
    Two uses:
        a) if filename exists:
            the function returns the path to the filename
            with the correct extension

            >> ls
            ima.nii
            >> getFSLExtension("ima")
            "ima.nii"
            >> getFSLExtension("ima.nii.gz")
            "ima.nii"

        b) if filename does not exists:
            return the filename with the FSL extension
            >> echo $FSLOUTPUTTYPE
            NIFTI_GZ
            >> getFSLExtension("outputima")
            "outputima.nii.gz"
    """
    exts = {"NIFTI": ".nii", "NIFTI_GZ": ".nii.gz", "NIFTI_PAIR": ".img",
            "NIFTI_PAIR2": ".hdr", "ANALIZE": ".img", "ANALYZE_GZ": ".img.gz"}

    if filename is None:
        if "FSLOUTPUTTYPE" in os.environ:
            return exts[os.environ["FSLOUTPUTTYPE"]]
        else:
            print(" ** WARNING :: FSLOUTPUTTYPE not configured!")
            return ".nii"
    if os.path.exists(filename):
        return filename

    # 1) Remove the extension if it is the wrong one
    name = filename
    for e in exts.values():
        if filename.endswith(e):
            name = filename.replace(e, '')
            break

    # 2) Check the existence of the image under any of the extensions
    for e in exts.values():
        if os.path.exists(name + e):
            return name + e

    # 3) Not found, just add the FSLOUTPUTTYPE extension
    if "FSLOUTPUTTYPE" in os.environ:
        return name + exts[os.environ["FSLOUTPUTTYPE"]]
    else:
        print(" ** WARNING :: FSLOUTPUTTYPE not configured!")
        return name + ".nii"


def isstring(s):
    """
    Return True if s is a string

    Parameters
    ----------
    s : no-TYPE
        any variable.

    Returns
    -------
    bool
        True if s is a string.
    """

    if isinstance(s, str):
        return True
    else:
        return False


'''
# =============================================================================
# IS INTEGR?
# return True is the string can be converted to a integer
# =============================================================================
'''


def isinteger(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


'''
# =============================================================================
# CREATE DIRECTORIES
# create directory if it doesn't exist it does nothing if it already exists
# =============================================================================
'''


def createDir(path):
    if isinstance(path, list):
        for p in path:
            createDir(p)
    elif not os.path.isdir(path):
        os.makedirs(path)


'''
# =============================================================================
# Read Subjects
# Generic function to read a list of patients where ID,Visit,others1,others2
# are separated with commas. Spaces are removed to simplify the reading
#
# P01,V01,      2002_02_02_PROTO_P01
# P01,V02,2003_02_02_PROTO_P01
# P02,V01, 2003_02_02_PROTO_3T_P02_E1
#
# RETURN: list of dictionnaries (each dic in the main list is a line in the
# list)
#
# subject['ID'] = P01
# subject['Visit'] = V01
# subject['Dicom_fold'] =  2002_02_02_PROTO_P01
# =============================================================================
'''


def readSubjectsLists(filename, verbose=False):
    lines = open(filename).readlines()
    subjects = []
    for li in lines:
        sid = ''.join(li.split()).split(',')
        if sid[0].startswith('#') or len(sid) < 1 or sid[0] == '':
            continue
        elif len(sid) == 1:
            subjects.append(dict(Id=sid[0]))
            continue
        elif len(sid) == 2:
            subjects.append(dict(Id=sid[0], Visit=sid[1]))
            continue
        elif len(sid) == 3:
            subjects.append(dict(Id=sid[0], Visit=sid[1], extra=sid[2]))
            continue
        subjects.append(dict(Id=sid[0], Visit=sid[1], extra=sid[2],
                             extra2=sid[3]))
    if verbose:
        print('')
        print(' -- Found ' + str(len(subjects)) + ' subjects')
        print('')
    return subjects


'''
# =============================================================================
# Read parameter files (the .project ones)
# paramsfile: Path to the parameter file
# =============================================================================
'''


def readProjectParams(paramsfile):
    lines = open(paramsfile).readlines()
    dicom_folder = False
    params = []
    keys = ['seqname', 'seqregex']
    for li in lines:
        line = ''.join(li.split())
        if line.startswith('#'):
            continue
        if line.count('dicom_folder'):
            dicom_folder = line.split('=')[1]
        seqdict = {}
        sp = ''.join(line.split()).split(',')
        if len(sp) < 2:
            continue
        for i, s in enumerate(sp):
            if s.count('='):
                option = s.split('=')
                if len(option) != 2:
                    print(' ** Skipping value: ' + s)
                    continue
                k = option[0]
                value = option[1]
                if isinteger(value):
                    value = int(value)
                elif value.lower() == 'true':
                    value = True
                elif value.lower() == 'false':
                    value = False
            else:
                k = keys[i]
                value = s
            seqdict[k] = value
        params.append(seqdict)
    return params, dicom_folder


'''
# =============================================================================
# REGEX Anything
# directory: path of where to look in
# regex: regelar expression to look for
# ignore_extension: True/False if you want to look for a file name that
#                   finishes in a special way
# =============================================================================
'''


def get_regex(directory, regex, ignore_extension=True, deep_search=True,
              casensitive=True, full=True):
    if not os.path.isdir(directory):
        return []
    List = [os.path.join(directory, x) for x in os.listdir(directory)]
    if not List:
        return []
    safe = List
    if ignore_extension:
        List = [os.path.splitext(os.path.splitext(x)[0])[0] for x in List]
    if casensitive:
        r = re.compile(regex)
    else:
        r = re.compile(regex, re.IGNORECASE)
    if full:
        rtrn = [safe[i] for i, x in enumerate(List) if re.search(r, x)]
    else:
        rtrn = [safe[i] for i, x in enumerate(List)
                if re.search(r, os.path.basename(x))]
    if deep_search:
        for li in List:
            if os.path.isdir(li):
                test = get_regex(li, regex, ignore_extension, deep_search,
                                 casensitive, full)
                if test:
                    rtrn.extend(test)
    rtrn.sort()
    return rtrn


'''
# =============================================================================
# SEQUENCE INFOS
# seq: Path to a sequence
# =============================================================================
'''


def seq_info(seq):
    directory = os.path.dirname(seq)
    name = os.path.basename(seq)
    tmp = name.split('.')
    name = tmp[0]
    tmp = name.split('_')
    name = '_'.join(tmp[0:3])
    seqname = '_'.join(tmp[3:])
    return name, seqname, directory


def ungzip_nii(nii_gz):
    nii = os.path.splitext(nii_gz)[0]
    tmp = gzip.GzipFile(nii_gz, 'rb')
    data = tmp.read()
    tmp.close()
    output = open(nii, 'wb')
    output.write(data)
    output.close()
    return nii


def gzip_nii(nii):
    nii_gz = nii + '.gz'
    tmp = open(nii, 'rb')
    data = tmp.read()
    tmp.close()
    output = gzip.GzipFile(nii_gz, 'wb')
    output.write(data)
    output.close()
    return nii_gz


'''
# =============================================================================
# COMPRESS DIREC
# modes = ['zip', 'tar', 'gztar', 'bztar', 'xztar']
# =============================================================================
'''


def compress(directory, mode='gztar'):
    root_dir = os.path.dirname(directory)
    base_dir = os.path.basename(directory)
    out = shutil.make_archive(directory, mode, root_dir, base_dir)
    shutil.rmtree(directory)
    return out


'''
# =============================================================================
# DECOMPRESS DIR
# modes = ['zip', 'tar', 'gztar', 'bztar', 'xztar']
# =============================================================================
'''


def decompress(filename, mode='gztar', copy=False):
    extract_dir = os.path.dirname(filename)
    shutil.unpack_archive(filename, extract_dir, mode)
    out = os.path.splitext(os.path.splitext(filename)[0])[0]
    if not copy:
        os.remove(filename)
    return out


'''
# =============================================================================
# SPHERE ARRAY
# shape: 3-tuple (matrix dimension MxNxL)
# radius: radius of the sphere (in index) int/float/double
# position: 3-tuple with the x,y,z coordinates of the center of the circle
# not mine, found on the internet
# =============================================================================
'''


def sphere(shape, radius, position):
    # assume shape and position are both a 3-tuple of int or float
    # the units are pixels / voxels (px for short)
    # radius is a int or float in px
    semisizes = (radius,) * 3
    # genereate the grid for the support points
    # centered at the position indicated by position
    grid = [slice(-x0, dim - x0) for x0, dim in zip(position, shape)]
    position = np.ogrid[grid]
    # calculate the distance of all points from `position` center
    # scaled by the radius
    arr = np.zeros(shape, dtype=float)
    for x_i, semisize in zip(position, semisizes):
        arr += (np.abs(x_i / semisize) ** 2)
    # the inner part of the sphere will have distance below 1
    return arr <= 1.0


'''
# =============================================================================
# Initialze A pysep3 script by setting correctly all variables
# =============================================================================
'''



def init_script(options):
    cprint('| Initializing Script', 'blue', attrs=['bold'])
    init = {}
    for option, value in options.__dict__.items():
        init[option] = True
# --------------------------- Directory definitions ---------------------------
    if 'PROJECT_DIR' not in os.environ:
        cprint(' ** ERROR: PROJECT_DIR was not set, please verify the sour'
               'ce file (.../clinic/PROTOCOL/scripts/source.protocol)',
               'red', attrs=['bold'])
        sys.exit(1)
    data = os.environ['PROJECT_DIR']
    cprint('|  PROJECT_DIR = ' + data, 'blue', attrs=['bold'])
    if not data.endswith(str(os.sep)):
        data += str(os.sep)
    utils = str(os.sep).join(data.split(str(os.sep))[0:-2] + ['utils'])
    cprint('|    UTILS DIR = ' + utils, 'blue', attrs=['bold'])
    init['data'] = data
    init['utils'] = utils
# ------------------------------ SUBJECTS_DIR  --------------------------------
    try:
        if init['freesurfer']:
            if (isinstance(options.freesurfer, str) and
                    os.path.isdir(options.freesurfer)):
                freesurfer = options.freesurfer
            elif 'SUBJECTS_DIR' in os.environ:
                freesurfer = os.environ['SUBJECTS_DIR']
            else:
                cprint(' ** ERROR: option -d (SUBJECTS_DIR directory) give'
                       'n points to a broken path', 'red', attrs=['bold'])
                sys.exit(2)
            cprint('| SUBJECTS_DIR = ' + freesurfer, 'blue', attrs=['bold'])
            init['freesurfer'] = freesurfer
    except KeyError:
        init['freesurfer'] = None
# --------------------------------- raw_dir  ----------------------------------
    try:
        if init['raw']:
            if options.raw is None:
                raw = os.path.join(data, 'raw')
            elif not os.path.exists(options.raw):
                cprint(' ** ERROR: option -r (raw directory) given points to a'
                       ' broken path', 'red', attrs=['bold'])
                sys.exit(3)
            else:
                raw = options.raw
            cprint('|      RAW DIR = ' + raw, 'blue', attrs=['bold'])
            init['raw'] = raw
    except KeyError:
        init['raw'] = None
# ------------------------------- pocess_dir  ---------------------------------
    try:
        if init['process']:
            if options.process is None:
                process = os.path.join(data, 'process')
            elif not os.path.exists(options.process):
                cprint(' ** ERROR: option -p (process directory) given points '
                       'to a broken path', 'red', attrs=['bold'])
                sys.exit(4)
            else:
                process = options.process
            cprint('|  PROCESS DIR = ' + process, 'blue', attrs=['bold'])
            init['process'] = process
    except KeyError:
        init['process'] = None
# ------------------------------ .project file  -------------------------------
    try:
        if init['params']:
            if options.params is None or not os.path.exists(options.params):
                cprint('|', 'blue', attrs=['bold'])
                print(colored('|', 'blue', attrs=['bold']),
                      colored('WARNING: protocol.project file was not given',
                              'yellow', attrs=['bold']))
                projs = get_regex(utils, ".project", False)
                if not(projs):
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('ERROR: No .project in ' + utils, 'red',
                                  attrs=['bold']))
                    sys.exit(5)
                print(colored('|', 'blue', attrs=['bold']),
                      colored('| Found following .project files in /utils plea'
                              'se choose one or exit the script with CTRL + C',
                              'yellow', attrs=['bold']))
                for a, b in enumerate(projs):
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('| ' + str(a + 1) + ':', 'yellow',
                                  attrs=['bold']), b)
                proj_idx = int(input(colored('| ', 'blue', attrs=['bold']) +
                                     colored('| Project number? :', 'yellow',
                                             attrs=['bold']))) - 1
                if proj_idx > len(projs) - 1:
                    print(colored('|', 'blue', attrs=['bold']),
                          colored('ERROR: index given out of range', 'red',
                                  attrs=['bold']))
                    sys.exit(6)
                cprint('|', 'blue', attrs=['bold'])
                parameters = projs[proj_idx]
            else:
                parameters = options.params
            init['params'], init['dicom'] = readProjectParams(parameters)
            cprint('|  PARAMs FILE = ' + parameters, 'blue', attrs=['bold'])
    except KeyError:
        init['params'] = None
# ------------------------------- .list file  ---------------------------------
    if options.subjects is None:
        print(colored('|', 'blue', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('WARNING: subjects.list file was not given',
                      'yellow', attrs=['bold']))
        lists = get_regex(utils, '.list', False)
        if not(lists):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('ERROR: No list in ' + utils, 'red',
                          attrs=['bold']))
            sys.exit(7)
        print(colored('|', 'blue', attrs=['bold']),
              colored('| Found following .list files in /utils please choo'
                      'se one or exit the script with CTRL + C', 'yellow',
                      attrs=['bold']))
        for a, b in enumerate(lists):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('| ' + str(a + 1) + ':', 'yellow', attrs=['bold']),
                  b)
        list_idx = int(input(colored('| ', 'blue', attrs=['bold']) +
                             colored('| Subject List number? :', 'yellow',
                                     attrs=['bold']))) - 1
        if list_idx > len(lists) - 1:
            print(colored('|', 'blue', attrs=['bold']),
                  colored('ERROR: index given out of range', 'red',
                          attrs=['bold']))
            sys.exit(8)
        print(colored('|', 'blue', attrs=['bold']))
        sub_list = readSubjectsLists(lists[list_idx])
        cprint('| SUBJECT LIST = ' + lists[list_idx], 'blue', attrs=['bold'])
    elif os.path.isfile(options.subjects):
        sub_list = readSubjectsLists(options.subjects)
        cprint('| SUBJECT LIST = ' + options.subjects, 'blue', attrs=['bold'])
    elif os.path.isdir(options.subjects):
        s = options.subjects
        sub_list = []
        base = os.path.basename(s)
        base2 = os.path.basename(os.path.dirname(s))
        raw = os.path.join(data, 'raw')
        test1 = os.path.join(raw, base)  # if match -> patient folder given
        test2 = os.path.join(raw, base2)  # if match -> visit folder given
        test3 = os.path.join(test2, base)  # just to be really sure
        if os.path.isdir(test1):
            visits = get_regex(test1, '.', deep_search=False)
            visits.sort()
            for v in visits:
                dic = {'Id': base,
                       'Visit': os.path.basename(v)}
                sub_list.append(dic)
        elif os.path.isdir(test2) and os.path.isdir(test3):
            dic = {'Id': base2,
                   'Visit': base}
            sub_list.append(dic)
    elif isinstance(options.subjects, str):
        s = options.subjects
        sub_list = []
        raw = os.path.join(data, 'raw')
        subjs = s.split(',')
        for s in subjs:
            id_visits = s.split('/')
            s_dir = os.path.join(raw, id_visits[0])
            if not os.path.isdir(s_dir):
                print(colored('|', 'blue', attrs=['bold']),
                      colored("ERROR: Subject inexistant", 'red',
                              attrs=['bold']), id_visits[0])
                continue
            if len(id_visits) == 1:  # no visit given = All visits
                visits = get_regex(s_dir, '.', deep_search=False)
                visits.sort()
                for v in visits:
                    if os.path.isdir(v):
                        dic = {'Id': id_visits[0],
                               'Visit': os.path.basename(v)}
                        sub_list.append(dic)
            else:
                v_dir = os.path.join(s_dir, id_visits[1])
                if not os.path.isdir(v_dir):
                    print(colored('|', 'blue', attrs=['bold']),
                          colored("ERROR: Visit inexistant", 'red',
                                  attrs=['bold']),
                          id_visits[0] + '/' + id_visits[1])
                    continue
                dic = {'Id': id_visits[0],
                       'Visit':  id_visits[1]}
                sub_list.append(dic)
    init['subjects'] = sub_list
    return init


'''
###############################################################################
################################# MOVE FILES ##################################
###############################################################################
'''


def moveFiles(path, files):
    createDir(path)
    if isinstance(files, str):
        files = [files]
    for f in files:
        shutil.move(f, path)
    return path


'''
###############################################################################
################################# CHANGE NAME #################################
###############################################################################
Create name from the original image

suffix: a suffix is added to the filename
outdir: replace the path of the name for another path
extension: change the extension of the input name ex: '.nii'.
    If None: does not change extension. It deals with .gz extensions
prefix: add a prefix to the name
return: string with the changed name
'''

# =============================================================================
# def changeName(filename, suffix='', outdir=None, newext=None, prefix=''):
#     # do fileparts
#     name = os.path.basename(filename)
#     path = os.path.dirname(filename)
#     if len(path) > 0 and not path[-1] == '/':
#         path = path + os.sep
#     ext = ''
#     spp = name.split('.')
#     if len(spp) == 1:
#         ext = ''
#     elif spp[-1] == 'gz' and len(spp) > 2:
#         ext = '.' + spp[-2] + '.' + spp[-1]
#         name = '.'.join(spp[:-2])
#     else:
#         ext = '.' + spp[-1]
#         name = '.'.join(spp[:-1])
#     # check prefix and suffix
#     if prefix:
#         name = prefix + name
#     if suffix:
#         name = name + suffix
#     newpath = ''
#     if outdir:
#         if len(outdir) > 0 and not outdir[-1] == '/':
#             outdir = outdir + os.sep
#         newpath = outdir + name
#     else:
#         newpath = path + name
#     # Add extension
#     if newext is None:
#         newpath = newpath + ext
#     else:
#         if len(newext) == 0 or newext[0] == '.':
#             newpath = newpath + newext
#         else:
#             newpath = newpath + '.' + newext
#     return newpath
# =============================================================================


def changeName(filename, suffix=None, outdir=None, newext=None, prefix=None,
               remove=None):
    # do fileparts
    name = os.path.basename(filename)
    path = os.path.dirname(filename)
    ext1 = os.path.splitext(name)[1]
    ext2 = os.path.splitext(os.path.splitext(name)[0])[1]
    name = os.path.splitext(os.path.splitext(name)[0])[0]
    if newext is not None:
        ext = newext
        if not ext.startswith('.') and ext != '':
            ext = '.' + ext
    elif ext2:
        ext = ext2 + ext1
    elif ext1:
        ext = ext1
    else:
        ext = ''
    if remove:
        start = name.find(remove)
        if 0 <= start:
            end = start + len(remove)
            name = name[:start] + name[end:]
    if prefix:
        name = prefix + name
    if suffix:
        name = name + suffix
    if outdir is not None:
        path = outdir
    return os.path.join(path, name + ext)


'''
###############################################################################
################################## WRITE LOG ##################################
###############################################################################
Function writeLOG,, will as it name states, write wanted events in the LOGFile
created in the data/raw/ as the format
log_yearmonthday_subjects.list_project.project.txt
'''


def writeLOG(strings, filename):
    if filename:
        p = open(filename, 'a')
        p.writelines(strings)
        p.close()


'''
###############################################################################
############################### REGEX DIRECTORY ###############################
###############################################################################
'''


def get_subdir_regex(dirs, regex, verbose=False):
    # check inputs
    if isinstance(dirs, str):
        dirs = [dirs]
    elif len(dirs) == 0:
        print(' ** NO directories found!!')
        return []
    if isinstance(regex, str):
        regex = [regex]
    if len(regex) == 1:
        finaldirs = []
        comp = re.compile(regex[0])
        for d in dirs:
            d = os.path.abspath(d)
            if not os.path.isdir(d):
                continue
            files = os.listdir(d)
            files.sort()
            for f in files:
                ff = d + os.sep + f
                if not os.path.isdir(ff):
                    continue
                if comp.search(f) is None:
                    continue
                finaldirs.append(ff)
        return finaldirs
    else:
        finaldirs = dirs
        for r in regex:
            finaldirs = get_subdir_regex(finaldirs, r)
        if verbose:
            for d in finaldirs:
                print(d)
        return finaldirs


'''
###############################################################################
################################# REGEX FILES #################################
###############################################################################
'''


def get_subdir_regex_files(dirs, regex, opts={'items': -1}):
    ''' get files from dirs depending in the regular expression
    dirs: is a list of input directories
    regex: is a list of regular expresions. Each item of the list is a
    subdirectory being the last regex the one refering to the filename
    items: is the maximum number of items for each folder

    Items are sorted in each file by alphabetical order
    '''

    # check inputs
    if isinstance(dirs, str):
        dirs = [dirs]
    elif not dirs:
        print(' ** Error: No dirs found!!')
        return []
    if isinstance(regex, str):
        regex = [regex]
    # extracting options
    items = -1
    if 'items' in opts:
        items = int(opts['items'])
    # search subdir levels
    if len(regex) == 1:
        finaldirs = []
        # this is the final level
        comp = re.compile(regex[0])
        for d in dirs:
            d = os.path.abspath(d)
            if not os.path.isdir(d):
                continue
            files = os.listdir(d)
            files.sort()
            i = 0
            for f in files:
                ff = d + os.sep + f
                if comp.search(f) is None:
                    continue
                finaldirs.append(ff)
                i = i + 1
            if items > 0 and i != items:
                print('WARNING found %d item and not %s in %s' % (i, items, d))
                break
        return finaldirs
    else:
        # decomposing recursively
        finaldirs = dirs
        for r in range(len(regex) - 1):
            finaldirs = get_subdir_regex(finaldirs, regex[r], opts)
        finaldirs = get_subdir_regex_files(finaldirs, regex[-1], opts)
        return finaldirs


'''
###############################################################################
#################################### COMMAND ##################################
###############################################################################
Execute a command line
commandline: list of strings containing each item of the command line
    ['cp','original.nii','copy.nii']
inputs: string or list of strings, all inputs must be present in order to
    execute the command, otherwise error is returned
outputs: string or list of strings. Two tests:
   - If all outputs exists BEFORE execution, the command is not executed
   - If not all outputs exists AFTER execution, returns error
clfile: if string, the commandline executed is appended into the file
logfile: if string, the output of the command is appended into the file
verbose: the output of the command is printed the screen
timecheck: if inputs are newer than outputs, the command is executed: TO CHECK
'''


def command(commandline, inputs=None, outputs=None, clfile=None,
            logfile=None, verbose=True, timecheck=False):
    strline = ' '.join(commandline)
    if verbose:
        print(strline)
    # Check newer input file
    itime = -1  # numer of seconds since epoch
    inputs_exist = True
    if inputs is not None:
        # check if input is only string and not list
        if isinstance(inputs, str):
            if not os.path.exists(inputs):
                inputs_exist = False
                print(' ** Error: Input does not exist! :: ' + str(inputs))
            else:
                itime = os.path.getmtime(inputs)
        else:
            for i in inputs:
                if not os.path.exists(i):
                    inputs_exist = False
                    print(' ** Error: One input does not exist! :: ' + i)
                else:
                    timer = os.path.getmtime(i)
                    if timer < itime or itime < 0:
                        itime = timer
    # Check if outputs exist AND is newer than inputs
    outExists = False
    otime = -1
    if outputs is not None:
        if isinstance(outputs, str):
            outExists = os.path.exists(outputs)
            if outExists:
                otime = os.path.getmtime(outputs)
        else:
            for o in outputs:
                outExists = os.path.exists(o)
                if outExists:
                    timer = os.path.getmtime(o)
                    if timer > otime:
                        otime = timer
                if not outExists:
                    break
    if outExists:
        if timecheck and itime > 0 and otime > 0 and otime < itime:
            if verbose:
                print(' -- Warning: Output exists but older than input! Redoin'
                      'g command')
                print('     otime ' + str(otime) + ' < itime ' + str(itime))
        else:
            if verbose:
                print(' -- Skipping: Output Exists')
            return 0
    # Check if inputs exist
    if inputs is not None and inputs_exist is False:
        print(' ** Error  in ' + commandline[0] + ':The input does not exists!\
              :: ' + str(inputs))
        return -1
    # run command
    # open comand line file
    if clfile is not None:
        f = open(clfile, 'a')
        f.write(strline + '\n')
        f.close()
    try:
        if logfile is not None:
            f = open(logfile, 'a')
            outvalue = call(commandline, stdout=f)
            f.close()
        else:
            outvalue = call(commandline)
    except OSError:
        print(' XX ERROR: unable to find executable ' + commandline[0])
        return -1
    if not outvalue == 0:
        print(' ** Error  in ' + commandline[0] + ': Executable output was',
              str(outvalue))
        return outvalue
    outExists = False
    if outputs is None:
        outExists = True
    elif isinstance(outputs, str):
        outExists = os.path.exists(outputs)
    else:
        for o in outputs:
            outExists = os.path.exists(o)
            if not outExists:
                break
    if not outExists:
        if verbose:
            print(' ** Error  in ' + commandline[0] + ': Error: output does no'
                  't exist!')
            return -1
    # return command output
    return outvalue


def cmdWoutput(commandline, clfile=None, verbose=False):
    """
        Execute a command line, the output is return as a string
        This is useful to obtain information from the command line (e.x. when
        using fslstat)
        commandline: list of strings containing each item of the command line
            ["cp","original.nii","copy.nii"]
        clfile: if string, the commandline executed is appended into the file
        return : False if error
    """
    if verbose:
        print(" ".join(commandline))
    if clfile:
        f = open(clfile, 'a')
        f.write(" ".join(commandline) + '\n')
        f.close()
    cline = commandline
    lines = []
    try:
        lines = Popen(cline, stdout=PIPE).communicate()
    except OSError:
        print(" XX ERROR: unable to find executable! " + str(commandline[0]))
        return -1
    return lines[0].decode()  # we ignore the error output :: lines[1]


def send_user(script):
    u = getpass.getuser()
    mail = smtplib.SMTP('smtp.gmail.com', 587)
    msg = EmailMessage()
    msg['Subject'] = 'Finished Script: ' + script
    msg['From'] = Address("Pysep3", "sep.script.finished", "gmail.com")
    msg['To'] = Address(u, u, "icm-institute.org")
    script_name = os.path.basename(__file__)
    name = u.split('.')[0].capitalize()
    msg.set_content('Hello ' + name + ',\n\nThe script you launched finished '
                    'without errors \n\nYours Sincerely,\nPysep3.')
    mail.ehlo()
    mail.starttls()
    mail.login('sep.script.finished@gmail.com', 'StankICM2020')
    mail.send_message(msg)
    mail.close()


def fetch_directory(directory, subject, dirname):
    fetched = os.path.join(directory, subject['Id'], subject['Visit'], dirname)
    if not os.path.isdir(fetched):
        print('|', '|', 'ERROR: Directory not found', fetched)
        print('|', 'L')
        return False
    print('|', '|', dirname, ':', fetched)
    return fetched


def create_prefix(s, fold):
    return os.path.join(fold, s['Id'] + '_' + s['Visit'] + '_')


def retrieve_name(var):
    callers_local_vars = inspect.currentframe().f_back.f_locals.items()
    return [var_name for var_name, var_val in callers_local_vars if var_val
            is var]
