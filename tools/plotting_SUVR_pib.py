#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 26 17:24:34 2021

@author: arya.yazdan-panah
"""

import os
import numpy as np
from common3 import get_regex, readSubjectsLists
from termcolor import cprint, colored
from tools_svca import readSIF, TAC
import matplotlib.pyplot as plt


raw = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/raw'
process = '/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/data/process'
subjects = readSubjectsLists('/network/lustre/iss01/lubetzki-stankoff/clinic/users/Arya/PIB_retest/utils/All.list')
tracer = 'pib'
print(colored("| Tracer used:", "blue", attrs=["bold"]), tracer.upper())
N = len(subjects)
# ------------------------------- processing  ---------------------------------
for i, s in enumerate(subjects):
    cprint('|', 'blue', attrs=['bold'])
# ----------------------------- Start of 1 subject ----------------------------
    print(s['Id'], s['Visit'])
# --------------------- Defining path to petpreproc ------------------------
    raw_pet_dir = os.path.join(raw, s['Id'], s['Visit'], tracer)
    if not os.path.isdir(raw_pet_dir):
        print(1)
        continue
    print(' raw: ', raw_pet_dir)
    test = get_regex(raw_pet_dir, tracer + '.sif', False)
    if not test:
        print(2)
        continue
    s['sif'] = test[0]
    print('SIF file :', s['sif'])
    time, no, no, delta = readSIF(s['sif'])
# --------------------- Defining path to petpreproc ------------------------
    petpreproc = os.path.join(process, s['Id'], s['Visit'], tracer + 'preproc')
    if not os.path.isdir(petpreproc):
        print(3)
        continue
    print('petpreproc', petpreproc)
    test = get_regex(petpreproc, '_movcorr.nii.gz', False)
    if not test:
        print(4)
        continue
    s['mov_corr'] = test[0]
    print('Found Motion Corrected', s['mov_corr'])
# ----------------------- Defining path to petsuper ---------------------------
    petsuper = os.path.join(process, s['Id'], s['Visit'], tracer + 'super')
    if not os.path.isdir(petsuper):
        print(5)
        continue
    print('petsuper', petsuper)
    test = get_regex(petsuper, 'refregion.nii', False)
    if not test:
        print(6)
        continue
    s['ref_reg'] = test[0]
    print('Found refreg', s['ref_reg'])
    refTAC = TAC(s['mov_corr'], s['ref_reg'])

# # ----------------------- Defining path to petROI -----------------------------
#     petROI = os.path.join(process, s['Id'], s['Visit'], tracer + 'ROI')
#     if not os.path.isdir(petsuper):
#         print(7)
#         continue
#     print('petROI', petROI)
#     test = get_regex(petROI, '_brainmask_p0.90_2pib.nii.gz', False)
#     if not test:
#         print(8)
#         continue
#     s['b_mask'] = test[0]
#     print('Found Bmask', s['b_mask'])
#     brainTAC = TAC(s['mov_corr'], s['b_mask'])
# # ---------------------------- Output Directory -------------------------------
#     if i == 0:
#         brainTACs = np.zeros((len(time), N))
#         refTACs = np.zeros((len(time), N))
#         suvrTACs = np.zeros((len(time), N))
#     brainTACs[:, i] = brainTAC
#     refTACs[:, i] = refTAC
#     suvrTACs[:, i] = np.divide(brainTACs[:, i], refTACs[:, i])
# brainTAC = np.nanmean(brainTACs, 1)
# refTAC = np.nanmean(refTACs, 1)
# suvrTAC = np.nanmean(suvrTACs, 1)

# plt.close()
# fig = plt.figure(1)
# ax = fig.add_subplot(1, 1, 1)
# # Move left y-axis and bottim x-axis to centre, passing through (0,0)
# ax.spines['bottom'].set_position('center')
# ax.spines['left'].set_position('zero')
# # Eliminate upper and right axes
# ax.spines['top'].set_color('none')
# ax.spines['right'].set_color('none')

# # ax.plot(time, brainTAC, marker='o')
# # ax.plot(time, refTAC, marker='o')
# ax.plot(time, suvrTAC, marker='o')
# plt.style.use('dark_background')
