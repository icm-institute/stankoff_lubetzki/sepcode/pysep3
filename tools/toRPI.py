#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import sys
from common3 import command, changeName, getFSLExtension, cmdWoutput


def toRPI(iima, oima, verbose=False):
    if verbose:
        print(" -- Reorienting the image to RPI")
        print(" The orientation won't be changed!")
        print("  only the way the image is stored in the file")
    stdima = changeName(oima, "_std")
    stdima = getFSLExtension(stdima)
    oima = getFSLExtension(oima)
    iima = getFSLExtension(iima)
    # if os.path.exists(oima):
    #   print " ** Skiping output image exists!" + oima
    #   return 0
    comm = ["fslreorient2std", iima, stdima]
    if command(comm, iima, stdima, verbose=verbose):
        print(" -- ERROR in " + comm[0])
        return -1
    convention = cmdWoutput(["fslorient", stdima])
    retval = 0
    if convention.count("RADIOLOGICAL") == 1:
        print(" ** Image is already Radiological: skipping")
        retval = command(["immv", stdima, oima], stdima, verbose=verbose)
    elif convention.count("NEUROLOGICAL") == 1:
        print(" ** Image is Neurological: flipping to radiological")
        comm = ["fslswapdim", stdima, "-x", "y", "z", oima]
        if command(comm, stdima, verbose=verbose):
            print(" -- ERROR in "+comm[0])
        comm = ["fslorient", "-forceradiological", oima]
        retval = command(comm, oima, verbose=verbose)
    command(["rm", "-f", stdima], verbose=verbose)
    return retval


if __name__ == "__main__":
    usage = " usage: %s <input_image> <reoriented_image>"
    if len(sys.argv) < 3:
        print(" -- Insufficient number of arguments")
        print(usage)
        sys.exit(1)
    iima = sys.argv[1]
    oima = sys.argv[2]
    if not os.path.exists(iima):
        print(" -- INPUT IMAGE does not exists")
        print(usage)
        sys.exit(1)
    if os.path.exists(oima):
        print(" -- OUPUT IMAGE already exists")
        print("         Erase the output image and redo this command")
        print(usage)
        sys.exit(1)
    toRPI(iima, oima, True)
