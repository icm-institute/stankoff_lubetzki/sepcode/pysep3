#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 16:42:21 2020

@author: arya.yazdan-panah
"""

import os
import time
from termcolor import colored
from common3 import createDir, changeName
from tools_nibabel import flip_nii
from nipype.interfaces.ants import (Registration, ApplyTransforms,
                                    N4BiasFieldCorrection)
from nipype.interfaces.ants.segmentation import BrainExtraction


def antsBrainExtraction(anat_file, template, template_mask, out_prefix, nt=6,
                        verbose=False):
    now = time.time()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Brain Extraction:', 'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| antsBrainExtraction() from tools_ants.py', 'cyan',
                  attrs=['bold']))
    bet = BrainExtraction()
    bet.inputs.dimension = 3
    if not os.path.isfile(anat_file):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored("ERROR: anat_file or seq_fix doesn't existing", 'red',
                      attrs=['bold']), anat_file)
        return
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| input file:', 'cyan', attrs=['bold']), anat_file)
    bet.inputs.anatomical_image = anat_file
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| input template:', 'cyan', attrs=['bold']), template)
    bet.inputs.brain_template = template
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| input template mask:', 'cyan', attrs=['bold']),
          template_mask)
    bet.inputs.brain_probability_mask = template_mask
    if not os.path.isdir(out_prefix):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored("ERROR: out_prefix non existant please make sure it exis"
                      "ts", 'red', attrs=['bold']), out_prefix)
        return
    if not out_prefix[-1] == os.sep:
        out_prefix = out_prefix + os.sep
    bet.inputs.out_prefix = out_prefix
    bet.inputs.num_threads = nt
    bet.inputs.keep_temporary_files = 1
    # bet.inputs.quick_registration = 1
    if verbose:
        bet.terminal_output = 'stream'
    else:
        bet.terminal_output = 'none'
    outs = bet._list_outputs()
    bmask_path = outs['BrainExtractionMask']
    if os.path.isfile(bmask_path):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored('WARNING: Skipping Brain Extraction since it was already'
                      ' performed', 'yellow', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored('| For a new extraction, delete/rename existing f'
                      'iles.', 'yellow', attrs=['bold']), bmask_path)
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return
    bet.run()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Brain mask created:', 'cyan', attrs=['bold']),
          bmask_path)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored("| Extraction time: {0:.2f} secs.".format(time.time() - now),
                  'cyan', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))


def n4_corr(input_file, N4_path, nt=6, verbose=False):
    """
    ANTS' N4 Bias Field Correction, (requires ANTs installed on the machine
    and ANTSPATH to be defined) WARNING! If 'N4_path' already exists, the
    function skips the bias field correction.

    Parameters
    ----------
    input_file : path
        Image to correct.
    N4_path : path
        Corrected image.
    nt : int, optional
        number of thread, check available cores on hardware before changing.
        The default is 6.

    Returns
    -------
    path
        path to bias field corrected image
    """
    now = time.time()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| N4 Correction:', 'cyan', attrs=['bold', 'reverse']))
    if os.path.isfile(N4_path):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored('WARNING: Skipping N4 since it was already performed',
                      'yellow', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored('| For a new correction, delete/rename existing f'
                      'iles.', 'yellow', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return N4_path
    n4 = N4BiasFieldCorrection()
    n4.inputs.dimension = int(3)
    n4.inputs.input_image = input_file
    n4.inputs.bspline_fitting_distance = 200
    n4.inputs.shrink_factor = 4
    n4.inputs.n_iterations = [50, 50, 50, 50]
    n4.inputs.convergence_threshold = 1e-7
    n4.inputs.output_image = N4_path
    n4.inputs.num_threads = nt
    if verbose:
        n4.terminal_output = 'stream'
    else:
        n4.terminal_output = 'none'
    if not(os.path.isfile(N4_path)):
        n4.run()
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Correction Successful', 'cyan', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored("| Correction time: {0:.2f} secs.".format(time.time() -
                                                                now), 'cyan',
                      attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
    return N4_path


def AntsRegistrationSyn(seq_mov, seq_fix, warp, invwarp, transfo_type,
                        msk_fix=False, initial_moving_transform=0,
                        big_image=False, nt=6, verbose=False):
    """
    ANTS' antsRegistrationSyN.sh, registration of an image onto another.
    This registration can be rigid, affine or synmetric.

    Parameters
    ----------
    seq_mov : path
        Image to register.
    seq_fix : path
        Image to register on.
    warp : path
        output image path.
    invwarp : path
        output inv image path.
    transfo_type : string
        Transformation type: 'r' = rigid | 'a' = affine | 's' = synmetric.
    msk_fix : path, optional
        Path to mask to mask reference image. The default is None.
    verbose : bool, optional
        stream antsRegistration verbose to terminal. The default is False.
    imt : int, optional
        initial moving transform, useful for shifted images. The default is 0.
    big_image : bool, optional
        uses different iteration steps better suited to larger images.
        The default is False.
    nt : int, optional
        number of thread, check available cores on hardware before changing.
        The default is 6.

    Returns
    -------
    dict
        containing:
            - dict['warped_image']: mov to fix
            - dict['inverse_warped_image']: fix to mov
            - dict['forwad_transforms']: transforms mov to fix
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| ANTS Registration: ',
                  'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| AntsRegistration() from tools_ants.py', 'cyan',
                  attrs=['bold']))
# ----------------------------- Checking inputs -------------------------------
    if not os.path.isfile(seq_mov) or not os.path.isfile(seq_fix):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored("ERROR: seq_mov or seq_fix doesn't existing", 'red',
                      attrs=['bold']))
        return
    if transfo_type not in {'r', 'a', 's'}:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored("ERROR: 'transfo_type' must be a string, values can only"
                      " be:", 'red', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored("- 'r' : for rigid, stages = ['Rigid']", 'red',
                      attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored("- 'a' : for affine, stages = ['Rigid','Affine']", 'red',
                      attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored("- 's' : for synmetric, stages = ['Rigid','Affine','SyN'"
                      "]", 'red',
                      attrs=['bold']))
        return
# ---------------------------- Stage definitions ------------------------------
    Rigid = {'transform': 'Rigid',
             'trans_param': (0.1,),
             'conv_iter': [1000, 500, 250, 100],
             'conv_thrs': 1e-6,
             'conv_wind': 10,
             'shrink_factors': [8, 4, 2, 1],
             'smoothing_sigma_unit': 'vox',
             'smoothing_sigmas': [3, 2, 1, 0],
             'metric_name': 'MI',
             'metric_weig': 1,
             'metric_nbin': 32,
             'metric_samp': 'Regular',
             'metric_perc': 0.25,
             'interpolation': 'Linear',
             'hist_match': True}  # False
    Affine = {'transform': 'Affine',
              'trans_param': (0.1,),
              'conv_iter': [1000, 500, 250, 100],
              'conv_thrs': 1e-6,
              'conv_wind': 10,
              'shrink_factors': [8, 4, 2, 1],
              'smoothing_sigma_unit': 'vox',
              'smoothing_sigmas': [3, 2, 1, 0],
              'metric_name': 'MI',
              'metric_weig': 1,
              'metric_nbin': 32,
              'metric_samp': 'Regular',
              'metric_perc': 0.25,
              'interpolation': 'Linear',
              'hist_match': False}
    Syn = {'transform': 'SyN',
           'trans_param': (0.1, 3, 0),
           'conv_iter': [100, 70, 50, 20],
           'conv_thrs': 1e-6,
           'conv_wind': 10,
           'shrink_factors': [8, 4, 2, 1],
           'smoothing_sigma_unit': 'vox',
           'smoothing_sigmas': [3, 2, 1, 0],
           'metric_name': 'CC',
           'metric_weig': 1,
           'metric_nbin': 4,
           'metric_samp': None,
           'metric_perc': None,
           'interpolation': 'Linear',
           'hist_match': False}
    if big_image:
        Rigid = {'transform': 'Rigid',
                 'trans_param': (0.1,),
                 'conv_iter': [1000, 500, 250, 100],
                 'conv_thrs': 1e-6,
                 'conv_wind': 10,
                 'shrink_factors': [12, 8, 4, 2],
                 'smoothing_sigma_unit': 'vox',
                 'smoothing_sigmas': [4, 3, 2, 1],
                 'metric_name': 'MI',
                 'metric_weig': 1,
                 'metric_nbin': 32,
                 'metric_samp': 'Regular',
                 'metric_perc': 0.25,
                 'interpolation': 'Linear',
                 'hist_match': False}
        Affine = {'transform': 'Affine',
                  'trans_param': (0.1,),
                  'conv_iter': [1000, 500, 250, 100],
                  'conv_thrs': 1e-6,
                  'conv_wind': 10,
                  'shrink_factors': [12, 8, 4, 2],
                  'smoothing_sigma_unit': 'vox',
                  'smoothing_sigmas': [4, 3, 2, 1],
                  'metric_name': 'MI',
                  'metric_weig': 1,
                  'metric_nbin': 32,
                  'metric_samp': 'Regular',
                  'metric_perc': 0.25,
                  'interpolation': 'Linear',
                  'hist_match': False}
        Syn = {'transform': 'SyN',
               'trans_param': (0.1, 3, 0),
               'conv_iter': [100, 100, 100, 50, 20],
               'conv_thrs': 1e-6,
               'conv_wind': 10,
               'shrink_factors': [10, 6, 4, 2, 1],
               'smoothing_sigma_unit': 'vox',
               'smoothing_sigmas': [5, 3, 2, 1, 0],
               'metric_name': 'CC',
               'metric_weig': 1,
               'metric_nbin': 4,
               'metric_samp': None,
               'metric_perc': None,
               'interpolation': 'Linear',
               'hist_match': False}
# ----------------------- Registration file management ------------------------
    trans_mat = os.path.join(os.path.dirname(warp), 'trans_mat')
    createDir(trans_mat)
    matrix_prefix = changeName(warp, outdir=trans_mat, newext='')
    reg = Registration()
    reg.terminal_output = 'none'
    reg.inputs.fixed_image = seq_fix
    reg.inputs.moving_image = seq_mov
    reg.inputs.output_transform_prefix = matrix_prefix
    reg.inputs.output_warped_image = warp
    reg.inputs.output_inverse_warped_image = invwarp
    reg.inputs.initial_moving_transform_com = initial_moving_transform
    reg.inputs.num_threads = nt
# ------------------- Arguments depending on the stage type -------------------
    transforms = []
    trns_prams = []
    conv_iters = []
    conv_thrss = []
    conv_winds = []
    shrk_facts = []
    sigm_units = []
    smth_sigms = []
    mtrc_names = []
    mtrc_weigs = []
    mtrc_nbins = []
    mtrc_samps = []
    mtrc_percs = []
    hist_mtchs = []
    interpolas = []
    transfos = {'r': [Rigid],
                'a': [Rigid, Affine],
                's': [Rigid, Affine, Syn]}
    stages = transfos[transfo_type]
    n_stages = len(stages)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Registration stages:', 'cyan', attrs=['bold']), n_stages)
    for i in range(n_stages):
        transforms.append(stages[i]['transform'])
        trns_prams.append(stages[i]['trans_param'])
        conv_iters.append(stages[i]['conv_iter'])
        conv_thrss.append(stages[i]['conv_thrs'])
        conv_winds.append(stages[i]['conv_wind'])
        shrk_facts.append(stages[i]['shrink_factors'])
        sigm_units.append(stages[i]['smoothing_sigma_unit'])
        smth_sigms.append(stages[i]['smoothing_sigmas'])
        mtrc_names.append(stages[i]['metric_name'])
        mtrc_weigs.append(stages[i]['metric_weig'])
        mtrc_nbins.append(stages[i]['metric_nbin'])
        mtrc_samps.append(stages[i]['metric_samp'])
        mtrc_percs.append(stages[i]['metric_perc'])
        hist_mtchs.append(stages[i]['hist_match'])
        interpolas.append(stages[i]['interpolation'])
    reg.inputs.transforms = transforms
    reg.inputs.transform_parameters = trns_prams
    reg.inputs.number_of_iterations = conv_iters
    reg.inputs.convergence_threshold = conv_thrss
    reg.inputs.convergence_window_size = conv_winds
    reg.inputs.shrink_factors = shrk_facts
    reg.inputs.sigma_units = sigm_units
    reg.inputs.smoothing_sigmas = smth_sigms
    reg.inputs.metric = mtrc_names
    reg.inputs.metric_weight = mtrc_weigs
    reg.inputs.radius_or_number_of_bins = mtrc_nbins
    reg.inputs.sampling_strategy = mtrc_samps
    reg.inputs.sampling_percentage = mtrc_percs
    reg.inputs.use_histogram_matching = hist_mtchs
# ---------------------- Mask usage (under developpement) ---------------------
    if msk_fix:
        reg.inputs.fixed_image_masks = [msk_fix] * n_stages
# ------------------- Constant arguments of the registration ------------------
    reg.inputs.dimension = 3
    reg.inputs.float = True
    reg.inputs.collapse_output_transforms = True
    reg.inputs.initialize_transforms_per_stage = False
    reg.inputs.winsorize_lower_quantile = 0.005
    reg.inputs.winsorize_upper_quantile = 0.995
    outs = reg._list_outputs()
    if verbose:
        print(reg.cmdline)
        reg.inputs.verbose = True
        reg.terminal_output = 'stream'
    if not os.path.isfile(outs['warped_image']):
        reg.run()
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Registration successful, outputs are:', 'cyan',
                      attrs=['bold']))
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping registration, files already exist:', 'cyan',
                      attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| 1: warped_image:', 'cyan', attrs=['bold']),
          outs['warped_image'])
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| 2: inverse_warped_image:', 'cyan', attrs=['bold']),
          outs['inverse_warped_image'])
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| 3: forward_transform matrix:', 'cyan', attrs=['bold']),
          str(outs['forward_transforms']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return outs


def ApplyRegistration(seq_mov, seq_fix, transforms, warp, flags=False,
                      interp='Linear', is_4D=False, nt=6, over=False):
    """
    ANTS' Apply Registration

    Parameters
    ----------
    seq_mov : path
        Image to register.
    seq_fix : path
        Image to register on.
    transforms : list[paths]
        transformations to be applied, Last in, First applied.
    warp : path
        output image path.
    flags : list[bool], optional
        invert respective transformation according to flag. The default is
        [False].
    interp : string, optional
        interpolation to be used, check ants' help. The default is 'Linear'.
        ‘Linear’ or ‘NearestNeighbor’ or ‘CosineWindowedSinc’ or
        ‘WelchWindowedSinc’ or ‘HammingWindowedSinc’ or ‘LanczosWindowedSinc’
        or ‘MultiLabel’ or ‘Gaussian’ or ‘BSpline’
    nt : int, optional
         number of thread, check available cores on hardware before changing.
         The default is 6.

    Returns
    -------
    path
        path to registered image.
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Registration: ',
                  'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| ApplyRegistration() from tools_ants.py', 'cyan',
                  attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Applied Transforms are:', 'cyan', attrs=['bold']))
    if type(transforms) != list:
        transforms = [transforms]
    for i, t in enumerate(transforms):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']), str(i + 1) + ': ' + t)
    at = ApplyTransforms()
    at.terminal_output = 'none'
    at.inputs.dimension = 3
    if is_4D:
        at.inputs.input_image_type = 3
    else:
        at.inputs.input_image_type = 0
    at.inputs.float = True
    at.inputs.input_image = seq_mov
    at.inputs.reference_image = seq_fix
    at.inputs.transforms = transforms
    at.inputs.output_image = warp
    at.inputs.interpolation = interp
    if not flags:
        flags = [False] * len(transforms)
    at.inputs.invert_transform_flags = flags
    at.terminal_output = 'none'
    at.inputs.num_threads = nt
    outs = at._list_outputs()
    if os.path.isfile(outs['output_image']) and not over:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping Registration, files already exist:', 'cyan',
                      attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              str(outs['output_image']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return outs['output_image']
    at.run()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Image created:', 'cyan', attrs=['bold']),
          outs['output_image'])
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return outs['output_image']


def flip_nii_reg(nii, out, transforms=False, flags=False, interp='Linear'):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Flipping and Registering on itself: ',
                  'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| flip_nii_reg() from tools_ants.py', 'cyan',
                  attrs=['bold']))
    outdir = os.path.dirname(out)
    flipped = flip_nii(nii, 0, outdir)
    useless = changeName(flipped, '_useless')
    if transforms:
        if type(transforms) != list:
            transforms = [transforms]
        if not flags:
            flags = [False] * len(transforms)
        ApplyRegistration(flipped, nii, transforms, out, flags, interp)
    else:
        reg = AntsRegistrationSyn(flipped, nii, out, useless, 'r')
        if os.path.isfile(useless):
            os.remove(useless)
        transforms = reg['forward_transforms']
    os.remove(flipped)
    return out, transforms
