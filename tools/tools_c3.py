#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 16:42:21 2020

@author: arya.yazdan-panah
"""

import os
import shutil
from termcolor import colored
from tools_fsl import fslstats, fslmaths
from common3 import seq_info, changeName, cmdWoutput, createDir
from nipype.interfaces.c3 import C3dAffineTool


def fsl2itk(mat, ref, mov):
    """
    Uses C3dAffineTool to convert an FSL matrix to ITK format.

    Parameters
    ----------
    mat : path
        path to matrix to convert.
    ref : path
        path to the reference image for the matrix creation.
    mov : path
        path to the moving image for the matrix creation.

    Returns
    -------
    path
        path, to converted matrix.
    """
    name, seqname, out_dir = seq_info(mat)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Converting ' + seqname + ' to ITK compatible: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| fsl2itk() from tools_c3.py', 'cyan', attrs=['bold']))
    output = os.path.join(out_dir, name + '_' + seqname + '_ITK.mat')
    c3 = C3dAffineTool()
    c3.inputs.transform_file = mat
    c3.inputs.reference_file = ref
    c3.inputs.source_file = mov
    c3.inputs.itk_transform = output
    c3.inputs.fsl2ras = True
    c3.ignore_exception = True
    outs = c3._list_outputs()
    if os.path.isfile(outs['itk_transform']):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping conversion, matrix already exist:', 'cyan',
                      attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              ' - ' + str(outs['itk_transform']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return outs['itk_transform']
    c3.run()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| ITK compatible matrix created:', 'cyan',
                  attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('|', 'cyan', attrs=['bold']),
          ' - ' + str(outs['itk_transform']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return outs['itk_transform']


def computeMTR(mton, mtoff, mtr, odir):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Computing MTR: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| computeMTR() from tools_c3.py', 'cyan', attrs=['bold']))
    if os.path.isfile(mtr):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping computation, image already exist:', 'cyan',
                      attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              ' - ' + str(mtr))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return mtr
    tmp1 = changeName(mton, "_mask", outdir=odir)
    tmp2 = changeName(mtoff, "_mask", outdir=odir)
    comm = ["c3d", mton, "-as", "M", mtoff, "-as", "N",
            "-pim", "Range", "-threshold", "5%", "100%", "1", "0", "-as", "B",
            "-scale", "-1", "-shift", "1", "-as", "C", "-push", "M", "-push",
            "B", "-multiply", "-push", "C", "-add", "-as", "MN", "-o", tmp1,
            "-push", "N", "-push", "B", "-multiply", "-push", "C", "-add",
            "-as", "NN", "-o", tmp2]
#          "-push","NN","-push","MN","-scale","-1","-add",
#          "-push","NN","-divide","-clip","0","1","-o",mtrimage]
    cmdWoutput(comm)
    comm2 = ["fslmaths", tmp2, "-sub", tmp1, "-div", tmp2, "-thr", "0",
             "-uthr", "1", mtr]
    cmdWoutput(comm2)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| ITK compatible matrix created:', 'cyan',
                  attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('|', 'cyan', attrs=['bold']), ' - ' + str(mtr))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return mtr


def biggest_components(mask, labelled=False, separated_dir=True, n_comp=1):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Labelling mask: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| biggest_components() from tools_c3.py', 'cyan',
                  attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Labelling Mask:', 'cyan', attrs=['bold']), mask)
    if not labelled:
        labelled = changeName(mask, '_labelled')
    cmd = ["c3d", mask, "-comp", "-o", labelled]
    c3d_output = cmdWoutput(cmd).split()
    num_sub = int(c3d_output[2])
    max_size = int(c3d_output[-2])
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Number of connected components in mask:', 'cyan',
                  attrs=['bold']), num_sub)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Biggest connected components in mask:', 'cyan',
                  attrs=['bold']), max_size)
    if n_comp > num_sub:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Mask has only' + str(num_sub) +
                      ' components, connot separate ' + str(n_comp),
                      'cyan', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        n_comp = num_sub
    if isinstance(separated_dir, str):
        createDir(separated_dir)
    elif separated_dir:
        separated_dir = changeName(mask, newext='')
        createDir(separated_dir)
    tmp = changeName(labelled, '_tmp', separated_dir)
    shutil.copyfile(labelled, tmp)
    for j in range(n_comp):
        output = changeName(labelled, '_' + str(j + 1), separated_dir)
        fslmaths(tmp, "-uthr", j + 1.1, '-bin', output)
        fslmaths(tmp, "-thr", j + 1.1, tmp)
    os.remove(tmp)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return labelled, separated_dir


def connected_components(mask, labelled=False, lesion_dir=True, min_size=50):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Labelling lesion mask: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| connected_components() from tools_c3.py', 'cyan',
                  attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Labelling Mask:', 'cyan', attrs=['bold']), mask)
    if not labelled:
        labelled = changeName(mask, '_labelled')
    cmd = ["c3d", mask, "-comp", "-o", labelled]
    c3d_output = cmdWoutput(cmd).split()
    num_sub = int(c3d_output[2])
    max_size = int(c3d_output[-2])
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Number of connected components in mask:', 'cyan',
                  attrs=['bold']), num_sub)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Biggest connected components in mask:', 'cyan',
                  attrs=['bold']), max_size)
    if max_size < min_size:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Mask has no components bigger than' + str(min_size) +
                      ' voxels', 'cyan', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return labelled, False
    if not lesion_dir:
        return labelled
    elif isinstance(lesion_dir, str):
        createDir(lesion_dir)
    elif lesion_dir:
        lesion_dir = changeName(mask, newext='')
        createDir(lesion_dir)
    tmp = changeName(labelled, '_tmp', lesion_dir)
    shutil.copyfile(labelled, tmp)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Creating mask for each lesion bigger than:', 'cyan',
                  attrs=['bold']), min_size)
    for j in range(num_sub):
        output = changeName(labelled, '_' + str(j + 1), lesion_dir)
        fslmaths(tmp, "-uthr", j + 1.1, '-bin', output)
        fslmaths(tmp, "-thr", j + 1.1, tmp)
        V = int(fslstats(output, '-V').split(' ')[0])
        if V < min_size:
            os.remove(output)
            break
    os.remove(tmp)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return labelled, lesion_dir


def c3d(*args):
    op_string = ['c3d']
    for arg in args:
        if isinstance(arg, (int, float, complex)):
            op_string.append(str(arg))
        elif isinstance(arg, str):
            op_string.append(arg)
    cmdWoutput(op_string)
    return
