#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 01:41:47 2020

@author: arya.yazdan-panah
"""


import os
import stat
from datetime import date
from common3 import createDir, cmdWoutput, isinteger


def qsub(commstrings, name="clujob", queue='normal', hold=[], etime="24:00:00",
         memory="20G", clfile=True, logdir="", cpus="1"):
    """ Send a list of commands into the qsub
        - commstrings: list of commands, each string is considered a new
          command
            ['source freesurfer5;','recon-all','cd toto']
        - name : name of the process - $process_$ID
        - queue: queue in the sge
        - logdir: where to write outputs and scripts
        - hold= list of job ids to wait for them to finish
        - etime= expected execution time in seconds
        - nosge= execute on command line instead of sge (for debug mainly)

        return: the job id, useful for "hold" dependencies
        """

    # create the log dir if necessary
    if logdir == "":
        logdir = os.environ["PROJECT_DIR"] + os.sep + "clulogs" + os.sep
        createDir(logdir)
    # write the commands in the output script
    elog = logdir + os.sep + "err_" + name +".err"
    olog = logdir + os.sep + "out_" + name + ".out"

    shfile = logdir + "job_" + name + ".sh"
    p = open(shfile, "w")
    p.write("#!/bin/bash\n")
    p.write("#SBATCH --partition=" + queue + "\n")
    p.write("#SBATCH --time=" + etime + "\n")
    p.write("#SBATCH --mem=" + memory + "\n")
    p.write("#SBATCH --cpus-per-task=" + cpus + "\n")
    p.write("#SBATCH --ntasks=1\n")
    p.write("#SBATCH --nodes=1\n")
    p.write("#SBATCH --output=" + olog + "\n")
    p.write("#SBATCH --error=" + elog + "\n")
    p.write("#SBATCH --job-name=" + name + "\n")
    p.write("module load python/2.7\n")
    p.write("module load FreeSurfer/6.0.0\n")

    if isinstance(commstrings, str):
        p.write("python " + commstrings + "\n")
    elif isinstance(commstrings, list):
        for c in commstrings:
            p.write(c + "\n")
    else:
        print(" ** ERROR " + str(commstrings) + " is not a valid command")
        return -1
    p.close()
    os.chmod(shfile, stat.S_IRWXU or stat.S_IRGR or stat.S_IXGR)

    # commandline
    hcmd = []
    if len(hold) > 0:
        hh = "--dependency afterok:"
        for h in hold:
            hh = hh + str(h) + ":"
        hcmd = [hh[:-1]]
    comm = ["sbatch"] + hcmd + [str(shfile)]
    if clfile:
        sid = name[-7:]
        outtxt = cmdWoutput(comm, logdir + os.sep + sid + ".txt")
    else:
        outtxt = cmdWoutput(comm)
    sp = outtxt.split()
    for s in sp:
        if isinteger(s.split(".")[0]):
            print(" -- Sent job " + str(s))
            return str(s)


def qsub3(commstrings, name="clujob", queue='normal', hold=[],
          etime="24:00:00", memory="20G", clfile=True, logdir="", cpus="1"):
    """ Send a list of commands into the qsub
        - commstrings: list of commands, each string is considered a new
          command
            ['source freesurfer5;','recon-all','cd toto']
        - name : name of the process - $process_$ID
        - queue: queue in the sge
        - logdir: where to write outputs and scripts
        - hold= list of job ids to wait for them to finish
        - etime= expected execution time in seconds
        - nosge= execute on command line instead of sge (for debug mainly)
        return: the job id, useful for "hold" dependencies
        """
    # create the log dir if necessary
    if logdir == "":
        logdir = os.path.join(os.environ["PROJECT_DIR"], "clulogs")
        createDir(logdir)
    # write the commands in the output script
    source_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/code/sepcode'
    source_file += '/source_sepproject3.bash'
    elog = os.path.join(logdir, name + ".err")
    olog = os.path.join(logdir, name + ".out")
    shfile = os.path.join(logdir, name + ".sh")
    p = open(shfile, "w")
    p.write("#!/bin/bash\n")
    p.write("#SBATCH --partition=%s\n" % queue)
    p.write("#SBATCH --time=%s\n" % etime)
    p.write("#SBATCH --mem=%s\n" % memory)
    p.write("#SBATCH --cpus-per-task=%s\n" % cpus)
    p.write("#SBATCH --ntasks=1\n")
    p.write("#SBATCH --nodes=1\n")
    p.write("#SBATCH --output=%s\n" % olog)
    p.write("#SBATCH --error=%s\n" % elog)
    p.write("#SBATCH --job-name=%s\n" % name)
    p.write("source %s \n" % source_file)
    if isinstance(commstrings, str):
        p.write("python " + commstrings + "\n")
    elif isinstance(commstrings, list):
        for c in commstrings:
            p.write(c + "\n")
    else:
        print(" ** ERROR " + str(commstrings) + " is not a valid command")
        return -1
    p.close()
    os.chmod(shfile, stat.S_IRWXU or stat.S_IRGR or stat.S_IXGR)
    # commandline
    hcmd = []
    if len(hold) > 0:
        hh = "--dependency afterok:"
        for h in hold:
            hh = hh + str(h) + ":"
        hcmd = [hh[:-1]]
    comm = ["sbatch"] + hcmd + [str(shfile)]
    if clfile:
        outtxt = cmdWoutput(comm, os.path.join(logdir, name + ".txt"))
    else:
        outtxt = cmdWoutput(comm)
    sp = outtxt.split()
    for s in sp:
        if isinteger(s.split(".")[0]):
            print(" -- Sent job " + str(s))
            return str(s)


def qsub3_1(commstrings, name="clujob", queue='normal', hold=[],
            etime="24:00:00", memory="20G", clfile=True, logdir="", cpus="1"):
    """ Send a list of commands into the qsub
        - commstrings: list of commands, each string is considered a new
          command
            ['source freesurfer5;','recon-all','cd toto']
        - name : name of the process - $process_$ID
        - queue: queue in the sge
        - logdir: where to write outputs and scripts
        - hold= list of job ids to wait for them to finish
        - etime= expected execution time in seconds
        - nosge= execute on command line instead of sge (for debug mainly)
        return: the job id, useful for "hold" dependencies
        """
    # create the log dir if necessary
    if logdir == "":
        logdir = os.path.join(os.environ["PROJECT_DIR"], "clulogs")
        createDir(logdir)
    # write the commands in the output script
    source_file = '/network/lustre/iss01/lubetzki-stankoff/clinic/code/sepcode'
    source_file += '/source_sepproject3.bash'
    elog = os.path.join(logdir, "err_" + name + ".err")
    olog = os.path.join(logdir, "out_" + name + ".out")
    shfile = os.path.join(logdir, "job_" + name + ".sh")
    p = open(shfile, "w")
    p.write("#!/bin/bash\n")
    p.write("#SBATCH --partition=%s\n" % queue)
    p.write("#SBATCH --time=%s\n" % etime)
    p.write("#SBATCH --mem=%s\n" % memory)
    p.write("#SBATCH --cpus-per-task=%s\n" % cpus)
    p.write("#SBATCH --ntasks=1\n")
    p.write("#SBATCH --nodes=1\n")
    p.write("#SBATCH --output=%s\n" % olog)
    p.write("#SBATCH --error=%s\n" % elog)
    p.write("#SBATCH --job-name=%s\n" % name)
    p.write("source %s \n" % source_file)
    if isinstance(commstrings, str):
        p.write("python " + commstrings + "\n")
    elif isinstance(commstrings, list):
        for c in commstrings:
            p.write(c + "\n")
    else:
        print(" ** ERROR " + str(commstrings) + " is not a valid command")
        return -1
    p.close()
    os.chmod(shfile, stat.S_IRWXU or stat.S_IRGR or stat.S_IXGR)
    # commandline
    hcmd = []
    if len(hold) > 0:
        hh = "--dependency afterok:"
        for h in hold:
            hh = hh + str(h) + ":"
        hcmd = [hh[:-1]]
    comm = ["sbatch"] + hcmd + [str(shfile)]
    if clfile:
        outtxt = cmdWoutput(comm, os.path.join(logdir, name + ".txt"))
    else:
        outtxt = cmdWoutput(comm)
    sp = outtxt.split()
    for s in sp:
        if isinteger(s.split(".")[0]):
            print(" -- Sent job " + str(s))
            return str(s)
