#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 16:55:28 2020

@author: arya.yazdan-panah
"""

import os
import numpy as np
import nibabel as nib
from termcolor import colored
from deepbrain import Extractor


def brain_mask(img, out_file=None):
    """
    Brain extraction based on Deepbrain-Extractor library found on
    https://pypi.org/project/deepbrain/

    Parameters
    ----------
    img : path or nifti1Image
        image to mask.
    out_file : path, optional
        path to give if output is wanted as a nifti file. The default is None.

    Returns
    -------
    path or nifti1image.
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Deep Brain Extractor:', 'cyan', attrs=['bold',
                                                            'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| brain_mask from DEEPtools.py', 'cyan', attrs=['bold']))
# ------------------------------ Loading Images -------------------------------
    try:
        if os.path.isfile(img):
            img = nib.load(img)
    except TypeError:
        pass
    affine = img.affine
    img = img.get_fdata()
# -------------------------------- Extraction ---------------------------------
    extractor = Extractor()
    prob = extractor.run(img)
    mask = prob > 0.5
    brain_mask = (1 * mask).astype(np.uint8)
    brain_mask = nib.Nifti1Image(brain_mask, affine)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Mask = 1 for prob > 0.5', 'cyan', attrs=['bold']))
    if out_file is None:
        return brain_mask
    elif os.path.isfile(out_file):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Mask already made, in order to avoid overwriting manu'
                      'ally corrected masks, rename or delete mannually',
                      'cyan', attrs=['bold']))
        return out_file
    else:
        nib.save(brain_mask, out_file)
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Creating mask:', 'cyan', attrs=['bold']), out_file)
        return out_file


def brain_only(mask, img, brain_path=None):
    """
    Skull stripping of a .nii file or a nifti1image object. Can be used to
    apply any mask on any image.

    Parameters
    ----------
    mask : path or nifti1image
        path to mask to apply.
    img : path or nifti1image
        path to image to be masked.
    brain_path : path, optional
        path to give if output is wanted as a nifti file. The default is None.

    Returns
    -------
    path or nifti1image
        masked image.
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Brain Extraction with mask: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| brain_only() from DEEPtools.py', 'cyan', attrs=['bold']))
    try:
        if os.path.isfile(img):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Image loaded:', 'cyan', attrs=['bold']), img)
            img = nib.load(img)
    except TypeError:
        pass
    try:
        if os.path.isfile(mask):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Mask loaded:', 'cyan', attrs=['bold']), mask)
            mask = nib.load(mask)
    except TypeError:
        pass
    affine = img.affine
    img = img.get_fdata()
    mask = mask.get_fdata()
    brain = img[:]
    mask = mask > 0.5
    brain[~mask] = 0
    brain = nib.Nifti1Image(brain, affine)
    if brain_path is None:
        return brain
    if os.path.isfile(brain_path):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored('WARNING: Previous brain extraction will be overwritten',
                      'yellow', attrs=['bold']))
    nib.save(brain, brain_path)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Brain saved as: ', 'cyan', attrs=['bold']), brain_path)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return brain_path
