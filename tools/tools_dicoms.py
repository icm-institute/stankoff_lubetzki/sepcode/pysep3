#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 16:57:52 2020

@author: arya.yazdan-panah
"""

import os
import pydicom
from termcolor import colored
import nibabel.nicom.csareader as csareader
from nipype.interfaces.dcm2nii import Dcm2niix, Dcm2nii
from common3 import command, get_regex, decompress
from common3 import createDir, compress
from pprint import pprint


def dicom2nifti(dcms, out_dir, name, option='nipype', bids=True,
                verbose=False):
    """
    Function implementing Chris Rorden's dcm2niix

    Parameters
    ----------
    dcms : path or list[path]
        path to dicoms or dicom's folder.
    out_dir : path
        output directory.
    name : string
        ID of the subject.
    option : string, optional
        run it with 'nipype' or 'commandline'. The default is 'nipype'.

    Returns
    -------
    list[path]
        list of nifti path(s) created
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('|', 'cyan', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('|', 'cyan', attrs=['bold']),
          colored('| Converting: ' + name, 'magenta',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('|', 'cyan', attrs=['bold']),
          colored('| dicom2nifti() from IMPORTtools.py', 'magenta',
                  attrs=['bold']))
    createDir(out_dir)
    if option == 'dcm2niix':
        converter = Dcm2niix()
        if isinstance(dcms, str):
            converter.inputs.source_dir = os.path.abspath(dcms)
        elif isinstance(dcms, list):
            converter.inputs.source_names = dcms
        converter.inputs.compress = 'y'
        converter.inputs.out_filename = name
        converter.inputs.output_dir = out_dir
        converter.inputs.bids_format = bids
        # converter.inputs.single_file = True
        if not verbose:
            converter.terminal_output = 'none'
        else:
            converter.terminal_output = 'stream'
        # print(converter.cmdline)
        converter.run()
    if option == 'dcm2nii':
        converter = Dcm2nii()
        if isinstance(dcms, str):
            converter.inputs.source_dir = os.path.abspath(dcms)
        elif isinstance(dcms, list):
            converter.inputs.source_names = dcms
        converter.inputs.date_in_filename = False
        converter.inputs.id_in_filename = True
        converter.inputs.events_in_filename = True
        converter.inputs.gzip_output = True
        converter.inputs.output_dir = out_dir
        converter.inputs.protocol_in_filename = True
        # converter.inputs.single_file = True
        if not verbose:
            converter.terminal_output = 'none'
        else:
            converter.terminal_output = 'stream'
        # print(converter.cmdline)
        converter.run()
# --------------------------- Does not Work below   ---------------------------
    if option == 'command':
        comm = ['dcm2niix', '-x', 'i', '-f', name, '-z', 'y', '-o', out_dir]
        + [dcms]
        command(comm)
    nifti = get_regex(out_dir, '.nii.', 0)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('|', 'cyan', attrs=['bold']))
    return nifti


def createAcqParamTxt(dcm, out_file):
    rowcol_to_niftidim = {'COL': 'i', 'ROW': 'j'}
    pedp_to_sign = {0: '-', 1: ''}
    if not os.path.isfile(dcm):
        return False
    if os.path.isfile(out_file):
        print("warning, out_file already exists, skip to prevent overwrite")
        return False
    dcm = pydicom.read_file(dcm)
    inplane_pe_dir = dcm[int('00181312', 16)].value
    csa_str = dcm[int('00291010', 16)].value
    csa_tr = csareader.read(csa_str)
    pedp = csa_tr['tags']['PhaseEncodingDirectionPositive']['items'][0]
    ij = rowcol_to_niftidim[inplane_pe_dir]
    sign = pedp_to_sign[pedp]
    return '{}{}'.format(ij, sign)
