#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 14:59:53 2021

@author: arya.yazdan-panah
"""

import numpy as np
import matplotlib.pyplot as plt
import os
import json
import nipype.interfaces.mrtrix3 as mrt
from common3 import cmdWoutput, changeName
from termcolor import colored
from pprint import pprint


def plot_bvecs(bvecs, bvals):
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
    if not isinstance(bvecs, list):
        bvecs = [bvecs]
    if not isinstance(bvals, list):
        bvals = [bvals]
    all_bvals = np.zeros((len(bvals), 1))
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    legends = []
    for ind, bvec in enumerate(bvecs):
        BVEC = open(bvec, 'r')
        BVAL = open(bvals[ind], 'r')
        bvl = np.float_(BVAL.read().split())
        bvc = np.zeros((len(bvl), 3))
        for i, l in enumerate(BVEC.readlines()):
            bvc[:, i] = np.float_(l.split())
        for i, j in enumerate(bvl):
            bvc[i, :] = bvc[i, :] * j
        BVEC.close()
        BVAL.close()
        ax.scatter3D(bvc[:, 0], bvc[:, 1], bvc[:, 2], color=colors[ind % 8])
        all_bvals[ind] = np.max(bvl)
        legends.append(os.path.basename(bvec))
    ax.set_xlim(-np.max(all_bvals), np.max(all_bvals))
    ax.set_ylim(-np.max(all_bvals), np.max(all_bvals))
    ax.set_zlim(-np.max(all_bvals), np.max(all_bvals))
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.legend(legends)
    ax.set_title('B-vectors')
    plt.show()


def read_json(json_file):
    if not os.path.isfile(json_file):
        return False
    elif os.path.splitext(json_file)[-1] != '.json':
        print("NOT A .json")
        return False
    f = open(json_file, 'r')
    data = json.load(f)
    f.close()
    return data


def read_bval(bval_file):
    if not os.path.isfile(bval_file):
        return False
    elif os.path.splitext(bval_file)[-1] != '.bval':
        print("NOT A .bval")
        return False
    f = open(bval_file, 'r')
    data = f.read().split()
    data = [int(d) for d in data]
    f.close()
    return data


def read_bvec(bvec_file):
    if not os.path.isfile(bvec_file):
        return False
    f = open(bvec_file, 'r')
    data = [[], [], []]
    for i, line in enumerate(f.readlines()):

        data[i] = line.split()
        data[i] = [float(d) for d in data[i]]
    f.close()
    return data


def write_bval(bval_list, bval_file):
    f = open(bval_file, 'w')
    for b in bval_list:
        f.write(str(b) + '  ')
    f.close()
    return bval_file


def write_bvec(bvec_list, bvec_file):
    f = open(bvec_file, 'w')
    for line in bvec_list:
        for b in line:
            f.write(str(b) + '  ')
        f.write('\n')
    f.close()
    return bvec_file


def dwidenoise(*args):
    op_string = ['dwidenoise']
    for arg in args:
        if isinstance(arg, (int, float, complex)):
            op_string.append(str(arg))
        elif isinstance(arg, str):
            op_string.append(arg)
    cmdWoutput(op_string)
    return


def nii2mif(dwinii, bvecs, bvals, dwimif=False):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Converting Diffusion.nii to .mif: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| nii2mif() from tools_diffusion.py', 'cyan',
                  attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Converting DWI:', 'cyan', attrs=['bold']), dwinii)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Including FSL type bvals:', 'cyan', attrs=['bold']),
          bvals)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Including FSL type bvecs:', 'cyan', attrs=['bold']),
          bvecs)
    cvt = mrt.MRConvert()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| input file:', 'cyan', attrs=['bold']), dwinii)
    cvt.inputs.in_file = dwinii
    cvt.inputs.grad_fsl = (bvecs, bvals)
    cvt.terminal_output = 'none'
    if not dwimif:
        dwimif = changeName(dwinii, newext='.mif')
    cvt.inputs.out_file = dwimif
    if not os.path.isfile(dwimif):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Running...', 'cyan', attrs=['bold']))
        cvt.run()
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping conversion, .mif already exists:', 'cyan',
                      attrs=['bold']), dwimif)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return dwimif


def dwi2response(dwi, wm, gm, csf, voxels=False, mask=False, nthreads=4):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Estimating response functions in WM, CSF, GM: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| dwi2response() from tools_diffusion.py', 'cyan',
                  attrs=['bold']))
    repondeur = mrt.ResponseSD()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| input file:', 'cyan', attrs=['bold']), dwi)
    repondeur.inputs.in_file = dwi
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| algorithm:', 'cyan', attrs=['bold']), 'dhollander')
    repondeur.inputs.algorithm = 'dhollander'
    repondeur.inputs.nthreads = nthreads
    args = '-scratch ' + os.path.join(os.path.dirname(dwi), 'scratch')
    if voxels:
        args = args + ' -voxels ' + voxels
    repondeur.inputs.args = args
    if mask:
        repondeur.inputs.in_mask = mask
    repondeur.inputs.wm_file = wm
    repondeur.inputs.gm_file = gm
    repondeur.inputs.csf_file = csf
    if np.all([os.path.isfile(rsp) for rsp in [wm, gm, csf]]):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping response estimation, they already exist',
                      'cyan', attrs=['bold']))
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Running...', 'cyan', attrs=['bold']))
        repondeur.terminal_output = 'none'
        repondeur.run()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| WM:', 'cyan', attrs=['bold']), wm)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| GM:', 'cyan', attrs=['bold']), gm)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| CSF:', 'cyan', attrs=['bold']), csf)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return wm, gm, csf


def dwi2fod(dwi, wm_rf, gm_rf, csf_rf, wm_fod, gm_fod, csf_fod, mask=False,
            algorithm='msmt_csd', nthreads=4):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Estimating FOD in WM, CSF, GM: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| dwi2fod() from tools_diffusion.py', 'cyan',
                  attrs=['bold']))
    fod = mrt.ConstrainedSphericalDeconvolution()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| input file:', 'cyan', attrs=['bold']), dwi)
    fod.inputs.in_file = dwi
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| algorithm:', 'cyan', attrs=['bold']), algorithm)
    fod.inputs.algorithm = algorithm
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| wm RF:', 'cyan', attrs=['bold']), wm_rf)
    fod.inputs.wm_txt = wm_rf
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| gm RF:', 'cyan', attrs=['bold']), gm_rf)
    fod.inputs.gm_txt = gm_rf
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| csf RF:', 'cyan', attrs=['bold']), csf_rf)
    fod.inputs.csf_txt = csf_rf
    fod.terminal_output = 'none'
    if mask:
        fod.inputs.mask_file = mask
    fod.inputs.nthreads = nthreads
    if np.all([os.path.isfile(fod) for fod in [wm_fod, gm_fod, csf_fod]]):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping FOD estimation, they already exist',
                      'cyan', attrs=['bold']))
    else:
        fod.inputs.wm_odf = wm_fod
        fod.inputs.gm_odf = gm_fod
        fod.inputs.csf_odf = csf_fod
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Running...', 'cyan', attrs=['bold']))
        fod.run()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| WM FOD:', 'cyan', attrs=['bold']), wm_fod)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| GM FOD:', 'cyan', attrs=['bold']), gm_fod)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| CSF FOD:', 'cyan', attrs=['bold']), csf_fod)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return wm_fod, gm_fod, csf_fod


def gen5tt(input5tt, ftt, algorithm='hsvs', nthreads=4):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Generate 5 Tissue Type: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| gen5tt() from tools_diffusion.py', 'cyan',
                  attrs=['bold']))
    c = ['5ttgen']
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Algorithm:', 'cyan', attrs=['bold']), algorithm)
    c.append(algorithm)
    if algorithm.lower() == 'hsvs':
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| input FSdir:', 'cyan', attrs=['bold']), input5tt)
        c.append(input5tt)
        c.append(ftt)
        args = ['-white_stem', '-nocrop', '-hippocampi', 'subfields',
                '-thalami', 'nuclei']
        c.extend(args)
        c.extend(['-scratch', input5tt])
    elif algorithm.lower() == 'fsl':
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| input T1 image:', 'cyan', attrs=['bold']), input5tt)
        c.append(input5tt)
        c.append(ftt)
        c.extend(['-scratch', os.path.dirname(input5tt)])
    if not os.path.isfile(ftt):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Running...', 'cyan', attrs=['bold']))
        c.append('-quiet')
        c.extend(['-nthreads', str(nthreads)])
        cmdWoutput(c)
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping 5TT generation, it already exists',  'cyan',
                      attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| 5TT:',  'cyan', attrs=['bold']), ftt)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return ftt


# =============================================================================
# def Generate5tt(*args):
#     op_string = ['5ttgen', '-quiet']
#     for arg in args:
#         if isinstance(arg, (int, float, complex)):
#             op_string.append(str(arg))
#         elif isinstance(arg, str):
#             op_string.append(arg)
#     cmdWoutput(op_string)
#     return
# =============================================================================


def gmwm_interface(ftt, out, nthreads=4):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Generate GM/WM interface: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| gmwm_interface() from tools_diffusion.py', 'cyan',
                  attrs=['bold']))
    c = ['5tt2gmwmi', '-quiet']
    c.extend(['-nthreads', str(nthreads)])
    os.environ['MRTRIX_QUIET'] = 'PLEASE'
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| input 5TT:', 'cyan', attrs=['bold']), ftt)
    c.append(ftt)
    c.append(out)
    if not os.path.isfile(out):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Running...', 'cyan', attrs=['bold']))
        cmdWoutput(c)
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping GM/WM interface generation, already exists',
                      'cyan', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| GM/WM interface:',  'cyan', attrs=['bold']), out)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return out


def tractography(wmfod, ftt, seed_gmwmi, tracto, seeds, mask=False,
                 max_length=250, cutoff=0.06, ntracks=10000000, nthreads=4):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Tractography: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| tractography() from tools_diffusion.py', 'cyan',
                  attrs=['bold']))
    tk = mrt.Tractography()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| input WM FOD:', 'cyan', attrs=['bold']), wmfod)
    tk.inputs.in_file = wmfod
    if mask:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| input mask:', 'cyan', attrs=['bold']), mask)
        tk.inputs.roi_mask = mask
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Backtracking activated', 'cyan', attrs=['bold']))
    tk.inputs.backtrack = True
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| ACT File (5TT)', 'cyan', attrs=['bold']), ftt)
    tk.inputs.act_file = ftt
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| GM/WM interface', 'cyan', attrs=['bold']), seed_gmwmi)
    tk.inputs.seed_gmwmi = seed_gmwmi
    tk.inputs.nthreads = nthreads
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Maximum track length', 'cyan', attrs=['bold']),
          max_length)
    tk.inputs.max_length = max_length
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| FOD cutoff for stopping tracks', 'cyan', attrs=['bold']),
          cutoff)
    tk.inputs.cutoff = cutoff
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Desired number of tracks', 'cyan', attrs=['bold']),
          ntracks)
    tk.inputs.args = '-select ' + str(ntracks)
    # tk.inputs.n_tracks = ntracks  # version of mrtrix is too low according to nipype
    tk.inputs.out_file = tracto
    tk.inputs.out_seeds = seeds
    tk.terminal_output = 'stream'
    if os.path.isfile(tracto):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping tractography, already exists', 'cyan',
                      attrs=['bold']))
    else:
        if os.path.isfile(seeds):
            os.remove(seeds)
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Running...', 'cyan', attrs=['bold']))
        tk.run()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Processed tractography:',  'cyan', attrs=['bold']),
          tracto)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return tracto


def down_sample_tck(tck, out, ntracks='200k'):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Downsampling Tractography for visualisation: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| down_sample_tck() from tools_diffusion.py', 'cyan',
                  attrs=['bold']))
    c = ['tckedit']
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| input Tractography:', 'cyan', attrs=['bold']), tck)
    c.append(tck)
    c.extend(['-number', ntracks])
    c.append(out)
    if not os.path.isfile(out):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Running...', 'cyan', attrs=['bold']))
        cmdWoutput(c)
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping downsampling, already exists',
                      'cyan', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Downsampled tracks:',  'cyan', attrs=['bold']), out)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return out

# def tckgen(*args):
#     op_string = ['tckgen']
#     for arg in args:
#         if isinstance(arg, (int, float, complex)):
#             op_string.append(str(arg))
#         elif isinstance(arg, str):
#             op_string.append(arg)
#     pprint(op_string)
#     cmdWoutput(op_string)
#     return