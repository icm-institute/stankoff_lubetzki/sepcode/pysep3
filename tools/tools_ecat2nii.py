#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 23 13:28:46 2020

@author: arya.yazdan-panah
"""

import os
import sys
from termcolor import cprint, colored
from common3 import cmdWoutput, compress, changeName, command


def ecat2nii_tpc(ecat_pet, output_dir, subject_visit_tracer, sif=True):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Converting: ' + subject_visit_tracer, 'magenta',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| ecat2nii_tpc() from tools_ecat2nii.py', 'magenta',
                  attrs=['bold']))
    cmd = ["ecat2nii", "-O=" + output_dir]
    if sif:
        cmd.append("-sif")
    cmd.append(ecat_pet)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Command run:', 'magenta', attrs=['bold']),
          " ".join(cmd))
    cmdWoutput(cmd)
    predicted_nii = changeName(ecat_pet, outdir=output_dir, newext='.nii')
    predicted_sif = changeName(ecat_pet, outdir=output_dir, newext='.sif')
    new_nii = os.path.join(output_dir, subject_visit_tracer + '.nii')
    new_sif = os.path.join(output_dir, subject_visit_tracer + '.sif')
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Renaming output .nii:', 'magenta', attrs=['bold']),
          predicted_nii)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('|                   to:', 'magenta', attrs=['bold']),
          new_nii + ".gz")
    os.rename(predicted_nii, new_nii)
    cmdWoutput(["gzip", new_nii])
    new_nii = new_nii + ".gz"
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Renaming output .sif:', 'magenta', attrs=['bold']),
          predicted_sif)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('|                   to:', 'magenta', attrs=['bold']),
          new_sif)
    os.rename(predicted_sif, new_sif)
    return new_nii, new_sif


def createSIF(minffile,siffile):
    print(" -- Creating SIF file")   
    att = "".join(open(minffile).readlines())
    exec(att,globals())
    durtime = None
    strtime = None
    if 'duration_time' in attributes and 'start_time' in attributes:
            durtime = attributes['duration_time']
            strtime = attributes['start_time']
    if durtime and strtime and not os.path.exists(siffile):
        import datetime        
        hoy = datetime.datetime.now()
        hoy.strftime("%d/%m/%y %H:%M:%S")
        p = open(siffile,"w")
        p.write(hoy.strftime("%d/%m/%y %H:%M:%S") + " " + str(len(strtime)) +
                " 4 2 DGL F-18\n")
        for i in range(len(strtime)):
            p.write(str(strtime[i] / 1000.0) + "\t" + str((float(strtime[i]) + 
                                                           float(durtime[i])) / 
                                                          1000.0) + "\t0\t0\n")
        p.close()
    else: 
        print(" -- MISSING INFO OR FILE EXISTS")


def convertPET(vfile,output):
    if os.path.exists(output):
	    return
    # Convert (in the same folder) the v file to nifti (if necessary)
    niifile = ""
    try:
        [head,tail] = os.path.split(output)
        siffile = changeName(output,newext=".sif")
        niifile = changeName(vfile,"_tmp",outdir=head,newext=".nii.gz")
#        niifile=vfile[:-2]+"_tmp.nii.gz"
        minf = niifile+".minf"
        if vfile.endswith(".v"):
            print(" -- Converting intput in Nifti")
            comm = ["AimsFileConvert","-i",vfile,"-o",niifile]
            if command(comm,vfile,niifile):
                print(" ** ERROR in AimsFileConvert: Please verify the file is"
                      " supported by this tool")
                sys.exit(1)
            print(minf)
            if os.path.exists(minf) and not os.path.exists(siffile):
                createSIF(minf,siffile)
            elif not os.path.exists(minf):
                print(" ** minf not found!!")
        else:
            print(" -- Input file already in Nifti")
            niifile = vfile
        # Reorient to RPI and move to the good foler
        print(" -- Converting image to RPI ")
        comm=["toRPI.py",niifile,output]
        if command(comm,niifile,output)==0:
            print(" -- Finish!")
            print(" -- Image is found in")
            print(output)
        else:
            print(" ** ERROR in toRPI.py")
            sys.exit(1)
    finally:
        if not niifile==vfile:
            print(" -- Erasing intermediary files")
            comm=['rm','-f',niifile,minf]
            command(comm)
