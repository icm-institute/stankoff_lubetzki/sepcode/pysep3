#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 18 21:08:42 2020

@author: arya.yazdan-panah
"""

import os
from nipype.interfaces.freesurfer import ReconAll, MRIConvert
from common3 import seq_info, changeName, cmdWoutput, command
from pprint import pprint
from tools_c3 import c3d


def recon_all(subject_dir, subject_id, steps='-all', T1=False, openmp=6,
              log=False):
    op_string = ['recon-all', steps, '-openmp', openmp, '-subjid', subject_id,
                 '-sd', subject_dir]
    if T1:
        op_string.extend(['-i', T1])
    if log:
        op_string.extend(['-log', log])
    command(op_string)
    return


def freesurfer(anat_nii, subjects_dir, Id=False, steps='all', args=False,
               T2=False, hemi=False, openmp=False, ignore=True,
               brainstem=True):
    """
    Implementation of a Python function to run Freesurfer's recon-all

    Parameters
    ----------
    anat_nii : path
        path to anatomical file.
    subjects_dir : path
        path to freesurfer directory.
    Id : string, optional
        Subject ID. The default is False.
    steps : string, optional
        steps to perform. The default is 'all'.
    args : string, optional
        long or base. The default is False.
    T2 : path, optional
        path to T2 file. The default is False.
    hemi : TYPE, optional
        DESCRIPTION. The default is False.
    openmp : TYPE, optional
        DESCRIPTION. The default is False.
    ignore : bool, optional
        DESCRIPTION. The default is True.

    Returns
    -------
    dict
    """

    print('| |')
    print('| | | FreeSurfer Analysis:')
    print('| | | freesurfer() from FREESURFERtools.py')
    reconall = ReconAll()
    if not Id:
        Id, X, X = seq_info(anat_nii)
    reconall.inputs.subject_id = Id
    print('| | | Subject Id:', Id)
    reconall.inputs.directive = steps
    print('| | | Steps to preform:', steps)
    reconall.inputs.subjects_dir = subjects_dir
    print('| | | Subjects_dir:', subjects_dir)
    reconall.inputs.T1_files = anat_nii
    print('| | | Structural File:', anat_nii)
    reconall.terminal_output = 'file_split'
    if args in ['long', 'base']:
        reconall.inputs.args = '-' + args
    if T2:
        reconall.inputs.use_T2 = True
        reconall.inputs.T2_files = T2
    if hemi:
        reconall.inputs.hemi = hemi
    if openmp:
        reconall.inputs.openmp = openmp
    if brainstem:
        reconall.inputs.brainstem = brainstem
    outs = reconall._list_outputs()
    reconall.ignore_exception = ignore
# =============================================================================
#     print(reconall.cmdline)
# =============================================================================
    reconall.run()
    return outs


def freesurfer7(anat_nii, subjects_dir, Id=False, steps='all', args=False,
                T2=False, hemi=False, openmp=False, ignore=True,
                brainstem=True, thalamicnuclei=True, hippocampus=True,
                logfile=False, highres=False):
    """
    Implementation of a Python function to run Freesurfer's recon-all

    Parameters
    ----------
    anat_nii : path
        path to anatomical file.
    subjects_dir : path
        path to freesurfer directory.
    Id : string, optional
        Subject ID. The default is False.
    steps : string, optional
        steps to perform. The default is 'all'.
    args : string, optional
        long or base. The default is False.
    T2 : path, optional
        path to T2 file. The default is False.
    hemi : TYPE, optional
        DESCRIPTION. The default is False.
    openmp : TYPE, optional
        DESCRIPTION. The default is False.
    ignore : bool, optional
        DESCRIPTION. The default is True.

    Returns
    -------
    dict
    """
    print('| |')
    print('| | | FreeSurfer Analysis:')
    print('| | | freesurfer() from FREESURFERtools.py')
    reconall = ReconAll()
    if not Id:
        Id = os.path.basename(subjects_dir)
    reconall.inputs.subject_id = Id
    print('| | | Subject Id:', Id)
    if steps == "pial":
        command(['recon-all', '-subjid', Id, '-sd', subjects_dir,
                 '-autorecon-pial'])
        return
    elif steps == "add_brain":
        command(['recon-all', '-subjid', Id, '-sd', subjects_dir,
                 '-skullstrip', '-wsthresh', 36, '-clean-bm'])
    reconall.inputs.directive = steps
    print('| | | Steps to preform:', steps)
    reconall.inputs.subjects_dir = subjects_dir
    print('| | | Subjects_dir:', subjects_dir)
    reconall.inputs.T1_files = anat_nii
    print('| | | Structural File:', anat_nii)
    reconall.terminal_output = 'file_split'
    if args in ['long', 'base']:
        reconall.inputs.args = '-' + args
    if T2:
        reconall.inputs.use_T2 = True
        reconall.inputs.T2_files = T2
    if hemi:
        reconall.inputs.hemi = hemi
    if openmp:
        reconall.inputs.openmp = openmp
    outs = reconall._list_outputs()
    reconall.ignore_exception = ignore
    cmd = reconall.cmdline.split()
    if logfile:
        cmd.extend(['-log', logfile])
    if highres:
        cmd.extend(['-hires'])
    cmd.append('-debug')
    pprint(cmd)
    command(cmd, logfile=None, verbose=False)
    if brainstem and steps != 'all':
        command(['segmentBS.sh', Id, subjects_dir])
    if thalamicnuclei and steps != 'all':
        command(['segmentThalamicNuclei.sh', Id, subjects_dir])
    if hippocampus and steps != 'all':
        command(['segmentHA_T1.sh', Id, subjects_dir])
    return outs


def MRI_convert(input_file, output_file, out_type='niigz'):
    Allowed_extensions = dict(cor="cor", mgh="mgh", mgz="mgz", minc="mnc",
                              afni="brik", brik="brik", bshort="bshort",
                              spm="img", analyze="img", analyze4d="img",
                              bfloat="bfloat", nifti1="img", nii="nii",
                              niigz="nii.gz")
    if out_type not in Allowed_extensions.keys():
        print("Extension not available")
        pprint(Allowed_extensions)
        return False
    cvt = MRIConvert()
    cvt.inputs.in_file = input_file
    cvt.inputs.out_file = output_file
    cvt.inputs.out_type = out_type
    cvt.terminal_output = 'none'
    cvt.run()
    return output_file


def fsImage2niftispace(mgz_images, output_dir, refima, prefix, removetmp=True):
    """ Function to convert and reslice original mgz images from freesurfer to
    the original space

    As volume size changes, images are resliced into the refima space if given
    Normally no interpolation is required"""

    if isinstance(mgz_images, str):
        mgz_images = [mgz_images]
    for mgz in mgz_images:
        if not os.path.isfile(mgz):
            continue
        nifti = changeName(mgz, '_fsspace', output_dir, '.nii.gz', prefix)
        MRI_convert(mgz, nifti)
        final = changeName(mgz, '', output_dir, '.nii.gz', prefix)
        c3d(refima, nifti, "-interpolation", "NearestNeighbor",
            '-reslice-identity', '-o', final)
        if removetmp:
            os.remove(nifti)
    return


def mri_annotation2label(*args):
    op_string = ['mri_annotation2label']
    for arg in args:
        if isinstance(arg, (int, float, complex)):
            op_string.append(str(arg))
        elif isinstance(arg, str):
            op_string.append(arg)
    cmdWoutput(op_string)
    return


def mri_aparc2aseg(*args):
    op_string = ['mri_aparc2aseg']
    for arg in args:
        if isinstance(arg, (int, float, complex)):
            op_string.append(str(arg))
        elif isinstance(arg, str):
            op_string.append(arg)
    cmdWoutput(op_string)
    return
