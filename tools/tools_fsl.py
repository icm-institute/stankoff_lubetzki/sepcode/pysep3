#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 27 16:52:59 2020

@author: arya.yazdan-panah
"""

import os
import time
import shutil
import sys
from termcolor import colored
from common3 import (get_subdir_regex_files, moveFiles, createDir, changeName,
                     cmdWoutput, get_all_values, get_regex)
from nipype.interfaces.fsl import (FAST, ImageMaths, FLIRT, ConvertXFM,
                                   ApplyXFM, Merge, TOPUP, MeanImage, BET,
                                   Eddy, DTIFit)
import nibabel as nib
from pprint import pprint


"""
################################ DTIFIT #######################################
Use FSL dtifit command for fitting a diffusion tensor model at each voxel.
"""


def dtifit(diffusion, brain_mask, bvec, bval, out_base, verbose=False,
           overwrite=False):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Diffusion Tensor Fitting: ', 'cyan', attrs=['bold',
                                                                 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| dtifit() from FSLtools.py', 'cyan', attrs=['bold']))
    dtfit = DTIFit()
    dtfit.inputs.dwi = diffusion
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input diffusion', 'cyan', attrs=['bold']), diffusion)
    dtfit.inputs.mask = brain_mask
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input brain_mask', 'cyan', attrs=['bold']), brain_mask)
    dtfit.inputs.bvecs = bvec
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input bvec', 'cyan', attrs=['bold']), bvec)
    dtfit.inputs.bvals = bval
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input bval', 'cyan', attrs=['bold']), bval)
    dtfit.inputs.output_type = "NIFTI_GZ"
    if not out_base:
        out_dir = os.path.join(os.path.dirname(diffusion), 'dtifit/')
    else:
        out_dir = os.path.dirname(out_base)
    createDir(os.path.dirname(out_base))
    os.chdir(out_dir)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| DTI directory:', 'cyan', attrs=['bold']), out_dir)
    dtfit.inputs.base_name = out_base
    dtfit.ignore_exception = True
    if verbose:
        dtfit.terminal_output = 'stream'
    else:
        dtfit.terminal_output = 'none'
    skip = get_regex(out_dir, changeName(diffusion, '_dti', '', ''))
    if skip and not overwrite:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping DTIFit, files already exist:', 'cyan',
                      attrs=['bold']), out_dir)
        for a, b in enumerate(skip):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| ' + str(a + 1) + ':', 'cyan', attrs=['bold']), b)
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return out_dir
    elif skip and overwrite:
        for f in skip:
            try:
                os.remove(f)
            except OSError:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('|', 'cyan', attrs=['bold']),
                      colored('ERROR: Cannot delete file:', 'red',
                              attrs=['bold']), f)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Starting DTIfit', 'cyan', attrs=['bold']))
    then = time.time()
    dtfit.run()
    duration = time.time() - then
    mins, secs = divmod(duration, 60)
    hours, mins = divmod(mins, 60)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| DTIFIT completed in:', 'cyan', attrs=['bold']),
          '%d:%d:%d' % (hours, mins, secs))
    # outs = list(get_all_values(dtfit._list_outputs()))
    # for f in outs:
    #     if (isinstance(f, str) and os.path.isfile(f)
    #             and os.path.dirname(f) != out_dir):
    #         shutil.move(f, out_dir)
    # print(colored('|', 'blue', attrs=['bold']),
    #       colored('|', 'green', attrs=['bold']),
    #       colored('| Moved all files to DTIfit directory', 'cyan',
    #               attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return out_dir


"""
################################# EDDY ########################################
Interface for FSL eddy, a tool for estimating and correcting eddy currents
induced distortions.
"""


def fslEddy(diffusion, brain_mask, index, acqparam, bvec, bval, fieldcoef,
            movpar, out_base, verbose=False, overwrite=False, nt=6):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| FSL EDDY: ', 'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| fslEddy() from FSLtools.py', 'cyan', attrs=['bold']))
    eddy = Eddy()
    eddy.inputs.in_file = diffusion
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input diffusion', 'cyan', attrs=['bold']), diffusion)
    eddy.inputs.in_mask = brain_mask
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input brain_mask', 'cyan', attrs=['bold']), brain_mask)
    eddy.inputs.in_index = index
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input index file', 'cyan', attrs=['bold']), index)
    eddy.inputs.in_acqp = acqparam
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input acqparam', 'cyan', attrs=['bold']), acqparam)
    eddy.inputs.in_bvec = bvec
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input bvec', 'cyan', attrs=['bold']), bvec)
    eddy.inputs.in_bval = bval
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input bval', 'cyan', attrs=['bold']), bval)
    eddy.inputs.in_topup_fieldcoef = fieldcoef
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input fieldcoef', 'cyan', attrs=['bold']), fieldcoef)
    eddy.inputs.in_topup_movpar = movpar
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input movpar', 'cyan', attrs=['bold']), movpar)
    eddy.inputs.num_threads = nt
    eddy.inputs.output_type = "NIFTI_GZ"
    if not out_base:
        out_dir = os.path.join(os.path.dirname(diffusion), 'eddy/')
    else:
        out_dir = os.path.dirname(out_base)
    createDir(os.path.dirname(out_base))
    os.chdir(out_dir)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| eddy directory:', 'cyan', attrs=['bold']), out_dir)
    eddy.inputs.out_base = out_base
    eddy.ignore_exception = True
    eddy.inputs.is_shelled = True
    if verbose:
        eddy.terminal_output = 'stream'
    else:
        eddy.terminal_output = 'none'
    skip = get_regex(out_dir, changeName(diffusion, '_eddy', '', ''))
    if skip and not overwrite:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping Eddy, files already exist:', 'cyan',
                      attrs=['bold']), out_dir)
        for a, b in enumerate(skip):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| ' + str(a + 1) + ':', 'cyan', attrs=['bold']), b)
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return out_dir
    elif len(skip) == 13 and overwrite:
        for f in skip:
            try:
                os.remove(f)
            except OSError:
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('|', 'cyan', attrs=['bold']),
                      colored('ERROR: Cannot delete file:', 'red',
                              attrs=['bold']), f)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Starting eddy', 'cyan', attrs=['bold']))
    then = time.time()
    eddy.run()
    duration = time.time() - then
    mins, secs = divmod(duration, 60)
    hours, mins = divmod(mins, 60)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Eddy completed in:', 'cyan', attrs=['bold']),
          '%d:%d:%d' % (hours, mins, secs))
    # outs = list(get_all_values(eddy._list_outputs()))
    # for f in outs:
    #     if (isinstance(f, str) and os.path.isfile(f)
    #             and os.path.dirname(f) != out_dir):
    #         shutil.move(f, out_dir)
    # print(colored('|', 'blue', attrs=['bold']),
    #       colored('|', 'green', attrs=['bold']),
    #       colored('| Moved all files to Eddy directory', 'cyan',
    #               attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return out_dir


"""
################################# TOPUP #######################################
Interface for FSL topup, a tool for estimating and correcting susceptibility
induced distortions.
"""


def Topup(file_4D, acqparam, out_dir=False, verbose=False, overwrite=False):
    """
    FSL's TOPUP

    Parameters
    ----------
    file_4D : path
        name of 4D file with images.
    acqparam : path
        name of text file with PE directions/times.
    out_basename : path-prefix
        base-name of output files (spline coefficients (Hz) and movement
                                   parameters).

    Returns
    -------
    None.

    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| FSL TOPUP: ', 'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Topup() from FSLtools.py', 'cyan', attrs=['bold']))
    topup = TOPUP()
    topup.inputs.in_file = file_4D
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input file', 'cyan', attrs=['bold']), file_4D)
    Z_dim = nib.load(file_4D).shape[2]
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Checking if number of slice is a multiple of 2', 'cyan',
                  attrs=['bold']), Z_dim)
    config_dir = os.path.join(os.path.dirname(os.path.dirname(sys.argv[0])),
                              'extern', 'FSL', 'topup_configs')
    if Z_dim % 2 == 0:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Z_dimension is even, using config b02b0.cnf', 'cyan',
                      attrs=['bold']))
        config_file = os.path.join(config_dir, 'b02b0.cnf')
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Z_dimension is not even, using config b02b0_1.cnf',
                      'cyan', attrs=['bold']))
        config_file = os.path.join(config_dir, 'b02b0_1.cnf')
    topup.inputs.config = config_file
    topup.inputs.encoding_file = acqparam
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input acquisition parameters file', 'cyan',
                  attrs=['bold']), acqparam)
    topup.inputs.output_type = "NIFTI_GZ"
    if not out_dir:
        out_dir = os.path.join(os.path.dirname(file_4D), 'topup')
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| topup directory:', 'cyan', attrs=['bold']), out_dir)
    if verbose:
        topup.terminal_output = 'stream'
    else:
        topup.terminal_output = 'none'
    createDir(out_dir)
    skip = get_regex(out_dir, '(fieldcoef|movpar|corrected)')
    if len(skip) == 4 and not overwrite:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping Topup, files already exist:', 'cyan',
                      attrs=['bold']), out_dir)
        for a, b in enumerate(skip):
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| ' + str(a + 1) + ':', 'cyan', attrs=['bold']), b)
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return out_dir
    elif skip and overwrite:
        os.chdir(os.path.dirname(out_dir))
        shutil.rmtree(out_dir)
    createDir(out_dir)
    os.chdir(out_dir)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Starting topup', 'cyan', attrs=['bold']))
    then = time.time()
    topup.run()
    print(topup.cmdline)
    duration = time.time() - then
    mins, secs = divmod(duration, 60)
    hours, mins = divmod(mins, 60)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Topup completed in:', 'cyan', attrs=['bold']),
          '%d:%d:%d' % (hours, mins, secs))
    # outs = list(get_all_values(topup._list_outputs()))
    # for f in outs:
    #     if (isinstance(f, str) and os.path.isfile(f)
    #             and os.path.dirname(f) != out_dir):
    #         shutil.move(f, out_dir)
    # print(colored('|', 'blue', attrs=['bold']),
    #       colored('|', 'green', attrs=['bold']),
    #       colored('| Moved all files to topup directory', 'cyan',
    #               attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return out_dir


def fslroi(*args):
    op_string = ['fslroi']
    for arg in args:
        if isinstance(arg, (int, float, complex)):
            op_string.append(str(arg))
        elif isinstance(arg, str):
            op_string.append(arg)
    cmdWoutput(op_string)
    return


def flirt(in_file=False, reference=False, angle_rep=False, apply_isoxfm=False,
          apply_xfm=False, args=False, bbrslope=False, bbrtype=False,
          bgvalue=False, bins=False, coarse_search=False, cost=False,
          cost_func=False, datatype=False, display_init=False, dof=False,
          echospacing=False, fieldmap=False, fieldmapmask=False,
          fine_search=False, force_scaling=False, ignore_exception=False,
          in_matrix_file=False, in_weight=False, interp=False,
          min_sampling=False, no_clamp=False, no_resample=False,
          no_resample_blur=False, no_search=False, out_file=False,
          out_log=False, out_matrix_file=False, output_type=False,
          padding_size=False, pedir=False, ref_weight=False, rigid2D=False,
          save_log=False, schedule=False, searchr_x=False, searchr_y=False,
          searchr_z=False, sinc_width=False, sinc_window=False,
          uses_qform=False, verbose=False, wm_seg=False, wmcoords=False,
          wmnorms=False):
    """
    FSL flirt but with all the options

    Parameters
    ----------
    in_file: (an existing file name)
            input file
            flag: -in %s, position: 0
    reference: (an existing file name)
            reference file
            flag: -ref %s, position: 1

    [Optional]
    angle_rep: ('quaternion' or 'euler')
            representation of rotation angles
            flag: -anglerep %s
    apply_isoxfm: (a float)
            as applyxfm but forces isotropic resampling
            flag: -applyisoxfm %f
            mutually_exclusive: apply_xfm
    apply_xfm: (a boolean)
            apply transformation supplied by in_matrix_file
            flag: -applyxfm
            requires: in_matrix_file
    args: (a string)
            Additional parameters to the command
            flag: %s
    bbrslope: (a float)
            value of bbr slope
            flag: -bbrslope %f
    bbrtype: ('signed' or 'global_abs' or 'local_abs')
            type of bbr cost function: signed [default], global_abs, local_abs
            flag: -bbrtype %s
    bgvalue: (a float)
            use specified background value for points outside FOV
            flag: -setbackground %f
    bins: (an integer (int or long))
            number of histogram bins
            flag: -bins %d
    coarse_search: (an integer (int or long))
            coarse search delta angle
            flag: -coarsesearch %d
    cost: ('mutualinfo' or 'corratio' or 'normcorr' or 'normmi' or
             'leastsq' or 'labeldiff' or 'bbr')
            cost function
            flag: -cost %s
    cost_func: ('mutualinfo' or 'corratio' or 'normcorr' or 'normmi' or
             'leastsq' or 'labeldiff' or 'bbr')
            cost function
            flag: -searchcost %s
    datatype: ('char' or 'short' or 'int' or 'float' or 'double')
            force output data type
            flag: -datatype %s
    display_init: (a boolean)
            display initial matrix
            flag: -displayinit
    dof: (an integer (int or long))
            number of transform degrees of freedom
            flag: -dof %d
    echospacing: (a float)
            value of EPI echo spacing - units of seconds
            flag: -echospacing %f
    environ: (a dictionary with keys which are a value of type 'str' and
             with values which are a value of type 'str', nipype default value:
             {})
            Environment variables
    fieldmap: (a file name)
            fieldmap image in rads/s - must be already registered to the
            reference image
            flag: -fieldmap %s
    fieldmapmask: (a file name)
            mask for fieldmap image
            flag: -fieldmapmask %s
    fine_search: (an integer (int or long))
            fine search delta angle
            flag: -finesearch %d
    force_scaling: (a boolean)
            force rescaling even for low-res images
            flag: -forcescaling
    ignore_exception: (a boolean, nipype default value: False)
            Print an error message instead of throwing an exception in case the
            interface fails to run
    in_matrix_file: (a file name)
            input 4x4 affine matrix
            flag: -init %s
    in_weight: (an existing file name)
            File for input weighting volume
            flag: -inweight %s
    interp: ('trilinear' or 'nearestneighbour' or 'sinc' or 'spline')
            final interpolation method used in reslicing
            flag: -interp %s
    min_sampling: (a float)
            set minimum voxel dimension for sampling
            flag: -minsampling %f
    no_clamp: (a boolean)
            do not use intensity clamping
            flag: -noclamp
    no_resample: (a boolean)
            do not change input sampling
            flag: -noresample
    no_resample_blur: (a boolean)
            do not use blurring on downsampling
            flag: -noresampblur
    no_search: (a boolean)
            set all angular searches to ranges 0 to 0
            flag: -nosearch
    out_file: (a file name)
            registered output file
            flag: -out %s, position: 2
    out_log: (a file name)
            output log
            requires: save_log
    out_matrix_file: (a file name)
            output affine matrix in 4x4 asciii format
            flag: -omat %s, position: 3
    output_type: ('NIFTI_PAIR' or 'NIFTI_PAIR_GZ' or 'NIFTI_GZ' or
             'NIFTI')
            FSL output type
    padding_size: (an integer (int or long))
            for applyxfm: interpolates outside image by size
            flag: -paddingsize %d
    pedir: (an integer (int or long))
            phase encode direction of EPI - 1/2/3=x/y/z & -1/-2/-3=-x/-y/-z
            flag: -pedir %d
    ref_weight: (an existing file name)
            File for reference weighting volume
            flag: -refweight %s
    rigid2D: (a boolean)
            use 2D rigid body mode - ignores dof
            flag: -2D
    save_log: (a boolean)
            save to log file
    schedule: (an existing file name)
            replaces default schedule
            flag: -schedule %s
    searchr_x: (a list of from 2 to 2 items which are an integer (int or
             long))
            search angles along x-axis, in degrees
            flag: -searchrx %s
    searchr_y: (a list of from 2 to 2 items which are an integer (int or
             long))
            search angles along y-axis, in degrees
            flag: -searchry %s
    searchr_z: (a list of from 2 to 2 items which are an integer (int or
             long))
            search angles along z-axis, in degrees
            flag: -searchrz %s
    sinc_width: (an integer (int or long))
            full-width in voxels
            flag: -sincwidth %d
    sinc_window: ('rectangular' or 'hanning' or 'blackman')
            sinc window
            flag: -sincwindow %s
    terminal_output: ('stream' or 'allatonce' or 'file' or 'none')
            Control terminal output: `stream` - displays to terminal
            immediately (default), `allatonce` - waits till command is
            finished to display output, `file` - writes output to file, `none`
            - output is ignored
    uses_qform: (a boolean)
            initialize using sform or qform
            flag: -usesqform
    verbose: (an integer (int or long))
            verbose mode, 0 is least
            flag: -verbose %d
    wm_seg: (a file name)
            white matter segmentation volume needed by BBR cost function
            flag: -wmseg %s
    wmcoords: (a file name)
            white matter boundary coordinates for BBR cost function
            flag: -wmcoords %s
    wmnorms: (a file name)
            white matter boundary normals for BBR cost function
            flag: -wmnorms %s

    Returns
    -------

    out_file: (an existing file name)
            path/name of registered file (if generated)
    out_log: (a file name)
            path/name of output log (if generated)
    out_matrix_file: (an existing file name)
            path/name of calculated affine transform (if generated)
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| FLIRT Registration: ', 'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| flirt() from FSLtools.py', 'cyan', attrs=['bold']))
    flt = FLIRT()
    if in_file:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| in_file:', 'cyan', attrs=['bold']), in_file)
        flt.inputs.in_file = in_file
    if reference:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| reference:', 'cyan', attrs=['bold']), reference)
        flt.inputs.reference = reference
    if angle_rep:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| angle_rep:', 'cyan', attrs=['bold']), angle_rep)
        flt.inputs.angle_rep = angle_rep
    if apply_isoxfm:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| apply_isoxfm:', 'cyan', attrs=['bold']), apply_isoxfm)
        flt.inputs.apply_isoxfm = apply_isoxfm
    if apply_xfm:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| apply_xfm:', 'cyan', attrs=['bold']), apply_xfm)
        flt.inputs.apply_xfm = apply_xfm
    if args:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| args:', 'cyan', attrs=['bold']), args)
        flt.inputs.args = args
    if bbrslope:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| bbrslope:', 'cyan', attrs=['bold']), bbrslope)
        flt.inputs.bbrslope = bbrslope
    if bbrtype:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| bbrtype:', 'cyan', attrs=['bold']), bbrtype)
        flt.inputs.bbrtype = bbrtype
    if bgvalue:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| bgvalue:', 'cyan', attrs=['bold']), bgvalue)
        flt.inputs.bgvalue = bgvalue
    if bins:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| bins:', 'cyan', attrs=['bold']), bins)
        flt.inputs.bins = bins
    if coarse_search:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| coarse_search:', 'cyan', attrs=['bold']),
              coarse_search)
        flt.inputs.coarse_search = coarse_search
    if cost:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| cost:', 'cyan', attrs=['bold']), cost)
        flt.inputs.cost = cost
    if cost_func:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| cost_func:', 'cyan', attrs=['bold']), cost_func)
        flt.inputs.cost_func = cost_func
    if datatype:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| datatype:', 'cyan', attrs=['bold']), datatype)
        flt.inputs.datatype = datatype
    if display_init:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| display_init:', 'cyan', attrs=['bold']), display_init)
        flt.inputs.display_init = display_init
    if dof:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| dof:', 'cyan', attrs=['bold']), dof)
        flt.inputs.dof = dof
    if echospacing:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| echospacing:', 'cyan', attrs=['bold']), echospacing)
        flt.inputs.echospacing = echospacing
    if fieldmap:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| fieldmap:', 'cyan', attrs=['bold']), fieldmap)
        flt.inputs.fieldmap = fieldmap
    if fieldmapmask:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| fieldmapmask:', 'cyan', attrs=['bold']), fieldmapmask)
        flt.inputs.fieldmapmask = fieldmapmask
    if fine_search:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| fine_search:', 'cyan', attrs=['bold']), fine_search)
        flt.inputs.fine_search = fine_search
    if force_scaling:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| force_scaling:', 'cyan', attrs=['bold']),
              force_scaling)
        flt.inputs.force_scaling = force_scaling
    if ignore_exception:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| ignore_exception:', 'cyan', attrs=['bold']),
              ignore_exception)
        flt.inputs.ignore_exception = ignore_exception
    if in_matrix_file:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| in_matrix_file:', 'cyan', attrs=['bold']),
              in_matrix_file)
        flt.inputs.in_matrix_file = in_matrix_file
    if in_weight:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| in_weight:', 'cyan', attrs=['bold']), in_weight)
        flt.inputs.in_weight = in_weight
    if interp:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| interp:', 'cyan', attrs=['bold']), interp)
        flt.inputs.interp = interp
    if min_sampling:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| min_sampling:', 'cyan', attrs=['bold']), min_sampling)
        flt.inputs.min_sampling = min_sampling
    if no_clamp:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| no_clamp:', 'cyan', attrs=['bold']), no_clamp)
        flt.inputs.no_clamp = no_clamp
    if no_resample:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| no_resample:', 'cyan', attrs=['bold']), no_resample)
        flt.inputs.no_resample = no_resample
    if no_resample_blur:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| no_resample_blur:', 'cyan', attrs=['bold']),
              no_resample_blur)
        flt.inputs.no_resample_blur = no_resample_blur
    if no_search:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| no_search:', 'cyan', attrs=['bold']), no_search)
        flt.inputs.no_search = no_search
    if out_file:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| out_file:', 'cyan', attrs=['bold']), out_file)
        flt.inputs.out_file = out_file
    if out_log:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| out_log:', 'cyan', attrs=['bold']), out_log)
        flt.inputs.out_log = out_log
    if out_matrix_file:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| out_matrix_file:', 'cyan', attrs=['bold']),
              out_matrix_file)
        flt.inputs.out_matrix_file = out_matrix_file
    if output_type:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| output_type:', 'cyan', attrs=['bold']),
              output_type)
        flt.inputs.output_type = output_type
    if padding_size:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| padding_size:', 'cyan', attrs=['bold']),
              padding_size)
        flt.inputs.padding_size = padding_size
    if pedir:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| pedir:', 'cyan', attrs=['bold']), pedir)
        flt.inputs.pedir = pedir
    if ref_weight:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| ref_weight:', 'cyan', attrs=['bold']),
              ref_weight)
        flt.inputs.ref_weight = ref_weight
    if rigid2D:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| rigid2D:', 'cyan', attrs=['bold']), rigid2D)
        flt.inputs.rigid2D = rigid2D
    if save_log:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| save_log:', 'cyan', attrs=['bold']), save_log)
        flt.inputs.save_log = save_log
    if schedule:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| schedule:', 'cyan', attrs=['bold']), schedule)
        flt.inputs.schedule = schedule
    if searchr_x:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| searchr_x:', 'cyan', attrs=['bold']), searchr_x)
        flt.inputs.searchr_x = searchr_x
    if searchr_y:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| searchr_y:', 'cyan', attrs=['bold']), searchr_y)
        flt.inputs.searchr_y = searchr_y
    if searchr_z:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| searchr_z:', 'cyan', attrs=['bold']), searchr_z)
        flt.inputs.searchr_z = searchr_z
    if sinc_width:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| sinc_width:', 'cyan', attrs=['bold']), sinc_width)
        flt.inputs.sinc_width = sinc_width
    if sinc_window:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| sinc_window:', 'cyan', attrs=['bold']), sinc_window)
        flt.inputs.sinc_window = sinc_window
    if uses_qform:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| uses_qform:', 'cyan', attrs=['bold']), uses_qform)
        flt.inputs.uses_qform = uses_qform
    if verbose:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| verbose:', 'cyan', attrs=['bold']), verbose)
        flt.inputs.verbose = 3
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| terminal_output:', 'cyan', attrs=['bold']), 'stream')
        flt.terminal_output = 'stream'
    if wm_seg:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| wm_seg:', 'cyan', attrs=['bold']), wm_seg)
        flt.inputs.wm_seg = wm_seg
    if wmcoords:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| wmcoords:', 'cyan', attrs=['bold']), wmcoords)
        flt.inputs.wmcoords = wmcoords
    if wmnorms:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| wmnorms:', 'cyan', attrs=['bold']), wmnorms)
        flt.inputs.wmnorms = wmnorms
    outs = flt._list_outputs()
    if not (os.path.isfile(outs['out_file']) and
            os.path.isfile(outs['out_matrix_file'])):
        flt.run()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return outs


def fast(brain, seq='t1', verbose=True):
    """
    FSL Fast segmentation of the brain

    Parameters
    ----------
    brain : path
        Path to the skull-stripped image.
    verbose : bool, optional
        Stream processing steps to the terminal. The default is False.

    Returns
    -------
    fast_dir : path
        Directory where images are stored.
    paths : list[path]
        Segmentations produced.
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Brain Segmentation with FAST: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| fast() from FSLtools.py', 'cyan', attrs=['bold']))
    t1preproc = os.path.dirname(brain)
    fast_dir = os.path.join(t1preproc, 'fast')
    createDir(fast_dir)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Created/Existing fast_dir: ', 'cyan', attrs=['bold']),
          fast_dir)
    paths = get_subdir_regex_files(fast_dir, '.')
    if not os.path.isfile(brain):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored('ERROR: Brain file not found: ' + brain, 'red',
                      attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return fast_dir, paths
    if len(paths) == 9:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Segmentation already done, skipping... for a new one,'
                      ' rename/delete previous files', 'cyan', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return fast_dir, paths
# ------------------ Performing FAST only on the brain region -----------------
    fast = FAST()
    fast.inputs.in_files = brain
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| fast input:', 'cyan', attrs=['bold']), brain)
    seq = seq.upper()
    if seq in ('T1', 'T2', 'PD'):
        if seq == 'T2':
            fast.inputs.img_type = 2
        elif seq == 'PD':
            fast.inputs.img_type = 3
        else:
            fast.inputs.img_type = 1
    fast.inputs.segments = True
    fast.inputs.verbose = verbose
    fast.inputs.no_bias = True
    fast.output_type = 'NIFTI_GZ'
    outs = fast._list_outputs()
    if verbose:
        fast.terminal_output = 'stream'
        print(fast.cmdline)
    else:
        fast.terminal_output = 'none'
    fast.run()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Segmentation Succesful', 'cyan', attrs=['bold']))
# ----------------- Moving outputs of FAST in their Directory -----------------
    MixElType = outs['mixeltype']
    ParVofile = outs['partial_volume_files']
    ParVoMap = outs['partial_volume_map']
    TisClFile = outs['tissue_class_files']
    TisClMap = outs['tissue_class_map']
    moveFiles(fast_dir, MixElType)
    moveFiles(fast_dir, ParVofile)
    moveFiles(fast_dir, ParVoMap)
    moveFiles(fast_dir, TisClFile)
    moveFiles(fast_dir, TisClMap)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return fast_dir, outs


def conv_image(image, odt, out, verbose=False):
    """
    Convert a nifti image into another datatype and save it.

    Parameters
    ----------
    image : path
        Path to image to convert.
    odt : string
        FSL output data type (check ImageMaths on nipype website for info).
    out_dir : path, optional
        output directory for the converted image. The default is 'same'.
    verbose : boolean, optional
        stream steps to terminal. The default is False.

    Returns
    -------
    path
        converted image path.
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Converting sequence data type to ' + odt + ': ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| conv_image() from FSLtools.py', 'cyan', attrs=['bold']))
    if not os.path.isfile(image):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored('ERROR: Image to convert not found:', 'red',
                      attrs=['bold']), image)
    maths = ImageMaths()
    maths.inputs.in_file = image
    maths.inputs.output_type = 'NIFTI_GZ'
    maths.inputs.out_data_type = odt
    maths.inputs.out_file = out
    outs = maths._list_outputs()
    if not os.path.isfile(outs['out_file']):
        maths.run()
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Converted image: ', 'cyan', attrs=['bold']),
              outs['out_file'])
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping conversion, file exists', 'cyan',
                      attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']), outs['out_file'])
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return outs['out_file']


def flirt_bbr(seq_mov, seq_fix, warp, WM_msk=None, interpo='trilinear', dof=6,
              verbose=False):
    """
    Registration based on FSL's FLIRT

    Parameters
    ----------
    out_dir : path
        output directory.
    seq_mov : path
        Seqence to register.
    seq_fix : path
        Seqence to register on.
    BBR : boolean, optional
        Use the 2 step fsl bbr registration. The default is False.
    WM_msk : path, optional
        White matter mas path to use for the registration. The default is None.
    interpo : string, optional
        Interpolation. The default is 'trilinear'.

    Returns
    -------
    list[path]
        registered image and transformation matrices.
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| FLIRT BBR Registration: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| flirt_bbr() from FSLtools.py', 'cyan', attrs=['bold']))
    out_dir = os.path.dirname(warp)
    trans_mat = os.path.join(out_dir, 'trans_mat')
    createDir(trans_mat)
    initmat = changeName(warp, '_initxfm', trans_mat, '.mat')
    initima = changeName(warp, '_initxfm', trans_mat)
    flt = FLIRT()
    flt.terminal_output = 'none'
    flt.inputs.in_file = seq_mov
    flt.inputs.reference = seq_fix
    flt.inputs.output_type = 'NIFTI_GZ'
    flt.inputs.dof = dof
    flt.inputs.out_matrix_file = initmat
    flt.inputs.out_file = initima
    if verbose:
        flt.terminal_output = "stream"
        flt.inputs.verbose = True
    if not (os.path.isfile(initmat) or os.path.isfile(initima)):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Running first registration...', 'cyan',
                      attrs=['bold']))
        flt.run()
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping first registration as files already exists, '
                      'if a new registration is wanted please rename or delete'
                      ' following files:', 'cyan', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']), initmat)
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']), initima)
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']))
    flt2 = FLIRT()
    flt2.terminal_output = 'none'
    flt2.inputs.in_file = seq_mov
    flt2.inputs.reference = seq_fix
    flt2.inputs.output_type = 'NIFTI_GZ'
    flt2.inputs.dof = 6
    flt2.inputs.wm_seg = WM_msk
    flt2.inputs.cost = 'bbr'
    flt2.inputs.schedule = os.path.join(os.environ['FSLDIR'], 'etc',
                                        'flirtsch', 'bbr.sch')
    flt2.inputs.in_matrix_file = initmat
    initmat = changeName(warp, outdir=trans_mat, newext='.mat')
    flt2.inputs.out_matrix_file = initmat
    flt2.inputs.interp = interpo
    flt2.inputs.out_file = warp
    outs2 = flt2._list_outputs()
    if verbose:
        flt2.terminal_output = "stream"
        flt2.inputs.verbose = True
    if not (os.path.isfile(initmat) or os.path.isfile(warp)):
        flt2.run()
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping BBR registration as files already exists'
                      ', if a new registration is wanted please rename or '
                      'delete following files:', 'cyan', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']), initmat)
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']), warp)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return outs2['out_file'], outs2['out_matrix_file'], initima


def convert_xfm(mat, omat=False, invert=True, concat=False, infile2=False):
    """
    Conversion based on FSL's ConvertXFM
    Parameters
    ----------
    mov2ref_mat : path
        Path to transformation matrix in FSL format.
    invert : boolean, optional
        Invert matrix. The default is True.

    Returns
    -------
    path
        Path to the converted matrix.
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| XFM Conversion: ', 'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| convert_xfm() from FSLtools.py', 'cyan', attrs=['bold']))
    invt = ConvertXFM()
    invt.inputs.in_file = mat
    if invert:
        invt.inputs.invert_xfm = invert
    invt.inputs.out_file = omat
    if concat:
        invt.inputs.concat_xfm = concat
        invt.inputs.in_file2 = infile2
    outs = invt._list_outputs()
    if not os.path.isfile(omat):
        invt.run()
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| XFM Conversion already made, if a new matrix inversio'
                      'n is wanted please rename/delete following files:',
                      'cyan', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']), omat)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return outs['out_file']


def apply_xfm(seq_mov, seq_ref, xfm_mat, warp, interp='trilinear'):
    """
    Registration based on FSL's apply_xfm

    Parameters
    ----------
    seq_mov : path
        Absolute path to the moving nifti file.
    seq_ref : path
        Absolute path to reference nifti file.
    xfm_mat : path
        Absolute pth to the transformation matrix.
    out_dir : path
        Path to the output directory.

    Returns
    -------
    path
        Registered image.
    path
        Transformation Matrix.
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| FSL ApplyXFM: ', 'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| apply_xfm() from FSLtools.py', 'cyan', attrs=['bold']))
    applyxfm = ApplyXFM()
    applyxfm.inputs.in_file = seq_mov
    applyxfm.inputs.reference = seq_ref
    applyxfm.inputs.apply_xfm = True
    applyxfm.inputs.in_matrix_file = xfm_mat
    applyxfm.inputs.out_file = warp
    applyxfm.inputs.out_matrix_file = xfm_mat
    applyxfm.inputs.interp = interp
    outs = applyxfm._list_outputs()
    if not os.path.isfile(warp) and os.path.isfile(xfm_mat):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Running Apply XFM...', 'cyan', attrs=['bold']))
        applyxfm.run()
    else:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Apply XFM Conversion already made, if a new image is '
                      'wanted please rename/delete following files:',
                      'cyan', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']), warp)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return outs['out_file'], outs['out_matrix_file']


"""
################################ FSLROI #######################################
Uses FSL Fslroi command to extract region of interest (ROI) from an image.
You can a) take a 3D ROI from a 3D data set (or if it is 4D, the same ROI is
taken from each time point and a new 4D data set is created), b) extract just
some time points from a 4D data set, or c) control time and space limits to
the ROI. Note that the arguments are minimum index and size (not maximum
index). So to extract voxels 10 to 12 inclusive you would specify 10 and 3
(not 10 and 12).
"""


# def fslroi(image, output, time_start=0, time_size=0, space=False):
#     FSLroi = ExtractROI()
#     FSLroi.terminal_output = 'none'
#     FSLroi.inputs.output_type = 'NIFTI_GZ'
#     FSLroi.inputs.in_file = image
#     FSLroi.inputs.roi_file = output
#     if time_start:
#         FSLroi.inputs.t_min = time_start - 1
#         if time_size == 0:
#             print("Time size = 0, invalid input")
#             return False
#         FSLroi.inputs.t_size = time_size
#     elif space:
#         FSLroi.inputs.crop_list = space
#     FSLroi.run()


"""
############################### FSLMERGE ######################################
Wraps command fslmerge
Use fslmerge to concatenate images
Images can be concatenated across time, x, y, or z dimensions. Across the time
(t) dimension the TR is set by default to 1 sec.
Note: to set the TR to a different value, specify ‘t’ for dimension and specify
the TR value in seconds for the tr input. The dimension will be automatically
updated to ‘tr’.
"""


def fslmerge(infiles, output, merge_dim='t'):
    merger = Merge()
    merger.inputs.in_files = infiles
    merger.inputs.merged_file = output
    merger.inputs.dimension = merge_dim
    merger.inputs.output_type = 'NIFTI_GZ'
    merger.run()
    return output


"""
###############################################################################
"""


def fslmean(file, out_file):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| FSL MEAN: ', 'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| fslmean() from FSLtools.py', 'cyan', attrs=['bold']))
    math = MeanImage()
    math.inputs.in_file = file
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Meaning:', 'cyan', attrs=['bold']), file)
    math.inputs.out_file = out_file
    math.inputs.nan2zeros = True
    math.inputs.output_type = 'NIFTI_GZ'
    math.terminal_output = 'none'
    math.run()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Mean file:', 'cyan', attrs=['bold']), out_file)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return out_file


def fslstats(*args):
    op_string = ['fslstats']
    for arg in args:
        if isinstance(arg, (int, float, complex)):
            op_string.append(str(arg))
        elif isinstance(arg, str):
            op_string.append(arg)
    rtrn = cmdWoutput(op_string)
    return rtrn


def fslmaths(*args):
    op_string = ['fslmaths']
    for arg in args:
        if isinstance(arg, (int, float, complex)):
            op_string.append(str(arg))
        elif isinstance(arg, str):
            op_string.append(arg)
    cmdWoutput(op_string)
    return


def bet(anat):
    btr = BET()
    btr.inputs.in_file = anat
    btr.inputs.robust = 1
