#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 26 16:58:22 2020

@author: arya.yazdan-panah
"""

import os
import numpy as np
import nibabel as nib
import matplotlib.pyplot as plt
import imageio
import shutil
from common3 import changeName, createDir
from termcolor import colored
from scipy.io import loadmat


def flip_nii(nii, axis, outdir=None, suffix='_flpd', prefix='',
             flip_file=False):
    """
    Flip nifti image along given axis (does not change label position in the
    viewer just data in the array). Similar to a fslswapdim

    Parameters
    ----------
    nii : path
        path to Nifti imge to flip.
    axis : int or tuple
        axis along which the flilpping is done.
    outdir : TYPE, optional
        cf Change Name options. The default is None.
    suffix : TYPE, optional
        cf changeName options. The default is ''.
    prefix : TYPE, optional
        cf Change Name options. The default is ''.

    Returns
    -------
    flip_file : path
        flipped niti image.
    """
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Flipping Image: ', 'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| flip_nii() from tools_nibabel.py', 'cyan',
                  attrs=['bold']))
    pet = nib.load(nii)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Image Loaded', 'cyan', attrs=['bold']))
    pet_affine = pet.affine
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Affine retrieved', 'cyan', attrs=['bold']))
    pet = pet.get_fdata()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Image shape:', 'cyan', attrs=['bold']), pet.shape)
    pet = np.flip(pet, axis)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Image flipped along:', 'cyan', attrs=['bold']), axis)
    if not flip_file:
        flip_file = changeName(nii, suffix, outdir, None, prefix)
    pet = nib.Nifti1Image(pet, pet_affine)
    nib.save(pet, flip_file)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Image saved as:', 'cyan', attrs=['bold']), flip_file)
    return flip_file


def computeDVR(R1, k2, k2prime, out):
    if os.path.isfile(out):
        return out
    k2prime = float(loadmat(k2prime)['k2prime'])
    R1 = nib.load(R1)
    aff = R1.affine
    R1 = R1.get_fdata()
    k2 = nib.load(k2).get_fdata()
    DVR = np.divide(R1*k2prime, k2)
    DVR = nib.Nifti1Image(DVR, aff)
    nib.save(DVR, out)
    return


def relative_difference(im1, im2, out):
    if not os.path.isfile(out):
        print('    Calculating relative differences')
        im1 = nib.load(im1)
        im2 = nib.load(im2)
        aff = im1.affine
        data1 = im1.get_fdata()
        data2 = im2.get_fdata()
        # Calculating relative differences
        abs_diff = np.absolute(data1 - data2)
        average = (data1 + data2) / 2
        relative_diff = np.divide(abs_diff, average)
        rel_diff_img = nib.Nifti1Image(relative_diff, aff)
        print('    Saving relative difference')
        nib.save(rel_diff_img, out)
    return


def mean_masks(ms, out):
    if not os.path.isfile(out):
        DATA = []
        for m in ms:
            im1 = nib.load(m)
            aff = im1.affine
            data = im1.get_fdata()
            DATA.append(data)
        DATA = np.array(DATA)
        DATA = np.mean(DATA, 0)
        DATA[DATA < 0.5] = 0
        DATA[DATA >= 0.5] = 1
        im2 = nib.Nifti1Image(DATA, aff)
        nib.save(im2, out)
    return



def show_slices(slices):
   """ Function to display row of image slices """
   fig, axes = plt.subplots(1, len(slices))
   for i, slice in enumerate(slices):
       axes[i].imshow(slice.T, cmap="gray", origin="lower")


def make_gif(nii, out_gif, roll_axis, cmap, plane="", pos_in_plane=0):
    img = nib.load(nii).get_fdata()
    shape = img.shape
    if roll_axis >= len(shape):
        print("Roll axis is bigher than the number of dimension of the image")
        return False
    scratch_dir = os.path.join(os.path.dirname(nii), 'scratch')
    createDir(scratch_dir)
    to_merge = []
    if roll_axis < 3:
        img = np.moveaxis(img, roll_axis, 0)
        shape = img.shape
        for idx in range(shape[0]):
            image = np.rot90(img[idx,...], 1)
            tmpath = changeName(nii, str(idx), scratch_dir, '.png')
            plt.imsave(tmpath, image, cmap=cmap)
            to_merge.append(imageio.imread(tmpath))
    else:
        plane_axis = {"AXI":2,
                      "COR":1,
                      "SAG":0}
        if plane.upper() not in plane_axis.keys():
            print('Plane unknown')
            return False
        img = np.moveaxis(img, plane_axis[plane.upper()], 0)
        shape = img.shape
        if pos_in_plane > shape[0] - 1:
            print('Position out of bounds ->', pos_in_plane)
            return False
        for frame in range(shape[-1]):
            image = np.rot90(img[pos_in_plane,..., frame], 1)
            tmpath = changeName(nii, str(frame), scratch_dir, '.png')
            plt.imsave(tmpath, image, cmap=cmap, vmin=0)
            to_merge.append(imageio.imread(tmpath))
    imageio.mimsave(out_gif, to_merge, duration=1)
    shutil.rmtree(scratch_dir)
