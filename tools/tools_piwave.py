#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 18 16:21:59 2020

@author: arya.yazdan-panah
"""

import sys
import os
import shutil
import matlab.engine as me
from termcolor import colored, cprint
from common3 import gzip_nii, changeName


def piw_reslice(filename, tgframe, iframe, fframe, out_dir=None):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Wavelet Filtering and Realigning with SPM: ', 'cyan',
                  attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| piw_reslice() from tools_piwave.py', 'cyan',
                  attrs=['bold']))
    realigned = changeName(filename, '_movcorr', outdir=out_dir)
    if os.path.isfile(realigned):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Skipping, realignement done', 'cyan', attrs=['bold']))
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']))
        return realigned
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input file:', 'cyan', attrs=['bold']), filename)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Target Frame:', 'cyan', attrs=['bold']), tgframe)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Initial Frame:', 'cyan', attrs=['bold']), iframe)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Final Frame:', 'cyan', attrs=['bold']), fframe)
    if not(iframe <= tgframe and tgframe <= fframe):
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Please verify values of the variables above', 'cyan',
                      attrs=['bold']))
        sys.exit(1)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Starting matlab...', 'cyan', attrs=['bold']))
    try:
        eng = me.start_matlab()
    except:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored('Matlab not installed on machine or module MATLAB not '
                      'loaded... exiting', 'red', attrs=['bold']))
        sys.exit(1)
    cprint('    ============MATLAB============', 'yellow', attrs=['bold'])
    try:
        realigned = eng.piw_reslice_batch(filename, tgframe, iframe, fframe,
                                          nargout=1)
    except:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('|', 'cyan', attrs=['bold']),
              colored('Matlab Function piw_reslice_batch not found, check if '
                      'its parent directory is in MATLABPATH with the command:'
                      ' echo $MATLABPATH', 'red', attrs=['bold']))
    eng.quit()
    cprint('    ============FINISH============', 'yellow', attrs=['bold'])
    realigned_gz = gzip_nii(realigned)
    os.remove(realigned)
    if out_dir:
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| Moving realigned in:', 'cyan', attrs=['bold']),
              out_dir)
        moved_gz = changeName(realigned_gz, outdir=out_dir)
        shutil.move(realigned_gz, moved_gz)
        realigned_gz = moved_gz
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    return realigned_gz
