#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 16:23:37 2020

@author: arya.yazdan-panah
"""


import os
import shutil
from termcolor import colored
from common3 import gzip_nii, ungzip_nii, changeName, createDir
from nipype.interfaces.spm import Realign


def spmRealign(pet, output_dir, prefix="r", quality=0.5, reg2mean=True,
               interp=2):
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Realigning ', 'cyan', attrs=['bold', 'reverse']))
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| spmRealign() from tools_spm.py', 'cyan',
                  attrs=['bold']))
    if os.path.splitext(pet)[1] == ".gz":
        print(colored('|', 'blue', attrs=['bold']),
              colored('|', 'green', attrs=['bold']),
              colored('| PET file compressed, unziping...', 'cyan',
                      attrs=['bold']))
        unzipped_pet = ungzip_nii(pet)
    else: 
        unzipped_pet = pet
    realign = Realign()
    realign.inputs.in_files = unzipped_pet
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Input file:', 'cyan', attrs=['bold']), unzipped_pet)
    realign.inputs.fwhm = 3
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| FWHM:', 'cyan', attrs=['bold']), 3)
    realign.inputs.separation = 2
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Separation:', 'cyan', attrs=['bold']), 2)
    realign.inputs.out_prefix = prefix
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Output prefix:', 'cyan', attrs=['bold']), prefix)
    realign.inputs.quality = quality  # 0.1 to 1.0
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Quality [0.1 to 1.0]:', 'cyan', attrs=['bold']), quality)
    realign.inputs.register_to_mean = reg2mean
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Register to mean:', 'cyan', attrs=['bold']), reg2mean)
    realign.inputs.write_interp = interp  # 0 to 7
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Interpolation [0 to 7]:', 'cyan', attrs=['bold']), interp)
    realign.inputs.wrap = [0, 0, 0]
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Wrap:', 'cyan', attrs=['bold']), [0, 0, 0])
    outs = realign._list_outputs()
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| REALIGNING. . .', 'cyan', attrs=['bold']))
    realign.run()
    os.remove(unzipped_pet)
    print(colored('|', 'blue', attrs=['bold']),
          colored('|', 'green', attrs=['bold']),
          colored('| Removed temporary unzipped pet:', 'cyan', attrs=['bold']),
          unzipped_pet)
    createDir(output_dir)
    for key in outs:
        output = outs[key]
        if isinstance(output, list):
            for out in output:
                source = out
                dest = changeName(out, outdir=output_dir)
                print(colored('|', 'blue', attrs=['bold']),
                      colored('|', 'green', attrs=['bold']),
                      colored('| Moving ', 'cyan', attrs=['bold']),
                      os.path.basename(source),
                      colored(' to ', 'cyan', attrs=['bold']), output_dir)
                shutil.move(source, dest)
        elif isinstance(output, str):
            source = output
            dest = changeName(out, outdir=output_dir)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Moving ', 'cyan', attrs=['bold']),
                  os.path.basename(source),
                  colored(' to ', 'cyan', attrs=['bold']), output_dir)
            shutil.move(source, dest)
        if os.path.splitext(dest)[1] == ".nii":
            gzip_nii(dest)
            os.remove(dest)
            print(colored('|', 'blue', attrs=['bold']),
                  colored('|', 'green', attrs=['bold']),
                  colored('| Gzipping realigned pet', 'cyan', attrs=['bold']))
    return output_dir
