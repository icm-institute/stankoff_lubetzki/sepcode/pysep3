#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 13:40:47 2021

@author: arya.yazdan-panah
"""

import os
import numpy as np
import nibabel as nib
import pandas as pd
from scipy import integrate
from scipy.interpolate import interp1d
from scipy.optimize import nnls
from scipy.ndimage import gaussian_filter
from scipy.signal import savgol_filter
from tqdm import tqdm
from scipy import interpolate
from common3 import changeName
from tools_fsl import fslroi, fslmaths, fslstats


np.seterr(divide='ignore', invalid='ignore')


def readSIF(siffile, timeunit='s'):
    if not os.path.isfile(siffile):
        return False
    A = open(siffile, 'r')
    lines = A.readlines()
    nlines = len(lines)
    A.close()
    start_time = np.zeros((nlines - 1,))
    end_time = np.zeros((nlines - 1,))
    for i, line in enumerate(lines):
        if not i:
            continue
        line = line.split()
        start_time[i - 1] = float(line[0])
        end_time[i - 1] = float(line[1])
    delta = end_time - start_time
    time = start_time + (0.5 * delta)
    if timeunit == 's':
        return time / 60, start_time / 60, end_time / 60, delta / 60
    elif timeunit == 'm':
        return time, start_time, end_time, delta
    elif timeunit == 'h':
        return time * 60, start_time * 60, end_time * 60, delta * 60
    else:
        return False


def dynPET_normalization(DYN, mask):
    if isinstance(DYN, str):
        DYN = nib.load(DYN).get_fdata()
    if isinstance(mask, str):
        mask = nib.load(mask).get_fdata().astype(bool)
    DIM = DYN.shape
    normDYN = np.zeros(DIM)
    for frame in range(DIM[3]):
        mean = np.mean(DYN[:, :, :, frame][mask])
        std = np.std(DYN[:, :, :, frame][mask])
        normDYN[:, :, :, frame] = (DYN[:, :, :, frame] - mean) / std
    return normDYN


def computeIDIF(PET, delta, timeToPeak, BRAIN, nVoxel, fwhm=1):
    endFrame = integrate.cumtrapz(delta, initial=0)
    inds = endFrame < timeToPeak
    inds[0] = False
    # last_true = [i for i, x in enumerate(inds) if x][-1]
    # wanted_frames = [False] * len(inds)
    # wanted_frames[last_true - 2] = True
    # wanted_frames[last_true - 1] = True
    # wanted_frames[last_true] = True
    # print("INDS=", wanted_frames)
    FWHM = fwhm
    pixdim = 1.218750
    sigma = FWHM / (2 * np.sqrt(2 * np.log(2))) / pixdim
    PETsm = gaussian_filter(PET, sigma=sigma)
    sumPET = np.sum(PETsm[:, :, :, inds], axis=3)
    # sumPET = np.multiply(sumPET, BRAIN)
    indices = np.argpartition(sumPET.flatten(), -nVoxel)[-nVoxel:]
    indices = np.vstack(np.unravel_index(indices, sumPET.shape)).T
    IDIF = np.zeros(delta.shape)
    for ind in indices:
        IDIF = IDIF + PET[ind[0], ind[1], ind[2], :]
    IDIF = IDIF / nVoxel
    return IDIF


def superX_extractNormTACs(PET, brain_mask, white_matter_mask,
                           gray_matter_mask, high_bind_mask, delta,
                           carotid=False, nVoxel=40, fwhm=1):
    """
    Tissue classes include white matter, gray matter, blood, high binding gray
    matter.
    Python Implementation of superX_extractNormTACs byJulia Schubert and
    Mattia Veronese
    Copyright - Julia Schubert and Mattia Veronese - London 2020
    Parameters
    ----------
    dynPET : PATH
        Path to dynamic PET data.
    brainMask : PATH
        Path to 3D matrix containing binary brain data specifying voxels of
        brain mask. Must be the same mask used for extracting the kinetic
        classes!.
    whiteMask : PATH
        Path to white matter mask.
    grayMask : PATH
        Path to gray matter mask.
    highBindMask : PATH
        path to 3D matrix containing binary data specifying voxels of high
        binding tissue (ex. thalamus).
    delta : numpy.array
        1D double with PET mid-frame times (in minutes).

    Returns
    -------
    (normGrayTAC, normWhiteTAC, normIDIF, normHighBindTAC): numpy.array
        2D double with normalised time activity curves in the following order:
            GM,
            WM,
            image-derived blood input function,
            high binding gray matter
    """
    PET = nib.load(PET).get_fdata()
    WB = nib.load(brain_mask).get_fdata().astype(bool)
    WM = nib.load(white_matter_mask).get_fdata().astype(bool)
    GM = nib.load(gray_matter_mask).get_fdata().astype(bool)
    HB = nib.load(high_bind_mask).get_fdata().astype(bool)
    if carotid:
        BV = nib.load(carotid).get_fdata().astype(bool)
    PET_size = PET.shape
    n_frames = PET_size[3]
    normGrayTAC = np.zeros((n_frames,))
    normWhiteTAC = np.zeros((n_frames,))
    normIDIF = np.zeros((n_frames,))
    normHighBindTAC = np.zeros((n_frames,))
    if len(delta) != n_frames:
        print('ERROR: image dimensions of dynPET and time must agree')
        return False
    PET_norm = dynPET_normalization(PET, WB)
    time2peak = 1.5
    if not carotid:
        normIDIF = computeIDIF(PET_norm, delta, time2peak, WB, nVoxel, fwhm)
        # nib.save(nib.Nifti1Image(BLOOD, nib.load(brain_mask).affine), b_name)
    for frame in range(n_frames):
        normGrayTAC[frame] = np.mean(PET_norm[:, :, :, frame][GM])
        normWhiteTAC[frame] = np.mean(PET_norm[:, :, :, frame][WM])
        normHighBindTAC[frame] = np.mean(PET_norm[:, :, :, frame][HB])
        if carotid:
            normIDIF[frame] = np.mean(PET_norm[:, :, :, frame][BV])
    return normGrayTAC, normWhiteTAC, normIDIF, normHighBindTAC


def superX_referExtraction(pet_file, kinetics_file, brain_mask, time,
                           reference_mask_file, thr=0.9):
    pet = nib.load(pet_file).get_fdata()
    WB = nib.load(brain_mask).get_fdata().astype(bool)
    RM = nib.load(reference_mask_file).get_fdata().astype(bool)
    kf = pd.read_csv(kinetics_file, header=None)
    ref_time = kf[0].to_numpy().astype(float)
    if ref_time[-1] < 150:
        ref_time = ref_time * 60
    if time[-1] < 150:
        time = time * 60
    kin_GM = kf[1].to_numpy().astype(float)
    kin_BV = kf[2].to_numpy().astype(float)
    kin_WM = kf[3].to_numpy().astype(float)
    kin_HB = kf[4].to_numpy().astype(float)
    # kin_GM = savgol_filter(kin_GM, 5, 3)
    # kin_BV = savgol_filter(kin_BV, 5, 3)
    # kin_WM = savgol_filter(kin_WM, 5, 3)
    # kin_HB = savgol_filter(kin_HB, 5, 3)
    for max_time, moment in reversed(list(enumerate(time))):
        if moment <= ref_time[-1]:
            break
    limited_time = time[:max_time]
    kinetics = np.column_stack((kin_GM, kin_BV, kin_WM, kin_HB))
    kinetics_interped = interp1d(ref_time, kinetics, axis=0)(limited_time)
    PET_norm = dynPET_normalization(pet, WB)
    Voxels_to_fit = PET_norm[RM, :]
    # print(Voxels_to_fit.shape)
    # PET_norm_flat = PET_norm.reshape(-1, PET_norm.shape[-1])
    Ratios = np.zeros((Voxels_to_fit.shape[0], 4))
    Residuals = np.zeros(Voxels_to_fit.shape[0])
    failed = 0
    for vox in tqdm(range(len(Voxels_to_fit))):
        v = Voxels_to_fit[vox, 0:max_time]
        no_outliers = ~np.logical_or(np.isnan(v), np.isinf(v))
        Ratios[vox, :], Residuals[vox] = nnls(kinetics_interped[no_outliers],
                                              v[no_outliers])
        if sum(Ratios[vox, :]) <= 0:
            failed += 1
    print('failed :', failed/len(Voxels_to_fit) * 100, '%')
    sum_Ratios = np.sum(Ratios, len(Ratios.shape) - 1)
    for k in range(4):
        Ratios[..., k] = np.divide(Ratios[..., k], sum_Ratios)
    output_Ratios = np.zeros(WB.shape + (4,))
    output_Residuals = np.zeros(WB.shape)
    output_Ratios[RM, :] = Ratios
    output_Residuals[RM] = Residuals
    ref_mask = np.zeros(WB.shape)
    ref_mask[(RM == 1) & (output_Ratios[..., 0] > thr)] = 1
    return ref_mask, output_Ratios, output_Residuals


def TAC(dynPET, mask=False):
    if isinstance(dynPET, str):
        PET = nib.load(dynPET).get_fdata()
    else:
        PET = dynPET
    if mask is not False:
        if isinstance(mask, str):
            M = nib.load(mask).get_fdata().astype(bool)
        else:
            M = mask
        if M.shape != PET.shape[0:-1] and len(PET.shape) == 4:
            return False
        return np.mean(PET[M], 0)
    return np.mean(PET, (0, 1, 2))


def computeSUVR(dynPET, refTAC, out_file):
    img = nib.load(dynPET)
    affine = img.affine
    petDATA = img.get_fdata()
    SUVR = np.zeros(petDATA.shape)
    for t in range(len(refTAC)):
        SUVR[:, :, :, t] = petDATA[:, :, :, t] / refTAC[t]
    out_img = nib.Nifti1Image(SUVR, affine)
    nib.save(out_img, out_file)
    return


def interpolate_blood_tac(blood_time, blood_input_function, start_time,
                          end_time, delay=0):
    interpolated_blood = np.zeros(start_time.shape)
    blood_time = blood_time - delay
    ici = np.where(np.diff(blood_input_function) == 0)
    blood_input_function[ici] += 1e-6
    for i in range(len(start_time)):
        idx = np.where((blood_time >= start_time[i]) &
                       (blood_time <= end_time[i]))
        time_frame = blood_time[idx]
        blood_frame = blood_input_function[idx]
        if not(len(idx[0])) or blood_time[idx[0][0]] != start_time[i]:

            interp_blood = interpolate.interp1d(blood_time,
                                                blood_input_function,
                                                'linear',
                                                fill_value="extrapolate")
            blood_interped1 = interp_blood(start_time[i])
            time_frame = np.append(start_time[i], time_frame)
            blood_frame = np.append(blood_interped1, blood_frame)
        if not(len(idx[0])) or blood_time[idx[0][-1]] != end_time[i]:
            interp_blood = interpolate.interp1d(blood_time,
                                                blood_input_function,
                                                'linear',
                                                fill_value="extrapolate")
            blood_interped2 = interp_blood(end_time[i])
            time_frame = np.append(time_frame, end_time[i])
            blood_frame = np.append(blood_frame, blood_interped2)
        interpolated_blood[i] = np.trapz(blood_frame, time_frame) / (end_time[i] - start_time[i])
    return interpolated_blood


def autoCAROTID(pet, targetframes=[0], smoothing_sigma=4, min_vol=5000):
    if not os.path.isfile(pet):
        return False
    t_frame = changeName(pet, '_target_frames')
    if not os.path.isfile(t_frame):
        fslroi(pet, t_frame, targetframes[0], len(targetframes))

    t_frame_sm = changeName(t_frame, '_sm_' + str(smoothing_sigma))
    if not os.path.isfile(t_frame_sm):
        fslmaths(t_frame, '-s', smoothing_sigma, t_frame_sm)

    t_frames_sm_mean = changeName(t_frame_sm, '_mean')
    if not os.path.isfile(t_frames_sm_mean):
        fslmaths(t_frame_sm, '-Tmean', t_frames_sm_mean)
    min_val = float(fslstats(t_frames_sm_mean, '-R').split()[0])
    max_val = float(fslstats(t_frames_sm_mean, '-R').split()[-1])
    t_frames_sm_mean_norm = changeName(t_frames_sm_mean, '_norm')
    if not os.path.isfile(t_frames_sm_mean_norm):
        fslmaths(t_frames_sm_mean, '-sub', min_val, '-div', max_val-min_val,
                 t_frames_sm_mean_norm)

    carotid_volume = 0
    intensity_thr = 0.95
    thr_step = 0.05
    t_frames_sm_mean_norm_thr = changeName(pet, '_auto_carotid')
    while carotid_volume < min_vol:
        fslmaths(t_frames_sm_mean_norm, '-thr', intensity_thr, '-bin',
                 t_frames_sm_mean_norm_thr)
        carotid_volume = float(fslstats(t_frames_sm_mean_norm_thr,
                                        '-V').split()[0])
        intensity_thr = intensity_thr - thr_step
    trash_can = [t_frame, t_frame_sm, t_frames_sm_mean]
    for trash in trash_can:
        os.remove(trash)
    return t_frames_sm_mean_norm_thr


def superX_extractNormTACs_new(PETfile, brain_mask, white_matter_mask,
                               gray_matter_mask, high_bind_mask, carotid=False):
    if not carotid:
        carotid = autoCAROTID(PETfile, [0], 3.4, 1000)
    PET = nib.load(PETfile).get_fdata()
    WB = nib.load(brain_mask).get_fdata().astype(bool)
    WM = nib.load(white_matter_mask).get_fdata().astype(bool)
    GM = nib.load(gray_matter_mask).get_fdata().astype(bool)
    HB = nib.load(high_bind_mask).get_fdata().astype(bool)
    BV = nib.load(carotid).get_fdata().astype(bool)
    PET_norm = dynPET_normalization(PET, WB)
    normWhiteTAC = np.mean(PET_norm[WM], 0)
    normGrayTAC = np.mean(PET_norm[GM], 0)
    normHighBindTAC = np.mean(PET_norm[HB], 0)
    normIDIF = np.mean(PET_norm[BV], 0)
    return normGrayTAC, normWhiteTAC, normIDIF, normHighBindTAC
